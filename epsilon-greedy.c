/* -*- Mode: C -*- */

/**
 * @file
 * @brief Implementation of the @f$\epsilon@f$-greedy Policy.
 */

/*
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* Local includes */
#include "epsilon-greedy.h"
#include "model.h"

struct _epsilon_greedy {
	Policy_EXTEND ;
	uint32_t	epsilon ;
	const Task*	task ;
} ;

const State*
EpsilonGreedy_apply( const Policy* self, const State* state )
{
	int r = rand() ;
	const State* a = NULL ;
	Model* model = State_getModel( state ) ;
	Map_Iterator i = Model_getActions( model ) ;
	EpsilonGreedy* policy = ( EpsilonGreedy* )self ;

	if ( policy->epsilon > ( r % 100 ) ) {
		/* Sample randomly */
		Mapping* mapping ;
		size_t skip = r % Model_numNeighbours( model ) ;
		while ( skip && (mapping = Map_Iterator_next( &i )) ) {
			skip-- ;
		}
		a = Mapping_getKey( mapping ) ;
	}
	else {
		/* Choose a greedy Action. */
		Mapping* mapping ;
		int32_t best_value = INT32_MIN ;
		while ( (mapping = Map_Iterator_next( &i )) ) {
			int32_t expected_value ;
			const State* key = Mapping_getKey( mapping ) ;
			if ( key && (expected_value = Task_estimateActionValue( policy->task, state, key )) > best_value ) {
				a = key ;
				best_value = expected_value ;
			}
		}
	}

	return a ;
}

Policy*
EpsilonGreedy_Create( const Task* task, uint32_t epsilon )
{
	EpsilonGreedy* policy = calloc( 1, sizeof( EpsilonGreedy ) ) ;
	if ( policy ) {
		policy->apply = EpsilonGreedy_apply ;
		policy->epsilon = epsilon ;
		policy->task = task ;
	}
	return ( Policy* )policy ;
}
