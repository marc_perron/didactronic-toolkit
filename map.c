/* -*- Mode: C -*- */
/*
 * map.c - Defines a template ADT for a Map.
 *
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/* Local includes */
#include "util/map.h"

/******************************************************************************
 * Mapping
 ******************************************************************************/

void*
Mapping_getKey( const Mapping* self )
{
	void* key = NULL ;
	if ( self ) {
		key = self->key ;
	}
	return key ;
}

void*
Mapping_getValue( const Mapping* self )
{
	void* value = NULL ;
	if ( self ) {
		value = self->value ;
	}
	return value ;
}

void
Mapping_setValue( Mapping* self, void* value )
{
	assert( self ) ;
	self->value = value ;
}

int32_t
Mapping_getWeight( const Mapping* self )
{
	assert( self ) ;
	return self->weight ;
}

void
Mapping_setWeight( Mapping* self, int32_t weight )
{
	assert( self ) ;
	self->weight = weight ;
}

/******************************************************************************
 * Map
 ******************************************************************************/

Set_IMPLEMENT( Map, Mapping ) ;

static int
Map_DefaultComparator( const void* mapping1, const void* mapping2, size_t unused )
{
	const Mapping* m1 = mapping1 ;
	const Mapping* m2 = mapping2 ;
	return memcmp( m1->key, m2->key, sizeof( void* ) ) ;
}

Map*
Map_Create( Set_Comparator comparator )
{
	if ( comparator == NULL ) {
		comparator = Map_DefaultComparator ;
	}
	return List_Create( comparator ) ;
}

Mapping*
Map_findMapping( const Map* self, const void* key )
{
	Mapping model = {
		.key = ( void* )key
	} ;
	return Set_contains( ( Set* )self, &model, sizeof( model ) ) ;
}

static void
Map_swap( Map* self, Mapping* item1, Mapping* item2 )
{
	if ( item1 && item2 && item1 != item2 ) {
		size_t i1 = List_find( ( List* )self, item1 ) ;
		size_t i2 = List_find( ( List* )self, item2 ) ;
		List_swapItems( self, i1, i2 ) ;
	}
}

void
Map_swapOrder( Map* self, void* key1, void* key2 )
{
	Mapping model1 = {
		.key = ( void* )key1,
	} ;
	Mapping model2 = {
		.key = ( void* )key2,
	} ;

	Map_swap( self, &model1, &model2 ) ;
}

int
Map_add( Map* self, const void* key, void* value )
{
	int ret = -1 ;
	Mapping* entry = Map_findMapping( self, key ) ;
	if ( !entry ) {
		Mapping new_item = {
			.key = key,
			.value = value,
		} ;

		ret = List_append( self, &new_item, sizeof( new_item ) ) ;
	}
	else {
		entry->value = value ;
		ret = 0 ;
	}
	return ret ;
}

void
Map_Delete( Map** self )
{
	List_Delete( self ) ;
}

Map_Iterator
Map_getFirst( const Map* self )
{
	return Set_getFirst( ( Set* )self ) ;
}

void*
Map_getValue( Map* self, const void* key )
{
	void* value = NULL ;
	if ( self ) {
		Mapping* mapping = Map_findMapping( self, key );
		value = Mapping_getValue( mapping ) ;
	}
	return value ;
}

void
Map_setWeight( Map* self, const void* key, int32_t weight )
{
	Mapping* mapping = Map_findMapping( self, key ) ;
	if ( mapping ) {
		Mapping_setWeight( mapping, weight ) ;
	}
}

int32_t
Map_getWeight( Map* self, const void* key )
{
	int32_t weight = INT32_MIN ;
	Mapping* mapping = Map_findMapping( self, key ) ;
	if ( mapping ) {
		weight = Mapping_getWeight( mapping ) ;
	}
	return weight ;
}

/******************************************************************************
 * Map_Iterator
 ******************************************************************************/

Mapping*
Map_Iterator_next( Map_Iterator* self )
{
	Mapping* mapping = Iterator_next( self, NULL ) ;
	return mapping ;
}
