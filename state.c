/* -*- Mode: C -*- */
/*
 * state.c - Defines an ADT to express states in a dynamic program.
 *
 * Copyright (c) 2014-2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX inlcudes */
#include <assert.h>
#include <stdlib.h>
#include <string.h>

/* Local includes */
#include "environment.h"
#include "model.h"
#include "transition.h"
#include "util/list.h"

/******************************************************************************
 * State
 ******************************************************************************/

typedef union _state_impl
{
	State			_public ;
	struct {
		StateID			id ;
		bool			terminal ;
		int32_t			value ;
		void*			model ;
		Map*			neighbourhood ;
		/**
		 * @protected
		 * @brief Pointer to the virtual destructor of the State.
		 * @memberof State
		 *
		 * @details If this member is set, the function it points to
		 * will be called when the State is deleted.
		 *
		 * @see State_Delete
		 */
		State_Destructor*	cleanup ;
	} ;
} StateImpl ;

/**
 * @protected
 * @brief Initialize a previously allocated State.
 * @memberof State
 *
 * @details This function is provided for derived types to initialize
 * the base State data. The State will have the specified ID
 * associated with it as well as an initial value.
 *
 * @param[in,out] self_ptr A generic pointer to a State instance to
 *	initialize.
 * @param[in] id The ID to associate with the current State.
 * @param[in] value A 32-bit unsigned integer to assign as the current
 *	State value.
 * @param[in] is_terminal A flag indicating whether or not the current
 *	State is a terminal in its Environment.
 * @param[in] opt_cleanup An optional pointer to a custom cleanup
 *	function that will be invoked to release resources used by the
 *	State instance
 */
void
State_init( void* self_ptr, StateID id, int32_t value, bool is_terminal, State_Destructor* opt_cleanup )
{
	StateImpl* self = self_ptr ;
	if ( self ) {
		self->id = id ;
		self->terminal = is_terminal ;
		self->value = value ;
		self->model = NULL ;
		self->neighbourhood = Map_Create( NULL ) ;
		self->cleanup = opt_cleanup ;
	}
}

State*
State_Create( StateID id, int32_t value )
{
	State* s = malloc( sizeof( State ) ) ;
	if ( s ) {
		State_init( s, id, value, false, NULL ) ;
	}
	return s ;
}

void
State_cleanup( void* abs_self )
{
	StateImpl* self = abs_self ;
	if ( self && self->cleanup ) {
		self->cleanup( abs_self ) ;
	}
}

void
State_Delete( State** self_base )
{
	StateImpl* self ;
	if ( self_base && (self = *( StateImpl** )self_base) ) {
		Mapping* mapping ;
		List* transitions ;
		Map_Iterator i = Map_getFirst( self->neighbourhood ) ;
		while ( (mapping = Map_Iterator_next( &i )) &&
			(transitions = Mapping_getValue( mapping )) ) {
			free( transitions ) ;
		}
		State_cleanup( self ) ;
		free( self->neighbourhood ) ;
		free( self ) ;
		*self_base = NULL ;
	}
}

StateID
State_getId( const State* self )
{
	assert( self ) ;
	return self->id ;
}

int32_t
State_getValue( const State* abs_self )
{
	int32_t value = INT32_MIN ;
	const StateImpl* self = ( const StateImpl* )abs_self ;
	if ( self ) {
		value = self->value ;
	}
	return value ;
}

float
State_getRealValue( const State* abs_self, int32_t unit )
{
	float val ;
	const StateImpl* self = ( const StateImpl* )abs_self ;
	assert( self ) ;
	assert( unit ) ;
	val = 1.0f * self->value ;
	return val / unit ;
}

bool
State_isTerminal( const State* self )
{
	assert( self ) ;
	return self->terminal ;
}

void
State_setValue( State* abs_self, int32_t value )
{
	StateImpl* self = ( StateImpl* )abs_self ;
	assert( self ) ;
	self->value = value ;
}

void
State_setRealValue( State* abs_self, float val, int32_t unit )
{
	StateImpl* self = ( StateImpl* )abs_self ;
	assert( self ) ;
	assert( unit ) ;
	self->value = unit * val ;
}

void*
State_getModel( const State* abs_self )
{
	Model* model = NULL ;
	const StateImpl* self = ( const StateImpl* )abs_self ;
	if ( self ) {
		model = self->model ;
	}
	return model ;
}

void
State_setModel( State* abs_self, Model* model )
{
	StateImpl* self = ( StateImpl* )abs_self ;
	if ( self ) {
		self->model = model ;
	}
}

Map*
State_getNeighbourhood( const State* abs_self )
{
	const StateImpl* self = ( const StateImpl* )abs_self ;
	assert( self ) ;
	return self->neighbourhood ;
}

void
State_setNeighbourhood( State* abs_self, Map* neighbours )
{
	StateImpl* self = ( StateImpl* )abs_self ;
	assert( self ) ;
	self->neighbourhood = neighbours ;
}

/******************************************************************************
 * StateSet
 ******************************************************************************/

Set_IMPLEMENT( StateSet, State* ) ;

extern void HashMap_init( HashMap* self, HashMap_hashFunc* hash, Set_Comparator compareEntries ) ;

static int
StateSet_CompareStates( const void* state1, const void* state2, size_t size )
{
	int order = 0 ;
	if ( state1 && state2 ) {
		const State* s1 = *( const State** )state1 ;
		const State* s2 = *( const State** )state2 ;
		order = s1->id > s2->id ? 1 : s1->id < s2->id ? -1 : 0 ;
	}		
	return order ;
}

static size_t
StateSet_hashStateID( StateID id )
{
	return id / 512 ;
}

static size_t
StateSet_hashState( const void* state_ptr )
{
	size_t hash = -1 ;
	State* const* s = state_ptr ;
	if ( !s || !(*s) ) {
		errno = EINVAL ;
	}
	else {
		hash = StateSet_hashStateID( (*s)->id )  ;
	}
	return hash ;
}

/**
 * @protected
 * @brief Initialize a StateSet instance.
 * @memberof StateSet
 *
 * @details This will initialize an previously allocated StateSet
 * instance. This function should be called by derived types to
 * initialize the StateSet part of their instance. It should be noted
 * that this function does not clean up any previously allocated
 * members of a StateSet, and should therefore not be used to
 * re-initialize a StateSet instance (unless that set has previously
 * been cleaned up).
 *
 * @see StateSet_cleanup
 *
 * @param[in,out] self A pointer to a StateSet instance that will be
 *	initialized.
 */
void
StateSet_init( StateSet* self )
{
	if ( self ) {
		memset( self, 0, sizeof( *self ) ) ;
		HashMap_init( ( HashMap* )self, StateSet_hashState, StateSet_CompareStates ) ;
	}
}

/**
 * @protected
 * @brief Cleanup a StateSet instance.
 * @memberof StateSet
 *
 * @details This function is used internally to clean up any resources
 * allocated by the StateSet including its index and all States
 * contained therein. This function should be used to ensure that a
 * StateSet is properly cleaned up before initializing a StateSet.
 *
 * @see StateSet_init
 *
 * @param[in,out] self A pointer to a StateSet instance whose internal
 *	data resources will be cleaned up.
 */
void
StateSet_cleanup( StateSet* self )
{
	if ( self ) {
		State* s ;
		Iterator i = StateSet_getFirst( self ) ;
		while ( Iterator_remove( &i, &s, sizeof( s ) ) ) {
			State_Delete( &s ) ;
		}
		List_clear( ( List* )self ) ;
	}
}

StateSet*
StateSet_Create()
{
	StateSet* set = NULL ;

	set = malloc( sizeof( StateSet ) ) ;
	StateSet_init( set ) ;
	return set ;
}

void
StateSet_Delete( StateSet** self )
{
	if ( self ) {
		StateSet_cleanup( *self ) ;
		free( *self ) ;
		*self = NULL ;
	}
}

State*
StateSet_getState( const StateSet* self, StateID id )
{
	State* s = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		State* const* current ;
		size_t hash = StateSet_hashStateID( id ) ;
		Iterator i = HashMap_getItem( self, hash ) ;
		while ( !s && (current = Iterator_next( &i, NULL )) ) {
			if ( current && State_getId( *current ) == id ) {
				s = *current ;
			}
		}
	}
	return s ;
}

State*
StateSet_addState( StateSet* self, State* state )
{
	State* new_state = NULL ;
	if ( 0 == HashMap_addItem( self, &state, sizeof( State* ) ) ) {
		new_state = state ;
	}

	return new_state ;
}

State*
StateSet_getRandom( const StateSet* self )
{
	State* const* state_ptr = NULL ;
	const Set* set = ( const Set* )self ;
	Iterator i = Set_getFirst( set ) ;
	size_t idx = rand() % Set_getSize( set ) ;
	while ( (state_ptr = Iterator_next( &i, NULL )) && idx-- )
		;
	return state_ptr ? *state_ptr : NULL ;
}

bool
StateSet_contains( const StateSet* self, const State* s )
{
	size_t hash = StateSet_hashState( s ) ;
	return ( StateSet_getState( self, hash ) != NULL ) ;
}
