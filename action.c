/* -*- Mode: C -*- */
/*
 * action.c - Defines an ADT to express actions in a dynamic program.
 *
 * Copyright (c) 2014-2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 */

/* POSIX includes */
#include <assert.h>
#include <stdlib.h>
#include <time.h>

/* Local includes */
#include "action.h"

/******************************************************************************
 * Action
 ******************************************************************************/

typedef union _action_impl {
	Action				_public ;
	struct {
		ActionID		id ;
		Action_cleanup_f*	cleanup ;
	} ;
} ActionImpl ;

Action*
Action_Create( ActionID id, Action_cleanup_f* opt_cleanup )
{
	Action* new = calloc( 1, sizeof( Action ) ) ;
	Action_initialize( new, id, opt_cleanup ) ;
	return new ;
}

void
Action_Delete( Action** self_ptr )
{
	Action* self ;
	if ( self_ptr && (self = *self_ptr) ) {
		*self_ptr = NULL ;
		Action_cleanup( self ) ;
		free( self ) ;		
	}
}

void
Action_initialize( void* self_ptr, ActionID id, Action_cleanup_f* opt_cleanup )
{
	ActionImpl* self = self_ptr ;
	if ( self ) {
		self->id = id ;
		self->cleanup = opt_cleanup ;
	}
}

void
Action_cleanup( void* abs_self )
{
	ActionImpl* self = abs_self ;
	if ( self && self->cleanup ) {
		self->cleanup( abs_self ) ;
	}
}

ActionID
Action_getId( const Action* self )
{
	assert( self ) ;
	return self->id ;
}

/******************************************************************************
 * ActionSet
 ******************************************************************************/

static int
ActionSet_CompareActions( const void* a1, const void* a2, size_t size )
{
	assert( a1 ) ;
	assert( a2 ) ;
	const Action** action1 = ( const Action** )a1 ;
        const Action** action2 = ( const Action** )a2 ;

	return ( *action1 )->id > ( *action2 )->id ? 1 : ( *action1 )->id < ( *action2 )->id ? -1 : 0 ;
}

static size_t
ActionSet_hashID( ActionID id )
{
	return id / 512 ;
}

static size_t
ActionSet_hashAction( const void* action )
{
	size_t hash = -1 ;
	Action* const* a = action ;
	if ( !a || !(*a) ) {
		errno = EINVAL ;
	}
	else {
		hash = ActionSet_hashID( (*a)->id ) ;
	}
	return hash ;
}

ActionSet*
ActionSet_Create()
{
	return HashMap_Create( ActionSet_hashAction, ActionSet_CompareActions ) ;
}

Action*
ActionSet_getRandom( const ActionSet* self )
{
	Action* const* action = NULL ;
	Iterator i = Set_getFirst( ( const Set* )self ) ;
	size_t idx = rand() % Set_getSize( ( const Set* )self ) ;
	while ( (action = Iterator_next( &i, NULL )) && idx-- ) ;
	return action ? *action : NULL ;
}

Action*
ActionSet_getAction( const ActionSet* self, ActionID id )
{
	Action* action = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		Action* const* cur_action ;
		size_t hash = ActionSet_hashID( id ) ;
		Iterator i = HashMap_getItem( self, hash ) ;
		while ( !action && (cur_action = Iterator_next( &i, NULL )) ) {
			if ( cur_action && (*cur_action)->id == id ) {
				action = *cur_action ;
			}
		}
	}
	return action ;
}

Set_IMPLEMENT( ActionSet, Action* ) ;

/******************************************************************************
 * ActionMap
 ******************************************************************************/

typedef struct _transition {
	const State*	state ;
	uint32_t	weight ;
} Transition ;

static int
ActionMap_CompareEntries( const void* e1, const void* e2, size_t unused )
{
	int order = 0 ;
	if ( e1 && e2 ) {
		const Action* a1 = *( const Action** )e1 ;
		const Action* a2 = *( const Action** )e2 ;
		order = a1->id - a2->id ;
	}
	return order ;
}

static size_t
ActionMap_hashEntry( const void* entry )
{
	size_t hash = -1 ;
	Action* const* a = entry ;
	if ( !a || !(*a) ) {
		errno = EINVAL ;
	}
	else {
		hash = (*a)->id / 512 ;
	}
	return hash ;
}

ActionMap*
ActionMap_Create()
{
	ActionMap* new = HashMap_Create( ActionMap_hashEntry, ActionMap_CompareEntries ) ;
}

int
ActionMap_addEntry( ActionMap* self, const Action* a, const State* s, uint32_t weight )
{
	Transition tr = {
		.state = s,
		.weight = weight,
	} ;
	return Dictionary_addEntry( self, &a, sizeof( a ), &tr, sizeof( tr ) ) ;
}

Iterator
ActionMap_findTransitions( const ActionMap* self, const Action* a )
{
	return Dictionary_findEntries( self, &a ) ;
}

const State*
ActionMap_NextState( Iterator* current_state, uint32_t* weight )
{
	const State* s = NULL ;
	const Transition* t = Dictionary_NextEntry( current_state, NULL ) ;
	if ( t ) {
		s = t->state ;
		if ( weight ) {
			*weight = t->weight ;
		}
	}
	return s ;
}
