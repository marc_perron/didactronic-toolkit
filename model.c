/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>
#include <errno.h>
#include <math.h>

/* Local includes */
#include "model.h"
#include "transition.h"
#include "util/map.h"

typedef union _model_impl {
	Model   _interface ;
	struct {
		Model_TransitionProbability_f*	transitionProbability ;
		Model_GetReward_f*		getReward ;
		State*				state ;
		Map*				transitions ;
	} ;
} ModelImpl ;

Model*
Model_Create( State* state )
{
	ModelImpl* new = NULL ;
	if ( !state ) {
		errno = EINVAL ;
	}
	else if ( State_getModel( state ) ) {
		errno = EEXIST ;
	}
	else if ( (new = malloc( sizeof( ModelImpl ) )) ) {
		new->state = state ;
		new->transitions = Map_Create( NULL ) ;
	}
	else {
		errno = ENOMEM ;
	}
	return ( Model* )new ;
}

struct _cayley_graph_model {
	Model	_base ;
	Graph*	g ;
} ;

/**
 * @brief Calculate a transition probability.
 * @memberof CayleyGraph_Model
 * @private
 *
 * @details This implementation of the TransitionProbability function
 * will estimate the probability of getting to state @a f based on the
 * hypergeometric distribution using the population size K = max state
 * value - min state value + 1, and K = sum of all state values - min
 * state value + 1.
 */
static int32_t
CayleyGraph_Model_transitionProbability( const void* abs_model, const State* i, const State* f, const Action* a )
{
	int32_t prob = 0 ;
	const CayleyGraph_Model* model = abs_model ;
	if ( model ) {
		size_t N ;
		size_t K ;
		const Edge* e ;
		int32_t min = INT32_MAX ;
		int32_t max = INT32_MIN ;
		int32_t val = State_getValue( f ) ;
		size_t i_ref = Graph_findVertex( model->g, i ) ;
		Iterator n = Graph_getNeighbourhood( model->g, i_ref ) ;

		/* Find the min and max State values. */
		while ( (e = Graph_NextNeighbour( &n, i_ref )) ) {
			const Action* decoration = Edge_getDecoration( e, NULL ) ;
			if ( decoration == a ) {
				int32_t v = State_getValue( Edge_getFinalVertex( e, NULL ) ) ;
				if ( v < min ) {
					min = v ;
				}
				if ( v > max ) {
					max = v ;
				}
			}
		}

		/* Find the population size and the number of success
		 * states therein. */
		min -= 1 ;
		K = max - min ;
		n = Graph_getNeighbourhood( model->g, i_ref ) ;
		while ( (e = Graph_NextNeighbour( &n, i_ref )) ) {
			const Action* decoration = Edge_getDecoration( e, NULL ) ;
			if ( decoration == a ) {
				int32_t v = State_getValue( Edge_getFinalVertex( e, NULL ) ) ;
				N += ( v - min ) ;
			}
		}

		/* Determin the hypergeometric probability using n=1,
		 * k=1, N and K. Let b(a,b) be the binomial
		 * coefficient a!/[b!(a-b)!], the p.d.f. is calculated
		 * as:
		 *
		 * f(x) = b(1,1)b(N-1,K-1)/b(N,K)
		 *      = b(N-1,K-1)/b(N,K)
		 *      = (N-1)!/[(K-1)!(N-K)!]*K!(N-K)!/N!
		 *      = (N-1)!/(K-1)!*(N-K)!/(N-K)!*K!/N!
		 *      = (N-1)!/(K-1)!*K!/N!
		 *      = K(K-1)!/(K-1)! * (N-1)!/N(N-1)!
		 *      = K/N
		 */
		prob = 100 * K/N ;
	}
	return prob ;
}

Model*
CayleyGraph_Model_Create( Graph* graph, Model_GetReward_f* getReward )
{
	Model* model = NULL ;
	CayleyGraph_Model* cg_model = malloc( sizeof( CayleyGraph_Model ) ) ;
	if ( Model_init( &cg_model->_base,
			 CayleyGraph_Model_transitionProbability,
			 getReward ) == 0 ) {
		cg_model->g = graph ;
		model = &cg_model->_base ;
	}
	return model ;
}

int
Model_init( Model* self_if,
	    Model_TransitionProbability_f* transitionProbability,
	    Model_GetReward_f* getReward )
{
	int rc = -1 ;
	if ( !self_if ) {
		errno = EINVAL ;
	}
	else {
		ModelImpl* self = ( ModelImpl* )self_if ;
		self->transitionProbability = transitionProbability ;
		self->getReward = getReward ;
	}
	return rc ;
}

void
Model_Delete( Model** self_ptr )
{
	if ( self_ptr ) {
		ModelImpl* self = ( ModelImpl* )*self_ptr ;
		State_setModel( self->state, NULL ) ;
		free( *self_ptr ) ;
		*self_ptr = NULL ;
	}
}

void
Model_setWeight( Model* self_if, const Action* action, int32_t weight )
{
	ModelImpl* self = ( ModelImpl* )self_if ;
	if ( self && action ) {
		Map_setWeight( self->transitions, action, weight ) ;
	}
}

int32_t
Model_getWeight( const Model* self_if, const Action* action )
{
	int32_t weight = 0 ;
	const ModelImpl* self = ( const ModelImpl* )self_if ;
	if ( self && action ) {
		weight = Map_getWeight( self->transitions, action ) ;
	}
	return weight ;
}

const TransitionList*
Model_getTransitions( const Model* self_if, const State* action )
{
	const TransitionList* transitions = NULL ;
	const ModelImpl* self = ( const ModelImpl* )self_if ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		transitions = Map_getValue( self->transitions, action ) ;
	}
	return transitions ;
}

int
Model_addTransition( Model* self_if, const Action* action, const State* state )
{
	int err = -1 ;
	if ( !self_if || !action || !state ) {
		errno = EINVAL ;
	}
	else {
		ModelImpl* self = ( ModelImpl* )self_if ;
		TransitionList* transitions = Map_getValue( self->transitions, action ) ;
		if ( TransitionList_add( &transitions, state ) >= 0 &&
		     Map_add( self->transitions, action, transitions ) >= 0 ) {
			err = 0 ;
		}
	}
	return err ;
}

Map_Iterator
Model_getActions( Model* self )
{
	Iterator i = 0 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		i = Map_getFirst( ((ModelImpl*)self)->transitions ) ;
	}
	return i ;
}

int
Model_numNeighbours( const Model* self )
{
	int neighbours = -1 ;
	if ( !self ) {
		neighbours = List_getSize( ((const ModelImpl*)self)->transitions ) ;
	}
	return neighbours ;
}

int32_t
Model_transitionProbability( const Model* self, const State* s, const State* s_f, const Action* a )
{
	int32_t prob = -1 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( !self->transitionProbability ) {
		errno = ENOSYS ;
	}
	else {
		prob = self->transitionProbability( self, s, s_f, a ) ;
	}
	return prob ;
}

int32_t
Model_getReward( const Model* self, const State* s, const State* s_f, const Action* a )
{
	assert( self ) ;
	assert( self->getReward ) ;

	return self->getReward( self, s, s_f, a ) ;
}
