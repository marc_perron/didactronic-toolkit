/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>
#include <stdlib.h>

/* Local includes */
#include "agent.h"
#include "policy.h"

/******************************************************************************
 * StateValue
 ******************************************************************************/

typedef struct _agent_state_value {
	const State*	state ;
	int32_t		value ;
} Agent_StateValue ;

int
Agent_StateValue_compare( const void* v1, const void* v2, size_t size )
{
	int order = -1 ;
	const Agent_StateValue* val1 = v1 ;
	const Agent_StateValue* val2 = v2 ;
	if ( val1 && val2 ) {
		order = val1->state > val2->state ? 1 : val1->state < val2->state ? -1 : 0 ;
	}
	return order ;
}

/******************************************************************************
 * Agent
 ******************************************************************************/

typedef union _agent_impl {
	Agent	_public ;
	struct {
		Task*			task ;
		StateSet*		capabilities ;
		Policy*			policy ;
		List*			value_map ;
		Agent_destructorProto*	dtor ;
		
	} ;
} AgentImpl ;

/**
 * @brief Initialize the Agent instance.
 * @memberof Agent
 * @protected
 *
 * @details This function is used internally to initialize instances
 * of the Agent class. It should be called by derived types to
 * initialize the base class before custom initialization is
 * performed.
 *
 * @param[in,out] self A pointer to an Agent instance to initialize.
 * @param[in] task The Task the new Agent will execute.
 * @param[in] policy The initial Policy that will be followed by the
 *	Agent.
 * @param[in] capabilities The StateSet which defines the
 *	capabilities of the Agent.
 * @param[in] dtor A pointer to a specialized destructor that will be
 *	invoked when the Agent is deleted.
 *
 * @see Agent_destructorProto
 */
void
Agent_init( void* self, Task* task, Policy* policy, StateSet* capabilities, Agent_destructorProto* dtor )
{
	AgentImpl* agent ;
	assert( (agent = self) ) ;
	agent->task = task ;
	agent->policy = policy ;
	agent->capabilities = capabilities ;
	agent->value_map = NULL ;
	agent->dtor = dtor ;
}

Agent*
Agent_Create( StateSet* caps, Task* task )
{
	Agent* new = malloc( sizeof( Agent ) ) ;
	if ( new ) {
		Agent_init( new, task, NULL, caps, NULL ) ;
	}
	return new ;
}

void
Agent_Delete( Agent** agent )
{
	if ( agent && *agent ) {
		AgentImpl* impl = ( AgentImpl* )*agent ;
		if ( impl->dtor ) {
			impl->dtor( *agent ) ;
		}
		free( impl->capabilities ) ;
		free( impl ) ;
		*agent = NULL ;
	}
}

const StateSet*
Agent_getCapabilities( const Agent* self )
{
	assert( self ) ;
	return self->capabilities ;
}

/**
 * @protected
 * @brief Associate a Policy with an Agent.
 * @memberof Agent
 *
 * @details This will latently associate a Policy instance with the
 * current Agent. This policy will be used by the Agent to determine
 * its next Action in a given State.
 *
 * @see Agent_nextAction
 *
 * @param[in] self A pointer to an Agent instance.
 * @param[in] policy The Policy instance to asssociate with the Agent.
 */
void
Agent_setPolicy( Agent* self, const Policy* policy )
{
	assert( self ) ;
	self->policy = policy ;
}

const State*
Agent_nextAction( const Agent* self, const State* state )
{
	const State* a = NULL ;
	if ( self->policy ) {
		a = Policy_apply( self->policy, state, 0 ) ;
	}
	else {
		a = StateSet_getRandom( self->capabilities ) ;
	}
	return a ;
}

int
Agent_getValue( const Agent* self, const State* state, int32_t* value )
{
	int rc = -1 ;
	if ( !self || !state || !value ) {
		errno = EINVAL ;
	}
	else {
		int i ;
		const Agent_StateValue* mapping ;
		const AgentImpl* impl = ( const AgentImpl* )self ;
		int32_t val = State_getValue( state ) ;
		if ( (i = List_find( impl->value_map, state )) < 0 &&
		     (mapping = List_getItem( impl->value_map, i )) ) {
			val = mapping->value ;
		}
		*value = val ;
	}
	return rc ;
}

int
Agent_setValue( Agent* self, const State* state, int32_t value )
{
	int i ;
	int rc = -1 ;
	if ( !self || !state ) {
		errno = EINVAL ;
	}
	else {
		int i ;
		Agent_StateValue mapping = {
			.state = state,
			.value = value,
		} ;
		AgentImpl* impl = ( AgentImpl* )self ;
		if ( (i = List_find( impl->value_map, state )) < 0 ) {
			rc = List_append( impl->value_map, &mapping, sizeof( mapping ) ) ;
		}
		else {
			rc = List_setItem( impl->value_map, i, &mapping, sizeof( mapping ) ) ;
		}
	}
	return rc ;
}

/******************************************************************************
 * Agency
 ******************************************************************************/

typedef union _agency_impl {
	Agency		_public ;
	struct {
		List_EXTEND ;
		Agency_next_f*	nextAgent ;
	} ;
} AgencyImpl ;

void
Agency_init( void* self, Agency_next_f* opt_next )
{
	AgencyImpl* agency ;
	List_init( self, NULL ) ;
	if ( (agency = self) ) {
		agency->nextAgent = opt_next ;
	}
}

const Agent*
Agency_next( const Agency* self, Iterator* i )
{
	const Agent* member = NULL ;

	if ( !i || !self ) {
		errno = EINVAL ;
	}
	else {
		const AgencyImpl* agency = ( const AgencyImpl* )self ;
		if ( *i ) {
			member = Iterator_next( i, NULL ) ;
		}
		else {
			/* Get the first member of the Agency. */
			member = List_getItem( ( List* )agency, 0 ) ;
			*i = Iterator_Create( member ) ;
		}
	}
	return member ;
}
