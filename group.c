/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Definition of a Group class.
 *
 * @details This file provides the implementation of Group class which
 * is used to express a abstract combinatorial group of arbitrary
 * data.
 */

/* Local includes */
#include "util/group.h"
#include "util/hashmap.h"

/* POSIX includes */
#include <string.h>

struct _group {
	const Set*		generators ;
	const RelationSet*	relations ;
	const void*		identity ;
	Group_Law*		product ;
} ;

Group*
Group_Create( const Set* generators, const RelationSet* relations, const void* identity, Group_Law* product )
{
	Group* new = NULL ;
	if ( !generators || !relations || !product || !identity ) {
		errno = EINVAL ;
	}
	else if ( !(new = calloc( 1, sizeof( Group ) )) ) {
		errno = ENOMEM ;
	}
	else {
		new->generators = generators ;
		new->relations = relations ;
		new->identity = identity ;
		new->product = product ;
	}
	return new ;
}

void
Group_Delete( Group** self_ptr )
{
	Group* self ;
	if ( self_ptr && (self = *self_ptr) ) {
		*self_ptr = NULL ;
		free( self ) ;
	}
}

void*
Group_compose( const Group* self, const void* e1, const void* e2 )
{
	void* product = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		product = self->product( self, e1, e2 ) ;
	}
	return product ;
}

const void*
Group_reduce( const Group* self, const void* elem )
{
	const void* image = elem ;
	if ( self ) {
		image = RelationSet_findImage( self->relations, elem ) ;
	}
	return image ;
}

Iterator
Group_firstGenerator( const Group* self )
{
	Iterator i = 0 ;
	if ( !self || !self->generators ) {
		errno = EINVAL ;
	}
	else {
		i = Set_getFirst( self->generators ) ;
	}
	return i ;
}

const void*
Group_getIdentity( const Group* self )
{
	const void* identity = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		identity = self->identity ;
	}
	return identity ;
}

/******************************************************************************
 * Relation
 ******************************************************************************/

typedef struct _relation {
	const void*	item ;
	const void*	image ;
	RelationSet*	set ;
} Relation ;

struct _relation_set {
	HashMap_EXTEND ;
	HashMap_hashFunc*	hashItem ;
} ;

static int
RelationSet_compare( const void* item1, const void* item2, size_t item_size __attribute__((unused)) )
{
	int order = 0 ;
	const Relation* r1 = item1 ;
	const Relation* r2 = item2 ;
	if ( r1 && r2 && r1->set && r1->set == r2->set ) {
		int hash_r1 = HashMap_hash( &r1->set->_HashMap, r1 ) ;
		int hash_r2 = HashMap_hash( &r2->set->_HashMap, r2 ) ;
		order = hash_r1 - hash_r2 ;
	}
	return order ;
}

static size_t
RelationSet_hashRelation( const void* item )
{
	size_t hash = -1 ;
	const Relation* r = item ;
	if ( r ) {
		hash = r->set->hashItem( r->item ) ;
	}
	return hash ;
}

RelationSet*
RelationSet_Create( HashMap_hashFunc* hashItem )
{
	RelationSet* new = NULL ;
	if ( !hashItem ) {
		errno = EINVAL ;
	}
	else if ( (new = malloc( sizeof( RelationSet ) )) ) {
		HashMap_init( &new->_HashMap, RelationSet_hashRelation, RelationSet_compare ) ;
		new->hashItem = hashItem ;
	}
	return new ;
}

int
RelationSet_addRelation( RelationSet* self, const void* item, const void* image )
{
	int rc = -1 ;
	struct _relation r = {
		.item = item,
		.image = image,
		.set = self,
	} ;

	if ( !item ) {
		errno = EINVAL ;
	}
	else {
		rc = HashMap_addItem( &self->_HashMap, &r, sizeof( r ) ) ;
	}
	return rc ;
}

const void*
RelationSet_findImage( const RelationSet* self, const void* item )
{
	const void* image = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		size_t idx = self->hashItem( item ) ;
		Iterator i = HashMap_getItem( &self->_HashMap, idx ) ;
		const Relation* relation = Iterator_getItem( i, NULL ) ;
		if ( relation && (self->hashItem( relation->item ) - self->hashItem( item )) == 0 ) {
			image = relation->image ;
		}
		else {
			image = item ;
		}
	}
	return image ;
}
