/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

/* Local includes */
#include "util/set.h"

/******************************************************************************
 * Set_Element
 ******************************************************************************/

struct _set_element {
	size_t		size ;
	Set_Element*	next ;
	Set_Element*	prev ;
	Set*		container ;
	char		item[ 0 ] ;
} ;

static Set_Element*
Set_Element_Create( const void* item, size_t size )
{
	Set_Element* new = calloc( 1, sizeof( Set_Element ) + size ) ;
	if ( new ) {
		new->size = size ;
		memcpy( new->item, item, size ) ;
	}
	return new ;
}

void*
Set_Element_data( const Set_Element* self )
{
	assert( self ) ;
	return ( void* )self->item ;
}

size_t
Set_Element_dataSize( const Set_Element* self )
{
	assert( self ) ;
	return self->size ;
}

int
Set_Element_copyItem( const Set_Element* self, void* item, size_t size )
{
	int ret = -1 ;
	if ( !self || !item ) {
		errno = EINVAL ;
	}
	else if ( size > 0 ) {
		if ( size > self->size ) {
			size = self->size ;
		}
		memcpy( item, self->item, size ) ;
		ret = size ;
	}
	return ret ;
}

static Set_Element*
Set_Element_clone( const Set_Element* self )
{
	Set_Element* clone = NULL ;
	if ( self ) {
		clone = Set_Element_Create( self->item, self->size ) ;
	}
	return clone ;
}

void*
Set_Element_getItem( Set_Element* self, size_t* size )
{
	void* item = NULL ;
	if ( self && self->size > 0 ) {
		item = self->item ;
		if ( size ) {
			*size = self->size ;
		}
	}
	return item ;
}

int
Set_Element_compare( const Set_Element* self, const void* item, size_t item_size )
{
	Set_Comparator* compare ;
	assert( self ) ;
	if ( self->container ) {
		compare = self->container->compare ;
	}
	else {
		compare = memcmp ;
	}
	return compare( self->item, item, item_size > self->size ? self->size : item_size ) ;
}

int
Set_Element_compareElement( const Set_Element* self, const Set_Element* element )
{
	return Set_Element_compare( self, element->item, element->size ) ;
}

static bool
Set_Element_equals( const Set_Element* self, const void* item, size_t size )
{
	bool equal = false ;
	if ( self && item && size == self->size ) {
		equal = !Set_Element_compare( self, item, size ) ;
	}
	return equal ;
}

void
Set_Element_detach( Set_Element* self )
{
	if ( self ) {
		if ( self->prev ) {
			self->prev->next = self->next ;
		}
		if ( self->next ) {
			self->next->prev = self->prev ;
		}
		self->next = self->prev = NULL ;
		if ( self->container && self->container->cardinality > 0 ) {
			self->container->cardinality -= 1 ;
		}
		self->container = NULL ;
	}
}

int
Set_Element_append( Set_Element* self, Set_Element* element )
{
	int err = -1 ;
	if ( !self || !element ) {
		errno = EINVAL ;
	}
	else {
		Set_Element_detach( element ) ;
		element->prev = self ;
		element->next = self->next ;
		self->next = element ;
		if ( element->next ) {
			element->next->prev = element ;
		}
		element->container = self->container ;
		if ( element->container ) {
			element->container->cardinality += 1 ;
		}
		err = 0 ;
	}
	return err ;
}

static int
Set_Element_prepend( Set_Element* self, Set_Element* element )
{
	int err = -1 ;
	if ( !self || !element ) {
		errno = EINVAL ;
	}
	else {
		Set_Element_detach( element ) ;
		element->prev = self->prev ;
		element->next = self ;
		if ( element->prev ) {
			element->prev->next = element ;
		}
		element->container = self->container ;
		if ( element->container ) {
			element->container->cardinality += 1 ;
		}
		err = 0 ;
	}
	return err ;
}

/******************************************************************************
 * Iterator
 ******************************************************************************/

static Set_Element*
Iterator_toElement( const Iterator self )
{
	return *( Set_Element** )self ;
}

static Set_Element*
Iterator_getElement( const Iterator self )
{
	Set_Element* e ;
	Set_Element* element = NULL ;
	if ( self && (e = Iterator_toElement( self )) && e->next && e->next->size > 0 ) {
		element = e->next ;
	}
	return element ;
}

static Set_Element*
Iterator_nextElement( Iterator* self )
{
	Set_Element* element = NULL ;
	if ( Iterator_next( self, NULL ) ) {
		element = Iterator_toElement( *self ) ;
	}
	return element ;
}

/**
 * @protected
 * @brief Create an Iterator to an item in a Set.
 * @memberof Set
 *
 * @details This will return an Iterator which points to the specified
 * @a item. This operation is the inverse of invoking getItem() on an
 * Iterator. If this operation is successful, the Iterator will be
 * located just before the referenced @a item:
   @verbatim
   ...->[Item]->...
       ^
       |
   [Iterator]
   @endverbatim
 *
 * @param[in] item A pointer to the immutable buffer containing the
 *	data of an element which is the member of a Set. This @e must
 *	be a pointer that was returned by a previous call to
 *	getItem(), or the Iterator will be invalide and have undefined
 *	behaviour.
 *
 * @see Iterator_getItem
 *
 * @return An Iterator which points to the specified @a item in a
 * Set. This iterator can be used to walk the Set elements starting
 * from the current one.
 */
Iterator
Iterator_Create( const void* item )
{
	Iterator i = 0 ;
	if ( item ) {		
	        const Set_Element* element = ( const Set_Element* )( item - offsetof( Set_Element, item ) ) ;
		if ( element->prev ) {
			i = ( Iterator )&element->prev ;
		}
	}
	return i ;
}

const void*
Iterator_getItem( const Iterator self, size_t* opt_size )
{
	const void* item = NULL ;
	Set_Element* element = Iterator_getElement( self ) ;
	if ( !element ) {
		errno = ENOENT ;
	}
	else {
		item = Set_Element_data( element ) ;
		if ( opt_size ) {
			*opt_size = Set_Element_dataSize( element ) ;
		}
	}
	return item ;
}

const void*
Iterator_toItem( const Iterator self, size_t* opt_size )
{
	Set_Element* element ;
	const void* item = NULL ;
	if ( self == 0 || !(element = Iterator_toElement( self )) ) {
		errno = ENOENT ;
	}
	else {
		item = Set_Element_data( element ) ;
		if ( opt_size ) {
			*opt_size = Set_Element_dataSize( element ) ;
		}
	}
	return item ;
}

bool
Iterator_hasNext( const Iterator self )
{
	Set_Element* element = Iterator_toElement( self ) ;
	return ( element->next && element->next->size > 0 ) ;
}

const void*
Iterator_next( Iterator* self, size_t* size )
{
	Set_Element* element ;
	const void* item = NULL ;
	if ( self && *self && (element = Iterator_toElement( *self )) && element->next ) {
		item = Set_Element_getItem( element->next, size ) ;
		*self = ( Iterator )&element->next ;
	}
	return item ;
}

const void*
Iterator_previous( Iterator* self, size_t* opt_size )
{
	Set_Element* element ;
	const void* item = NULL ;
	if ( self && *self && (element = Iterator_toElement( *self )) && element->prev ) {
		item = Set_Element_getItem( element, opt_size ) ;
		*self = ( Iterator )&element->prev ;
	}
	return item ;
}

bool
Iterator_remove( Iterator* self, void* item, size_t size )
{
	bool removed = false ;
	if ( self && *self ) {
		Set_Element* element = Iterator_getElement( *self ) ;
		if ( element && element->container ) {
			element->container->cardinality -= 1 ;
			Set_Element_copyItem( element, item, size ) ;
			Set_Element_detach( element ) ;
			free( element ) ;
			removed = true ;
		}
	}
	return removed ;
}

int
Iterator_append( Iterator self, const void* item, size_t item_size )
{
	int rc = -1 ;
	Set_Element* current = Iterator_toElement( self ) ;
	if ( !item ) {
		errno = EINVAL ;
	}
	else if ( !current ) {
		errno = ENOENT ;
	}
	else {
		Set_Element* element = Set_Element_Create( item, item_size ) ;
		if ( !element ) {
			errno = ENOMEM ;
		}
		else {
			Set_Element_append( current, element ) ;
			rc = 0 ;
		}
	}
	return rc ;
}

/******************************************************************************
 * Set
 ******************************************************************************/

typedef struct {
	/**
	 * @brief The number of members in the current Set.
	 * @memberof Set
	 * @protected
	 *
	 * @details This property tracks the number of elements that have been
	 * added as members in the current Set instance.
	 */
	size_t		cardinality ;

	/**
	 * @brief The comparator associated with the current Set.
	 * @memberof Set
	 * @protected
	 *
	 * @details This property holds a pointer to the function that is used
	 * to determine the partial order of the members in the current Set.
	 */
	Set_Comparator*	compare ;

	Set_Element	head ;
	Set_Element	tail ;
	Set_Element*	front ;
	Set_Element*	back ;
} SetImpl ;

/**
 * @brief Initialize a previously allocated Set.
 * @memberof Set
 * @protected
 *
 * @details This function is provided for derived types to initialize
 * the base Set instance data. The base Set will be initialized to be
 * initially empty, and an optional comparator function can be
 * associated with the Set.
 *
 * @param[in,out] self A pointer to the Set instance to initialize.
 * @param[in] compare An optional comparator function that will be
 *	used to determine the partial order of Set members. If this
 *	argument is omitted, the standard memcmp() function will be
 *	used.
 */
void
Set_init( Set* self, Set_Comparator compare )
{
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		SetImpl* set = ( SetImpl* )self ;
		set->head.container = self ;
		set->tail.container = self ;
		Set_clear( self ) ;
		set->compare = compare ? compare : memcmp ;
	}
}

Set*
Set_Create( Set_Comparator compare )
{
	Set* new = calloc( 1, sizeof( SetImpl ) ) ;
	Set_init( new, compare ) ;
	return new ;
}

Set*
Set_Union( const Set* s1, const Set* s2, Set_Comparator* comparator )
{
	Set* new = Set_Create( s1->compare == s2->compare ? s1->compare : NULL ) ;
	if ( new ) {
		size_t item_size ;
		const void* item ;
		Iterator i = Set_getFirst( s1 ) ;
		while ( (item = Iterator_next( &i, &item_size )) ) {
			Set_insert( new, item, item_size ) ;
		}
		i = Set_getFirst( s2 ) ;
		while ( (item = Iterator_next( &i, &item_size )) ) {
			Set_insert( new, item, item_size ) ;
		}
	}
	else {
		errno = ENOMEM ;
	}
	return new ;
}

Set*
Set_Intersection( const Set* s1, const Set* s2, Set_Comparator comparator )
{
	Set* new = Set_Create( comparator ? comparator : s1->compare ) ;
	if ( new ) {
		Iterator i = Set_getFirst( s1 ) ;
		while ( Iterator_next( &i, NULL ) ) {
			Set_Element* e = Iterator_toElement( i ) ;
			if ( e && !Set_contains( s2, e->item, e->size ) ) {
				Set_insert( new, e->item, e->size ) ;
			}
		}
	}
	return new ;
}

Set*
Set_clone( const Set* self )
{
	Set* clone = NULL ;
	const SetImpl* set = ( const SetImpl* )self ;
	if ( set && (clone = Set_Create( set->compare )) ) {
		Iterator i = Set_getFirst( self ) ;
		while ( Iterator_next( &i, NULL ) ) {
			Set_Element* last_element = set->tail.prev ;
			Set_Element* element = Iterator_toElement( i ) ;
			Set_Element_append( last_element, element ) ;
		}
	}
	return clone ;
}

void
Set_Delete( Set** self )
{
	if ( self && *self ) {
		Set_clear( *self ) ;
		free( *self ) ;
		*self = NULL ;
	}
}

int
Set_compare( const Set* self, const void* item1, const void* item2, size_t size )
{
	assert( self ) ;
	assert( self->compare ) ;
	return self->compare( item1, item2, size ) ;
}

Iterator
Set_getFirst( const Set* self )
{
	assert( self ) ;
	return ( Iterator )&(( const SetImpl* )self)->front ;
}

Iterator
Set_getLast( const Set* self )
{
	assert( self ) ;
	return ( Iterator )&(( const SetImpl* )self)->back ;
}

size_t
Set_getSize( const Set* self )
{
	assert( self ) ;
	return self->cardinality ;
}

const void*
Set_find( const Set* self, const void* model, size_t model_size )
{
	Set_Element* element ;
	const void* item = NULL ;
	Iterator i = Set_getFirst( self ) ;

	while ( !item && (element = Iterator_getElement( i )) ) {
		if ( Set_Element_equals( element, model, model_size ) ) {
			item = Set_Element_data( element ) ;
		}
		Iterator_next( &i, NULL ) ;
	}
	return item ;
}

static Set_Element*
Set_findElement( const Set* self, const void* item, size_t item_size )
{
	Set_Element* element ;
	Set_Element* found = NULL ;
	Iterator i = Set_getFirst( self ) ;

	while ( !found && (element = Iterator_getElement( i )) ) {
		if ( Set_Element_equals( element, item, item_size ) ) {
			found = element ;
		}
		else {
			Iterator_next( &i, NULL ) ;
		}
	}
	return found ;
}

void*
Set_contains( const Set* self, const void* item, size_t size )
{
	void* item_ptr = NULL ;
	assert( self ) ;
	if ( !item ) {
		errno = EINVAL ;
	}
	else {
		Set_Element* found = Set_findElement( self, item, size ) ;
		if ( found ) {
			item_ptr = Set_Element_data( found ) ;
		}
	}
	return item_ptr ;
}

int
Set_insertElement( Set* self, Set_Element* element )
{
	int err = -1 ;
	if ( !self || !element ) {
		errno = EINVAL ;
	}
	else {
		int order ;
		Iterator i ;
		Set_Element* current ;

		order = 1 ;
		i = Set_getFirst( self ) ;
		while ( (current = Iterator_getElement( i )) &&
			(order = Set_Element_compareElement( current, element )) < 0 ) {
			Iterator_next( &i, NULL ) ;
		}

		if ( order == 0 ) {
			element->container = NULL ;
			errno = EEXIST ;
		}
		else {
			err = Set_Element_prepend( current, element ) ;
		}
	}
	return err ;
}

/**
 * @brief Create a new Set_Element instance.
 * @memberof Set
 * @protected
 *
 * @details This will allocate and initialize a new Set_Element
 * instance which encapsulates the specified payload data. This
 * function will attempt to insert the new element into the current
 * Set instance before returning. This operation is identical to the
 * Set_insert() function except that it returns a pointer to the newly
 * allocated Set_Element instance.
 *
 * @see Set_insert
 *
 * @param[in,out] self A pointer to the Set in which the newly created
 *	member will be inserted.
 * @param[in] data A pointer to a buffer containing the payload data
 *	that will be associated with the new element. This payload
 *	data will be copied into the buffer which will be used to
 *	encapsulate the Element's payload.
 * @param[in] size The size, in bytes, of the data payload of the
 *	element.
 *
 * @return A pointer to a new Set_Element instance, created on the
 * heap, and inserted as a member of the specified Set container. The
 * ownership of the memory returned by this function is retained by
 * the caller and must not be deleted by the callee. If an error
 * occurs while creating the element, this function will return a
 * @c NULL pointer and set errno accordingly.
 */
Set_Element*
Set_createElement( Set* self, const void* data, size_t size )
{
	Set_Element* element = Set_Element_Create( data, size ) ;
	if ( !element ) {
		errno = ENOMEM ;
	}
	else if ( Set_insertElement( self, element ) != 0 ) {
		free( element ) ;
		element = NULL ;
	}
	return element ;
}

Iterator
Set_insert( Set* self, const void* item, size_t size )
{
	Iterator element = 0 ;
	if ( !self || !item || size < 0 ) {
		errno = EINVAL ;
	}
	else {
		int order ;
		Set_Element* current ;
		Iterator i = Set_getFirst( self ) ;

		while ( (current = Iterator_getElement( i )) &&
			(order = Set_Element_compare( current, item, size )) < 0 ) {
			Iterator_next( &i, NULL ) ;
		}

		if ( current && order == 0 ) {
			errno = EEXIST ;
		}
		else if ( Iterator_append( i, item, size ) == 0 ) {
			element = i ;
		}
	}
	return element ;
}

/**
 * @protected
 * @brief Add or replace a member of a Set.
 * @memberof Set
 *
 * @details This will insert a shallow copy of the specified item as a
 * new member of the current Set. If an equal item already exists in
 * the Set, it will be removed from the set, deleted, and replaced
 * with the new item.
 *
 * @param[in] self A pointer to a Set instance.
 * @param[in] item A pointer to a buffer containing the data of the
 *	item to add to the Set.
 * @param[in] size The size of the item's data buffer in bytes.
 *
 * @retval 0 The new item was added successfully.
 * @retval -1 The insertion of the new item failed; @e errno will be
 *	set accordingly.
 *
 * @par Errors
 * - EINVAL: The Set instance of item data are invalid.
 * - ENOMEM: Allocation of the new item failed.
 */
int
Set_replace( Set* self, const void* item, size_t size )
{
	int err = -1 ;
	if ( !self || !item || size < 0 ) {
		errno = EINVAL ;
	}
	else {
		int order ;
		Set_Element* current ;
		Iterator i = Set_getFirst( self ) ;

		while ( (current = Iterator_getElement( i )) &&
			(order = Set_Element_compare( current, item, size )) < 0 ) {
			Iterator_next( &i, NULL ) ;
		}
		if ( current && order == 0 ) {
			/* Remove the previous element. */
			Set_Element_detach( current ) ;
			free( current ) ;
		}
		err = Iterator_append( i, item, size ) ;
	}
	return err ;
}

bool
Set_remove( Set* self, const void* item, size_t size )
{
	bool removed = false ;
	if ( !self || !item || size == 0 ) {
		errno = EINVAL ;
	}
	else {
		Set_Element* i = Set_findElement( self, item, size ) ;
		if ( i ) {
			Set_Element_detach( i ) ;
			free( i ) ;
		}
	}
	return removed ;
}

void
Set_clear( Set* self )
{
	if ( self ) {
		SetImpl* set = ( SetImpl* )self ;
		Set_Element* e = set->head.next ;
		while ( e && e != set->back ) {
			Set_Element* tmp = e ;
			e = e->next ;
			free( tmp ) ;
		}
		set->front = set->tail.prev = &set->head ;
		set->back = set->head.next = &set->tail ;
		set->cardinality = 0 ;
	}
}
