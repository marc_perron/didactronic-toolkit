/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 *
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 *
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Definition of an ADT to express arbitrary graphs.
 */

/* POSIX */
#include <stdio.h>
#include <string.h>

/* Local includes */
#include "util/graph.h"
#include "util/group.h"
#include "util/hashmap.h"

typedef union _graph_impl
{
	Graph		_public ;
	struct {
		char			name[ 257 ] ;
		size_t			num_partitions ;
		HashMap			edges ;
		HashMap			vertices ;
		HashMap*		partitions ;
		HashMap_hashFunc*	hashVertex ;
	} ;
} GraphImpl ;

typedef struct _vertex {
	uint64_t		id ;
	const GraphImpl*	graph ;
	size_t			data_size ;
	char			data[ 0 ] ;
} Vertex ;

static size_t
Vertex_hash( const void* vertex_stub )
{
	size_t hash = -1 ;
	const Vertex* vertex = vertex_stub ;
	if ( vertex && vertex->graph ) {
		if ( vertex->graph->hashVertex ) {
			hash = vertex->graph->hashVertex( vertex->data ) ;
		}
		else {
			hash = vertex->id ;
		}
	}
	return hash ;
}

static int
Vertex_compare( const void* stub1, const void* stub2, size_t __attribute__((unused))vertex_size )
{
	int order = 0 ;
	const Vertex* v1 = stub1 ;
	const Vertex* v2 = stub2 ;
	if ( v1 && v2 && v1->graph && v1->graph == v2->graph ) {
		order = (( int )v1->id) - v2->id ;
	}
	return order ;
}

struct _edge {
	GraphImpl*	graph ;
	const Vertex*	initial ;
	const Vertex*	final ;
	size_t		decoration_size ;
	char		decoration[ 0 ] ;
} ;

static int
Edge_compare( const void* edge1, const void* edge2, size_t unused )
{
	int order = 0 ;
	const Edge* e1 = edge1 ;
	const Edge* e2 = edge2 ;
	if ( e1 && e2 ) {
		order = Set_compare( ( Set* )&e1->graph->vertices, e1->initial, e2->initial, unused ) ;
		if ( !order ) {
			order = memcmp( e1->decoration, e2->decoration, e1->decoration_size ) ;
			if ( !order ) {
				order = Set_compare( ( Set* )&e1->graph->vertices, e1->final, e2->final, unused ) ;
			}
		}
	}
}

static size_t
Edge_hash( const void* edge_ptr )
{
	size_t hash = -1 ;
	const Edge* edge = edge_ptr ;
	if ( edge ) {
		hash = HashMap_hash( &edge->graph->vertices, edge->initial ) ;
	}
	return hash ;
}

const void*
Edge_getInitialVertex( const Edge* self, size_t* opt_data_size )
{
	const void* vertex = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		vertex = self->initial->data ;
		if ( opt_data_size ) {
			*opt_data_size = self->initial->data_size ;
		}
	}
	return vertex ;
}

const void*
Edge_getFinalVertex( const Edge* self, size_t* opt_data_size )
{
	const void* vertex = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		vertex = self->final->data ;
		if ( opt_data_size ) {
			*opt_data_size = self->final->data_size ;
		}
	}
	return vertex ;
}

void*
Edge_getDecoration( const Edge* self, size_t* decoration_size )
{
	void* decoration = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( self->decoration_size == 0 ) {
		errno = ENOENT ;
	}
	else {
		decoration = ( void* )self->decoration ;
		if ( decoration_size ) {
			*decoration_size = self->decoration_size ;
		}
	}
	return decoration ;
}

int
Graph_init( Graph* abs_self, const char* name, size_t k, HashMap_hashFunc* hashVertex )
{
	int rc = -1 ;
	if ( !abs_self ) {
		errno = EINVAL ;
	}
	else if ( !name ) {
		errno = EINVAL ;
	}
	else {
		GraphImpl* self = ( GraphImpl* )abs_self ;
		strlcpy( self->name, name, sizeof( self->name ) ) ;
		self->num_partitions = k > 0 ? k : 1 ;
		self->hashVertex = hashVertex ;
		HashMap_init( &self->edges, Edge_hash, Edge_compare ) ;
		HashMap_init( &self->vertices, Vertex_hash, Vertex_compare ) ;
		self->partitions = calloc( self->num_partitions, sizeof( HashMap ) ) ;
		if ( !self->partitions ) {
			errno = ENOMEM ;
		}
		else {
			size_t i ;
			for ( i = 0 ; i < self->num_partitions ; i++ ) {
				HashMap_init( &self->partitions[ i ], NULL, NULL ) ;
			}
			rc = 0 ;
		}
	}
	return rc ;
}

void
Graph_cleanup( Graph* abs_self )
{
	GraphImpl* self = ( GraphImpl* )abs_self ;
	if ( self ) {
		size_t i ;
		Set_clear( ( Set* )&self->edges ) ;
		Set_clear( ( Set* )&self->vertices ) ;
		for ( i = 0 ; i < self->num_partitions ; i++ ) {
			Set_clear( ( Set* )&self->partitions[ i ] ) ;
		}
		free( self->partitions ) ;
	}
}

static size_t
Graph_hashVertex( const Graph* abs_self, const void* vertex_data )
{
	size_t referent = -1 ;
	if ( !abs_self || !vertex_data ) {
		errno = EINVAL ;
	}
	else {		
		const GraphImpl* self = ( const GraphImpl* )abs_self ;
		referent = self->hashVertex( vertex_data ) ;
	}
	return referent ;
}

size_t
Graph_findVertex( const Graph* abs_self, const void* vertex_data )
{
	return Graph_hashVertex( abs_self, vertex_data ) ;
}

int
Graph_createVertex( Graph* abs_self, size_t k, const void* data, size_t data_size )
{
	int rc = -1 ;
	GraphImpl* self = ( GraphImpl* )abs_self ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( k >= self->num_partitions ) {
		errno = ERANGE ;
	}
	else if ( HashMap_getItem( &self->vertices, self->hashVertex( data ) ) ) {
		errno = EEXIST ;
	}
	else {
		size_t size = sizeof( Vertex ) + data_size ;
		char buf[ size ] ;
		Vertex* v = ( Vertex* )buf ;
		if ( data_size > 0 ) {
			memcpy( v->data, data, data_size ) ;
		}
		v->graph = self ;
		v->id = Set_getSize( ( const Set* )&self->vertices ) + 1 ;
		v->data_size = data_size ;
		
		if ( !(rc = HashMap_addItem( &self->vertices, v, size )) ) {
			rc = self->hashVertex ? self->hashVertex( data ) : v->id ;
			/* TODO:: Add to the correct partition. */
		}
	}
	return rc ;
}

/**
 * @private
 * @brief Get the vertex associated with the specified referent.
 * @memberof Graph
 *
 * @details This will retrieve a Vertex from a Graph by its
 * referent. The referent will have been assigned to the Vertex when
 * it was initially created.
 *
 * @see Graph_createVertex
 *
 * @note This operation has O(k) complexity for a k-partite graph.
 *
 * @param[in] abs_self A pointer to a Graph instance.
 * @param[in] ref The referent of a Vertex in the graph.
 *
 * @return The Vertex, which is a member of the current Graph, which
 * is associated with the specified referent. In the case of an error,
 * this function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The Graph is invalid.
 * - ERANGE: The Graph does not have a Vertex with the specified
 *   referent.
 */
static const Vertex*
Graph_getVertex( const Graph* abs_self, size_t ref )
{
	const Vertex* v = NULL ;
	if ( !abs_self ) {
		errno = EINVAL ;
	}
	else {
		Iterator i ;
		const GraphImpl* self = ( const GraphImpl* )abs_self ;
		i = HashMap_getItem( &self->vertices, ref ) ;
		v = Iterator_getItem( i, NULL ) ;
	}
	return v ;
}

int
Graph_addDecoratedEdge( Graph* abs_self, size_t v1_ref, size_t v2_ref, const void* decoration, size_t decoration_size )
{
	int rc = -1 ;
	GraphImpl* self = ( GraphImpl* )abs_self ;
	size_t e_size = sizeof( Edge ) + decoration_size ;
	if ( !self || ( decoration_size > 0 && !decoration ) ) {
		errno = EINVAL ;
	}
	else {
		char e_buffer[ e_size ] ;
		Edge* e = ( Edge* )e_buffer ;
		e->graph = self ;
		e->initial = Graph_getVertex( abs_self, v1_ref ) ;
		e->final = Graph_getVertex( abs_self, v2_ref ) ;
		e->decoration_size = decoration_size ;
		if ( !e->initial || !e->final ) {
			errno = ENOENT ;
		}
		else {
			if ( decoration ) {
				memcpy( e->decoration, decoration, decoration_size ) ;
			}
			rc = HashMap_addItem( &self->edges, e, e_size ) ;
		}
	}
	return rc ;
}

int
Graph_addEdge( Graph* self, size_t v1_ref, size_t v2_ref, const char* label )
{
	size_t label_size = label ? strlen(label)+1 : 0 ;
	
	return Graph_addDecoratedEdge( self, v1_ref, v2_ref, label, label_size ) ;
}

Iterator
Graph_getNeighbourhood( const Graph* abs_self, size_t referent )
{
	const Vertex* v ;
	Iterator neighbour = 0 ;

	if ( (v = Graph_getVertex( abs_self, referent )) ) {
		const GraphImpl* self = ( const GraphImpl* )abs_self ;
		if ( (neighbour = HashMap_getItem( &self->edges, referent )) == 0 ) {
			errno = ENOENT ;
		}
	}
	return neighbour ;
}

const Edge*
Graph_NextNeighbour( Iterator* i, size_t referent )
{
	const Edge* edge = NULL ;
	if ( i && *i ) {
		const Edge* tmp_e = Iterator_getItem( *i, NULL ) ;
		if ( tmp_e && tmp_e->initial->id != referent ) {
			errno = ERANGE ;
		}
		else {
			/* Get the edge and advance the iterator. */
			edge = Iterator_next( i, NULL ) ;
		}
	}
	return edge ;
}

Iterator
Graph_getFirstVertex( const Graph* abs_self )
{
	Iterator i = 0 ;
	const GraphImpl* self = ( const GraphImpl* )abs_self ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		i = Set_getFirst( ( Set* )&self->vertices ) ;
	}
	return i ;
}

const void*
Graph_NextVertex( Iterator* i, size_t* vertex_size )
{
	const void* data = NULL ;
	const Vertex* v = Iterator_next( i, vertex_size ) ;
	if ( v ) {
		data = v->data ;
		if ( vertex_size ) {
			*vertex_size -= ( sizeof( Vertex ) ) ;
		}
	}
	return data ;
}

int
Graph_getOrder( const Graph* self )
{
	int order = -1 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		order = Set_getSize( ( const Set* )self ) ;
	}
	return order ;
}

int
Graph_buildCayleyGraph( Graph* self, const char* label, const Group* presentation, HashMap_hashFunc* hashVertex )
{
	int rc = -1 ;
	if ( !self || !label || !presentation ) {
		errno = EINVAL ;
	}
	else {
		Iterator i ;
		size_t v_size ;
		const void* v ;
		Graph_cleanup( self ) ;
		Graph_init( self, label, 1, hashVertex ) ;
		Graph_createVertex( self, 0, Group_getIdentity( presentation ), sizeof( void* ) ) ;
		/* Expand the Cayley Graph for the group. */
		i = Graph_getFirstVertex( self ) ;
		while ( (v = Graph_NextVertex( &i, &v_size )) ) {
			const void* a ;
			size_t v_ref = Graph_findVertex( self, v ) ;
			Iterator j = Group_firstGenerator( presentation ) ;
			while ( (a = Iterator_next( &j, NULL )) ) {
				const void* img ;
				void* new = Group_compose( presentation, v, a ) ;
				if ( new && (img = Group_reduce( presentation, new )) ) {
					size_t img_ref = Graph_createVertex( self, 0, img, v_size ) ;
					/* Add a new edge decorated
					 * with a pointer to its
					 * generator. */
					Graph_addDecoratedEdge( self, v_ref, img_ref, &a, sizeof( a ) ) ;
				}
				free( new ) ;
			}
		}
		rc = 0 ;
	}			
	return rc ;
}
