/* -*- Mode: C -*- */
/*
 * policy.c - Defines an ADT representing an action policy.
 *
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>
#include <stdlib.h>
#include <string.h>

/* Local includes */
#include "agent.h"
#include "model.h"
#include "policy.h"
#include "state.h"
#include "transition.h"
#include "util/map.h"

/******************************************************************************
 * Transition
 ******************************************************************************/

Transition*
Transition_Create( State* state )
{
	Transition* new = calloc( 1, sizeof( Transition ) ) ;
	if ( new ) {
		new->state = state ;
		new->weight = 1 ;
	}
	return new ;
}

const State*
Transition_getState( const Transition* self )
{
	return self->state ;
}

/******************************************************************************
 * TransitionList
 ******************************************************************************/

int
TransitionList_add( TransitionList** self_ptr, const State* state )
{
	int index = -1 ;
	if ( !self_ptr ) {
		errno = EINVAL ;
	}
	else {
		TransitionList* self = *self_ptr ;
		if ( !self && !(self = List_Create( NULL )) ) {
			errno = ENOMEM ;
		}
		else {
			Transition t = {
				.state = state,
				.weight = 1,
			} ;
			*self_ptr = self ;
			index = List_append( *self_ptr, &t, sizeof( t ) ) ;
		}
	}
	return index ;
}

Transition*
TransitionList_getItem( const TransitionList* self, size_t i )
{
	return ( Transition* )List_getItem( ( List* )self, i ) ;
}

/******************************************************************************
 * Policy_Mapping
 ******************************************************************************/

typedef struct _policy_mapping {
	const State*	state ;
	const State*	action ;
	int32_t		value ;
} Policy_Mapping ;

int
Policy_Mapping_compare( const void* m1, const void* m2, size_t size )
{
	int order = -1 ;
	const Policy_Mapping* mapping1 = m1 ;
	const Policy_Mapping* mapping2 = m2 ;
	if ( mapping1 && mapping2 ) {
		order = mapping1->state > mapping2->state ? 1 : mapping1->state < mapping2->state ? -1 : 0 ;
	}
	return order ;
}

/******************************************************************************
 * Policy
 ******************************************************************************/

struct _policy {
	Agent*			agent ;
	List*			mappings ;
	/**
	 * @protected
	 * @brief Callout function used to apply the Policy.
	 * @memberof Policy
	 *
	 * @details This member holds a pointer to the callout that
	 * will be invoked to apply the current Policy on the
	 * specified State.
	 *
	 * @see Policy_apply
	 */
	Policy_applyProto*	apply ;
} ;

__attribute__((constructor))
void Policy_Initialize()
{
	srand( time(NULL) ) ;
}

/**
 * @protected
 * @brief Initialize the base Policy structure.
 * @memberof Policy
 */
void
Policy_init( void* policy, Agent* agent )
{
	Policy* self = policy ;
	if ( self ) {
		self->agent = agent ;
		Agent_setPolicy( agent, self ) ;
	}
}

uint8_t
Policy_evaluate( const Policy* self, const State* state, const State* action )
{
	Map_Iterator i ;
	Mapping* mapping ;
	int32_t num = 0 ;
	int32_t denom = 0 ;
	Model* model = State_getModel( state ) ;

	assert( self ) ;
	assert( state ) ;
	assert( action ) ;
	i = Model_getActions( model ) ;
	while ( (mapping = Map_Iterator_next( &i )) ) {
		const State* a = Mapping_getKey( mapping ) ;
		denom += ( 0x1 << Mapping_getWeight( mapping ) ) ;
		if ( a == action ) {
			num = ( 0x1 << Mapping_getWeight( mapping ) ) ;
		}
	}

	return ( 100 * num / denom ) ;
}

void
Policy_update( Policy* self, const State* state, const State* action )
{
	int rc = -1 ;
	if ( self ) {
		int i ;
		Policy_Mapping mapping = {
			.state = state,
			.action = action,
		} ;
		if ( (i = List_find( self->mappings, state )) < 0 ) {
			mapping.value = State_getValue( state ) ;
			rc = List_append( self->mappings, &mapping, sizeof( mapping ) ) ;
		}
		else {
			const Policy_Mapping* cur_mapping = List_getItem( self->mappings, i ) ;
			if ( cur_mapping ) {
				mapping.value = cur_mapping->value ;
			}
			rc = List_setItem( self->mappings, i, &mapping, sizeof( mapping ) ) ;
		}
	}
}

int32_t
Policy_estimateReward( const Policy* self, const State* current, const State* action, const State* next )
{
	Transition* t ;
	size_t i = 0 ;
	int32_t reward = 0 ;
	Model* model = State_getModel( current ) ;
	const TransitionList* transitions = Model_getTransitions( model, action ) ;
	while ( (t = TransitionList_getItem( transitions, i++ )) ) {
		if ( t->state == next ) {
			Agent_getValue( self->agent, t->state, &reward ) ;
		}
	}
	return reward ;
}

const State*
Policy_apply( const Policy* self, const State* state, int32_t epsilon )
{
	const State* a = NULL ;
	if ( self->apply == NULL ) {
		errno = ENOENT ;
	}
	else {
		a = self->apply( self, state ) ;
	}
	return a ;
}
