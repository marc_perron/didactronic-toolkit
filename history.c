/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <history.h>

struct _history_step {
	const State*	state ;
	const State*	action ;
	int32_t		reward ;
} ;

struct _history {
	List_EXTEND ;
	int32_t		return_val ;
} ;

History*
History_Create()
{
	void* history = calloc( 1, sizeof( History ) ) ;
	if ( history ) {
		List_init( history, NULL ) ;
	}
	return history ;
}

void
History_Delete( History** self )
{
	/* TODO:: Do any custom cleanup here. */
	List_Delete( (List**)self ) ;
}

int
History_addStep( History** self, const State* state, const State* macro_action, int32_t reward )
{
	int t = -1 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		History* history = *self ;
		if ( !history ) {
			errno = EINVAL ;
		}
		else {
			size_t list_size = List_getSize( (List*)history ) ;
			struct _history_step step = {
				.state	= state,
				.action = macro_action,
				.reward = reward
			} ;
			if ( List_append( (List*)history, &step, sizeof( step ) ) == 0 ) {
				history->return_val += reward ;
				t = list_size ;
			}
		}
	}
	return t ;
}

const State*
History_getAction( const History* self, size_t t )
{
	const State* a = NULL ;
	const struct _history_step* step = List_getItem( (List*)self, t ) ;
	if ( step ) {
		a = step->action ;
	}
	return a ;
}

int32_t
History_getReward( const History* self, size_t t )
{
	int32_t reward = 0 ;
	const struct _history_step* step = List_getItem( (List*)self, t ) ;
	if ( step ) {
		reward = step->reward ;
	}
	return reward ;
}

int32_t
History_getReturn( const History* self )
{
	int32_t retval = 0 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		retval = self->return_val ;
	}
	return retval ;
}

const State*
History_getState( const History* self, size_t t )
{
	const State* s = NULL ;
	const struct _history_step* step = List_getItem( (List*)self, t ) ;
	if ( step ) {
		s = step->state ;
	}
	return s ;
}
