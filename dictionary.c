/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>
#include <string.h>

/* Local inlcudes */
#include "util/dictionary.h"

/******************************************************************************
 * Dictionary_Entry
 ******************************************************************************/

struct _dictionary_entry {
	size_t		key_size ;
	size_t		value_size ;
	char		data[ 0 ] ;
} ;

/**
 * @private
 * @brief Create a new Dictionary Entry.
 * @memberof Dictionary_Entry
 *
 * @details This will create a new Dictionary_Entry as a key-value
 * pair which maps a key of arbitrary type to a value. The key size
 * and value size must be known a priori.
 *
 * @param[in] key A pointer to a buffer containing the key data.
 * @param[in] key_size The size, in bytes, of the key data.
 * @param[in] value A pointer to a buffer containing the value data.
 * @param[in] value_size The size, in bytes, of the value data.
 *
 * @return A pointer to a new Dictionary_Entry instance allocated on
 * the heap. The ownership of the memory returned by this function is
 * transferred to the caller who is responsible for deleting it. If
 * the requested dictionary entry could not be created for any reason,
 * this function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 */
static Dictionary_Entry*
Dictionary_Entry_Create( const void* key, size_t key_size, const void* value, size_t value_size )
{
	Dictionary_Entry* new = malloc( key_size + value_size + sizeof( Dictionary_Entry ) ) ;
	if ( !new ) {
		errno = ENOMEM ;
	}
	else {
		memcpy( new->data, key, key_size ) ;
		memcpy( new->data + key_size, value, value_size ) ;
		new->value_size = value_size ;
	}
	return new ;
}

const void*
Dictionary_Entry_getKey( const Dictionary_Entry* self )
{
	const void* key = NULL ;
	if ( self ) {
		key = self->data ;
	}
	return key ;
}

const void*
Dictionary_Entry_getValue( const Dictionary_Entry* self )
{
	const void* value = NULL ;
	if ( self ) {
		value = self->data + self->key_size ;
	}
	return value ;
}

static int32_t
Dictionary_Entry_hash( const Dictionary* self, const void* entry )
{
	const void* key = Dictionary_Entry_getKey( entry ) ;
	return HashMap_hash( self, key ) ;
}

Dictionary*
Dictionary_Create( HashMap_hashFunc* opt_hash, Set_Comparator* opt_compareEntries )
{
	Dictionary* new = HashMap_Create( opt_hash, opt_compareEntries ) ;
	return new ;
}

int
Dictionary_addEntry( Dictionary* self, const void* key, size_t key_size, const void* value, size_t value_size )
{
	int rc = -1 ;
	Dictionary_Entry* entry ;

	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( (entry = Dictionary_Entry_Create( key, key_size, value, value_size )) ) {
		rc = HashMap_addItem( ( HashMap* )self, entry, sizeof( *entry ) + key_size + value_size ) ;
	}
	return rc ;
}

Iterator
Dictionary_findEntries( const Dictionary* self, const void* key )
{
	Iterator i = 0 ;	
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		size_t key_hash = HashMap_hash( self, key ) ;
		i = HashMap_getItem( ( HashMap* )self, key_hash ) ;
	}
	return i ;	
}

const void*
Dictionary_NextEntry( Iterator* current_entry, size_t* opt_size )
{
	const void* value = NULL ;
	if ( current_entry ) {
		const Dictionary_Entry* entry = Iterator_next( current_entry, NULL ) ;
		if ( entry ) {
			const Dictionary_Entry* next_entry = Iterator_getItem( *current_entry, NULL ) ;
			if ( next_entry &&
			     ( entry->key_size != next_entry->key_size ||
			       memcmp( entry->data, next_entry->data, entry->key_size ) ) ) {
				*current_entry = 0 ;
			}
			value = &entry->data[ entry->key_size ] ;
		}
	}
	return value ;
}
