/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <string.h>

/* Local includes */
#include "util/list.h"

/******************************************************************************
 * List
 ******************************************************************************/

typedef union {	
	List			_public ;
	struct {
		Set		_base ;
		size_t		size ;
		const void**	item ;
	} ;
} ListImpl ;

void
List_init( List* self, Set_Comparator comparator )
{
	Set_init( ( Set* )self, comparator ) ;
}

List*
List_Create( Set_Comparator comparator )
{
	List* new = calloc( 1, sizeof( List ) ) ;
	List_init( new, comparator ) ;
	return new ;
}

void
List_clear( List* self )
{
	ListImpl* list = ( ListImpl* )self ;
	if ( self ) {
		Set_clear( &list->_base ) ;
		memset( list->item, 0, sizeof( const void* ) * list->size ) ;
	}
}

void
List_Delete( List** self )
{
	if ( self && *self ) {
		ListImpl* list = ( ListImpl* )*self ;
		free( list->item ) ;
		Set_Delete( ( Set** )self ) ;
	}
}

size_t
List_getSize( const List* self )
{
	return Set_getSize( ( const Set* )self ) ;
}

size_t
List_getCapacity( const List* self )
{
	assert( self ) ;
	return self->size ;
}

extern Iterator Iterator_Create( const void* ) ;

Iterator
List_getIterator( const List* self, size_t idx )
{
	Iterator i = 0 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( idx >= self->size ) {
		errno = ERANGE ;
	}
	else {
		const ListImpl* list = ( const ListImpl* )self ;
		i = Iterator_Create( list->item[ idx ] ) ;
	}
	return i ;
}

int
List_setItem( List* self, size_t idx, const void* new_item, size_t item_size )
{
	Iterator i ;
	int ret = -1 ;
	if ( !self || !new_item ) {
		errno = EINVAL ;
	}
	else if ( self->size <= idx ) {
		errno = ERANGE ;
	}
	else if ( (i = Set_insert( ( Set* )self, new_item, item_size )) ) {
		ListImpl* list = ( ListImpl* )self ;
		list->item[ idx ] = Iterator_getItem( i, NULL ) ;
		ret = 0 ;
	}
	return ret ;
}

extern const void* Iterator_toItem( const Iterator ) ;

const void*
List_getItem( const List* self, size_t idx )
{
	const void* item = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( idx >= self->size ) {
		errno = ERANGE ;
	}
	else {
		const ListImpl* list = ( const ListImpl* )self ;
		item = list->item[ idx ] ;
	}
	return item ;
}

int
List_swapItems( List* self, size_t i, size_t j )
{
	int ret = -1 ;
	ListImpl* list = ( ListImpl* )self ;
	
	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( i >= self->size || j >= self->size ) {
		errno = ERANGE ;
	}
	else {
		const void* tmp = list->item[ i ] ;
		list->item[ i ] = list->item[ j ] ;
		list->item[ j ] = tmp ;
		ret = 0 ;
	}
	return ret ;
}

int
List_insert( List* self, size_t i, const void* item, size_t item_size )
{
	int ret = -1 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		const void** items ;
		ListImpl* list = ( ListImpl* )self ;
		size_t num_items = i > self->size ? i : self->size ;

		if ( !(items = realloc( list->item, ( num_items+1 )*sizeof( void* const* ) )) ) {
			errno = ENOMEM ;
		}
		else {
			list->item = items ;
			if ( i < num_items ) {
				memmove( &list->item[ i+1 ], &list->item[ i ], ( num_items - i )*sizeof( Iterator ) ) ;
			}
			else {
				memset( &list->item[ self->size ], 0, sizeof( Iterator )*( i - self->size + 1 ) ) ;
			}
			
			self->size = num_items + 1 ;
			ret = List_setItem( self, i, item, item_size ) ;
		}
	}
	return ret ;
}

int
List_append( List* self, void* item, size_t item_size )
{
	int ret = -1 ;
	if ( !self || !item ) {
		errno = EINVAL ;
	}
        else {
		ret = List_insert( self, self->size, item, item_size ) ;
	}
	return ret ;
}

int
List_remove( List* self, size_t idx, void* removed_item, size_t buf_size )
{
	int ret = -1 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( self->size <= idx ) {
		errno = ERANGE ;
	}
	else {
		size_t num_items = self->size - 1 ;
		ListImpl* list = ( ListImpl* )self ;
		Iterator i = Iterator_Create( list->item[ idx ] ) ;
		Iterator_remove( &i, removed_item, buf_size ) ;

		if ( num_items > i ) {
			memmove( &list->item[ idx ], &list->item[ idx+1 ], num_items - idx ) ;
		}
		ret = 0 ;
	}
	return ret ;
}

int
List_find( const List* self, void* model )
{
	int found = -1 ;
	const ListImpl* list = ( const ListImpl* )self ;
	if ( self ) {
		size_t i ;
		for ( i = 0 ; !found && i < self->size ; i++ ) {
			size_t element_size ;
			if ( list->item[ i ] && !Set_compare( &list->_base, list->item[ i ], model, element_size ) ) {
				found = i ;
			}
		}
	}

	return found ;
}

/**
 * @protected
 * @brief Pad the list with a set number of items.
 * @memberof List
 *
 * @details This will grow the list index by the specified number if
 * items by prepending NULL Iterators thereto.
 *
 * @param[in] self A pointer to the List container to pad.
 * @param[in] num_items The number of items by which to pad the list.
 *
 * @retval 0 The List was successfully padded.
 * @retval -1 An error prevented the list from being padded; @e errno
 *	will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The List instance is invalid.
 * - ENOMEM: There was not enough memory to pad the List.
 */
int
List_pad( List* self, size_t num_items )
{
	const void** items ;
	int rc = -1 ;
	ListImpl* list = ( ListImpl* )self ;
	size_t size = self->size + num_items ;

	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( !(items = realloc( list->item, size*sizeof( Iterator ) )) ) {
		errno = ENOMEM ;
	}
	else {
		list->item = items ;
		memmove( &list->item[ num_items ], list->item, self->size*sizeof( const void* ) ) ;
		memset( &list->item[ 0 ], 0, num_items*sizeof( const void* ) ) ;
		self->size = size ;
	}

	return rc ;
}
