/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of types to model an Environment in a
 * reinforcement learning task
 */

#ifndef __MODEL_H__
#define __MODEL_H__

#include "state.h"
#include "transition.h"
#include "util/graph.h"

/**
 * @brief Prototype of transition probability rules.
 * @memberof Model
 *
 * @details Implementations of the Model interface must provide a
 * function of this type to calculate the probability of a specified
 * state transition.
 *
 * @see Model_transitionProbability
 *
 * @param[in] model A generic pointer to a Model implementation.
 * @param[in] i A pointer to the initial transition State.
 * @param[in] f A pointer to the final transition State.
 * @param[in] a A pointer to the Action which caused the transition.
 *
 * @return A probability value expressed as a fixed point number. In
 * the case of an error implementations of this function could return
 * a negative value; it is recommended to set @e errno accordingly.
 */
typedef int32_t Model_TransitionProbability_f( const void* model, const State* i, const State* f, const Action* a ) ;

/**
 * @brief Prototype of the reward function for a given Task.
 * @memberof Model
 *
 * @details Implementations of the Model interface must provide a
 * function of this type to calculate the reward associated with a
 * State transition.
 *
 * @see Model_getReward
 *
 * @param[in] model A generic pointer to a Model implementation.
 * @param[in] i A pointer to the initial transition State.
 * @param[in] f A pointer to the final transition State.
 * @param[in] a A pointer to the Action which caused the transition.
 *
 * @return An integer value (if positive) or cost (if negative)
 * associated with a State transition.
 */
typedef int32_t Model_GetReward_f( const void* model, const State* i, const State* f, const Action* a ) ;

/**
 * @class Model
 * @brief Class used to define a Model of a reinforcement learning
 *	Task.
 *
 * @details This class expresses a bi-partite graph which can be used
 * to define a model of an Environment. The nodes in the graph are
 * partitioned into the set of Actions and the set of States. The
 * graph is bi-partite insofar as there cannot be edges which connect
 * a State to another State.
 */
typedef struct _model {
	const Model_TransitionProbability_f*	transitionProbability ;
	const Model_GetReward_f*		getReward ;
	char					reserved[ sizeof( void* )*2 ] ;
} Model ;

/**
 * @brief Create a graph to Model an Environment.
 * @memberof Model
 *
 * @details This will create an initial Model of an unspecified
 * Environment and associated it with the specified State. Initially
 * the Model is an empty graph; more specifically a tree. The Model
 * instance must be augmented to properly express the possible state
 * transitions by adding Action-State edges to the graph.
 *
 * Each State node in a graph is also itself a graph. Therefore the
 * current Model instance may or may not express a sub-set of the
 * closed State space of an Environment.
 *
 * @param[in] state A pointer to a State instance that will be the
 *	root of the current Model.
 *
 * @return A pointer to a Model instance, created on the heap, which
 * consists initially of a single State. The ownership of the memory
 * returned by this function is retained to the callee and must @e not
 * be deleted by the caller. If the allocation of the Model instance
 * fails for any reason, this function returns @e NULL and @e errno
 * will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The State is invalid.
 * - EEXIST: A model already exists for the specified State.
 * - ENOMEM: There was not enough memory to allocate the Model.
 */
Model* Model_Create( State* state ) ;

/**
 * @private
 * @brief Delete a Model.
 * @memberof Model
 *
 * @details This will de-allocate the current Model, excluding its
 * downstream dependencies. The caller is responsible
 *
 * @param[in,out] self_ptr The address of a pointer to a Model
 *	instance to delete. The pointer will be set to @e NULL
 *	following a call to this function.
 */
void Model_Delete( Model** self_ptr ) ;

/**
 * @brief Retrieve the list of transitions following an Action.
 * @memberof Model
 *
 * @details This will retrieve a TransitionList which contains all the
 * known State transitions which may occur when taking the specified
 * Action in the modelled State.
 *
 * @param[in] self A pointer to a Model instance which models the
 *	current State.
 * @param[in] action A pointer to an immutable State instance
 *	expressing the sequence of actions which would theoretically
 *	be taken in the current State of the Model.
 *
 * @return A pointer to an immutable TransitionList containing all the
 * possible final States when taking the specified Action from the
 * current State. The ownership of the memory returned by this
 * function is retained by the callee and must not be deleted by the
 * caller.
 */
const TransitionList* Model_getTransitions( const Model* self, const State* action ) ;

/**
 * @brief Add a transition to the current Model.
 * @memberof Model
 *
 * @details This will add an Action-State transition to the current
 * Model which expresses a possibility of a final State if the
 * specified Action is taken in the currently modelled initial
 * State. This will effectively add two nodes to the current Model
 * graph, one Action node as the direct neighbour of the current
 * State, and a State as a neighbour of the Action. If the specified
 * Action is already a neighbour of the current node, that node will
 * be reused rather than adding a new one.
 *
 * @param[in] self A pointer to the current State Model instance.
 * @param[in] action A pointer to a possible Action that can be taken
 *	in the currently modelled State.
 * @param[in] state A possible final State instance resulting from
 *	taking the specified Action in the currently modelled State.
 *
 * @return An error code indicating whether or not the operation was
 * successful.
 *
 * @retval 0 The transition was successfully added to the Model.
 * @retval <0 An error occured while adding the transition, errno will
 *	be set accordingly.
 */
int Model_addTransition( Model* self, const Action* action, const State* state ) ;

/**
 * @brief Get an Iterator over the possible Action in the current
 *	Model.
 * @memberof Model
 *
 * @details This will retrieve an Iterator that can be used to walk
 * all of the possible Action in the currently Modelled State, and to
 * retrieve the possible Transitions resulting from executing the
 * Action.
 *
 * @param[in] self A pointer to an immutable Model of the current State.
 *
 * @return A Map_Iterator instance whose key is a valid Action in the
 * currently modelled State, and whose value is the possible
 * Transitions resulting from executing the Action. Use the
 * Map_Iterator functions to walk the neighbourhood of the current
 * State with the returned Iterator. If the current Model is invalid,
 * the Iterator will be @e NULL and errno will be set to EINVAL.
 */
Map_Iterator Model_getActions( Model* self ) ;

/**
 * @brief Determine the size of a node's neighbourhood.
 * @memberof Model
 *
 * @details This will report the number of neighbours adjacent to the
 * currently modelled State. Only Action instances can be neighbours
 * of a State.
 *
 * @param[in] self A pointer to an immutable Model instance.
 *
 * @return The number of Action instances that are adjacent to the
 * currently modelled State instance. If the Model is invalid, this
 * function will return a negative value and errno will be set to
 * @e EINVAL.
 */
int Model_numNeighbours( const Model* self ) ;

/**
 * @brief Get the weight associated with an Action edge.
 * @memberof Model
 *
 * @details This function retrieves the weight associated with the
 * edge connecting the currently modelled State with the specified
 * Action node. If the specified Action is not adjacent to the
 * currently modelled State, the reported weight will be
 * zero. However, since zero may be a valid weight, this return value
 * cannot be relied upon for determining the adjacency of States and
 * Actions in the Model.
 *
 * @param[in] self A pointer to a Model of the current State.
 * @param[in] action A pointer to the Action instance, adjacent to the
 *	currently modelled State, whose weight will be reported.
 *
 * @return A 32-bit signed integer weight of the edge connecting the
 * currently modelled State and an adjacent Action. If no such edge
 * exists, this function returns zero.
 */
int32_t Model_getWeight( const Model* self, const Action* action ) ;

/**
 * @brief Set the weight associated with an Action edge.
 * @memberof Model
 *
 * @details This function will assign a weight to the edge connecting
 * the currently modelled State with the specified Action adjacent to
 * it. If the Action is not adjacent to the State, not assignment will
 * be made.
 *
 * @param[in] self A pointer to a Model of the current State.
 * @param[in] action A pointer to an Action instance, adjacent to the
 *	currently modelled State, whose weight will be reported.
 * @param[in] weight A signed 32-bit integer expressing the weight
 *	associated with the edge connecting thee Modelled State with
 *	the specified adjacent Action.
 */
void Model_setWeight( Model* self, const Action* action, int32_t weight ) ;

/**
 * @brief Calculate a state transition probability.
 * @memberof Model
 *
 * @details This will query the current model to determine the
 * probability of transitioning from a State @a i, to State @a f under
 * the control specified by Action @a a.
 *
 * @param[in] self A pointer to the current Model embodiment.
 * @param[in] i The initial State in the modelled transition.
 * @param[in] f The final State in the modelled transition.
 * @param[in] a The Action expressing the control under which the
 *	transition will be evaluated.
 *
 * @return The probability of the specified State transition, under
 * control @a a, expressed as a signed 32-bit integer. The probability
 * may be encoded as a fixed-point value. The probability @e must be a
 * positive value, if an error occurs in the calculate of this
 * probability, a negative value (-1) is returned and @e errno will be
 * set accordingly.
 *
 * @par Errors
 * - EINVAL: The model in invalid.
 * - ENOSYS: The model does not provide a rule for calculating
 *   transition probabilities.
 */
int32_t Model_transitionProbability( const Model* self, const State* i, const State* f, const Action* a ) ;

/**
 * @brief Determine the reward of a State transition.
 * @memberof Model
 *
 * @details This will determine the reward (if positive) or cost (if
 * negative) associated with a State transition from @a i to @a f
 * under the control specified by Action @a a.
 *
 * @param[in] self A pointer to the current Model embodiment.
 * @param[in] i The initial State in the modelled transition.
 * @param[in] f The final State in the modelled transition.
 * @param[in] a The Action expressing the control under which the
 *	transition will be evaluated.
 *
 * @return An integer reward or cost value associated with the State
 * transition. If the model does not provide a reward function, the
 * reward will always be zero.
 */
int32_t Model_getReward( const Model* self, const State* i, const State* f, const Action* a ) ;

/******************************************************************************
 * CayleyGraph::Model
 ******************************************************************************/

/**
 * @class CayleyGraph_Model
 * @extends Model
 * @brief Model based on a Cayley Graph.
 *
 * @details Implementation of a reinforcement learning model which
 * uses a Cayley Graph to represent the task dynamics.
 */
typedef struct _cayley_graph_model CayleyGraph_Model ;

/**
 * @brief Create a Model from a Cayley graph.
 * @memberof CayleyGraph_Model
 *
 * @details This will create a Model of a reinforcement learning
 * problem which uses a Cayley Graph to calculate transition
 * probabilities.
 *
 * @param[in] graph A Graph presentation of a group which models the
 *	Task to learn.
 * @param[in] getReward Function used to evaluate the cost or reward
 *	of a State in the Model.
 *
 * @return A pointer to an abstract Model based on a Cayley Graph. The
 * ownership of the memory returned by this function is transferred to
 * the caller who is responsible for deleting it.
 */
Model* CayleyGraph_Model_Create( Graph* graph, Model_GetReward_f* getReward ) ;

#endif	/* __MODEL_H__ */
