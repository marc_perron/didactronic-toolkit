/* -*- Mode: C -*- */
/*
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of the base Policy class.
 */

#ifndef __POLICY_H__
#define __POLICY_H__

#include "action.h"
#include "state.h"

/******************************************************************************
 * Policy
 ******************************************************************************/

/**
 * @class Policy
 * @brief Opaque type representing an agent's Policy.
 *
 * @details This class is used to model a policy for an arbitrary
 * reinforcement learning Task. A Policy is a mapping of a State and
 * an Action to the probability of taking the Action in a given
 * State. An instance of this class expresses a type of state-action
 * Policy, however, an actual embodiement of the policy requires an
 * associated Agent to act on the Policy.
 */
typedef struct _policy Policy ;

/**
 * @brief Global macro used to extend the Policy class.
 *
 * @details Declare this macro as the first member in the body of a
 * struct definition to mark the structure as a specialization of the
 * Policy class. This will ensure that the derived type's memory is
 * correctly aligned for it to be used as a Policy instance.
 *
 * The following example illustrates how to declare a GreedyPolicy
 * class as a specialization of Policy:
 * @code{.c}
 * typedef _greedy_policy {
 *	Policy_EXTEND ;
 *	Environment*	env ;
 * } GreedyPolicy ;
 * @endcode
 */
#define Policy_EXTEND							\
	char			policy_reserved[ sizeof( void* )*2 ] ;	\
	Policy_applyProto*	apply

/**
 * @memberof Policy
 * @brief Evaluate the current Policy instance.
 *
 * @details This function evaluates the current Policy,
 * @f$\pi(s,a)@f$, calculating the probability of taking an @a action
 * in a specified @a state.  This function is used to define a greedy
 * Policy; the action with the highest probability will be selected as
 * the next Action to take in the specified @a state. Policy
 * evaluation can be expressed mathematically as:
 *
 * @f[
 * \pi(s,a) = P(a|s)
 * @f]
 *
 * @param[in] self A pointer to a Policy instance.
 * @param[in] state A pointer to a State instance to evaluate.
 * @param[in] action A pointer to a State instance expressing the
 *	action, or macro-action, to evaluate.
 *
 * @return A 16-bit unsigned integer expressing the probability of
 * applying the @a action in the specified @a state. The probability
 * is expressed in fixed-point notation, quantized to 0.01.
 */
uint8_t Policy_evaluate( const Policy* self, const State* state, const State* action ) ;

/**
 * @brief Prototype of functions which apply a Policy to a State.
 * @memberof Policy
 *
 * @details This type describes the prototype expected of functions
 * which apply the current Policy to a given State in order to obtain
 * the Action to take when following the Policy. Derived Policy type
 * must provide an implementation of this function, associating it
 * with the derived Policy instance.
 *
 * @see Policy_apply
 *
 * @param[in] self A pointer to an immutable Policy instance to apply.
 * @param[in] s A pointer to a State instance to for which an Action
 *	will be selected by the Policy.
 *
 * @return A pointer to a State instance expressing the sequence of
 * actions to take in the specified State when following the current
 * Policy.
 */
typedef const State* Policy_applyProto( const Policy* self, const State* s ) ;

/**
 * @brief Apply the Policy to the current state.
 * @memberof Policy
 *
 * @details This will select an Action to take in the specified State,
 * following the current @f$\epsilon@f$-greedy policy. This
 * corresponds with the term @f$\pi(s)@f$ in the Bellman equation. If
 * the Task associated with the current Policy provides a custom
 * function for determining the next action, this function will be
 * used to determine the next Action under the greedy policy. However,
 * the policy may also randomly sample an arbitrary Action with
 * probability @p epsilon.
 *
 * @param[in] self A pointer to the current Policy instance
 *	(@f$\pi@f$).
 * @param[in] state A pointer to the current State (@f$s@f$).
 * @param[in] epsilon The probability with which the current Policy
 *	will sample randomly from the set of valid Actions as opposed
 *	to following a greedy policy. This probability will be
 *	expressed as a fixed-point value quantized to 0.01 (i.e. 1 =
 *	0.01 and 100 = 1.0). By setting this argument to zero, the
 *	policy will always choose the greedy action.
 *
 * @return A pointer to an Action to take in the specified State when
 * following the current @f$\epsilon@f$-greedy Policy. The ownership
 * of the memory returned by this function is retained by the callee
 * and must not be deleted by the caller.
 */
const State* Policy_apply( const Policy* self, const State* state, int32_t epsilon ) ;

/**
 * @brief Update the policy for the specified State.
 * @memberof Policy
 *
 * @details This function is used by the Policy improvement algorithms
 * to update the greedy Policy to select the specified @p action in
 * the specified @p state.
 *
 * @param[in,out] self A pointer to a Policy instance, @f$\pi@f$, to
 *	update.
 * @param[in] state A pointer to a State instance for which the Policy
 *	will be updated.
 * @param[in] action The State expressing the sequence of actions to
 *	take, when applying the current greedy Policy, in @p state.
 */
void Policy_update( Policy* self, const State* state, const State* action ) ;

#endif	/* __POLICY_H__ */
