/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of a StreamPolicy class.
 */

#ifndef __STREAM_POLICY_H__
#define __STREAM_POLICY_H__

/* Local includes */
#include "environment.h"
#include "policy.h"

/* POSIX includes */
#include <stdio.h>

/******************************************************************************
 * StreamPolicy
 ******************************************************************************/

/**
 * @class StreamPolicy
 * @brief Opaque policy which queries an input stream for actions.
 * @extends Policy
 *
 * @details This class provides a mechanism for obtaining actions from
 * an input stream. The class can be used to implement a policy which
 * reads actions from the standard input (i.e. manually entered by a
 * user) and returns this action as the next Action.
 */
typedef struct _stream_policy StreamPolicy ;

/**
 * @brief Prototype of functions which read actions from a stream.
 * @memberof StreamPolicy
 *
 * @details This type describes the prototype expected of functions
 * that are provided to the StreamPolicy which are used to read
 * Actions from the associated Stream. A function of this type will be
 * invoked each time the StreamPolicy_apply() function is called.
 *
 * @see StreamPolicy_apply
 *
 * @param[in] env A pointer to an immutable Environment in which the
 *	Policy is being applied.
 * @param[in] s A pointer to a State instance expressing the current
 *	State of the Environment.
 * @param[in] stream A pointer to the input stream from which the
 *	Action will be read.
 *
 * @return This function must return a pointer to an Action instance
 * that was read from the associated input stream.
 */
typedef const State* StreamPolicy_readAction_f( const Environment* env, const State* s, FILE* stream ) ;

/**
 * @brief Create a new StreamPolicy instance.
 * @memberof StreamPolicy
 *
 * @details This will allocate a new StreamPolicy instance which will
 * operate in the specified Environment, using the @p readAction
 * function to read action from an @p input_stream.
 *
 * @param[in] environment A pointer to the Environment on which the
 *	current Policy is defined. This is required to ensure that
 *	actions read from the stream are valid.
 * @param[in] readAction A pointer to a callback function that will be
 *	invoked to read an Action from the input stream.
 * @param[in] input_stream A pointer to a FILE structure which
 *	encapsulates the input stream from which Actions will be read.
 *
 * @return A new Policy instance, created on the heap, which expresses
 * a StreamPolicy that will read actionsfrom the associatedinput
 * stream. The ownership of the memory returned by this function is
 * transferred to the caller who is responsible for deleting it.
 */
Policy* StreamPolicy_Create( const Environment* environment, StreamPolicy_readAction_f* readAction, FILE* input_stream ) ;

/**
 * @brief Apply the StreamPolicy to a specified State.
 * @memberof StreamPolicy
 *
 * @details This function will read the Action to take in the current
 * State from the input stream associated with the Policy. How the
 * Action is read is determined by the @e readAction function provided
 * to the Policy when it was created. This function will be invoked
 * whenever the Policy is applied.
 *
 * @see Policy_apply
 *
 * @param[in] self A pointer to an immutable StreamPolicy instance.
 * @param[in] s A pointer to the State to which the Policy will be
 *	applied.
 *
 * @return A pointer to a State instance expressing the action
 * sequence which was read from the input stream. The ownership of the
 * memory returned by this function is retained by the callee and must
 * not be deleted by the caller.
 */
const State* StreamPolicy_apply( const Policy* self, const State* s ) ;
#endif	/* __STREAM_POLICY_H__ */
