/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, 2016 Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of an Environment in a reinforcement learning
 *	task.
 *
 * @mainpage

 * The Didactronic Toolkit is free software which provides a software
 * framework that can be used to implement reinforcement learning
 * algorithms to solve a particular task. The toolkit is provided as a
 * run-time library and a C API. The objects exposed by the framework
 * are built upon the notion of their corresponding algebraic
 * structures. Therefore the framework closely models the abstrac
 * albebra formulation of reinforcement learning tasks.
 */

#ifndef __ENVIRONMENT_H__
#define __ENVIRONMENT_H__

#include "action.h"
#include "model.h"
#include "policy.h"
#include "state.h"
#include "transition.h"

/**
 * @brief Prototype of functors used to define Environment morphisms.
 * @memberof Environment
 *
 * @brief This type describes the prototype expected of functors that
 * are used to define the morphisms of an Environment. Functor
 * implementations are expected to determine the algebraic product of
 * the input states (in terms of the Environment monoid), creating and
 * allocating a new State instance expressing the result of the
 * product.
 *
 * @param[in] self A generic pointer to an Environment instance.
 * @param[in] s1 The initial state of the application functor.
 * @param[in] s2 The state to apply to the initial state.
 *
 * @return A State instance expressing the product of the input
 * states. The ownership of the memory returned by this function is
 * retained bythe callee who is responsible for deleting it; it is
 * part of the Environment's state set. If the product of the input
 * states is undefined, then the functor must return @c NULL.
 */
typedef const State* Environment_Apply_f( void* self, const State* s1, const State* s2 ) ;

/**
 * @brief Prototype of Environment destructors.
 * @memberof Environment
 *
 * @brief This type describes the prototype expected of functors that
 * will be used to cleanup derived Environment types. Functor
 * implementations are responsible for cleaning up any resources that
 * were allocated to the Environment instance. However, the
 * Environment instance itself must not be de-allocated by the functor
 * or undefined behaviour will result.
 *
 * @param[in] self A generic pointer to an Environment instance to
 *	clean up. This data type should be cast to the appropriate
 *	derived type to access derived members.
 */
typedef void Environment_Cleanup_f( const void* self ) ;

/**
 * @class Environment
 * @brief Class expressing a reinforcement learning Environment.
 *
 * @details The environment on which a task is defined is generalized
 * as a structure comprised of a set of objects -- the states $S$ --
 * and action morphisms that describe state transitions. The state set
 * can either be defined exhaustively (i.e. each state is enumerated
 * explicitly) or using a generator set consisting of the states which
 * can be reached from the initial configuration by a single action.
 *
 * The morphisms are defined by a functor which combines two states to
 * produce a third state. A single action will be expressed as the
 * application of a state from the generators of $S$ to any other
 * state in the environment.
 *
 * Instances of the Environment class will also have information to
 * track the evolution of the environment as agents operate
 * thereupon. The required information includes the initial state of
 * the environment (generally constant), as well as the current state
 * of the environment (which results from applying controls
 * thereupon).
 */
typedef struct _environment {
	/**
	 * @brief An immutable pointer to the Environment's initial
	 * state.
	 * @memberof Environment
	 */
	const State*			initial ;

	/**
	 * @brief A pointer to an immutable State expressing Environment's
	 * current State.
	 * @memberof Environment
	 */
	const State*			current ;

	/**
	 * @protected
	 * @brief The generator set of the Environment.
	 * @memberof Environment
	 */
	const StateSet*			states ;

	/**
	 * @brief Compose two states in the Environment.
	 * @memberof Environment
	 */
	Environment_Apply_f*		apply ;

	/**
	 * @protected
	 * @brief Cleanup the current Environment.
	 * @memberof Environment
	 *
	 * @details This functor will be called when the Environment
	 * instance is deleted to ensure that all resources are
	 * properly cleaned up.
	 */
	const Environment_Cleanup_f*	cleanup ;
	char				reserved[ sizeof( void* )*6 ] ;
} Environment ;

/**
 * @brief Prototype of action evaluator functions.
 * @memberof Environment
 *
 * @details This type defines the prototype required for custom
 * implementations of the Action evaluator callout which is invoked by
 * to determine if an Action is valid in a given State.
 *
 * @param[in] self A pointer to an immutable Environment instance.
 * @param[in] state A pointer to the initial State in which an Action
 *	will be validated.
 * @param[in] action A pointer to the Action instance to validate.
 *
 * @return A flag indicating whether it is valid to the the specified
 * @a action in the current @a state (@e true), or not (@e false).
 */
typedef bool Environment_isActionValidProto( const Environment* self,
					     const State* state,
					     const Action* action ) ;

/**
 * @brief Prototype of functions which calculate transition
 *	probabilities.
 * @memberof Environment
 *
 * @details This type defines the prototype required for function that
 * will be used as callouts to determine the probability of getting to
 * the @p next State from the @p current State by taking the specified
 * Action. This corresponds with the @f$\mathscr{P}_{ss'}^{a}@f$ term
 * of the Bellman equation. Mathematically, its value is expressed as:
 *
 * @f[
 * \mathscr{P}^{a}_{ss'} = {Pr}\{s_{t+1} = s' \mid s_t = s, a_t = a \}
 * @f]
 *
 * Derived implementations of the Environment class @e must provide an
 * implementation of this function to properly define a model of the
 * Environment.
 *
 * @param[in] self A pointer to an immutable Environment instance.
 * @param[in] current A pointer to an immutable State instance
 *	expressing the current State of a Task.
 * @param[in] action A pointer to an immutable State instance
 *	expressing the sequence of one or more actions to take in the
 *	current State to solve a Task.
 * @param[in] next A pointer to an immutable State instance expressing
 *	a candidate State that may follow the current one when taking
 *	the specified Action.
 *
 * @return A fixed point value expressing the probability that taking
 * the specified Action from the current State will move the task into
 * the specified @p next State. The probability range is [0-100]
 * (i.e. 0 to 1.0). If the probability cannot be calculated for any
 * reason, this function @e must returns a negative value and errno
 * must be set accordingly.
 *
 * @par Expected Errors:
 * - EINVAL: The Environment is invalid (@e NULL), one of the States
 *   is not valid in the current Environment, or the Action is not
 *   valid in the current State.
 */
typedef int32_t Environment_transitionProbabilityProto( const Environment* self,
							const State* state,
							const State* action,
							const State* next ) ;

/**
 * @brief Prototype of functions which execute an Action.
 * @memberof Environment
 *
 * @details This type defines the prototype required for function
 * implementations which provide the logic for executing the specified
 * @p action in the current State of the Environment.
 *
 * @param[in,out] self A pointer to an Environment instance.
 * @param[in] action A pointer to an Action instance that will be
 *	executed in the current State of the Environment.
 *
 * @return A pointer to the State that results from executing the
 * specified Action in the current State. If the Action could not be
 * executed, the function must return @e NULL and set @e errno
 * accordingly.
 *
 * @see Environment_execute
 */
typedef const State* Environment_executeProto( Environment* self, const Action* action ) ;

/**
 * @brief Prototype for callouts to assert terminal States.
 * @memberof Environment
 *
 * @details This type defines the prototype expected of callout
 * functions which assert whether or not a given State is a terminal
 * State in the current Environment implementation. A function of this
 * type @e must be associated with each Environment instance to
 * determine the terminal States.
 *
 * @param[in] self A pointer to the current Environment instance.
 * @param[in] state A pointer to an immutable State instance.
 *
 * @retval true The State is a terminal State.
 * @retval false The State is @e not a terminal State.
 */
typedef bool Environment_isTerminalProto( const Environment* self, const State* state ) ;

/**
 * @brief Initialize a pre-allocated Environment instance.
 * @memberof Environment
 *
 * @brief This will initialize the specified memory location as an
 * Environment instance. This function can be used to initialize the
 * base of derived Environment types (before initializing derived
 * members).
 *
 * @param[in,out] self A pointer to a memory location that will be
 *	initialized as an Environment instance. The referenced memory
 *	must be large enough to contain the Environment instance.
 * @param[in] states A pointer to a StateSet instance which contains
 *	all the valid States for the new Environment. The ownership of
 *	the memory referenced by this argument is transferred to the
 *	Environment instance and will be deleted by its destructor.
 * @param[in] isTerminal Callback which asserts whether or not a State
 *	is terminal.
 * @param[in] validator A pointer to an implementation of an Action
 *	validator for a derived Environment type. If this argument is
 *	@e NULL, a default validator will be used. The validator
 *	implementation will be used to determine whether or not a
 *	given action is valid in the current State based on the
 *	mechanics of the specific Environment.
 * @param[in] evaluator An optional pointer to a function that will be
 *	used to calculate the probability of reaching a specified
 *	state when taking a given action from the current State. If
 *	this argument is @e NULL, a default implementation will be
 *	used.
 *
 * @see Environment_transitionProbability
 * @see Environment_isActionValid
 *
 * @see Environment_Delete
 */
void Environment_init( void* self,
		       StateSet* states,
		       Environment_isTerminalProto* isTerminal,
		       Environment_isActionValidProto* validator,
		       Environment_transitionProbabilityProto* evaluator ) ;

/**
 * @brief Create an Environment instance on the heap.
 * @memberof Environment
 *
 * @brief This will create a new Environment instance on the heap,
 * associating with it the specified StateSet.
 *
 * @param[in] states A pointer to a StateSet instance which contains
 *	all the possible States in the Environement. The ownership of
 *	the referenced memory is transferred to the Environment
 *	instance and will be deleted by the Environment_Delete()
 *	function.
 *
 * @return A pointer to a new Environment instance, created on the
 * heap. The ownership of the memory returned by this function is
 * transferred to the caller who is responsible for ensuring it is
 * deleted.
 *
 * @see Environment_Delete
 */
Environment* Environment_Create( StateSet* states ) ;

/**
 * @brief Delete the specified Environment instance.
 * @memberof Environment
 *
 * @details This will delete the specified Environment and its
 * associated model (including all the State instances contained
 * therein). If a destructor was provided, this destructor will be
 * called before the Environment is deleted.
 *
 * @param[in,out] environment The address of a pointer to an
 *	Environment instance that will be deleted. The pointer
 *	addressed by this argument will be set to @c NULL.
 */
void Environment_Delete( Environment** environment ) ;

/**
 * @brief Get the current State of the Environment.
 * @memberof Environment
 *
 * @details This will retrieve the current State of the
 * Environment. The State may change following the execution of
 * Actions. When the environment is first created, the current State
 * should be the same as the initial State.
 *
 * @see Environment_execute
 * @see Environment_getInitialState
 *
 * @param[in] self A pointer to an immutable Environment instance.
 *
 * @return A pointer to an immutable State instance expressing the
 * current State of the Environment. If this current State is not
 * known, this function returns @c NULL.
 */
const State* Environment_getCurrentState( const Environment* self ) ;

/**
 * @brief Return the initial State of the Environment.
 * @memberof Environment
 *
 * @details Use this function to determine the State of the current
 * Environment instance before any Action was executed on it.
 *
 * @param[in] self A pointer to an immutable Environment instance.
 *
 * @return A pointer to an immutable State instance expressing the
 * initial State of the current Environment. It is an error for the
 * initial State to be @c NULL.
 */
const State* Environment_getInitialState( const Environment* self ) ;

/**
 * @brief Retrieve an arbitrary State from the Environment.
 * @memberof Environment
 *
 * @details This will retrieve the State corresponding with the
 * specified @a id from the current Environment.
 *
 * @param[in] self A pointer to an immutable Environment instance.
 * @param[in] id The ID of the State to retrieve from the Environment.
 *
 * @return An immutable pointer to the Environment's State
 * corresponding with the specified @a id. If no such state exists, or
 * an error occurs, this function returns @c NULL and sets @e errno
 * accordingly.
 *
 * @par Errors
 * - EINVAL: The environment instance is invalid.
 * - ERANGE: No state corresponding with the id was found.
 */
const State* Environment_getState( const Environment* self, StateID id ) ;

/**
 * @brief Get the set of all States in the environment.
 * @memberof Environment
 *
 * @details This function can be used to retrieve the set of all
 * possible States that are defined by the current Environment.
 *
 * @param[in] self A pointer to an Environment instance.
 *
 * @return A pointer to a StateSet instance which contains the
 * complete set of State instances associated with the current
 * Environment. The ownership of the memory returned by this function
 * is retained by the callee and must not be deleted by the caller.
 */
StateSet* Environment_getStates( const Environment* self ) ;

/**
 * @brief Determine if an action is valid.
 * @memberof Environment
 *
 * @details This will validate whether or not an @p action is valid in
 * the specified @p state. An action is valid if, in the current
 * Environment, it will yield a valid State if applied to the current
 * @p state.
 *
 * @param[in] self A pointer to the current Environment.
 * @param[in] state A pointer to the State instance expressing the
 *	current state.
 * @param[in] action A pointer to an Action instance expressing the
 *	action to apply to the current @p state.
 *
 * @return A flag indicating whether or not an @a action is valid (@e
 * true) or invalid (@e false) in the current State.
 */
bool Environment_isActionValid( const Environment* self, const State* state, const Action* action ) ;

/**
 * @brief Apply a sequence of actions to the sepcified State.
 * @memberof Environment
 *
 * @details Given an initial @p state, this function will retrive a
 * list of possible states that can follow from applying the specified
 * sequence of @p actions. In a deterministic domain, the list will
 * contain a single State. However, stochastic environments may have
 * several possible final states.
 *
 * This function takes advantage of the graph organization of the
 * State-Action sets. The edge corresponding with the specified
 * @p actions will be followed, starting from the @p initial State. The
 * outgoing edges from the Action node will compose list of possible
 * target States.
 *
 * @param[in] self A pointer to an Environment instance.
 * @param[in] initial A pointer to an initial State.
 * @param[in] action A pointer to a State instance which expresses the
 *	sequence of actions to be applied to the initial State.
 *
 * @return A pointer to a TransitionList expressing the set of
 * weighted edges connecting the Action node in the @p initial State's
 * neighbourhood, to all the possible result State instances. The
 * ownership of the memory returned by this function is retained by
 * the callee and must not be deleted by the caller.
 */
TransitionList* Environment_applyAction( const Environment* self, const State* initial, const State* action ) ;

/**
 * @brief Execute the specified Action in the current State.
 * @memberof Environment
 *
 * @details This function changes the State of the current Environment
 * instance by taking the specified Action in the current
 * State. Derived Environment types must provide an implementation for
 * this function. The result of the function is the State that follows
 * the current when when the specified Action is taken.
 *
 * @param[in,out] self A pointer to an Environment instance that will
 *	be updated by taking the specified Action in the current
 *	State.
 * @param[in] action A pointer to an instance of Action that will be
 *	taken in the current State.
 *
 * @return A pointer to the State of the environment following the
 * execution of the specified Action from the previous State. If the
 * Action could not be taken for any reason, this function will return
 * @e NULL and set @e errno accordingly.
 *
 * @par Errors:
 * - EINVAL: The specific Action is not valid in the current State.
 * - ENOENT: The Environment instance is invalid.
 */
const State* Environment_execute( Environment* self, const Action* action ) ;

/**
 * @brief Determine the probability that taking an Action will yield a
 *	given State.
 * @memberof Environment
 *
 * @details This function will determine the probability of getting to
 * the @p next State, @f$s'@f$, from the @p current State, @f$s@f$, by
 * taking the specified Action. This corresponds with the
 * @f$\mathscr{P}_{ss'}^{a}@f$ term of the Bellman
 * equation. Mathematically, its value is expressed as:
 *
 * @f[
 * \mathscr{P}^{a}_{ss'} = {Pr}\{s_{t+1} = s' \mid s_t = s, a_t = a \}
 * @f]
 *
 * Derived Environment types are expected to provide implementations
 * of this function. If none are provided, the default implementation
 * of this function simply calculates the probability from a uniform
 * distribution of the possible States that follow the current State
 * when taking the specified Action (if the Environment provides a
 * model). Derived types can provide implementations of this function
 * to perform a more intelligent calculation.
 *
 * @param[in] self A pointer to an immutable Environment instance.
 * @param[in] current A pointer to an immutable State instance
 *	expressing the current State of a Task.
 * @param[in] action A pointer to an immutable State instance
 *	expressing the sequence of one or more actions to take in the
 *	current State to solve a Task.
 * @param[in] next A pointer to an immutable State instance expressing
 *	a candidate State that may follow the current one when taking
 *	the specified Action.
 *
 * @return A fixed point value expressing the probability that taking
 * the specified Action from the current State will move the task into
 * the specified @p next State. The probability range is [0-100]
 * (i.e. 0 to 1.0). If the probability cannot be calculated for any
 * reason, this function returns a negative value and errno will be
 * set accordingly.
 *
 * @par Errors:
 * - EINVAL: The Environment is invalid (@e NULL), one of the States
 *   is not valid in the current Environment, or the Action is not
 *   valid in the current State.
 * - ENOSYS: No implementation of the evaluator was defined for the
 *   current Environment class.
 */
int32_t Environment_transitionProbability( const Environment* self,
					   const State* current,
					   const State* action,
					   const State* next ) ;

/**
 * @brief Update the Environment by executing a plan.
 * @memberof Environment
 *
 * @details This function will apply a sequence of actions to the
 * current State of an Environment. The sequence of actions is
 * abstracted as a State which expresses the plan as the history from
 * the initial state to that given. This uses the Environment's
 * operation to determine the result of applying the plan to its
 * current State.
 *
 * @param[in] self A pointer to an Environment to update with the
 *	given plan. The current State of the Environment will be
 *	updated to reflect the effect of the plan.
 * @param[in] plan The plan to execute in the given Environment.
 *
 * @return The State of the Environment following the execution of the
 * specified plan. If the plan cannot be executed for any reason, this
 * function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The environment or plan are invalid.
 * - ENOSYS: There is no operation associated with the Environment.
 */
const State* Environment_apply( Environment* self, const State* plan ) ;

#endif	/* __ENVIRONMENT_H__ */
