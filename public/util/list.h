/* -*- Mode: C -*- */
/*
 * list.h - Declares a List ADT.
 *
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef __LIST_H__
#define __LIST_H__

/* POSIX includes */
#include <stdlib.h>

/* Local includes */
#include "util/set.h"

/**
 * @brief Macro used to extend the List class.
 *
 * @details Use this macro to declare classes that derive from the
 * List class. This macro must be the first item in the structure
 * declaration, followed by derived attributes. This macro ensures
 * that the memory layout of the derived structure corresponds with
 * the base List class.
 */
#define List_EXTEND	List	__List__

/**
 * @brief Macro which declares a specialization of a generic List
 * container.
 *
 * @details Use this macro to declare a specialized List type and its
 * associated operations as a container of items of the specified
 * @p ItemType. This is a convenience macro that should be used in
 * cases where the derived type does not extend the List.
 */
#define List_DECLARE(ListType,ItemType)					\
	typedef List ListType ;						\
	int ListType##_append( ListType*, ItemType ) ;			\
	int ListType##_insert( ListType*, size_t, ItemType ) ;		\
	int ListType##_remove( ListType*, size_t, ItemType* ) ;		\
	int ListType##_setItem( ListType*, size_t, ItemType ) ;		\
	int ListType##_getItem( const ListType*, size_t, ItemType* )

/**
 * @brief Macro which provides implementations for derived List
 * functions.
 *
 * @details Use this macro to define specialized List operations for a
 * specific element type. This macro should be used in conjunection
 * with the List_DECLARE macro to provide specialized implementations
 * of the generic List class. This will define the following member
 * functions which overload their base List implementations:
 *
 * - ListType* ListType_Create()
 * - int ListType_append( ListType**, ItemType )
 * - int ListType_insert( ListType**, ItemType )
 * - int ListType_remove( ListType**, ItemType )
 * - int ListType_setItem( ListType*, size_t, ItemType )
 * - ListItem* ListType_getItem( ListType*, size_t )
 *
 * @param ListType The type name of the derived List type.
 * @param ItemType The data type of List elements.
 */
#define List_IMPLEMENT(ListType,ItemType)				\
	extern void List_init( List* list, Set_Comparator comparator ) ; \
	extern int List_pad( List* self, size_t num_items ) ;		\
	int								\
	ListType##_append( ListType* self, ItemType item )		\
	{								\
		return List_append( self, &item, sizeof( item ) ) ;	\
	}								\
	int								\
	ListType##_insert( ListType* self, size_t i, ItemType item )	\
	{								\
		return List_insert( self, i, &item, sizeof( item ) ) ;	\
	}								\
	int								\
	ListType##_remove( ListType* self, size_t i, ItemType* item )	\
	{								\
		return List_remove( self, sizeof( item ), item, sizeof( item )  ) ; \
	}								\
	int								\
	ListType##_setItem( ListType* self, size_t i, ItemType item )	\
	{								\
		return List_setItem( self, i, &item, sizeof( item ) ) ;	\
	}								\
	const ItemType*							\
	ListType##_getItem( const ListType* self, size_t i )		\
	{								\
		return List_getItem( self, i ) ;			\
	}

/******************************************************************************
 * List
 ******************************************************************************/

/**
 * @class List
 * @brief An opaque List container of arbitrary items.
 * @extends Set
 *
 * @details This class defines an un-ordered List container of
 * arbitrary items of a possibly variable size. The underlying
 * container type is the Set extended to allow for direct access to
 * random items from the List. Items in the list can be accessed by
 * their zero-based index, or by getting an Iterator on the List and
 * walking the elements in sort order. The sort order of the List
 * items depends on the Comparator provided when the List is
 * constructed.
 */
typedef struct {
	Set_EXTEND ;
	size_t		size ;
	char		reserved[ sizeof( void* ) ] ;
} List ;

/**
 * @brief Create a new List instance.
 * @memberof List
 *
 * @details This will create an empty List instance with a Set as the
 * backing store for the List. The items in the Set will be sorted
 * using the specified Set_Comparator functor. Items in the List will
 * be ordered in a first-in, first-out fashion. The exception to this
 * FIFO ordering will be when inserting items at specified indexes.
 *
 * @param[in] opt_comparator An optional Set_Comparator functor that
 *	will be used to determine the partial order of the elements in
 *	the List. If no functor is provided (i.e. the argument is
 *	@c NULL), the memcmp() function will be used by default.
 *
 * @see Set_Create
 *
 * @return A new, initially empty, List instance created on the
 * heap. The ownership of the memory returned by this function is
 * transferred to the caller who is responsible for deleting it using
 * the List destructor.
 *
 * @see List_Delete
 */
List* List_Create( Set_Comparator opt_comparator ) ;

/**
 * @brief Swap the items at the specified indices.
 * @memberof List
 *
 * @details This will swap the position of the specified items in the
 * current List instance. The items themselves will be
 * unmodified. Iterators on the items will continue to be valid. The
 * only change resulting from this is the order in which items will be
 * returned by the List_getItem() function.
 *
 * @param[in,out] self A pointer to a List instance.
 * @param[in] i The index of the first element to swap.
 * @param[in] j The index of the second element to swap.
 *
 * @return An integer flag indicating whether or not the items were
 * successfully swapped.
 *
 * @retval 0 The swap operation was successful.
 * @retval <0 An error prevented the element swap from succeeding;
 * errno will be set accordingly.
 *
 * @par Errors:
 * - EINVAL: The List instance was invalid.
 * - ERANGE: One or both of the indices were out of range.
 */
int List_swapItems( List* self, size_t i, size_t j ) ;

/**
 * @brief Insert an item at a specified position in a List.
 * @memberof List
 *
 * @details This will insert a new item in the list between items at
 * positions @p i - 1, and @p i. The current List will be resized to
 * accomodate the new item as needed. The block of items from the
 * element @p i to the last element will be shifted by one position to
 * make room for the new item.
 *
 * @pre The List must have at least @p i items.
 *
 * @param[in,out] self A pointer to a List instance into which an new
 *	item will be inserted.
 * @param[in] i The index in the list where the new item will be
 *	inserted.
 * @param[in] item A buffer containing the data of the item to add to
 *	the List.
 * @param[in] item_size The size of the memory occupied by the
 *	item. The lesser of this and the list item size bytes of data
 *	will be copied into the List item's buffer.
 *
 * @retval 0 The new item was successfully inserted into the List.
 * @retval <0 An error occurred while inserting the item into the
 *	List; errno will be set accordingly.
 *
 * @par Errors:
 * - ENOMEM: A buffer could not be allocated for the List.
 * - EINVAL: The item argument is invalid (possibly NULL).
 * - ERANGE: The index that specifies where to insert the item is out
 *   of range.
 */
int List_insert( List* self, size_t i, const void* item, size_t item_size ) ;

/**
 * @brief Append an item to the end of a List.
 * @memberof List
 *
 * @details This function will append a shallow copy of the specified
 * item to the tail of the current List instance, resizing it as
 * needed. The specified item will not be modified by this function
 * call, therefore it is safe to provide literal types to this
 * function.
 *
 * @param[in,out] self A pointer to a List instance to which the new
 *	item will be added.
 * @param[in] item A buffer containing the data of the item to add to
 *	the List.
 * @param[in] item_size The size of the memory occupied by the
 *	item. The lesser of this and the list item size bytes of data
 *	will be copied into the List item's buffer.
 *
 * @return The index of the item that was appended to the List. If the
 * append operation fails, a negative index is returned, and errno
 * will be set accordingly.
 *
 * @par Errors:
 * - ENOMEM: A buffer could not be allocated for the List.
 * - EINVAL: The item argument is invalid (possibly NULL).
 */
int List_append( List* self, void* item, size_t item_size ) ;

/**
 * @brief Remove an item from a List.
 * @memberof List
 *
 * @details This will remove the i-th item from the current List;
 * provided that item @p i exists. If the item occurs in the middle of
 * the List, items following it will be moved up to occupy the space
 * vacated by the item. This will not invalidate any iterators on the
 * List.
 *
 * @param[in,out] self A pointer to a List instance from which an item
 *	will be removed.
 * @param[in] i The index of the item to remove from the List.
 * @param[out] opt_removed_item An optional pointer to a memory buffer
 *	where the removed item will be copied before removing it.
 * @param[in] opt_buf_size The size, in bytes, of the optional buffer
 *	where the copy of the removed item will be stored. If no
 *	buffer is provided, this argument is ignored.
 *
 * @retval 0 The item was successfully removed from the List.
 * @retval <0 The item could not be removed from the List; errno will
 *	be set accordingly.
 *
 * @par Errors:
 * - EINVAL: The List argument was not valid.
 * - ERNAGE: The i-th item was not found in the List.
 */
int List_remove( List* self, size_t i, void* opt_removed_item, size_t opt_buf_size ) ;

/**
 * @brief Replace the i-th element of a List to a new item.
 * @memberof List
 *
 * @details This will overwrite the item that is the @p i-th List
 * entry with the specified @p new_item. Any scopes holding pointers
 * to the item in the List will be updated. The old item will be lost.
 *
 * @param[in] self A pointer to a List in which the i-th item will be
 *	replaced.
 * @param[in] i The index of the element in the List to replace.
 * @param[in] new_item A buffer containing the new item data to copy
 *	into the List; replacing the old item.
 * @param[in] item_size The size of the memory occupied by the new
 *	item. The lesser of this and the list item size bytes of data
 *	will be copied into the List item's buffer.
 *
 * @retval 0 The new item was successfully inserted into the List.
 * @retval <0 An error occurred while inserting the item into the
 *	List; errno will be set accordingly.
 *
 * @par Errors:
 * - EINVAL: The item argument is invalid (possibly NULL).
 * - ERANGE: The index that specifies which item to replace is out of
 *   range.
 */
int List_setItem( List* self, size_t i, const void* new_item, size_t item_size ) ;

/**
 * @brief Retrieve the i-th element of a List.
 * @memberof List
 *
 * @details If the List has at least @p i items, this function will
 * retrieve the i-th item from the List. A generic pointer to the item
 * data is returned, allowing the caller to cast the data to the
 * appropriate type.
 *
 * @param[in] self A pointer to a List instance.
 * @param[in] i The index of the item to retrieve.
 *
 * @return A pointer to an immutable buffer containing the payload
 * data of the i-th item in the List. If no such item exists, this
 * function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors:
 * - EINVAL: One of the arguments was invalid (@c NULL or zero).
 * - ERANGE: The index @p i was out of range.
 */
const void* List_getItem( const List* self, size_t i ) ;

/**
 * @brief Get an Iterator on the i-th List element.
 * @memberof List
 *
 * @details This function will get an Iterator pointing to the i-th
 * element of the current List. The Iterator can be used to retrieve
 * the element's data, or to traverse the remainder of the List.
 *
 * @see Iterator_getItem
 * @see Iterator_next
 *
 * @param[in] self A pointer to an immutable List instance.
 * @param[in] i The index of the List element that will be referenced
 *	by the returned Iterator.
 *
 * @return An Iterator instance that points to the i-th element of the
 * current List. If the requested Iterator could not be retrieved, the
 * @c NULL Iterator will be returned and @e errno will be set
 * accordingly.
 *
 * @par Errors
 * - EINVAL: The List instance is invalid.
 * - ERANGE: There is no element at index i in the List.
 */
Iterator List_getIterator( const List* self, size_t i ) ;

/**
 * @brief Empty an item list.
 *
 * @details This will empty the current List instance without deleting
 * the buffer allocated to contain it.
 *
 * @param[in,out] self A pointer to a List instance.
 */
void List_clear( List* self ) ;

/**
 * @brief Get the number of items in the current List.
 * @memberof List
 *
 * @details This function will report the number of items that have
 * been added to the current List instance. The number of items in the
 * List will be less than or equal to its capacity.
 *
 * @see List_getCapacity
 *
 * @param[in] self A pointer to an immutable List instance.
 *
 * @return The number of items in the current List.
 */
size_t List_getSize( const List* self ) ;

/**
 * @brief Get the capacity of the current List.
 * @memberof List
 *
 * @details This will return the size of the current List. The size of
 * the list does not necessarily correspond with the number of items
 * contained therein. However, this will always be at least as large
 * as the number of items in the List. To determine the number of
 * items contained in the List, use the List_getSize() function
 * provided by the Set class.
 *
 * @see List_getSize
 *
 * @param[in] self An immutable pointer to a List instance.
 *
 * @return The current capacity of the List instance.
 */
size_t List_getCapacity( const List* self ) ;

#endif /* __LIST_H__ */
