/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef __DICTIONARY_H__
#define __DICTIONARY_H__

#include "hashmap.h"

/**
 * @class Dictionary_Entry
 * @brief Class which expresses a Dictionary entry.
 *
 * @details The Dictionary_Entry class encapsulates a key-value pair
 * which expresses a single entry in a Dictionary container.
 */
typedef struct _dictionary_entry Dictionary_Entry ;

/**
 * @brief Retrieve the key of a Dictionary entry.
 * @memberof Dictionary_Entry
 *
 * @details Retrieves the anonymously typed key of the current
 * Dictionary_Entry instance. The type of the key element is not
 * specified and must be know a priory for it to be useful.
 *
 * @param[in] self A pointer to an immutable Dictionary_Entry
 *	instance.
 *
 * @return A pointer to the key element of the current
 * Dictionary_Entry instance. This pointer can be cast to an
 * appropriate type. The ownership of the memory returned by this
 * function is retained by the callee and must not be deleted by the
 * caller.
 */
const void* Dictionary_Entry_getKey( const Dictionary_Entry* self ) ;

/**
 * @brief Retrieve the value of a Dictionary entry.
 * @memberof Dictionary_Entry
 *
 * @details Retrieve the anonymously typed value element of the
 * current Dictionary_Entry instance. The type of the value element is
 * not mandated by the prototype; it must be known by the caller for
 * the returned value to be of any use.
 *
 * @param[in] self A pointer to an immutable Dictionary_Entry
 *	instance.
 *
 * @return A pointer to the value of a Dictionary_Entry instance. This
 * pointer can be cast to an appropriate type. The ownership of the
 * memory returned by this function is retained by the callee and must
 * not be deleted by the caller.
 */
const void* Dictionary_Entry_getValue( const Dictionary_Entry* self ) ;

/******************************************************************************
 * Dictionary
 ******************************************************************************/

/**
 * @class Dictionary
 * @brief Container expressing a Dictionary container.
 * @extends HashMap
 *
 * @details The Dictionary container is an associated array which maps
 * unique keys to a value. Although the keys in the Dictionary must be
 * unique, there is no such restriction on the values. Therefore the
 * Dictionary provides a one-to-many mapping of keys to values.
 */
typedef HashMap Dictionary ;

/**
 * @brief Add an entry to a Dictionary.
 * @memberof Dictionary
 *
 * @details This wll add a key-value mapping to the current Dictionary
 * instance. If a mapping already exists for the specified key, it
 * will be added to the Dictionary anyway. When retrieving the entries
 * for the specified key, it will be returned as part of the list of
 * values. If the key-value pair exactly matches an entry in the
 * Dictionary, the new pair will not be added.
 *
 * @see Dictionary_findEntries
 *
 * @param[in] self A pointer to the current Dictionary instance.

 * @param[in] key A pointer to an immutable buffer which contains the
 *	key data.
 * @param[in] key_size The size, in bytes, of the key data.
 * @param[in] value A pointer to an immutable buffer which contains
 *	the value data of the new entry.
 * @param[in] value_size The size, in bytes, of the value data.
 *
 * @retval 0 The new entry was added successfully.
 * @retval -1 The new entry could not be added to the Dictionary;
 *	@e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The dictionary instance is invalid.
 * - ENOMEM: There was not enough memory to add the entry.
 * - EEXIST: The key-value pair already exists in the Dictionary.
 */
int Dictionary_addEntry( Dictionary* self, const void* key, size_t key_size, const void* value, size_t value_size ) ;

/**
 * @brief Find the Dictionary entries associated with a key.
 * @memberof Dictionary
 *
 * @details This will retrieve an Iterator to the first item in the
 * Dictionary which corresponds with the specified key. This Iterator
 * can be used to enumerate all of the entries that share the key.
 *
 * @see Dictionary_NextEntry
 *
 * @param[in] self A pointer to an immutable Dictionary instance.
 * @param[in] key The key associated with the entries to retrieve,
 *
 * @return An Iterator referencing the first Dictionary entry
 * corresponding with the specified key. This Iterator can be used to
 * enumerate the entries which follow. Note that if using the standard
 * iterator next() function, the returned data will not be the value,
 * but the internal representation of the key-value pair. Use
 * Dictionary_NextEntry() to walk only the values associated with they
 * key.
 */
Iterator Dictionary_findEntries( const Dictionary* self, const void* key ) ;

/**
 * @brief Get the next entry in a key series.
 * @memberof Dictionary
 *
 * @details This will get the value data associated with the
 * Dictionary entry associated with the Iterator. This function is
 * similar to the standard function to proceed the Iterator, however,
 * it will stop if the key of the entry does not correspond with the
 * entry series, and only returns the entry's value.
 *
 * @param[in] current_entry A pointer to the Iterator pointing to the
 *	current Entry. This Iterator will be advanced to the next item
 *	with the same key when this function returns.
 * @param[out] opt_size An optional buffer where the size of the
 *	returned data buffer, expressed in bytes, can be stored.
 *
 * @return A pointer to an immutable buffer containing the value data
 * associated with the Dictionary Entry corresponding with a given
 * key.
 *
 * @see Dictionary_findEntries
 */
const void* Dictionary_NextEntry( Iterator* current_entry, size_t* opt_size ) ;

#endif	/* __DICTIONARY_H__ */
