/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of a Category data structure.
 *
 * @details This file declares the API of a Category data structure
 * which is used to define abstract algebra categories of arbitrary
 * data.
 */

#ifndef __CATEGORY_H__
#define __CATEGORY_H__

/* Local includes */
#include "util/set.h"

/**
 * @class Category
 * @brief Container expressing an algebraic category.
 * @extends Set
 *
 * @details A category is a group-like algebraic structure consisting
 * of a Set of elements, coupled with a binary operation which is used
 * to map any two elements of the category to a third element
 * therein. Instances of the Category class will express a finite
 * category. Category instances must satisfy one or more of the
 * following axioms:
 *
 * - Closure: For all elements of the group, the application of the
 *   operation on any two elements of the group @e must map to an
 *   element in the Group.
 *
 * - Associativity: For all elements @f$a@f$, @f$b@f$ and @f$c@f$ in
 *   the Group, @f$a*(b*c)=(a*b)*c@f$.
 *
 * - Identity: There exists an element @f$e@f$ in the Group which
 *   satisfies the following equation: @f$e*a=a=a*e@f$
 *
 * - Divisibility: For every element @f$a@f$ in the set, there exists
 *   an inverse element @f$b@f$ in the set which satisfies @f$a*b=e@f$
 *   where @f$e@f$ is the identity element.
 *
 * - Commutativity: For any two elements, @f$a, b@f$, in the Group,
 *   @f$a*b=b*a@f$. If this axiom is satisfied, the Group defines an
 *   abelian group.
 *
 * The following table summarizes the various finite group-like
 * structures that can be expressed by the Category class, enumerating
 * the axioms that must be satisfied for each:
 *
 * Structure    |Closure |Associativity| Identity   |Divisibility|Commutativity
 * :------------|:------:|:-----------:|:----------:|:----------:|:------------:
 * Semicategory |Unneeded| Required    | Uneeded    | Unneeded   | Unneeded
 * Category     |Unneeded| Required    | Required   | Unneeded   | Unneeded
 * Groupoid     |Unneeded| Required    | Required   | Required   | Unneeded
 * Magma        |Required| Unneeded    | Unneeded   | Unneeded   | Unneeded
 * Quasigroup   |Required| Unneeded    | Unneeded   | Required   | Unneeded
 * Loop         |Required| Unneeded    | Required   | Required   | Unneeded
 * Semigroup    |Required| Required    | Unneeded   | Unneeded   | Unneeded
 * Monoid       |Required| Required    | Required   | Unneeded   | Unneeded
 * Group        |Required| Required    | Required   | Required   | Unneeded
 * Abelian Group|Required| Required    | Required   | Required   | Required
 *
 * (see https://en.wikipedia.org/wiki/Group_%28mathematics%29).
 */
typedef struct _category Category ;

/**
 * @brief Prototype of Category operation functions.
 * @memberof Category
 *
 * @details This type describes the prototype expected of functions
 * which implement the operation associated with a Category
 * instance. This function is expected to map a pair of items, members
 * of the Category, to a third element which may be a member of the
 * Category.
 *
 * @param[in] self A pointer to a Category instance that defines the
 *	parameters of the operation.
 * @param[in] item1 An abstract pointer to the first operand of the
 *	operation.
 * @param[in] item2 An abstract pointer to the second operand of the
 *	operation.
 *
 * @return A pointer to an immutable element which is the result of
 * applying the operation expressed by this function to the input
 * elements. If an error occurs while applying the operation, this
 * function should return @c NULL and set @e errno accordingly.
 */
typedef const void* Category_Operation( const Category* self, const void* item1, const void* item2 ) ;

#endif	/* __CATEGORY_H__ */
