/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of an abstract Set container
 *
 * @details This file provides an implementation of a Set container of
 * un-specified items.
 */

#ifndef __SET_H__
#define __SET_H__

/* POSIX includes */
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/******************************************************************************
 * Iterator
 ******************************************************************************/

/**
 * @class Iterator
 * @brief Iterator on the elements of a collection.
 *
 * @details This class provides an implementation of a bi-directional
 * iterator type which can be used to walk over all the elements
 * contained in a collection. An iterator instance will reside between
 * elements of a Set. For example, the following illustrates the
 * situation where the Iterator is between the first and second item
 * in the Set:
   @verbatim
   ...->[Item1]->[Item2]->[Item3]->...
                ^
                |
            [Iterator]
   @endverbatim

 * In this case, getting the item referred to by the Iterator will
 * retrieve Item2. If the Iterator follows the last Set element, it
 * will refer to a @c NULL item.
 */
typedef intptr_t Iterator ;

/**
 * @brief Determine if there are items that follow an Iterator.
 * @memberof Iterator
 *
 * @details This function will ascertain whether or not there is an
 * item following the current iterator instance. In otherwords, it
 * determines if the next call to Iterator_next() will return a
 * non-@c NULL value, without advancing the iterator.
 *
 * @see Iterator_next
 *
 * @param[in] self An Iterator instance.
 *
 * @return If there is an item that follows the current iterator, this
 * function returns @e true. Otherwise it returns @e false.
 */
bool Iterator_hasNext( const Iterator self ) ;

/**
 * @brief Move the Iterator to the next item in a collection.
 * @memberof Iterator
 *
 * @details This will advance the iterator to the next member in the
 * associated collection, returning the referenced item. If the
 * Iterator is already passed the last item, it will not be
 * advanced. For example, if the Iterator references the second item
 * in a Set, as it does in the following illustration:
   @verbatim
   [Item1]->[Item2]->[Item3]->...
           ^
           |
       [Iterator]
   @endverbatim
 * Invoking this function will advance the Iterator to just following
 * Item2, having it reference Item3 as illustrated in the following
 * figure:
   @verbatim
   [Item1]->[Item2]->[Item3]->...
                    ^
                    |
                [Iterator]
   @endverbatim
 *
 * @param[in,out] self A pointer to an Iterator instance. The iterator
 *	will be advanced to the next item following a call to this
 *	function.
 * @param[out] opt_size An optional buffer where the size, in bytes,
 *	of the item returned by this function.
 *
 * @return A pointer to the immutable item data of the Element
 * referenced by the Iterator after it is advanced. If there was no
 * next item when this function was called, the returned value will be
 * @c NULL and the iterator will not be advanced.
 */
const void* Iterator_next( Iterator* self, size_t* opt_size ) ;

/**
 * @brief Move the Iterator to the previous item in a collection.
 * @memberof Iterator
 *
 * @details This will recede the iterator to the previous member in
 * the assocaited collection, returning the referenced item following
 * the recession. If the Iterator already precedes the first item, it
 * will not be receded any further. For example, in the following
 * illustration, the Iterator references Item2 in a Set:
   @verbatim
   []->[Item1]->[Item2]->...
               ^
               |
           [Iterator]
   @endverbatim
 * Following the recession of the Iterator, it will reference Item1;
 * this is illustrated as follows:
   @verbatim
    []->[Item1]->[Item2]->...
       ^
       |
   [Iterator]
   @endverbatim
 *
 * @param[in,out] self A pointer to an Iterator instance. The iterator
 *	will be updated to reference the preceding item following a
 *	call to this function.
 * @param[out] opt_size A buffer where the size, in bytes, of the
 *	returned item will be stored when this function returns.
 *
 * @return A pointer to an immutable buffer containing the item that
 * preceded the Iterator before this function call. If there is no
 * previous item, this function returns @c NULL.
 */
const void* Iterator_previous( Iterator* self, size_t* opt_size ) ;

/**
 * @brief Remove the current item from the associated collection.
 * @memberof Iterator
 *
 * @details This will remove the element referenced by the current
 * Iterator from the associated collection. The iterator will remain
 * valid following a call to this function. The next call to
 * Iterator_next() will continue enumerating the remaining elements of
 * the set as though the current item was never a member of the
 * set. For example, given the Iterator position illustrated as
 * follows:
   @verbatim
   ...->[Item1]->[Item2]->[Item3]->...
                ^
                |
            [Iterator]
   @endverbatim
 * An invocation of this function will remove @e Item2 from the
 * set. The resulting Iterator position is illustrated as follows:
   @verbatim
   ...->[Item1]->[Item3]->...
                ^
                |
            [Iterator]
   @endverbatim
 * Successive calls to this function will remove additional items
 * until there are no items that follow the Iterator.
 *
 * @see Iterator_next
 *
 * @param[in,out] self A pointer to a Iterator instance that
 *	references the item to remove.
 * @param[out] opt_item An optional pointer to a buffer where a copy
 *	of the item being removed will be stored.
 * @param[in] item_size the size of the item buffer. If the item
 *	buffer is not provided, this argument is ignored.
 *
 * @return A flag indicating whether or not the current item was
 * successfully removed.
 */
bool Iterator_remove( Iterator* self, void* opt_item, size_t item_size ) ;

/**
 * @brief Append a new element to the current Iterator.
 * @memberof Iterator
 *
 * @details This function will allocate a new Set_Element instance
 * encapsulating the specified item, then append it to the item
 * preceding the current Iterator, effectively setting it as its next
 * item. This function can be used when iterating over the elements in
 * a Set, to append a new element in the desired position. For
 * example, given the Iterator position illustrated as follows:
   @verbatim
   ...->[Item1]->[Item2]->...
                ^
                |
            [Iterator]
   @endverbatim
 * If Item3 is appended to the Iterator, the result will be
 * illustrated as follows:
   @verbatim
   ...->[Item1]->[Item3]->[Item2]->...
                ^
                |
            [Iterator]
   @endverbatim
 * On the next call to Iterator_next(), the Iterator will step over
 * Item3 to reference Item2.
 *
 * @param[in] self The current Iterator.
 * @param[in] item A pointer to a buffer containing the item data.
 * @param[in] item_size The size of the item data.
 *
 * @retval 0 The new element was successfully appended to the current
 *	Iterator.
 * @retval -1 The new element could not be appended to the current
 *	Iterator; @e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The item data is invalid.
 * - ENOENT: The Iterator does not reference an Element.
 * - ENOMEM: There wasn't enough memory to allocate to the new
 *   Element.
 */
int Iterator_append( Iterator self, const void* item, size_t item_size ) ;

/**
 * @brief Retrieve the item referenced by the Iterator.
 * @memberof Iterator
 *
 * @details This will retrieve a pointer to the item referenced by the
 * current Iterator instance. The returned item is a generic pointer
 * which can be cast to the appropriate type which must be known a
 * priori. For example, in the following example, Item2 will be
 * returned by this function.
   @verbatim
   ...->[Item1]->[Item2]->[Item3]->...
                ^
                |
            [Iterator]
   @endverbatim
 *
 * @param[in] self An immutable Iterator instance.
 * @param[out] opt_size An optional buffer where the size, in bytes,
 *	of the item data will be written.
 *
 * @return A pointer to an immutable buffer containing the data
 * comprising the item referenced by the curent Iterator. If the
 * Iterator follows the last item in the associated Set, this function
 * returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 * - ENOENT: The current Iterator does not reference any item (i.e. it
 *   follows the last item in the collection).
 */
const void* Iterator_getItem( const Iterator self, size_t* opt_size ) ;

/**
 * @def Iterator_IMPLEMENT_OVERRIDES( Container, Item )
 * @brief Macro which defines a specialization of an Iterator
 *	class.
 * @memberof Iterator
 *
 * @details Use this macro to provide implementations of the Iterator
 * specialization functions for the specified Container type.
 *
 * @param[in] Container The type name of the container class for which
 *	the Iterator specialization is being provided.
 * @param[in] Item Type name of items contained in the Container
 *	type.
 */
#define Iterator_IMPLEMENT_OVERRIDES( Container, Item )			\
	Item const*							\
	Container##_Iterator_next( Iterator* self )			\
	{								\
		return ( Item const* )Iterator_next( self, NULL ) ;	\
	}								\
	bool								\
	Container##_Iterator_remove( Iterator* self, Item* item )	\
	{								\
		return Iterator_remove( self, item, sizeof( *item ) ) ; \
	}

/**
 * @def Iterator_DECLARE_OVERRIDES( Container, Item )
 * @brief Macro which declares specializations of Iterator functions.
 * @memberof Iterator
 *
 * @details Use this macro to declare overloaded functions for an
 * Iterator over a custom container class. This macro is a shortcut
 * when the default implementations of the functions are sufficient,
 * but that operate on sepcialized collections. The functions declared
 * by this macro include:
 *
 * - Collection_Iterator_next( Iterator*, Item* )
 * - Collection_Iterator_remove( Iterator*, Item* )
 *
 * The implementations of these functions can be defined using the
 * Iterator_IMPLEMENT macro.
 *
 * @see Iterator_IMPLEMENT
 *
 * @param[in] Container The type name of the specialized collection.
 * @param[in] Item The type name of the items of the specialized
 *	Container.
 */
#define Iterator_DECLARE_OVERRIDES( Container, Item )			\
	typedef Iterator Container##_Iterator ;				\
	Item const* Container##_Iterator_next( Iterator* ) ;		\
	bool Container##_Iterator_remove( Iterator*, Item* )

/******************************************************************************
 * Set
 ******************************************************************************/

/**
 * @brief Prototype of Set comparator functions.
 * @memberof Set
 *
 * @details This type describes the prototype expected of functions
 * that will be used to compare elements in the Set. This function
 * will be used to determine partial ordering of the members contained
 * in a Set, as well as to determine equality.
 *
 * @param[in] item1 An abstract pointer to the first Set element to
 *	compare.
 * @param[in] item2 An abstract pointer to the second Set element to
 *	compare.
 * @param[in] size The size, in bytes, of the items being compared. If
 *	the items are not the same size, this should be the size of
 *	the smaller of the two items.
 *
 * @return An integer expressing the partial order of the items being
 * compared.
 *
 * @retval <0 @p item1 comes before @p item2.
 * @retval >0 @p item2 comes before @p item1.
 * @retval 0 The two items are equal.
 */
typedef int Set_Comparator( const void* item1, const void* item2, size_t size ) ;

/**
 * @class Set
 * @brief Container expressing a Set of elements.
 *
 * @details This class defines a Set of elements of un-specified
 * type. The elements contained in the Set don't necessarily have to
 * all be of the same type, or even the same size or memory
 * layout. The members of the Set will be shallow copies of the items
 * that are added to thereto. Instances of the Set class are iterable,
 * therefore it is possible to walk the entire contents of the set
 * element-by-element.
 *
 * By definition, a Set cannot contain duplicate elements. To
 * accelerate retrieval, the members of a Set are stored in a total
 * order. Elements are compared byte-for-byte to determine equality
 * (by default using the memcmp() system call). However, a custom
 * comparator function can be specified which evaluates the partial
 * order of items in the Set.
 *
 * The Set class can be extended to specialize its contents by using
 * the Set_EXTEND macro. This will define overloaded functions for
 * insert() and remove() which determine the size of the item from its
 * type. The Set_IMPLEMENT macro provides implementations for these
 * functions.
 *
 * @see Set_DECLARE_OVERRIDES
 * @see Set_IMPLEMENT
 * @see Set_EXTEND
 */
typedef struct {
	/**
	 * @brief The number of members in the current Set.
	 * @memberof Set
	 *
	 * @details This property tracks the number of elements that have been
	 * added as members in the current Set instance.
	 */
	size_t		cardinality ;

	/**
	 * @brief The comparator associated with the current Set.
	 * @memberof Set
	 *
	 * @details This property holds a pointer to the function that is used
	 * to determine the partial order of the members in the current Set.
	 */
	Set_Comparator*	compare ;
	char		reserved[ 8*sizeof( void* ) + 2*sizeof( size_t ) ] ;
} Set ;

/**
 * @def Set_DECLARE_OVERRIDES( TypeName, ItemType )
 * @brief Macro which declares a specialization of the Set class.
 * @memberof Set
 *
 * @details Use this macro to declare the overloaded functions for a
 * Set specialization with the specified @a TypeName. The derived Set
 * class must have been declared for the functions provided by this
 * macro to compile successfully. For example:
 *
 * @code{.c}
 * typedef struct {
 *	Set_EXTEND ;
 *	int		extended_member ;
 * } SpecializedSet ;
 * Set_DECLARE_OVERRIDES( SpecializedSet, ItemType ) ;
 * @endcode
 *
 * The members of this specialization will operate on the specified
 * @a ItemType. This will declare the following overloadded
 * functions:
 *
 * - TypeName_insert( Set*, const ItemType* )
 * - TypeName_remove( Set*, const ItemType* )
 *
 * The implementations of these functions can be defined using the
 * Set_IMPLEMENT macro.
 *
 * @see Set_IMPLEMENT
 *
 * @param[in] TypeName The type name of the Set specialization.
 * @param[in] ItemType The type name of members of the Set
 *	specialization.
 */
#define Set_DECLARE_OVERRIDES( TypeName, ItemType )			\
	typedef ItemType TypeName##_Item ;				\
	Iterator TypeName##_insert( TypeName* self, const ItemType item ) ; \
	bool TypeName##_remove( TypeName* self, const ItemType item )


/**
 * @def Set_IMPLEMENT( TypeName, ItemType, Comparator )
 * @brief Macro which defines the specialization of a Set class.
 * @memberof Set
 *
 * @details Use this macro to provide implementations of the
 * overloaded functions declared by Set_EXTEND.
 *
 * @see Set_DECLARE_OVERRIDES
 *
 * @param[in] TypeName The type name of the Set specialization.
 * @param[in] ItemType The type name of members of the Set
 *	specialization.
 */
#define Set_IMPLEMENT( TypeName, ItemType )				\
	Iterator							\
	TypeName##_insert( TypeName* self, const ItemType item )	\
	{								\
		return Set_insert( (Set*)self, &item, sizeof(item) ) ;	\
	}								\
	bool								\
	TypeName##_remove( TypeName* self, const ItemType item )	\
	{								\
		return Set_remove( (Set*)self, &item, sizeof(item) ) ;	\
	}								\
	extern void Set_init( Set* self, Set_Comparator compare ) ;	\
	extern Set_Element* Set_createElement( Set* self,		\
					       const void* item,	\
					       size_t item_size )

/**
 * @brief Macro used to extend the set class.
 *
 * @details Declare this macro in the body of a struct, before any
 * members are declared, to mark the new structure as a derived type
 * of the Set class.
 *
 * For example:
 * @code{.c}
 * typedef _derived_set {
 *	Set_EXTEND ;
 *	int	derived_set_member ;
 * } DerivedSet ;
 * @endcode
 */
#define Set_EXTEND Set __Set__ ;

/**
 * @brief Create an empty Set.
 * @memberof Set
 *
 * @details Allocate a new, initially empty, Set instance. Members of
 * the set can be added by calling the Set_insert() function on the
 * resulting instance. Conversely, the Set_remove() function can be
 * used to remove members from the Set.
 *
 * @param opt_compare An optional pointer to a comparator function
 *	that will be used to determine the partial order of elements
 *	in the Set. If this function is not provided, elements will be
 *	compared using the standard memcmp() function.
 *
 * @return A new, initially empty, instance of the Set class created
 * on the heap. The ownership of the memory returned by this function
 * is transferred to the caller who is responsible for deleting it via
 * the Set_Delete() function.
 */
Set* Set_Create( Set_Comparator* opt_compare ) ;

/**
 * @brief Create a Set as the union of two other sets.
 * @memberof Set
 *
 * @details This function will create a new Set instance, initializing
 * its contents to express the union of Sets @p s1 and @p s2. The
 * members of the new Set will be shallow copies of the members of the
 * input Sets. The input sets will not be affected by this function.
 *
 * @param[in] s1 A pointer to an immutable Set instance.
 * @param[in] s2 A pointer to an immutable Set instance.
 * @param[in] comparator An optional comparator to determine the
 *	partial order of the elements of the Set.
 *
 * @return A new Set instance, created on the heap, expressing the
 * union of the Sets @p s1 and @p s2. The ownership of the memory
 * returned by this function is transferred to the caller who is
 * responsible for deleting it via the Set_Delete() function.
 *
 * @see Set_Delete
 */
Set* Set_Union( const Set* s1, const Set* s2, Set_Comparator* comparator ) ;

/**
 * @brief Delete a Set and all its members.
 * @memberof Set
 *
 * @details This will delete the current Set instance, deleting all of
 * its members in the process. Any reference to items in the current
 * Set will become invalid.
 *
 * @param[in,out] self The address of a pointer to a Set
 *	instance. This pointer will be set to @c NULL following a call
 *	to this function.
 */
void Set_Delete( Set** self ) ;

/**
 * @brief Compare two Set elements.
 * @memberof Set
 *
 * @details This will determine the partial order of two Set elements
 * using the comparator associated therewith.
 *
 * @param[in] self A pointer to an immutable Set instance.
 * @param[in] item1 A pointer to an immutable buffer containing the
 *	first item to compare.
 * @param[in] item2 A pointer to an immutable buffer containing the
 *	second item to compare.
 * @param[in] size The size, in bytes of the items being compared.
 *
 * @retval <0 item1 Precedes item2 in the Set order.
 * @retval 0 The order of the items is undetermined.
 * @retval >0 item1 Follows item2 in the Set order.
 */
int Set_compare( const Set* self, const void* item1, const void* item2, size_t size ) ;

/**
 * @brief Get the cardinality of a Set.
 * @memberof Set
 *
 * @details This will report the number of members contained in the
 * current Set instance. This is a fast operation since the
 * cardinality is recalculated on every insert() or delete()
 * operation.
 *
 * @param[in] self A pointer to a Set instance.
 *
 * @return The number of members contained in the Set.
 */
size_t Set_getSize( const Set* self ) ;

/**
 * @brief Get an Iterator at the front of the Set.
 * @memberof Set
 *
 * @details This will return an Iterator initialized to just before
 * the first element of the Set. This Iterator can be used to traverse
 * all the members of the associated Set.
 *
 * @see Iterator_next
 *
 * @pre The set instance must not be @c NULL.
 *
 * @param[in] self A pointer to a Set instance.
 *
 * @return A Iterator instance initialized to just before the first
 * member of the Set.
 */
Iterator Set_getFirst( const Set* self ) ;

/**
 * @brief Get an Iterator at the back of the Set.
 * @memberof Set
 *
 * @details This will return an Iterator that follows the last item in
 * the current Set. The returned iterator can be used to traverse the
 * contents of the Set in reverse-order.
 *
 * @see Iterator_previous
 *
 * @pre The Set instance must not be @c NULL.
 *
 * @param[in] self An immutable pointer to a Set instance.
 *
 * @return An Iterator instance which is positioned immediately
 * following the last item in the Set.
 */
Iterator Set_getLast( const Set* self ) ;

/**
 * @brief Find an element in the Set.
 * @memberof Set
 *
 * @details This will search the current Set for an item corresponding
 * with the given model. In the worst case, the search will be
 * performed with O(n) complexity, where n is the cardinality of the
 * Set.
 *
 * @param[in] self An immutable pointer to a Set instance.
 * @param[in] model A buffer containing data that will be used as the
 *	model for the item to find.
 * @param[in] model_size The size, in bytes, of the model.
 *
 * @return A pointer to an immutable buffer containing the data of the
 * element from the current Set which corresponds with the model. If
 * no such item is found, this function returns @c NULL.
 */
const void* Set_find( const Set* self, const void* model, size_t model_size ) ;

/**
 * @brief Determine if an item is a member of a Set.
 * @memberof Set
 *
 * @details This will search the current Set instance for a member
 * that is equal to the specified item. Equality is determined by the
 * comparator function that is associated with the Set. The default
 * comparator does a byte-for-byte comparison of the item with the
 * member item.
 *
 * @param[in] self An immutable pointer to a Set instance.
 * @param[in] item A pointer to a buffer containing the model of the
 *	member item to find.
 * @param[in] size The size of the model item.
 *
 * @return An abstract pointer to the item payload in the set. This is
 * a direct pointer to the element data in the Set.
 */
void* Set_contains( const Set* self, const void* item, size_t size ) ;

/**
 * @brief Add a member to a Set.
 * @memberof Set
 *
 * @details This will insert a shallow copy of the specified item as a
 * new member of the current Set. If the item already exists in the
 * Set, the insertion will fail.
 *
 * @param[in,out] self A pointer to a Set instance.
 * @param[in] item A pointer to a buffer containing the item's
 *	data. This buffer will be copied as a new member of the Set.
 * @param[in] size The size of the item's data buffer in bytes.
 *
 * @return If the insertion was successful, this function returns an
 * Iterator that references the newly inserted item. Otherwise a
 * @c NULL Iterator will be returned, and @e errno will be set to
 * indicate the reason why the insertion failed:
 *
 * @par Errors:
 * - @e EINVAL: One of the arguments is invalid.
 * - @e ENOMEM: Allocation of the item failed.
 * - @e EEXIST: A copy of the specified item already exists in the
 *   set.
 */
Iterator Set_insert( Set* self, const void* item, size_t size ) ;

/**
 * @brief Remove a member from a Set.
 * @memberof Set
 *
 * @details This will remove the member of the current set that is a
 * byte-for-byte copy of the specified item. The member will be
 * deleted, along with its item data. Any reference to the item will
 * become invalid following a call to this function.
 *
 * @param[in,out] self A pointer to a Set instance.
 * @param[in] item A pointer to an immutable buffer containing a copy
 *	of the item to be removed.
 * @param[in] size The size, in bytes, of the item buffer.
 *
 * @return A flag indicating whether or not the item was successfully
 * removed.
 */
bool Set_remove( Set* self, const void* item, size_t size ) ;

/**
 * @brief Clear a set of its members.
 * @memberof Set
 *
 * @details This will remove and delete all of the members from the
 * current Set. Since the elements of Set will be deleted, references
 * to any element will become invalid following a call to this
 * function.
 *
 * @param[in] self A pointer to a Set instance.
 */
void Set_clear( Set* self ) ;

/******************************************************************************
 * Set_Element
 ******************************************************************************/

/**
 * @class Set_Element
 * @brief Opaque type which encapsulates set element data.
 */
typedef struct _set_element Set_Element ;

/**
 * @brief Get the item data encapsulated by the pointer.
 * @memberof Set_Element
 *
 * @details This function provides access to the abstract payload data
 * encapsulated by the current Set_Element instance.
 *
 * @param[in] self A pointer to an immutable Set_Element instance.
 *
 * @return An immutable abstract pointer to a buffer containing the
 * data payload of the specified Set_Element. If the buffer is empty,
 * this function will return @c NULL.
 */
void* Set_Element_data( const Set_Element* self ) ;

/**
 * @brief Get the size of the element's payload.
 * @memberof Set_Element
 *
 * @details This function will report the size of the item payload
 * encapsulated by the current Set_Element instance.
 *
 * @param[in] self A pointer to an immutable Set_Element instance.
 *
 * @return The size of the Set_Element's item payload in bytes.
 */
size_t Set_Element_dataSize( const Set_Element* self ) ;

/**
 * @brief Copy the item payload into the specified buffer.
 * @memberof Set_Element
 *
 * @details This will copy up to @p size bytes of the item payload
 * associated with a Set_Element instance into the specified
 * buffer. The element's payload will be unmodified by this function
 * call.
 *
 * @param[in] self A pointer to an immutable Set_Element instance.
 * @param[out] item A pointer to a buffer where the payload data will
 *	be copied.
 * @param[in] size The size of the output buffer in bytes.
 *
 * @return The number of bytes copied into the item buffer. If the
 * operation fails for any reason, this function returns a negative
 * value and errno will be set accordingly.
 */
int Set_Element_copyItem( const Set_Element* self, void* item, size_t size ) ;

/**
 * @brief Compare an element instance with an item buffer.
 * @memberof Set_Element
 *
 * @details This will compare the payload data of the Set_Element
 * instance with the contents of the specified item buffer. The
 * comparison is performed using the comparator that was assigned to
 * the set at construction time. By default, items will be compared
 * byte-for-byte using the memcmp function.
 *
 * @param[in] self A pointer to an immutable Set_Element instance.
 * @param[in] item A pointer to a buffer containing data to compare
 *	with the element payload.
 * @param[in] size The size of the item buffer in bytes.
 *
 * @return An integer value which specifies the partial order of an
 * Element's payload and the specified item.
 *
 * @retval <0 The Set_Element instance precedes the specified item.
 * @retval >0 The Set_Element instance follows the specified item.
 * @retval 0 The Set_Element payload and the item are equal.
 */
int Set_Element_compare( const Set_Element* self, const void* item, size_t size ) ;

/**
 * @brief Detach the current Element from its Set.
 * @memberof Set_Element
 *
 * @details This will remove the current Set_Element instance from its
 * associated Set. This will decrement the cardinality of the Set
 * instance accordingly.
 *
 * @param[in] self A pointer to a Set_Element instance.
 */
void Set_Element_detach( Set_Element* self ) ;

/**
 * @brief Append the specified element to the current one.
 * @memberof Set_Element
 *
 * @details This will append the specified Set_Element instance to the
 * current element, adding it to the Set to which the current element
 * belongs. The @p element will be detached from its previous set
 * before appending it to the current element and adding it to the
 * set.
 *
 * @param[in] self A pointer to the current Set_Element instance.
 * @param[in] element A pointer to a Set_Element instance to append to
 *	the current instance.
 *
 * @retval 0 The append operation was successful.
 * @retval <0 An error occured while appending; errno will be set
 *	accordingly.
 *
 * @par Errors:
 * - EINVAL: One or both of the input elements was invalid.
 */
int Set_Element_append( Set_Element* self, Set_Element* element ) ;

#endif	/* __SET_H__ */
