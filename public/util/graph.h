/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of an ADT to express graphs.
 */

#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <stddef.h>

#include <util/hashmap.h>

/**
 * @brief Prototype of Vertex comparators.
 *
 * @details This type describes the prototype expected of functions
 * that can be used as Vertex comparators. Vertex comparators will
 * determine the partial order of two Vertex instances.
 *
 * @param[in] v1 A pointer to the first Vertex operand.
 * @param[in] v2 A pointer to the second Vertex operand.
 *
 * @retval <0 Vertex v1 precedes v2.
 * @retval 0 The relative order of v1 and v2 is undetermined; they may
 *	be equal.
 * @retval >0 Vertex v1 follows v2.
 */
typedef int Vertex_Comparator( const void* v1, const void* v2 ) ;

/**
 * @class Edge
 * @brief Class expressing an edge in a graph.
 *
 * @details This class is a specialization of a set which maps labels
 * to Node instances. Instances of this class can be used to express a
 * labelled edge connecting two nodes in a graph.
 */
typedef struct _edge Edge ;

/**
 * @brief Get the data of the initial Vertex of the Edge.
 * @memberof Edge
 *
 * @details This will retrieve the data associated with the initial
 * Vertex of the current Edge. The size of the Vertex data can
 * optionally be reported by this function.
 *
 * @param[in] self A pointer to an immutable Edge instance.
 * @param[out] opt_data_size A pointer to an optional buffer where the
 *	size of the Vertex data returned by this function can be
 *	stored.
 *
 * @return A pointer to an immutable buffer containing the data
 * associated with the initial Vertex of the current Edge. In the case
 * of an error, this function returns @c NULL and sets @e errno
 * accordingly.
 *
 * @par Errors
 * - EINVAL: The Edge instance is invalid.
 */
const void* Edge_getInitialVertex( const Edge* self, size_t* opt_data_size ) ;

/**
 * @brief Get the data of the final Vertex of the Edge.
 * @memberof Edge
 *
 * @details This will retrieve the data associated with the final
 * Vertex of the current Edge. The size of the Vertex data can
 * optionally be reported by this function.
 *
 * @param[in] self A pointer to an immutable Edge instance.
 * @param[out] opt_data_size A pointer to an optional buffer where the
 *	size of the Vertex data returned by this function can be
 *	stored.
 *
 * @return A pointer to an immutable buffer containing the data
 * associated with the final Vertex of the current Edge. In the case
 * of an error, this function returns @c NULL and sets @e errno
 * accordingly.
 *
 * @par Errors
 * - EINVAL: The Edge instance is invalid.
 */
const void* Edge_getFinalVertex( const Edge* self, size_t* opt_data_size ) ;

/**
 * @brief Retrieve an Edge's decoration.
 * @memberof Edge
 *
 * @details This will retrieve a pointer to the decoration data that
 * was attached to an Edge when it was created. In the normal case,
 * the decoration consists of an optional label string.
 *
 * @param[in] self A pointer to an Edge instance.
 * @param[out] opt_decoration_size An optional buffer where the size,
 *	in bytes, of the returned decoration data will be stored when
 *	this function returns.
 *
 * @return A pointer to a mutable buffer containing the Edge's
 * decoration data. This buffer is garanteed to be at least as large
 * as what is reported in @a opt_decoration_size. In the case of an
 * error, this function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 * - ENOENT: The Edge has no decoration data.
 * - EINVAL: The Edge instance is invalid.
 */
void* Edge_getDecoration( const Edge* self, size_t* opt_decoration_size ) ;

/**
 * @class Graph
 * @brief Class expressing a graph of arbitrary data.
 */
typedef struct _graph {
	char		name[ 257 ] ;
	size_t		num_partitions ;
	char		reserved[ sizeof( HashMap )*2 + sizeof( void* )*2 ] ;
} Graph ;

/**
 * @brief Initialize a pre-allocated Graph instance.
 * @memberof Graph
 *
 * @details This will initialize the specified pre-allocated Graph
 * instance. The graph will be assigned the specified name, and will
 * be initialized to have the specified number of partitions (>1 if
 * this is a k-partite graph).
 *
 * @pre The graph instance must have been allocated, but @e not
 * previously initialized; this may cause memory leaks. Alternatively,
 * a Graph can be re-initialized if it was cleaned up before invoking
 * the initializer.
 *
 * @see Graph_cleanup
 *
 * @post The graph will be initialized as a k-partite graph, for some
 * factor k > 0, with an initially empty vertex end edge sets.
 *
 * @param[in] self A pointer to the memory location which holds the
 *	Graph instance to initialize.
 * @param[in] name A constant character string that will be used as
 *	the graph's name. This string must be @c NULL-terminated. A
 *	maximum of the first 256 characters of this string will be
 *	used.
 * @param[in] k The number of partitions that the graph will have (the
 *	graph will be k-partite).
 * @param[in] opt_hashVertex A pointer to an optional function which
 *	calculates the hash of vertices in the Graph. The hash value
 *	will be used to order the vertices in the Vertex Set.
 *
 * @retval 0 The Graph was initialized successfully.
 * @retval -1 An error occurred while initializing the graph; @e errno
 *	will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The Graph instance or the name are invalid.
 * - ENOMEM: There was not enough system memory to allocate the
 *   internal graph members.
 */
int Graph_init( Graph* self, const char* name, size_t k, HashMap_hashFunc* opt_hashVertex ) ;

/**
 * @brief Cleanup a graph instance.
 * @memberof Graph
 *
 * @details This will cleanup all the internal data structures
 * associated with the current Graph instance.
 *
 * @param[in] self A pointer to a Graph instance to clean up.
 */
void Graph_cleanup( Graph* self ) ;

/**
 * @brief Create a Vertex and add it to a Graph.
 * @memberof Graph
 *
 * @details This will create a new Vertex instance which encapsulates
 * the specified data, and add it to the current graph. The vertex
 * will own a shallow copy of the data provided data buffer as the
 * body of the Vertex.
 *
 * @param[in] self A pointer to a Graph instance.
 * @param[in] k The zero-based index of the partition to which the
 *	edge will be added.
 * @param[in] data A pointer to a buffer containing the graph data.
 * @param[in] data_size The size, in bytes, of the data in the buffer.
 *
 * @retval >0 The referent that uniquely identifies the successfully
 *	created vertex.
 * @retval -1 An error prevented the creation of a Vertex; @e errno
 *	will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The graph instance is invalid.
 * - ERANGE: The specified partition does not exist in the graph.
 */
int Graph_createVertex( Graph* self, size_t k, const void* data, size_t data_size ) ;

/**
 * @brief Find the referent of a vertex in a Graph.
 * @memberof Graph
 *
 * @details This will determine the referent of the Vertex in the
 * current Graph which corresponds with the specified data
 * payload. Internally this will use the Vertex_hash function,
 * provided when the Graph was initialized, to determine the
 * referent.
 *
 * @param[in] self A pointer to the current Graph instance.
 * @param[in] vertex_data A buffer containing the data payload of the
 *	Vertex to find.
 *
 * @return The referent of a Vertex in the Graph which corresponds
 * with the specified payload data.
 */
size_t Graph_findVertex( const Graph* self, const void* vertex_data ) ;

/**
 * @brief Add an edge to the graph.
 * @memberof Graph
 *
 * @details This will add a directed edge connecting the Vertex
 * corresponding with the referent @a v1_ref to the Vertex with the
 * referent @a v2_ref. The edge will optionally be decorated with the
 * specified label. This function does not verify any conformity
 * relations when adding edges. Therefore it is possible to add an
 * edge connecting two nodes in the same partition, violating the
 * k-partite property of the graph.
 *
 * @see Graph_addDecoratedEdge
 *
 * @param[in] self A pointer to a Graph instance.
 * @param[in] v1_ref The referent of the initial Vertex.
 * @param[in] v2_ref The referent of the final Vertex.
 * @param[in] label An optional label to associate with the edge.
 *
 * @retval 0 The edge was added successfully.
 * @retval -1 An error occurred while adding the edge; @e errno will
 *	be set accordingly.
 *
 * @par Errors
 * - EINVAL: The graph is invalid.
 * - ENOENT: One or both of the referents does not correspond with a
 *   valid Vertex.
 * - ENOMEM: There was not enough memory to allocate the edge.
 */
int Graph_addEdge( Graph* self, size_t v1_ref, size_t v2_ref, const char* label ) ;

/**
 * @brief Add a decorated edge to the graph.
 * @memberof Graph
 *
 * @details This will add a directed edge connecting the specified
 * end-points, and attach a copy of the specified data to the edge
 * instance. The attached data is referred to as the Edge
 * decoration. In the normal case, the decoration consists only of a
 * textual label for the edge. This function does not verify any
 * conformity relations when adding edges. Therefore it is possible to
 * add an edge connecting two nodes in the same partition, violating
 * the k-partite property of such a Graph.
 *
 * @see Graph_addEdge
 *
 * @param[in] self A pointer to a Graph instance.
 * @param[in] v1_ref The referent of the initial Vertex.
 * @param[in] v2_ref The referent of the final Vertex.
 * @param[in] decoration A pointer to an immutable buffer containing
 *	the decoration data to attach to the edge.
 * @param[in] decoration_size The size, in bytes, of the decoration.
 *
 * @retval 0 The edge was added successfully.
 * @retval -1 An error occurred while adding the edge; @e errno will
 *	be set accordingly.
 *
 * @par Errors
 * - EINVAL: The graph is invalid.
 * - ENOENT: One or both of the referents does not correspond with a
 *   valid Vertex.
 * - ENOMEM: There was not enough memory to allocate the edge.
 */
int Graph_addDecoratedEdge( Graph* self, size_t v1_ref, size_t v2_ref, const void* decoration, size_t decoration_size ) ;

/**
 * @brief Retrieve the neighbourhood of the specified vertex.
 * @memberof Graph
 *
 * @details This will retrieve an Iterator which can be used to
 * enumerate the outgoing edges of Vertex in the current graph. The
 * Iterator can also be used to traverse all edges in the edge set
 * that follow the first neighbour edge in sort order. A specialized
 * iteration function is exposed to enumerate only the neighbourhood
 * edges.
 *
 * @see Graph_NextNeighbour
 *
 * @param[in] self A pointer to the current Graph instance.
 * @param[in] referent The referent of a Vertex instance in the
 *	current Graph.
 *
 * @return An iterator referencing the first Edge in the neighbourhood
 * of the specified Vertex. In the case of an error, the Iterator will
 * be @c NULL and @e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: An invalid Graph or Vertex was provided.
 * - ENOENT: There are no edges in the Vertex's neighbourhood.
 * - ERANGE: The vertex v is not a member of the Graph.
 */
Iterator Graph_getNeighbourhood( const Graph* self, size_t referent ) ;

/**
 * @brief Iterate to the next neighbour of a Vertex.
 * @memberof Graph
 *
 * @details This function will return the next neighbour of the
 * specified vertex referenced by the iterator, then proceed the
 * Iterator to the next edge. If the Iterator does not reference any
 * more edges outgoing from Vertex, it will not be advanced.
 *
 * @param[in] i A pointer to an Iterator that references edges in the
 *	Graph to which the specified Vertex belongs.
 * @param[in] v_ref A referent associated with the Vertex whose
 *	neighbourhood is being enumerated.
 *
 * @return A pointer to an immutable Edge instance which leads to a
 * neighbour of the specified Vertex. If no such edge exists, this
 * function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 * - ENOENT: There are no more neighbour edges for the specified
 *   Vertex.
 * - ERANGE: The Vertex specified by v_ref is not a member of the
 *   graph to which the edges belong.
 */
const Edge* Graph_NextNeighbour( Iterator* i, size_t v_ref ) ;

/**
 * @brief Get an Iterator to enumerate all the vertices in the Graph.
 * @memberof Graph
 *
 * @details This will retrieve an Iterator that references the first
 * Vertex in the Vertex Set, V, of the current Graph. This Iterator
 * can be used to enumerate all of the vertices therein using the
 * Graph_NextVertex() primitive. This function is slightly different
 * than that used to get the neighbourhood of a Vertex; iterating with
 * the Iterator returned by this function is guaranteed to visit every
 * single Vertex in the Graph in sorted order.
 *
 * @see Graph_NextVertex
 *
 * @param[in] self A pointer to an immutable Graph instance,
 *
 * @return An Iterator referencing the first Vertex in the current
 * Graph's Vertex Set. In the case of an error, this function returns
 * a @c NULL Iterator and sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The Graph is invalid.
 */
Iterator Graph_getFirstVertex( const Graph* self ) ;

/**
 * @brief Iterator to the next Vertex.
 * @memberof Graph
 *
 * @details This function will advance the specified Iterator,
 * returning a pointer to the associated vertex data.
 *
 * @param[in,out] i A pointer to the Iterator instance referencing the
 *	current Vertex.
 * @param[out] vertex_size The size, in bytes, of the vertex data
 *	structure referenced by the iterator before it was advanced.
 *
 * @return A pointer to immutable Vertex data.
 */
const void* Graph_NextVertex( Iterator* i, size_t* vertex_size ) ;

/**
 * @brief Get the order of the current Graph.
 * @memberof Graph
 *
 * @details This will report the order of the current Graph. A Graph's
 * order is the number of Vertices therein.
 *
 * @param[in] self A pointer to an immutable Graph instance.
 *
 * @return The order of the graph. In the case of an error, this
 * function returns -1 and sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The graph is invalid.
 */
int Graph_getOrder( const Graph* self ) ;

#endif	/* __GRAPH_H__ */
