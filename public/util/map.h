/* -*- Mode: C -*- */
/*
 * map.h - Declares a template ADT for a Map.
 *
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef __MAP_H__
#define __MAP_H__

#include "list.h"

/**
 * @class Mapping
 * @brief Class which expresses a weighted key-value pair.
 *
 * @details The Mapping class encapsulates a key-value pair used by
 * the Map container.
 */
typedef struct _mapping {
	void*		key ;
	void*		value ;
	int32_t		weight ;
	char		buffer[ 0 ] ;
} Mapping ;

/**
 * @brief Accessor to retrieve the key element of a Mapping.
 * @memberof Mapping
 *
 * @details Retrieves the anonymously typed key element of the current
 * Mapping instance. The type of the key element is not specified and
 * must be known a priori for it to be useful.
 *
 * @param[in] self A pointer to a Mapping instance.
 *
 * @return A pointer to the key element of the current Mapping
 * instance. This pointer can be cast to an appropriate type. The
 * ownership of the memory returned by this function is retained by
 * the callee and must not be deleted by the caller.
 */
void* Mapping_getKey( const Mapping* self ) ;

/**
 * @brief Accessor to retrieve the value element of a Mapping.
 * @memberof Mapping
 *
 * @details Retrieves the anonymously typed value element of the
 * current Mapping instance. The type of the value element is not
 * specified and must be known a priori for it to be useful.
 *
 * @param[in] self A pointer to a Mapping instance.
 *
 * @return A pointer to the value element of the current Mapping
 * instance. This pointer can be cast to an appropriate type. The
 * ownership of the memory returned by this function is retained by
 * the callee and must not be deleted by the caller.
 */
void* Mapping_getValue( const Mapping* self ) ;

/**
 * @brief Get the weight associated with the current pair.
 * @memberof Mapping
 *
 * @details This function retrieves the weight that was associated
 * with the current Mapping instance. The meaning of the weight is left
 * of to the caller.
 *
 * @param[in] self An immutable pointer to a Mapping instance.
 *
 * @return A signed integer expressing the weight associated with the
 * current pair.
 */
int32_t Mapping_getWeight( const Mapping* self ) ;

/**
 * @brief Set the weight associated with the current Mapping.
 * @memberof Mapping
 *
 * @details This will associated a signed integer weight to the
 * current Mapping instance.
 *
 * @param[in,out] self A pointer to a Mapping instance.
 * @param[in] weight A 32-bit signed integer that will be assigned to
 *	the Mapping as its weight.
 */
void Mapping_setWeight( Mapping* self, int32_t weight ) ;

/******************************************************************************
 * Map_Iterator
 ******************************************************************************/

/**
 * @class Map_Iterator
 * @extends Iterator
 * @brief Class describing an iterator over Mappings in a Map
 *	container.
 *
 * @details Instances of this class provide a mechanism for iterating
 * over the mappings contained in a Map collection.
 */
typedef Iterator Map_Iterator ;

/**
 * @memberof Map_Iterator
 * @brief Advance the Map_Iterator to the next Mapping.
 *
 * @details This will advance the current Map_Iterator instance to the
 * next item in the associated Map, returning the Mapping to the
 * caller. If the iterator is beyond the last Mapping defined in the
 * Map, it will not be advanced any further. The order in which the
 * Iterator traverses the Map is not necessarily the order of the
 * Mappings. To get the Mappings in order, use the getItem() member.
 *
 * @see List_getItem
 *
 * @param[in,out] self A pointer to a Map_Iterator instance. The
 *	iterator will be advanced to the next mapping following a call
 *	to this function.
 *
 * @return A pointer to the Mapping associated with the current
 * Iterator. If the Iterator is passed the last item in the Map, this
 * function will return @c NULL.
 */
Mapping* Map_Iterator_next( Map_Iterator* self ) ;

/******************************************************************************
 * Map
 ******************************************************************************/

/**
 * @class Map
 * @extends List
 * @brief Container expressing a weighted Map.
 *
 * @details The map is an associative array which manages a set of
 * key-value pairs. Fundamentally, a Map is a Set whose members are
 * Mappings which relate keys to their associated values. The keys in
 * the Map must be unique. However, there is no such restriction for
 * the values. By default, the Map is sorted by key. However, it is
 * possible to specify a custom comparator to determine the partial
 * ordering of mappings.
 *
 * @see Set_Comparator
 * @see Map_Create
 */
typedef List Map ;

/**
 * @brief Create an empty Map container.
 * @memberof Map
 *
 * @details This will create a new, initially empty, Map container. A
 * comparator function can optionally be specified which will be used
 * to determine the order of the Mappings contained within. If no
 * comparator is provided, the default behaviour is to order the
 * Mappings using a byte-for-byte comparison of the keys.
 *
 * @param[in] opt_comparator An optional comparator function to use to
 *	determine the order of Mappings in the container.
 *
 * @see Set_Comparator
 *
 * @return A new instance of an empty Map container created on the
 * heap. The ownership of the memory returned by this function is
 * transferred to the caller who is responsible for deleting it.
 *
 * @see Set_Create
 */
Map* Map_Create( Set_Comparator opt_comparator ) ;

/**
 * @brief Add a new key-value pair to the Map.
 * @memberof Map
 *
 * @details This function will search the current Map for an entry
 * that matches @a key. If such an item is found, its value will be
 * replaced by the specified @a value; any associated weight will be
 * retained. Otherwise, a new key-value pair will be added to the Map
 * with a default weight of zero. Any previously allocated value will
 * not be deleted.
 *
 * @param[in,out] self A pointer to a Map container instance to which
 *	the specified Mapping will be added.
 * @param[in] key A pointer to the key of the Mapping pair. The
 *	ownership of the memory is retained by the caller.
 * @param[in] value A pointer to the value item of the Mapping
 *	pair. The ownership of the memory is retained by the caller.
 *
 * @return If the creation and insertion of the Mapping was
 * successful, this function returns zero. Otherwise a negative value
 * will be returned and errno will be set to indicate the reason for
 * the failure.
 *
 * @par Errors:
 * - @e EINVAL: One of the arguments is invalid.
 * - @e ENOMEM: Allocation of the Mapping failed.
 */
int Map_add( Map* self, const void* key, void* value ) ;

/**
 * @brief Swap the mappings corresponding with the specified keys.
 * @memberof Map
 *
 * @details This will swap the key-value pairs corresponding with the
 * keys specified as arguments, effectively changing the order of the
 * mappings. In other words, if the mapping for @p key1 occurs before
 * the mapping for @p key2 in the current Map instance, following this
 * function call, their order will be reversed. All other mappings
 * will be unchanged. The following example illustrates this:
 *
 * > Map = [(a, 1), (b, 2), (c, 3), (d, 4)]
 *
 * By swapping mappings with keys a and c, the map will be re-ordered
 * to:

 * > Map = [(c, 3), (b, 2), (a, 1), (d, 4)]
 *
 * @pre Mappings for both keys @e must exist in the Map. Otherwise the
 * Map will be unchanged following a call to this function.
 *
 * @param[in,out] self A pointer to a Map instance. The order of the
 *	map will be updated following this function call.
 * @param[in] key1 The key of the first mapping to swap.
 * @param[in] key2 The key to the second mapping to swap.
 */
void Map_swapOrder( Map* self, void* key1, void* key2 ) ;

/**
 * @brief Move a mapping to the front of the Map.
 * @memberof Map
 *
 * @details This will shift the mapping, corresponding with the
 * specified @p key, to the front of the Map. This will result in the
 * mapping for @p key to occur first in the Map.
 *
 * @pre A mapping for the specified @p key @e must exist in the Map,
 * otherwise the order will be unchanged.
 *
 * @param[in,out] self A pointer to a Map instance. The order of the
 *	map will be updated following a call to this function.
 * @param[in] key A key corresponding with the mapping to shift to the
 *	front of the Map.
 */
void Map_shiftToFront( Map* self, const void* key ) ;

/**
 * @brief Get an Iterator to traverse all mappings.
 * @memberof Map
 *
 * @details This will retrieve an Iterator over the members of the
 * current Map instance. This Iterator provides a mechanism for
 * iterating over all the mappings defined in the Map.
 *
 * @param[in] self A pointer to an immutable Map instance.
 *
 * @return A Map_Iterator instance that is located just before the
 * first Mapping in the current Map collection.
 */
Map_Iterator Map_getFirst( const Map* self ) ;

/**
 * @brief Get the value associated with the specified key.
 *
 * @details Search the current Map for the value that corresponds with
 * the specified @a key.
 *
 * @param[in] self A pointer to a Map instance.
 * @param[in] key The key of the value to retrieve from a Map.
 *
 * @return A pointer to the value entry that corresponds with the
 * specified @a key. If no such item exists in the Map, this function
 * returns a @c NULL pointer.
 *
 * @memberof Map
 */
void* Map_getValue( Map* self, const void* key ) ;

/**
 * @brief Set the weight of a key-value pair in the Map.
 *
 * @details This will assign a @a weight to the mapping that
 * corresponds with the specified @a key in the current Map
 * instance. If no such entry is found, this function has no effect.
 *
 * @param[in] self A pointer to a Map instance.
 * @param[in] key The key of the mapping whose weight will be updated.
 * @param[in] weight An signed integer value to assign as a weight.
 *
 * @memberof Map
 */
void Map_setWeight( Map* self, const void* key, int32_t weight ) ;

/**
 * @brief Get the weight associated with a key-value pair in the Map.
 *
 * @details This will retrieve the weight assocaited with a mapping
 * that corresponds with the specified @a key.
 *
 * @param[in] self A pointer to a Map instance.
 * @param[in] key The key of a mapping in the current Map instance.
 *
 * @return The weight associated with the mapping specified by
 * @a key. If no such mapping exists, this function returns 0.
 *
 * @memberof Map
 */
int32_t Map_getWeight( Map* self, const void* key ) ;

#endif	/* __MAP_H__ */
