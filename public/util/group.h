/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of a Group class.
 *
 * @details This file provides the declaration of a Group class which
 * is used to express a abstract combinatorial group of arbitrary
 * data.
 */

#ifndef __GROUP_H__
#define __GROUP_H__

/* POSIX includes */
#include <stdlib.h>

/* Local includes */
#include <util/hashmap.h>

/******************************************************************************
 * Relation
 ******************************************************************************/

/**
 * @class RelationSet
 * @brief Set of relations which are used to present a Group.
 * @extends HashMap
 *
 * @details This type is a container expressing a set of relations
 * that can be used, along with a set of generator states, to define
 * the set of elements in a group. A single relation is a mapping from
 * one item to another; its image.
 *
 * A RelationSet may contain transitive relations. In this case, To
 * obtain the final image of an item, its image must be retrieved,
 * then the image of the image, and so on until the final image is
 * retrieved. This is inefficient, and therefore adding relations in
 * this way is discouraged; this should be expressed by a single
 * relation which maps the item to the final image value.
 */
typedef struct _relation_set RelationSet ;

/**
 * @brief Create a new set of relations.
 * @memberof RelationSet
 *
 * @details This will allocate a new RelationSet instance to express
 * an initially empty set of binary Relations. Each entry of this set
 * expresses a mapping from an item to an image. Relations in the set
 * are indexed by their item. A hash function must be provided which
 * maps the Relation items contained in the set to a unique
 * index. This hash function will be used to order the elements
 * internally and for applying relations.
 *
 * @note Each item can be in only one relation in the set. However, an
 * image may appear in the set multiple times.
 *
 * @param[in] hashItem A pointer to a function used to determine the
 *	hash of a relation item. This function must provide a unique
 *	hash for each item of the binary relations that will be added
 *	to the Set. If this criteria is not satisfied, some relations
 *	will be dropped from the set (i.e. the set may be incomplete).
 *
 * @return A new instance of an initially empty RelationSet. The
 * ownership of the memory returned by this function is transferred to
 * the caller who is responsible for deleting it. In the case of an
 * error, this function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The hash function is invalid.
 * - ENOMEM: There was not enough system memory to create the relation
 *	set.
 */
RelationSet* RelationSet_Create( HashMap_hashFunc* hashItem ) ;

/**
 * @brief Add a binary relation to the relation set.
 * @memberof RelationSet
 *
 * @details Add a relation to the relation set. The new relation will
 * map an item to an image. Typically the item and its image will be
 * expressed using the same data type, however, this is not required
 * by the RelationSet.
 *
 * @param[in] self A pointer to a RelationSet to which a new relation
 *	will be added.
 * @param[in] item An opaque pointer to the item data. The type of
 *	this data is not specified.
 * @param[in] image An opaque pointer to the image data of the
 *	relation. The type of this data is not specified.
 *
 * @retval 0 The new relation was added successfully.
 * @retval -1 An error prevented the new relation from being added to
 *	the set; @e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The relation set is invalid or item are invalid.
 * - ENOMEM: There was not enough system memory to allocate the
 *   relation.
 */
int RelationSet_addRelation( RelationSet* self, const void* item, const void* image ) ;

/**
 * @brief Find the image of an item.
 * @memberof RelationSet
 *
 * @details This will search the current RelationSet for a relation
 * corresponding with the specified item. If such a relation is found,
 * the corresponding image will be returned. Otherwise the item will
 * not be mapped.
 *
 * @param[in] self An immutable  pointer to a relation set.
 * @param[in] item An immutable pointer to an item's data.
 *
 * @return A pointer to the data of the image of the specified item in
 * the relation set. If no relation exists for the specified item, the
 * @a item itself will be returned. In the case of an error, this
 * function returns @c NULL and sets @e errno accordingly.
 */
const void* RelationSet_findImage( const RelationSet* self, const void* item ) ;

/******************************************************************************
 * Group
 ******************************************************************************/

/**
 * @class Group
 * @brief Class expressing a combinatorial group structure.
 *
 * @details Instances of this class are presentations of a group by a
 * set of generators and relations. Every element in the group can be
 * expressed as a product of the generators, using the group law,
 * constrained by a set of relations on those generators.
 */
typedef struct _group Group ;

/**
 * @brief Prototype of the group law.
 * @memberof Group
 *
 * @details This type defines the prototype expected of functions
 * which implement the group law of a combinatorial Group
 * instance. This function is expected to map a pair of items, members
 * of the Group, to a third Group element.
 *
 * @param[in] self A pointer to a Group instance on which the law is
 *	defined.
 * @param[in] item1 An abstract pointer to the first operand of the
 *	operation.
 * @param[in] item2 An abstract pointer to the second operand of the
 *	operation.
 *
 * @return A pointer to an element which is the result of applying the
 * operation expressed by this function to the input elements. If an
 * error occurs while applying the operation, this function should
 * return @c NULL and set @e errno accordingly.
 */
typedef void* Group_Law( const Group* self, const void* item1, const void* item2 ) ;

/**
 * @brief Create the presentation of a Group.
 * @memberof Group
 *
 * @details This will allocate a new Group instance defined by the
 * presentation specified by the @a generators and @a relations
 * provided as arguments.
 *
 * @param[in] generators A set of generators that define the elements
 *	of the group.
 * @param[in] relations A set of relations among the elements of the
 *	generators which describe the constraints of the presentation.
 * @param[in] identity An abstract pointer to an item that serves as
 *	the group identity.
 * @param[in] product The operation which defines the group
 *	law. This operation is used to determine the product of the
 *	generators.
 *
 * @return A new Group instance which expresses the combinatorial
 * group defined by the specified @a generators and @a relations. The
 * ownership of the memory returned by this function is transferred to
 * the caller who is responsible for deleting it. In case of an error,
 * this function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: Invalid generators, relations, or group law.
 * - ENOMEM: There was not enough memory to allocate the group.
 */
Group* Group_Create( const Set* generators, const RelationSet* relations, const void* identity,  Group_Law* product ) ;

/**
 * @brief Compose a new element from the specified operands.
 * @memberof Group
 *
 * @details This will apply the group law to compose the specified
 * items, returning the product.
 *
 * @param[in] self A pointer to the current Group instance.
 * @param[in] e1 An abstract pointer to the first element in the
 *	composition.
 * @param[in] e2 An abstract pointer to the second element in the
 *	composition
 *
 * @return A pointer to an element that expresses the product of the
 * specified operands. The ownership of the memory returned by this
 * function is transferred to the caller who is responsible for
 * deleting it. In the case of an error, this function returns @c NULL
 * and sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The group instance is invalid.
 */
void* Group_compose( const Group* self, const void* e1, const void* e2 ) ;

/**
 * @brief Reduce the specified element.
 * @memberof Group
 *
 * @details This will reduce the specified Group element to its
 * simplest form. The rules for how elements are reduced are specified
 * by the relation set associated with the group.
 *
 * @param[in] self A pointer to a group instance.
 * @param[in] elem A pointer to an element to reduce.
 *
 * @return A pointer to an immutable Group element which expresses the
 * most reduced form of the input item. If the item is already in its
 * most reduced form, the item itself is returned.
 */
const void* Group_reduce( const Group* self, const void* elem ) ;

/**
 * @brief Retrieve the first generator in the group.
 * @memberof Group
 *
 * @details This will retrieve an Iterator which references the first
 * generator that defines the current group. This Iterator can be used
 * to enumerate all the generators.
 *
 * @param[in] self A pointer to an immutable group instance.
 *
 * @return An Iterator position just before the first generator.
 */
Iterator Group_firstGenerator( const Group* self ) ;

/**
 * @brief Get the group identity.
 * @memberof Group
 *
 * @details This will retrieve the item which expresses the identity
 * element of the current Group. This element must satisfy the
 * relation whereby the application of the Group law to the identity
 * element and any other element, a, will yield element a.
 *
 * @param[in] self An immutable pointer to a Group instance.
 *
 * @return An abstract pointer to a Group element which expresses the
 * identity of the group. In the case of an error, this function will
 * return @c NULL and set @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The group is invalid.
 */
const void* Group_getIdentity( const Group* self ) ;

#endif	/* __GROUP_H__ */
