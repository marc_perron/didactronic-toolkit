/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <stdlib.h>

/* Local includes */
#include "util/list.h"

/**
 * @file
 * @brief Declaration of a HashMap container type.
 *
 * @details This file provides an implementation of a HashMap
 * container which is a specialization of a List which contains
 * pointers to sparse data using the hash of the items as the index
 * into the List. Each List entry is a slice of the super Set of the
 * List instance.
 */

#ifndef __HASH_MAP_H__
#define __HASH_MAP_H__

/**
 * @class HashMap
 * @brief Type representing a HashMap container
 * @extends List
 *
 * @details Instances of this class are used to define a generic
 * HashMap container which indexes its contents via the specified key
 * value. The HashMap is a specialization of a List container with
 * each list entry expressing a sub-set of the HashMap. If the maximum
 * sub-set size is \f$m\f$, then the search operation will have
 * \f$O(m)\f$ complexity in the worst case.
 *
 * Insertion in the HashMap will have \f$O(n)\f$ complexity for a
 * HashMap containing \f$n\f$ elements.
 */
typedef struct {
	List_EXTEND ;
	char	reserved[ sizeof( void* ) + 2*sizeof( size_t ) ] ;
} HashMap ;

/**
 * @brief Prototype of hashing functions used by the HashMap.
 * @memberof HashMap
 *
 * @details This type expresses the prototype expected of functions
 * which calculate the hash value of entries in a HashMap. Hash
 * functions must generate an integer values for each entry in a
 * HashMap which should unique in the container. The uniqueness of the
 * hash value is not a strict requirement, however, higher probability
 * of collisions will result in a less efficient lookup procedure for
 * items within the HashMap.
 *
 * @param[in] entry An abstract pointer to an immutable HashMap entry.
 *
 * @return An unsigned integer expressing the hash of the specified
 * entry. This integer is guaranteed to be useable as an index into an
 * array.
 */
typedef size_t HashMap_hashFunc( const void* entry ) ;

/**
 * @brief Macro used to extend the HashMap class.
 *
 * @details Use this macro to declare classes that derive from the
 * HashMap class. This macro must be the first item in the structure
 * declaration, followed by derived attributes. This macro ensures
 * that the memory layout of the derived structure corresponds with
 * the base HashMap class.
 */
#define HashMap_EXTEND HashMap _HashMap ;
struct _hashmap {
	List_EXTEND ;
	/**
	 * @brief A pointer to the hash function used by the map.
	 * @memberof HashMap
	 */
	HashMap_hashFunc*	hash ;
	size_t			slice_size ;
	char			HashMap_reserved[ sizeof( size_t ) ] ;
} ;

/**
 * @brief Create a new HashMap instance.
 * @memberof HashMap
 *
 * @details This function will allocate a new HashMap instance, on the
 * heap, to store entries of an arbitrary type. A function can
 * optionally be specified to determine the hash of the HashMap
 * entries. If no hash function is specified, a default will be
 * used. Similary, a function to determine the partial order of
 * HashMap elements can be provided. If none is specified, the default
 * Set partial order function will be used.
 *
 * @see HashMap_hash
 *
 * @note It is important that the partial order of the hash functions
 * is isometric to the partial order of the HashMap elements.
 *
 * @param[in] opt_hashImpl A pointer to an optional function which
 *	will be used to calculate the hash of the container
 *	entries. If this argument is @c NULL, the default hash
 *	function will be used.
 * @param[in] opt_comparator A pointer to an optional comparator
 *	function which will be used to determine the partial order of
 *	entries in the HashMap. If this argument is @c NULL, the
 *	default comparator will be used.
 *
 * @return A new HashMap instance, created on the heap, initialized
 * with an appropriate hashing function. The ownership of the memory
 * returned by this function is transferred to the caller who is
 * responsible for deleting it. If a new hash map cannot be created,
 * this function will return @c NULL and set @e errno accordingly.
 *
 * @par Errors
 * - ENOMEM: There was not enough system memory to allocate the
 *   container.
 */
HashMap* HashMap_Create( HashMap_hashFunc* opt_hashImpl, Set_Comparator* opt_comparator ) ;

/**
 * @brief Initialize a pre-allocated HashMap.
 * @memberof HashMap
 *
 * @details This function will initialize the member data of a HashMap
 * instance that was previously allocated. This function can be used
 * to initialize HashMaps allocated on the stack.
 *
 * @note This function should not be called to reset the HashMap
 * instance. This will cause memory to leak since it does not clear
 * the HashMap before initializing it.
 *
 * @pre The HashMap instance must be pre-allocated, and previously
 * un-used.
 * @post The HashMap will be initialized with the provided hash and
 * comparator functions. Initially, the HashMap will be empty and have
 * a density of up to 4096 entries per hash.
 *
 * @param[in] self A pointer to a HashMap instance that will be
 *	initialized.
 * @param[in] opt_hash An optional pointer to a hash function
 *	impelentation that will be used by the current HashMap
 *	instance. If no hash function is provided, the default hash
 *	will be used. It is essential that the hashing function is
 *	isometric to the comparator. Meaning that partial order is
 *	preserved.
 * @param[in] opt_compareEntries An optional pointer to a comparison
 *	function which will be used to determine the partial order of
 *	the entries. If no function is provided, the default Set
 *	comparator will be used.
 */
void HashMap_init( HashMap* self, HashMap_hashFunc* opt_hash, Set_Comparator* opt_compareEntries ) ;

/**
 * @brief Delete a HashMap instance.
 * @memberof HashMap
 *
 * @details This function will cleanup a HashMap instance that was
 * previously created on the heap. Use this function to ensure that
 * the HashMap and its contents are properly cleaned up.
 *
 * @see Set_Delete
 * @see HashMap_Create
 *
 * @param[in,out] self_ptr The address of a pointer to a HashMap
 *	instance to delete. The addressed pointer will be set to
 *	@c NULL when this function returns.
 */
void HashMap_Delete( HashMap** self_ptr ) ;

/**
 * @brief Hash the specified entry.
 * @memberof HashMap
 *
 * @details This function will calculate the hash of the specified
 * entry using the hashing method associated with the current HashMap
 * container. The current HashMap instance is used to provide the
 * necessary parameters required to determine the hash of the entry.
 *
 * @param[in] self A pointer to an immutable HashMap instance.
 * @param[in] entry An abstract immutable pointer to a buffer
 *	containing a HashMap entry whose hash will be calculated by
 *	this function.
 *
 * @return An integer value which is expresses the hash of the
 * specified entry. This integer is guaranteed to be usable as an
 * array index.
 */
size_t HashMap_hash( const HashMap* self, const void* entry ) ;

/**
 * @brief Add an item to a hash map.
 * @memberof HashMap
 *
 * @details This function will add a new item to the current HashMap
 * instance by first calculating the item's hash (using the map's hash
 * function), adding the item to the container, then adding the
 * appropriate entry to the index.
 *
 * @param[in] self A pointer to a HashMap collection instance.
 * @param[in] item An abstract pointer to the memory buffer where the
 *	item to add to the HashMap is located.
 * @param[in] item_size The size, in bytes, of the item to add to the
 *	HashMap.
 *
 * @retval 0 The item was successfully added to the HashMap.
 * @retval -1 An error prevented the insertion of the specified item
 *	into the HashMap; @e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The HashMap instance is invalid.
 * - ENOMEM: There wasn't enough system memory to add an item to the
 *   map.
 */
int HashMap_addItem( HashMap* self, const void* item, size_t item_size ) ;

/**
 * @brief Retrieve the items for a given hash.
 * @memberof HashMap
 *
 * @details This function will, given a hash value, retrieve an
 * Iterator to the first item which corresponds with the hash. This
 * Iterator can be used to traverse the values starting from the hash.
 *
 * @param[in] self A pointer to an immutable HashMap instance.
 * @param[in] hash The hash of the item to retrieve from the map.
 *
 * @return An Iterator referencing the first item with a hash
 * corresponding with the given value. This Iterator can be used to
 * walk the elements to find the desired entry which corresponds with
 * the supplied hash. If no such entry is found, the Iterator returned
 * will be @c NULL.
 */
Iterator HashMap_getItem( const HashMap* self, size_t hash ) ;

#endif	/* __HASH_MAP_H__ */
