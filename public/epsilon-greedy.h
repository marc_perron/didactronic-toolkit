/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Declaration of a Epsilon Greedy Policy class.
 */

#ifndef __EPSILON_GREEDY_H__
#define __EPSILON_GREEDY_H__

#include "policy.h"
#include "task.h"

/******************************************************************************
 * EpsilonGreedy
 ******************************************************************************/

/**
 * @class EpsilonGreedy
 * @brief Opaque type representing an @f$\epsilon@f$-greedy Policy.
 * @extends Policy
 *
 * @details This class is used to model a policy for an arbitrary
 * reinforcement learning Task which will randomly choose an Action
 * with probability @f$\epsilon@f$, and greedily choose an Action
 * based on its estimated value with probability @f$1-\epsilon@f$.
 */
typedef struct _epsilon_greedy EpsilonGreedy ;

/**
 * @brief Create an @f$\epsilon@f$-greedy Policy instance on the heap.
 * @memberof EpsilonGreedy
 *
 * @details This will create a task-specific greedy Policy which will
 * select a random Action with probability @f$\epsilon@f$. Setting a
 * value of @f$\epsilon@f$ equal to zero is a special case of the
 * Policy that will be strictly greedy without selecting exploratory
 * Actions.
 *
 * @param[in] task A pointer to an immutable Task instance expressing
 *	the task for which the Policy is valid. This is required
 *	because a greedy policy for one task may not be greedy for
 *	another.
 * @param[in] epsilon A fixed point value expressing the probability
 *	of selecting a random action rather than following the greedy
 *	Policy.
 *
 * @return A pointer to a new Policy instance created on the heap. The
 * ownership of the memory returned by this function is transferred to
 * the caller who is responsible for deleting it.
 */
Policy* EpsilonGreedy_Create( const Task* task, uint32_t epsilon ) ;

/**
 * @brief Apply the @f$\epsilon@f$-greedy Policy to a State.
 * @memberof EpsilonGreedy
 *
 * @details This will determine the Action proscribed by the current
 * @f$\epsilon@f$-greedy Policy in the specified State.
 *
 * @param[in] self A pointer to an immutable EpsilonGreedy Policy
 *	instance.
 * @param[in] s A pointer to a State for which the next Action will be
 *	selected by the Policy.
 *
 * @return A pointer to a State expressing the sequence of actions
 * proscribed by the current Policy in the specified State. This will
 * be a greedy Action with probability @f$1-\epsilon@f$. Otherwise the
 * Policy will select a random action sequence.
 */
const State* EpsilonGreedy_apply( const Policy* self, const State* s ) ;

#endif	/* __EPSILON_GREEDY_H__ */
