/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef __TASK_H__
#define __TASK_H__

#include "action.h"
#include "environment.h"
#include "state.h"

/******************************************************************************
 * Task
 ******************************************************************************/

/**
 * @brief Prototype of the Task-specific reward function.
 * @memberof Task
 *
 * @details This type expresses the prototype of functions to
 * calculate rewards associated with taking an @p action from the
 * specified @p initial State to reach the specified @p final
 * State. Derived Task types must provide implementations of this
 * function.
 *
 * @param[in] self A pointer to an immutable Task instance.
 * @param[in] initial A pointer to an immutable State instance
 *	expressing the initial State.
 * @param[in] action A pointer to an immutable State istance
 *	expressing the sequence of actions taken in the @p initial
 *	State.
 * @param[in] next A pointer to an immutable State instance expressing
 *	the next State reached by taking the specified @p action.
 *
 * @return A 32-bit signed integer expressing the Task-specific reward
 * associated with the specified state-action-state triple.
 */
typedef int32_t Task_GetReward_f( const void* self, const State* initial_state, const State* action, const State* final_state ) ;

/**
 * @class Task
 * @brief Abstract class expressing a reinforcement learning Task.
 *
 * @details This class provides a common interface that should be used
 * by implementations of domain specific reinforcement learning
 * tasks. A Task instance is comprised of a set of goals along with
 * the environment in which the Task is executed. A Task instance is
 * complete when the Agent executing it reaches one of the specified
 * goal States.
 */
#define Task_EXTEND Task	_Task
typedef struct _task {
	/**
	 * @brief A reference to the Task's Environment.
	 * @memberof Task
	 *
	 * @details This member points to an immutable Environment in
	 * which the current Task instance is defined.
	 */
	const Environment*		environment ;
	/**
	 * @brief A set of states expressing the Task's goals.
	 * @memberof Task
	 *
	 * @details This set is a subset of the Environment's State
	 * set which defines one or more goals to achieve in the
	 * current Task.
	 */
	const StateSet*			goals ;

	char				_reserved[ sizeof( void* ) ] ;
} Task ;

/**
 * @brief Create a new Task instance.
 * @memberof Task
 *
 * @details This constructor will allocate and initialize a new Task
 * definition on the specified Environment. The Task's goal set will
 * initially be empty.
 *
 * @param[in] env A pointer to the Environment on which the Task will
 *	be defined.
 *
 * @return A pointer to the newly allocate Task instance. The
 * ownership of the memory returned by this function is transferred to
 * the caller who is responsible for deleting it. In the case of an
 * error, this function returns @c NULL and sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The Environment argument is invalid.
 * - ENOMEM: There was not enough memory to allocate a new Task.
 */
Task* Task_Create( Environment* env ) ;

/**
 * @brief Calculate the Task-specific reward.
 * @memberof Task
 *
 * @details This is a virtual function which calculates the reward
 * associated with the specified state-action-state
 * triple. Implementations of this function @e must, given an
 * @p initial State and an @p action, along with the @p next state
 * which results from taking the specified Action, calculate the
 * expected reward of the step. This corresponds with the
 * @f$\mathscr{R}_{ss'}^{a}@f$ term of the Bellman equation. The
 * expected reward is expressed mathematically as:
 *
 * @f[
 * \mathscr{R}_{ss'}^{a} = E\{ r_{t+1} \mid s_{t} = s, a_{t} = a, s_{t+1} = s' \}
 * @f]
 *
 * The default implementation of this function will associate a
 * positive reward to cases where the initial State is *not* a goal,
 * and the final State is a goal. If both the inital and final States
 * are goals, then the reward will be zero. If only the initial State
 * is a goal, then the reward will be negative.
 *
 * This function, along with the transition probabilities, completely
 * specifies the most important aspects of the dynamics of a finite
 * Markov Decision Process (MDP).
 *
 * @param[in] self A pointer to an immutable Task instance for which
 *	the reward is calculated.
 * @param[in] initial A pointer to an immutable State instance
 *	expressing the initial State.
 * @param[in] action A pointer to an immutable State instance
 *	expressing the sequence of actions taken in the @p initial
 *	State.
 * @param[in] next A pointer to an immutable State instance expressing
 *	the resulting State from taking an @p action in the @p initial
 *	State.
 *
 * @return A 32-bit integer expressing the reward of the specified
 * State-Action-State triplet.
 */
int32_t Task_getReward( const Task* self, const State* initial, const State* action, const State* next ) ;

/**
 * @brief Determine if a State is a goal State.
 * @memberof Task
 *
 * @details This function will assert whether or not the specified
 * State is a goal for the current Task. The StateSet defining the
 * Task's goals will be searched for a State corresponding with
 * @a state. If the goals StateSet contains such an item, the state
 * is asserted to be a goal State.
 *
 * @note A goal State is a terminal State but a terminal State is not
 * necessarily a goal State.
 *
 * @param[in] self A pointer to the current Task instance.
 * @param[in] state A pointer to an immutable State instance that will
 *	be verified to determine if it is a goal State.
 *
 * @retval true The specified State is a goal of the Task.
 * @retval false The specified State is not a goal State.
 */
bool Task_isGoalState( const Task* self, const State* state ) ;

/**
 * @brief Determine if the current Task is complete.
 * @memberof Task
 *
 * @details This function will query the Task's Environment to
 * assert whether or not the current State is one of its goals.
 *
 * @pre A valid Task instance must be provided.
 *
 * @param[in] self A pointer to the current Task instance.
 *
 * @retval true The current Task is complete.
 * @retval false The current Task is incomplete.
 */
bool Task_isComplete( const Task* self ) ;

/**
 * @brief Add a goal to the current Task.
 * @memberof Task
 *
 * @details This function will add the specified goal State to the
 * current Task instance.
 *
 * @param[in] self A pointer to the Task instance whose goal set will
 *	be updated.
 * @param[in] goal A pointer to a State fron the Task's Environment
 *	which will be added to the set of goal States associated with
 *	the Task. The ownership of the goal is retained by the caller
 *	and will not be deleted by the Task.
 *
 * @retval 0 The goal State was successfully added to the Task
 *	definition.
 * @retval -1 An error occured while adding the goal state to the
 *	Task; @e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: Either the task definition or the specified goal State
 *   are invalid.
 */
int Task_addGoal( Task* self, const State* goal ) ;

/**
 * @brief The action-state value function for the current Task.
 * @memberof Task
 *
 * @details This will approximate the action-state value function
 * @f$Q(s,a)@f$ which is the expected return of taking an action @p a
 * in the specified initial State @p s. In the reinforcement learning
 * literature, the value of this function is expressed mathematically
 * as:
 *
 * @f{align*}{
 * Q^{\pi}(s,a) & = E_{\pi}\{r_{r+1} + \gamma V^{\pi}(s_{t+1}) \mid s_t = s, a_t = a \} \\
 *              & = \sum_{s'}{\mathscr{P}_{ss'}^{a}[\mathscr{R}_{ss'}^{a} + \gamma V^{\pi}(s')]}
 * @f}
 *
 * @see Environment_transitionProbability
 * @see Task_getReward
 * @see State_getValue
 *
 * @param[in] self A pointer to an immutable Task instance.
 * @param[in] s A pointer to the initial State instance.
 * @param[in] a A pointer to a State instance expressing an action
 *	sequence.
 *
 * @return A fixed-point value, quantized to 0.01, expressing the
 * expected return of taking the specified Action in the State @p s.
 */
int32_t Task_estimateActionValue( const Task* self, const State* s, const State* a ) ;

/**
 * @brief Estimate the state value for the current Task.
 * @memberof Environment
 *
 * @details This will approximate the state-value function
 * @f$V^{\pi}@f$ which is the expected return obtained when starting
 * in some initial State @f$s@f$, and following the @p policy
 * thereafter. This function implements the Bellman equation for the
 * State value which expresses a relationship between the value of a
 * State and the values of its successor states. Formally, the
 * state-value function @f$ V^{\pi} @f$ is expressed as:
 *
 * @f{align*}{
 * V^{\pi}(s) & = E_{\pi}\{R_t \mid s_t = s, a_t = a \} \\
 *            & = E_{\pi}\{\sum\limits_{k=0}^{\infty}{\gamma^k r_{t+k+1} \mid s_t = s}\} \\
 *            & = \sum_{a}{\pi(s,a)}\sum_{s'}{\mathscr{P}_{ss'}^{a}[\mathscr{R}_{ss'}^{a}+\gamma V^{\pi}(s')]}
 * @f}
 *
 * In this equation, @f$r_n@f$ represents the reward value from the
 * step @f$n@f$ in the current task.
 *
 * @see Environment_transitionProbability
 * @see Task_getReward
 * @see Policy_evaluate
 * @see State_getValue
 *
 * @param[in] self A pointer to an immutable Task instance.
 * @param[in] policy The policy to follow from the current State.
 * @param[in] s The state whose value will be estimated by this
 *	function.
 *
 * @return A fixed-point value, quantized to chunks of 0.01,
 * expressing the expected value of the specified State, provided the
 * current Policy is followed thereafter.
 */
int32_t Task_estimateStateValue( const Task* self, const Policy* policy, const State* s ) ;

/**
 * @brief Retrieve the set of States associated with a Task.
 * @memberof Task
 *
 * @details This will retrieve the StateSet expressing the set of all
 * known States in the Environment on which the current Task instance
 * operates.
 *
 * @param[in] self A pointer to an immutable Task instance.
 *
 * @return A pointer to an immutable StateSet associated with the
 * Environment on which the current Task operates. The ownership of
 * the memory returned by this function is retained by the callee and
 * must not be deleted by the caller. If an error is encountered while
 * retrieving the StateSet, this function returns @c NULL and sets
 * errno accordingly.
 *
 * @par Errors:
 * - EINVAL: The current Task instance (@p self) is @c NULL.
 * - ENOENT: The Task does not have an associated Environment.
 */
const StateSet* Task_getStates( const Task* self ) ;

#endif	/* __TASK_H__ */
