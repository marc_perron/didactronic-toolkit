/* -*- Mode: C -*- */
/*
 * transition.h - Declares an ADT to express a state transition in a
 *	reinforcement learning task.
 *
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef __TRANSITION_H__
#define __TRANSITION_H__

/* POSIX includes */
#include <stdint.h>

/* Local includes */
#include "state.h"
#include "util/list.h"

/******************************************************************************
 * Transition
 ******************************************************************************/

/**
 * @class Transition
 * @brief Opaque structure that pairs a weight with a State.
 *
 * @details This type represents a weighted, directed edge to a State.
 */
typedef struct _transition {
	const State*	state ;
	uint32_t	weight ;
} Transition ;

/**
 * @brief Create a new Transition instance.
 *
 * @details This will allocate a new Transition instance for the
 * specified state, setting its initial count to zero.
 *
 * @param[in] state The terminal state of the transition.
 *
 * @return A pointer to a Transition instance, created on the
 * heap. The ownership of the memory returned is transferred to the
 * caller who is responsible for deleting it.
 *
 * @memberof Transition
 */
Transition* Transition_Create( State* state ) ;

/**
 * @brief Get the Transition's State.
 * @memberof Transition
 *
 * @details This will retrieve the State instance that is associated
 * with the current Transition.
 *
 * @param[in] self A pointer to a Transition instance.
 *
 * @return A pointer to the State instance associated with the current
 * Transition. The returned State is mutable, however, the ownership
 * of the memory is retained by the callee and must not be deleted by
 * the caller.
 */
const State* Transition_getState( const Transition* self ) ;

/******************************************************************************
 * TransitionList
 ******************************************************************************/

/**
 * @class TransitionList
 * @brief Container expressing an un-ordered list of Transitions.
 * @extends List
 *
 * @details This is a simple specialization of the generic List class
 * which expresses a list of State Transitions.
 */
typedef List TransitionList ;

/**
 * @brief Add a Transition to a TransitionList.
 * @memberof TransitionList
 *
 * @details Add the specified Transition instance to the current list,
 * growning the assicated List as needed. If the list is @c NULL, a
 * new TransitionList will be created containing the single
 * element. This function is a wrapper around the List_append() member
 * of the List class.
 *
 * @param[in] self The address of a pointer to a TransitionList
 *	instance. The pointer will be updated with the new list.
 * @param[in] state A pointer to a State instance that is the end-point
 *	of the transition.
 *
 * @return The index of the newly added item. If the item could not be
 * added to the TransitionList, a negative index will be returned, and
 * errno will be set accordingly.
 *
 * @see List_append
 */
int TransitionList_add( TransitionList** self, const State* state ) ;

/**
 * @brief Get a pointer to the i-th item in a TransitionList.
 * @memberof TransitionList
 *
 * @details This function will retrive the i-th item from the
 * specified transition list, provided such an item exists. A pointer
 * to the item is returned, meaning that the Transition item can be
 * modified in place, updating its value in the list.
 *
 * @param[in] self A pointer to a TransitionList instance.
 * @param[in] i The index of the transition to retrieve.
 *
 * @return A pointer to the Transition instance at the i-th index in
 * the current TransitionList. If the index is out of range, this
 * function returns @c NULL, and errno is set to ERANGE.
 */
Transition* TransitionList_getItem( const TransitionList* self, size_t i ) ;

#endif	/* __TRANSITION_H__ */
