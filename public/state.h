/* -*- Mode: C -*- */
/*
 * state.h - Declares an ADT to express states in a dynamic program.
 *
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 */

#ifndef __STATE_H__
#define __STATE_H__

/* POSIX includes */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* Local includes */
#include "action.h"
#include "util/hashmap.h"
#include "util/map.h"

/******************************************************************************
 * State
 ******************************************************************************/

#ifndef STATE_ID_T
#define STATE_ID_T uint32_t
#endif	/* STATE_ID_T */

/**
 * @brief Type representing State IDs.
 *
 * @details This is the type used to express IDs which uniquely
 * identify individual states. This can be overriden by defining the
 * STATE_ID_T pre-processor symbol to the desired type. However, the
 * ID must be a first class type; meaning that the compiler must know
 * its type size.
 */
typedef STATE_ID_T StateID ;

/**
 * @class State
 * @brief Type representing an abstract state of a dynamic program.
 *
 * @details This type is an alias for the actual data type used to
 * express states in a dynamic program. This is a base type which can
 * be extended to adorn it the type with additional member
 * elements. Derived types may provide a destructor callback function
 * which will be invoked when the State is deleted to cleanup any
 * resources allocated by the derived type.
 *
 * @see State_Delete
 * @see State_Destructor
 */
typedef struct _state {
	/**
	 * @brief The ID of the current State.
	 * @memberof State
	 */
	StateID			id ;
	/**
	 * @brief A flag indicating if the current State is terminal.
	 * @memberof State
	 */
	bool			terminal ;
	char			reserved[ 4+3*sizeof( void* ) ] ;
} State ;

/**
 * @brief Prototype of virtual State destructor functions.
 * @memberof State
 *
 * @details Derived State types may provide custom cleanup code by
 * assigning a function with this prototype to State instance.
 *
 * @see State_init
 */
typedef void State_Destructor( State* self ) ;

/**
 * @def State_EXTEND
 * @brief Convenience macro for extending the State class.
 *
 * @details This macro must be the first item in the item in the
 * struct definition for a Class which extends the State class. This
 * will ensure that the derived structure's memory is properly aligned
 * and that it can be used with the base member functions.
 */
#define State_EXTEND State	_State

/**
 * @brief Allocate a new State instance.
 * @memberof State
 *
 * @details This function is used to allocate a new State instance on
 * the heap. The State will be initialized to have the specified State
 * id and value.
 *
 * @param[in] id The StateID to assign to the new State instance.
 * @param[in] value The initial value of the new State instance.
 *
 * @return A pointer to a new State instance, created on the heap, and
 * initialized to the provided specification. The ownership of the
 * memory returned by this function is transferred to the calling
 * context who is responsible for deleting it.
 */
State* State_Create( StateID id, int32_t value ) ;

/**
 * @brief Delete a State instance.
 * @memberof State
 *
 * @details This will delete the memory allocated for and by the State
 * instance at the specified address. This will in turn call any
 * destructor function that was associated with derived State
 * instances.
 *
 * @param[in,out] state The address of a pointer to a State instance
 *	that will be deleted by this function. The pointer at the
 *	specified address will be set to @c NULL following a call to
 *	this function.
 */
void State_Delete( State** state ) ;

/**
 * @brief Cleanup a State instance.
 * @memberof State
 *
 * @details This primitive will invoke the custom cleanup code that
 * should release any resources used by the current State
 * instance. This function is called by Delete to ensure that State
 * instances are properly cleaned up.
 *
 * @see State_Delete
 *
 * @param[in,out] self An abstract pointer to a State derived
 *	instance.
 */
void State_cleanup( void* self ) ;

/**
 * @brief Get the state's Id.
 *
 * @details A state's Id is an implementation specific value which is
 * used to retrieve information about the state (e.g. its value).
 *
 * @param[in] self A pointer to a State instance.
 *
 * @return The unique, implementation specific state Id.
 *
 * @memberof State
 */
StateID State_getId( const State* self ) ;

/**
 * @brief Test for a terminal State.
 * @memberof State
 *
 * @details This will assert whether or not the current State is a
 * terminal State.
 *
 * @param[in] self A pointer to an immutable State instance.
 *
 * @retval true The current State is a terminal.
 * @retval false The current State is @e not a terminal.
 */
bool State_isTerminal( const State* self ) ;

/**
 * @brief Get the value of the current state.
 *
 * @details This returns the current value of the specified
 * state. This value will be updated as the associated dynamic program
 * progressively sweeps through the state space. This corresponds with
 * the @f$V(s)@f$ term of the Bellman equation.
 *
 * @param[in] self A pointer to a State instance.
 *
 * @return A 32-bit signed integer expressing the value of the current
 * state.
 *
 * @memberof State
 */
int32_t State_getValue( const State* self ) ;

/**
 * @brief Get the real value of the current state.
 *
 * @details Retrieve the value of the current state as a real,
 * floating-point, number.
 *
 * @param[in] self A pointer to a State instance.
 * @param[in] unit The fixed-point unit value (1.0). This value will
 *	be used to calculate the real value of the fixed-point state
 *	value.
 *
 * @return The value of the current State instance converted from a
 * fixed-point to a floating-point number.
 */
float State_getRealValue( const State* self, int32_t unit ) ;

/**
 * @brief Update the value of the current state.
 *
 * @details This function allows the value, @f$V(s)@f$, of the current
 * state to be updated.
 *
 * @param[in] self A pointer to a State instance.
 * @param[in] value The new value of the state.
 *
 * @memberof State
 */
void State_setValue( State* self, int32_t value ) ;

/**
 * @brief Update the real value of the current state.
 *
 * @details This will update the value of the current state by
 * converting the provided real, floating-point, value into a fixed
 * point value quantized to the specified unit.
 *
 * @param[in] self A pointer to a State instance.
 * @param[in] value The real, floating-point, value to assign to the
 *	State.
 * @param[in] unit The fixed-point representation of the unit value
 *	(i.e. 1.0).
 */
void State_setRealValue( State* self, float value, int32_t unit ) ;

/**
 * @brief Get the Model associated with a State.
 * @memberof State
 *
 * @details Use this function to retrieve the Model associated with
 * the current State instance. The Model will optionally be defined by
 * the Environment to which the State belongs. A State Model is
 * required to solve reinforcement learning problems using Dynamic
 * Programming techniques.
 *
 * @param[in] self A pointer to an immutable State instance.
 *
 * @return A generic pointer to the Model associated with the State
 * instance.
 */
void* State_getModel( const State* self ) ;

/**
 * @brief Get the backup tree associated with the current state.
 *
 * @details This will return a Map corresponding with a partial backup
 * tree for the current State. This will map actions to possible final
 * states when those actions are applied to the current state.
 *
 * @param[in] self A pointer to a State instance.
 *
 * @return A pointer to a Map instance which expresses the partial
 * backup tree of the current state. If the state does not have an
 * associated backup, this function returns @c NULL.
 *
 * @memberof State
 *
 * @deprecated This function is deprecated. The backup tree is now
 * managed by a Model which can optionally be associated with a State.
 */
Map* State_getNeighbourhood( const State* self ) ;

/******************************************************************************
 * StateSet
 ******************************************************************************/

/**
 * @class StateSet
 * @brief Type representing the set of states in a dynamic program.
 * @extends HashMap
 *
 * @details Instances of this class are used to express the Set of
 * states used in a dynamic program. State members of the Set are
 * ordered by their IDs. This specialization enhances the Set class by
 * adding an index to speed up searching for States in the collection.
 */
typedef HashMap StateSet ;

/**
 * @fn bool StateSet_insert( StateSet* self, const State* state )
 * @memberof StateSet
 * @brief Add a State to the Set.
 * @details Add a pointer to an State to the current StateSet.
 * @param[in,out] self A pointer to an StateSet instance.
 * @param[in] state A pointer to an State instance.
 * @return An integer indicating whether or not the insertion was
 * successful.
 * @see Set_insert
 */
Set_DECLARE_OVERRIDES( StateSet, State* ) ;

#define StateSet_getFirst( s ) Set_getFirst( ( Set* )s )

/**
 * @brief Create a new state set.
 * @memberof StateSet
 *
 * @details This will allocate a buffer on the heap and initialize it
 * as a StateSet. To speed up access to items the contents of the
 * StateSet are index by State ID to allow entries to be accessed
 * without having to traverse the states that precede it.
 *
 * @return A new instance of StateSet, created on the heap,
 * initialized to the outlined specification. The ownership of the
 * memory returned by this function is transferred to the caller who
 * is responsible for deleting it.
 */
StateSet* StateSet_Create() ;

/**
 * @brief Delete a StateSet.
 *
 * @details This will delete all the States contained in the current
 * StateSet instance, then delete the StateSet itself.
 *
 * @param[in,out] self The address of a pointer to a StateSet instance
 * 	to delete. The referenced pointer will be set to @c NULL
 * 	following this function call.
 *
 * @memberof StateSet
 */
void StateSet_Delete( StateSet** self ) ;

/**
 * @brief Get the state that corresponds with the specified id.
 *
 * @details If the StateSet contains a State with the specified @a id,
 * this state will be returned by this function.
 *
 * @param[in] self A pointer to a StateSet instance.
 * @param[in] id The id of a State instance to retrieve from the set.
 *
 * @return A pointer to the State which corresponds with the specified
 * @a id. The ownership of the memory returned by this function is
 * retained by the callee and must not be deleted by the caller. If no
 * corresponding State instance is found in the set, this function
 * returns @c NULL.
 *
 * @memberof StateSet
 */
State* StateSet_getState( const StateSet* self, StateID id ) ;

/**
 * @brief Add a state to the set with the specified Id.
 *
 * @details This will add a new state to the current StateSet. If a
 * state with the same Id as the new state is already exists in the
 * StateSet, the existing state will be retained.
 *
 * @param[in] self A pointer to a StateSet instance.
 * @param[in] s A pointer to a State instance to add to the set.
 *
 * @return A pointer to the newly added State. The ownership of the
 * returned State is retained by the StateSet. If the State could not
 * be inserted for any reason, this will return @c NULL. This can be
 * used to determine if the state @a s was added to the set.
 *
 * @memberof StateSet
 */
State* StateSet_addState( StateSet* self, State* s ) ;

/**
 * @brief Randomly select a State from the current set.
 * @memberof StateSet
 *
 * @details This function will randomly sample a State instance from
 * the current set following a uniform distribution.
 *
 * @param[in] self A pointer to the current StateSet instance.
 *
 * @return A pointer to a State instance selected randomly from the
 * current set. The ownership of the memory returned by this function
 * is retained by the caller and must @e not be deleted thereby.
 */
State* StateSet_getRandom( const StateSet* self ) ;

/**
 * @brief Determine whether or not a State is a member of the set.
 * @memberof StateSet
 *
 * @details This will determine whether or not the specified State is
 * a member of the current StateSet instance. This is a more efficient
 * implementation of the base function which tests for Set
 * membership. In the worst case, this function will have O(n)
 * complexity, in the best case, it will have O(1) complexity.
 *
 * @see Set_contains
 *
 * @param[in] self A pointer to an immutable instance of a StateSet.
 * @param[in] s A pointer to an immutable State instance which will be
 *	tested for membership in the current StateSet.
 *
 * @retval true The State @a s is a member of the current set.
 * @retval false The State @a s is *not* a member of the current set.
 */
bool StateSet_contains( const StateSet* self, const State* s ) ;

#endif	/* __STATE_H__ */
