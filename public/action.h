/* -*- Mode: C -*- */
/*
 * action.h - Declares an ADT to express actions in a dynamic program.
 *
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 */

#ifndef __ACTION_H__
#define __ACTION_H__

/* Local includes */
#include "util/list.h"
#include "util/hashmap.h"
#include "util/map.h"

/* POSIX includes */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * @brief Type reprenting action IDs
 *
 * @details Action IDs correspond with StateIDs.
 */
typedef int32_t ActionID ;

/**
 * @class Action
 * @brief Type representing actions in a dynamic program.
 *
 * @details This type is an alias for the structure that represents
 * actions in a dynamic program. The Action class defines functions to
 * map actions to states.
 */
typedef struct _action {
	/** @brief Unique ID of the current Action instance. */
	ActionID	id ;
	char		reserved[ sizeof( void* ) ] ;
} Action ;

/**
 * @brief A convenience macro used when extending an Action.
 *
 * @details Declare this macro as the first member of a structure
 * which derives from the Action class. The public members of the base
 * class can be accessed via the _Action member; e.g. t._Action.id
 * will access the id member of the Action class in the derived
 * instance t.
 */
#define Action_EXTEND Action _Action

/**
 * @brief Prototype of the cleanup function for Action types.
 * @memberof Action
 *
 * @details This type defines the prototype expected of functions
 * which cleanup derived Action types. Functions of this type can be
 * associated with derived Action instances to ensure they are
 * properly cleaned up when they are deleted.
 *
 * @see Action_Delete
 */
typedef void Action_cleanup_f( Action* ) ;

/**
 * @brief Allocate a new Action instance.
 * @memberof Action
 *
 * @details This will create a new intance of Action on the heap, and
 * initialize it with the specified @a id.
 *
 * @param[in] id An ActionId to associate with the new Action
 *	instance.
 * @param[in] opt_cleanup A pointer to a cleanup function that will be
 *	called when the Action instance is deleted.
 *
 * @return A pointer to the newly created Action instance. The
 * ownership of the memory returned by this function is transferred to
 * the caller who is responsible for deleting it.
 *
 */
Action* Action_Create( ActionID id, Action_cleanup_f* opt_cleanup ) ;

/**
 * @brief Destructor for Action types.
 * @memberof Action
 *
 * @details This function will delete the specified Action
 * instance. If a custom cleanup function is associated with the
 * instance, it will be called before the Action is de-allocated. This
 * ensures that derived action types will be properly cleaned up
 * before being deleted.
 *
 * @param[in,out] self_ptr A pointer to the address of an Action
 *	derived instance to delete. The pointer will be set to @c NULL
 *	when this function returns.
 */
void Action_Delete( Action** self_ptr ) ;

/**
 * @brief Initialize an Action instance.
 * @memberof Action
 *
 * @details This will initialize the specified memory buffer as an
 * Action instance. The Action will be initialized with the specified
 * @a id. An optional custom cleanup function can be associated with
 * the Action instance.
 *
 * @param[in] self A pointer to a memory buffer that will be
 *	initialized to an Action. The initializer assumes that the
 *	buffer is large enough to hold the action.
 * @param[in] id An Action ID that will be assigned to the newly
 *	initialized Action instance.
 * @param[in] opt_cleanup A pointer to a cleanup function that will be
 *	called when the Action instance is deleted.
 */
void Action_initialize( void* self, ActionID id, Action_cleanup_f* opt_cleanup ) ;

/**
 * @brief Cleanup an Action instance.
 * @memberof Action
 *
 * @details This will cleanup any resources associated the current
 * Action instance by invoking the cleanup routine associated with the
 * Action when it was initialized. This function is invoked when an
 * Action is deleted.
 *
 * @see Action_Delete
 *
 * @param[in,out] self An abstract pointer to an Action derived
 *	instance.
 */
void Action_cleanup( void* self ) ;

/**
 * @brief Get the id assigned to the current Action.
 *
 * @details This will retrive the implementation specific Id that was
 * assigned to the current Action instance. This Id is implementation
 * specific and, as such, has no inherent semantic meaning. Its value
 * can be used to associate higher level data or processing with the
 * Action.
 *
 * @param[in] self A pointer to an Action instance.
 *
 * @return The integer Id assigned to the current Action.
 *
 * @memberof Action
 */
ActionID Action_getId( const Action* self ) ;

/******************************************************************************
 * ActionSet
 ******************************************************************************/

/**
 * @class ActionSet
 * @brief Class describing a Set of Actions
 * @extends HashMap
 *
 * @details This is a specialization of the HashMap collection which
 * provides a set of Action instances. Actions are indexed by their
 * IDs, thus they can be retrieved thereby.
 */
typedef HashMap ActionSet ;

/**
 * @brief Create an empty ActionSet
 * @memberof ActionSet
 *
 * @details Allocate a new ActionSet on the heap, initializing it to
 * initially be empty.
 *
 * @return A pointer to a newly allocated ActionSet. The memory
 * returned by this function is transfered to the caller who is
 * responsible for deleting it.
 *
 * @see HashMap_Create
 * @see HashMap_Delete
 */
ActionSet* ActionSet_Create() ;

/**
 * @fn bool ActionSet_insert( ActionSet* self, const Action* action )
 * @memberof ActionSet
 * @brief Add an Action to the Set.
 * @details Add a pointer to an Action to the current ActionSet.
 * @param[in,out] self A pointer to an ActionSet instance.
 * @param[in] action A pointer to an Action instance.
 * @return An integer indicating whether or not the insertion was
 * successful.
 *
 * @see Set_insert
 */
Set_DECLARE_OVERRIDES( ActionSet, Action* ) ;

/**
 * @brief Get the action which corresponds with an id.
 * @memberof ActionSet
 *
 * @details This function will search the current ActionSet instance
 * for an Action element which has the specified @a id. This operation
 * will be performed in constant, O(1), time.
 *
 * @param[in] self A pointer to an immutable ActionSet instance.
 * @param[in] id The ID of the Action instance to retrieve from the
 *	set.
 *
 * @return A pointer to the element of the current ActionSet which is
 * the Action associated with the specified @a id. In the case of an
 * error, this function will return @c NULL and set @e errno
 * accordingly.
 *
 * @par Errors
 * - EINVAL: The ActionSet instance is invalid.
 * - ENOENT: The Action associated with @a id is not a member of the
 *   ActionSet.
 */
Action* ActionSet_getAction( const ActionSet* self, ActionID id ) ;

/**
 * @brief Get a random Action from the set.
 * @memberof ActionSet
 *
 * @details This function will randomly select an Action in the
 * current set according to a uniform distribution.
 *
 * @param[in] self A pointer to an ActionSet instance.
 *
 * @return A pointer to an Action instance selected randomly in the
 * current set. The ownership of the memory returned by this function
 * is retained by the caller and must @e not be deleted by the caller.
 */
Action* ActionSet_getRandom( const ActionSet* self ) ;

/******************************************************************************
 * ActionMap
 ******************************************************************************/

#include "util/dictionary.h"

typedef struct _state State ;

/**
 * @class ActionMap
 * @implements Dictionary
 * @brief Mapping of actions to state transitions.
 *
 * @details This class defines an opaque type which provides a mapping
 * from Actions to a list of possible Transitions. The ActionMap
 * should be attached to a node in a graph to model its outgoing
 * edges.
 */
typedef Dictionary ActionMap ;

/**
 * @brief Create a new ActionMap container instance.
 * @memberof ActionMap
 *
 * @details This function will allocate a new Dictionary, setting it
 * up to map actions to possible final states, and initializing it as
 * an empty container. The Dictionary will be populated latently.
 *
 * @return A new instance of an ActionMap, allocated on the heap, and
 * setup to map actions to states. The ownership of the memory
 * returned by this function is transferred to the caller who is
 * responsible for deleting it.
 *
 * @see HashMap_Delete
 */
ActionMap* ActionMap_Create() ;

/**
 * @brief Add an entry to the ActionMap.
 * @memberof ActionMap
 *
 * @details This will add a transition to the current ActionMap. The
 * transition will indicate that taking the specified Action will
 * transition to the specified State.
 *
 * @param[in] self A pointer to an ActionMap container instance.
 * @param[in] a An Action instance to label the new transition.
 * @param[in] s An end State of the new transition.
 * @param[in] weight The weight to associate with the transition edge.
 *
 * @retval 0 The new transition has been added to the ActionMap.
 * @retval -1 An error prevented the new Transition from being added
 *	to the map; @e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: The ActionMap is invalid.
 * - ENOMEM: There was not enough system memory to create the
 *   transition.
 * - EEXIST: The transition already exists in the map.
 */
int ActionMap_addEntry( ActionMap* self, const Action* a, const State* s, uint32_t weight ) ;

/**
 * @brief Find the list of possible Transitions for an Action.
 * @memberof ActionMap
 *
 * @details This will retrieve an Iterator pointing to the first
 * transition defined for the specified Action label. The Iterator can
 * be used to enumerate all of the possible end States.
 *
 * @see ActionMap_NextState
 *
 * @param[in] self A pointer to an immutable ActionMap instance.
 * @param[in] action A pointer to an Action instance.
 *
 * @return An Iterator which can be used to enumerate all the possible
 * transitions defined for the specified Action.
 */
Iterator ActionMap_findTransitions( const ActionMap* self, const Action* action ) ;

/**
 * @brief Enumerate the end states of a transition.
 * @memberof ActionMap
 *
 * @details This function should be invoked to enumerate all of the
 * possible final States defined for a transition labelled with some
 * Action. The Action label will have been specified when the Iterator
 * was retrieved.
 *
 * @see ActionMap_findTransitions
 *
 * @param[in,out] i A pointer to an Iterator which references a
 *	Transition tagged with some Action label. The Iterator will be
 *	advanced to the next Transition before this function returns.
 * @param[out] weight The weight associated with the returned
 *	State-transition.
 *
 * @return A pointer to an immutable State instance expressing a final
 * State of a transition for some Action label. If there are no more
 * transitions this function will return @c NULL.
 */
const State* ActionMap_NextState( Iterator* i, uint32_t* weight ) ;

#endif	/* __ACTION_H__ */
