/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Provides implementations for various Monte Carlo
 *	reinforcement learning algorithms
 */

#include "history.h"
#include "policy.h"
#include "task.h"

#ifndef __MONTECARLO_H__
#define __MONTECARLO_H__

/**
 * @defgroup MonteCarlo Monte Carlo
 * @brief Module providing implementations for Monte Carlo
 *	reinforcement learning algorithms.
 *
 * @details The functions defined in this module can be used with the
 * Cyberdyn Toolkit framework to utilize Monte Carlo methods to solve
 * domain specific tasks.
 *
 * @{
 */

/**
 * @brief A Monte Carlo control algorithm assuming exploring starts.
 */
void MonteCarlo_ES( Policy* policy ) ;

/**
 * @brief Implementation of the first-visit Monte Carlo method for
 *	Policy evaluation.
 *
 * @details This function implements the first-visit MC method for
 * estimating the state-value function @f$ V^{\pi} @f$. This method
 * averages the returns following the first visit to @p state in an
 * episode of a given Task. The algorithm is expressed in the
 * following pseudocode [@cite Sutton1998]:
 *
 * > Initialize:\n
 * > @f$ \pi\leftarrow @f$ policy to be evaluated\n
 * > @f$ V\leftarrow @f$ an arbitrary state-value function.\n
 * > @f$ \textit{Returns}(s)\leftarrow\emptyset @f$, an empty list for all @f$ s\in S @f$\n\n
 * > Repeat forever:\n
 * > -# Generate an epsiode using @f$ \pi @f$.
 * > -# For each state @f$ s @f$ appearing in the episode:
 * >	-# @f$ R\leftarrow @f$ return following the first occurrence of @f$ s @f$
 * >	-# Append @f$ R @f$ to @f$ \textit{Returns}(s) @f$
 * >	-# @f$ V(s)\leftarrow\textrm{average}(\textit{Returns}(s)) @f$
 *
 * @param[in,out] policy A pointer to the Policy instance to
 *	evaluate. The state-value estimation of the Policy will be
 *	updated by this function.
 * @param[in] episode An immutable pointer to an History of a Task
 *	that was produced by following the current Policy.
 */
void MonteCarlo_FirstVisit( Policy* policy, History* episode ) ;

/** @} */

#endif	/* __MONTECARLO_H__ */
