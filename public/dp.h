/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "policy.h"
#include "task.h"

/**
 * @defgroup DP Dynamic Programming
 * @brief Module providing implementations for Dynamic Programming
 * algorithms.
 *
 * @details The functions defined in this module can be used with the
 * Cyberdyn toolkit framework to utilize Dynamic Programming
 * techniques to solve domain specific tasks. To use the algorithms
 * provided by this module, implementors must define the Task,
 * Environment, and Agent for a reinforcement learning problem to
 * solve. If these are defined correctly, the algorithms can be used
 * directly.
 *
 *  @{
 */

/**
 * @file
 * @brief Provides implementations for various dynamic programming
 *	algorithms.
 */

#ifndef __DP_H__
#define __DP_H__

/**
 * @brief Iterative Policy Evaluation
 * @ingroup DP
 *
 * @details This function provides an implementation for the iterative
 * policy evaluation algorithm. It produces successive approximations,
 * \f$V_{k+1}\f$ from \f$V_k\f$, by applying the same operation on
 * each state @e s: it replaces the old value of @e s with a new value
 * obtained from the old values of the successor states of @e s, and
 * the expected immediate rewards, along all the on-step transitions
 * possible under the policy being evaluated. The algorithm is
 * expressed in pseudocode as follows [@cite Sutton1998]:
 *
 * > Input @f$ \pi @f$ the policy to be evaluated\n
 * > Initialize @f$ V(s) = 0 @f$, for all @f$s \in S^{+} @f$\n
 * > Repeat:
 * >	-# @f$ \Delta \leftarrow 0 @f$
 * >	-# For each @f$ s \in S @f$:
 * >		-# @f$v \leftarrow V(s) @f$
 * >		-# @f$V(s)\leftarrow \sum_{a}{\pi(s,a)\sum_{s'}{\mathscr{P}_{ss'}^{a}[\mathscr{R}_{ss'}^{a} + \gamma V(s')]}}@f$
 * >		-# @f$\Delta\leftarrow \textrm{max}(\Delta,|v - V(s)|)@f$
 * >	.
 * > until @f$\Delta < \theta@f$ (a small positive number)\n
 * > Output @f$V \approx V^{\pi} @f$
 *
 * @param[in,out] policy A pointer to a Policy instance that will be
 *	improved to better solve the specified @p task.
 * @param[in] task A pointer to an immutable Task instance expressing
 *	the task for which the policy is being improved. The values of
 *	the States in the associated Environment's StateSet
 *	@f$S^{+}@f$ will be updated following this function call.
 *
 * @see Policy_evaluate
 * @see Task_getReward
 * @see Environment_transitionProbability
 * @see State_getValue
 */
void DP_IterativePolicyEvaluation( Policy* policy, const Task* task ) ;

/**
 * @brief Evaluate the specified policy.
 * @ingroup DP
 *
 * @details This is the first half of the Generalized Policy Iteration
 * (GPI) algorithm. It will perform a sweep through the entire state
 * set, updating the State values by following the Policy to choose
 * actions, then estimating the Q-value at each state. This process
 * repeats until State value updates are inferior to the specified
 * @a threshold. The algorithm is described in the following
 * pseudocode:
 *
 * > @b PolicyEvaluation [@cite Sutton1998]\n
 * > Repeat:
 * >	-# @f$ \Delta\leftarrow 0 @f$
 * >	-# For each @f$ s \in S @f$
 * >		-# @f$v\leftarrow V(s) @f$
 * >		-# @f$V(s)\leftarrow\sum_{s'}{\mathscr{P}_{ss'}^{\pi(s)}[\mathscr{R}_{ss'}^{\pi(s)} + \gamma V(s')]}@f$
 * >		-# @f$\Delta\leftarrow \textrm{max}(\Delta,\mid v - V(s)\mid)@f$
 * >	.
 * > until @f$ \Delta < \theta @f$ (a small positive number)
 *
 * @param[in,out] policy A pointer to a Policy instance. The values of
 *	the States in the associated StateSet will be updated
 *	following this function call.
 * @param[in] task A pointer to an immutable Task instance which
 *	expresses the Task for which the Policy is being evaluated.
 * @param[in] threshold The threshold that indicates the delta in
 *	State value change that, if all updates are less than the
 *	expressed value, will cause Policy evaluation to halt.
 *
 * @see Policy_getReward
 * @see Policy_transitionProbability
 * @see State_getValue
 */
void DP_PolicyEvaluation( Policy* policy, const Task* task, int32_t threshold ) ;

/**
 * @brief Improve the specified policy.
 * @ingroup DP
 *
 * @details This is the second half of the Generalized Policy
 * Iteration (GPI) algorithm. It sweeps the StateSet associated with
 * the Policy, finding the action which yields the highest Q-value for
 * each state, then ensuring that the policy chooses that action on
 * the next execution of the algorithm.
 *
 * > @b PolicyImprovement [@cite Sutton1998]\n
 * > @f$ \textrm{policy-stable}\leftarrow \text{true} @f$\n
 * > For each @f$ s \in S @f$:\n
 * >	-# @f$b\leftarrow\pi(s)@f$
 * >	-# @f$\pi(s)\leftarrow\textrm{arg max}_{a}\sum_{s'}{\mathscr{P}_{ss'}^{a}[\mathscr{R}_{ss'}^{a} + \gamma V(s')]}@f$
 * >	-# If @f$ b \neq \pi(s) @f$, then @f$\textrm{policy-stable}\leftarrow\text{false}@f$
 * >	.
 * > Return @f$ \textrm{policy-stable} @f$
 *
 * @param[in,out] policy A pointer to a Policy instance that will be
 *	updated.
 * @param[in] task A pointer to an immutable Task instance expressing
 *	the task for which the Policy is being improved.
 *
 * @return A boolean indicating whether or not the Policy is stable.
 *
 * @retval true The policy is stable. In other words, on subsequent
 *	sweeps of the StateSet, the policy selects the action with the
 *	highest Q-value for each state.
 * @retval false The policy is not stable.
 *
 * @see Policy_apply
 * @see Task_getReward
 * @see Environment_transitionProbability
 * @see State_getValue
 */
bool DP_PolicyImprovement( Policy* policy, const Task* task ) ;

/**
 * @brief Perform policy iteration to evaluate and improve the Policy.
 * @ingroup DP
 *
 * @details This function provides an implementation of the
 * Generalized Policy Iteration (GPI) algorithm used in dynamic
 * programming. The algorithm iteratively performs policy evaluation
 * and policy improvement to find the optimal Value function according
 * to the current Policy. This procedure is repeated until the policy
 * becomes stable. The GPI algorithm is expressed in the following
 * pseudocode [@cite Sutton1998]:
 *
 * > Set @f$ V(s) \in \mathfrak{R} @f$ and @f$ \pi(s) \in \mathscr{A}(s) @f$ arbitrarily for all @f$ s \in S @f$\n
 * > Repeat:
 * > -# @f$ \textrm{PolicyEvaluation}(\pi) @f$
 * > -# @f$ \textrm{policy-stable}\leftarrow \textrm{PolicyImprovement}(\pi) @f$
 * > .
 * > until @f$ \textrm{policy-stable} @f$
 *
 * @note If the Task implementation is not carefully defined, this
 * algorithm could loop indefinitely.
 *
 * @param[in,out] policy A pointer to a Policy instance @f$\pi@f$. The
 *	policy will be improved in place by updating the associated
 *	Environment.
 * @param[in] task A pointer to an immutable Task instance which
 *	represents the task the policy is being applied to (i.e. the
 *	agent is trying to solve the specified Task using the current
 *	Policy).
 *
 * @see DP_PolicyEvaluation
 * @see DP_PolicyImprovement
 */
void DP_GeneralizedPolicyIteration( Policy* policy, const Task* task ) ;

/**
 * @brief Perform value iteration to improve a greedy policy.
 * @ingroup DP
 *
 * @details This is a truncated version of the PolicyIteration
 * function. Instead of performing multiple sweeps of the state set to
 * converge to an optimal policy in the limit, this algorithm stops
 * after a single backup of each state. This algorithm is expressed as
 * the following pseudocode [@cite Sutton1998]:
 *
 * > Initialize @f$ V @f$ arbitrarily, e.g. @f$ V(s)=0 @f$, for all @f$ s\in S^{+} @f$\n
 * > Repeat:
 * >	-# @f$ \Delta\leftarrow 0 @f$
 * >	-# @f$ v\leftarrow V(s) @f$
 * >	-# @f$ V(s)\leftarrow\textrm{max}(\Delta,|v - V(s)|) @f$
 * >	.
 * > until @f$ \Delta < \theta @f$ (a small positive number)\n
 * > Output a deterministic policy, @f$ \pi @f$, such that @f$ \pi(s) = \textrm{arg max}_{a}\sum_{s'}{\mathscr{P}_{ss'}^{a}[\mathscr{R}_{ss'}^{a} + \gamma V(s')]} @f$
 *
 * @param[in,out] policy A pointer to the Policy instance. Its
 *	associated state set will be updated with new values following
 *	the backup.
 * @param[in] task A pointer to an immutable Task instance for which
 *	the Policy will be improved.
 */
void DP_ValueIteration( Policy* policy, const Task* task ) ;

/** @} */

#endif	/* __DP_H__ */
