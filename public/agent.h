/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
/**
 * @file
 * @brief Definition of a class representing an Agent in a
 *	reinforcement learning task.
 */

#include "action.h"
#include "state.h"
#include "task.h"

#ifndef __AGENT_H__
#define __AGENT_H__

/**
 * @class Agent

 * @brief Abstract class representing an Agent in a reinforcement
 *	learning task.
 *
 * @details An Agent represents an actor and learner in a
 * reinforcement learning task. An instance of a Task to execute will
 * be associated with the Agent as well as a Policy that will be
 * followed during execution. Instances of this class will expose an
 * Agent's capabilities which represents the available controls.
 */
typedef struct _agent {
	/**
	 * @brief The Task which the Agent will execute.
	 * @memberof Agent
	 *
	 * @details This member refers to the Task which will be
	 * executed by the current Agent instance. A Task may be
	 * shared by multiple Agents, so the Task's resources are not
	 * owned by the Agent instance.
	 */
	const Task*		task ;

	/**
	 * @brief The capabilities of the current Agent.
	 * @memberof Agent
	 *
	 * @details This member defines the set of actions which
	 * comprise the capabilities of the current Agent. This will
	 * be a sub-set of the generators of the Agent's Environment.
	 */
	const StateSet*		capabilities ;

	/**
	 * @brief The policy followed by the Agent.
	 * @memberof Agent
	 *
	 * @details This policy instance which is used by the Agent to
	 * select actions to resolve a given Task.
	 */
	const Policy*		policy ;

	/**
	 * @brief Padding used by private Agent data.
	 * @memberof Agent
	 * @private
	 *
	 * @details This is an opaque buffer which pads the Agent
	 * structure to allow it to hold the private Agent data.
	 */
	const char		reserved[ sizeof( void* )*2 ] ;
} Agent ;

/**
 * @brief Convenience macro for extending the Agent class.
 *
 * @details Declare this macro as the first member of a structure
 * definition to mark is as an extension of the Agent class. This
 * macro will ensure that the derived type's memory is correctly
 * aligned to be used with the base Agent member functions.
 */
#define Agent_EXTEND Agent _Agent

/**
 * @brief Prototype of the Agent destructor function.
 * @memberof Agent
 * @protected
 *
 * @details Type describing the prototype of custom destructor
 * functions which will be used to cleanup derived Agent
 * types. Functions of this type can be provided to the Agent_Create()
 * to perform the custom cleanup when Agent_Delete() is called.
 *
 * @param[in] inst A pointer to the instance that will be deleted by
 *	this function. This can be cast to the appropriate type in the
 *	destructor function implementation. When called by
 *	Agent_Delete(), this will be a pointer to the Agent being
 *	deleted.
 *
 * @see Agent_init
 * @see Agent_Delete
 */
typedef void Agent_destructorProto( void* inst ) ;

/**
 * @brief Create a new Agent instance on the heap.
 * @memberof Agent
 *
 * @details This will allocate a new Agent instance on the heap,
 * setting the provided StateSet as the agent's capabilities.
 *
 * @param[in] capabilities A pointer to an StateSet which defines the
 *	initial capabilities of the new Agent instance. The ownership
 *	of the capabilities set is transferred to the Agent instance,
 *	and will be deleted by the Agent_Delete() function.
 * @param[in] task A pointer to a Task definition that will be
 *	executed by the Agent instance. The ownership of the Task is
 *	transferred to the callee and will be deleted along with the
 *	Agent instance.
 *
 * @return A pointer to a new intsance of the Agent class, created on
 * the heap, the ownership of the memory returned by this function is
 * transferred to the caller who is responsible for ensuring it is
 * deleted.
 */
Agent* Agent_Create( StateSet* capabilities, Task* task ) ;

/**
 * @brief Delete the specified Agent instance.
 * @memberof Agent
 *
 * @details This will delete the specified Agent instance and its
 * associated capabilities (including the Action instances contained
 * therein). If a destructor was provided when the instance was
 * created, this function will be called before the Agent is deleted.
 *
 * @param[in,out] agent The address of a pointer to an Agent instance
 *	that will be deleted. The pointer addressed by this argument
 *	will be set to @c NULL.
 *
 * @see Agent_destructorProto
 */
void Agent_Delete( Agent** agent ) ;

/**
 * @brief Retrieve the Agent's capabilities.
 * @memberof Agent
 *
 * @details This will return the set of all Actions that can be
 * performed by the Agent represented by the current instance.
 *
 * @param[in] self A pointer to an Agent instance.
 *
 * @return A pointer to an StateSet instance which contains the set
 * of all Actions that can be taken by the current Agent. The
 * ownership of the memory returned by this function is retained by
 * the Agent instance and must not be deleted by the caller.
 */
const StateSet* Agent_getCapabilities( const Agent* self ) ;

/**
 * @brief Get the Agent's next Action.
 * @memberof Agent
 *
 * @details This will return a State which expresses the sequence of
 * actions that will be taken by the current Agent from the specified
 * @a state. If the Agent has an associated Policy, it will be used to
 * select the plan. Otherwise, the plan will be selected randomly from
 * the capabilities of the Agent following a uniform distribution.
 *
 * @param[in] self A pointer to an Agent instance.
 * @param[in] state A pointer to the current State in the
 *	reinforcement learning Task.
 *
 * @return A pointer to a State instance representing the plan to be
 * followed by the Agent in the specified @a state.
 */
const State* Agent_nextAction( const Agent* self, const State* state ) ;

/**
 * @brief Retrieve the State value for the current Agent.
 * @memberof Agent
 *
 * @details This will retrieve the value atributed to the specified
 * State by an Agent. As an agent learns a Task, its internal
 * state-value map will be updated accordingly. If the Agent has never
 * visited the specified state, the generalized value will be returned
 * instead.
 *
 * @param[in] self A pointer to an immutable Agent instance.
 * @param[in] state A pointer to the State whose value will be
 *	retrieved.
 * @param[in] value A buffer where the State's value will be saved
 *	when this function returns.
 *
 * @retval 0 The State's value was retrieved successfully.
 * @retval -1 An error occurred while retrieving the State's value;
 *	@e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: Either the agent, state or value buffer is invalid.
 */
int Agent_getValue( const Agent* self, const State* state, int32_t* value ) ;

/**
 * @brief Update the State-value belief of an Agent.
 * @memberof Agent
 *
 * @details This function will update the specified Agent's belief of
 * the specified State. This function is invoked throughout the
 * learning process to adjust the Agent's value-map for a particular
 * Task.
 *
 * @param[in] self A pointer to an Agent instance whose value-map will
 *	be updated.
 * @param[in] state A pointer to an immutable State instance whose
 *	believed value will be updated.
 * @param[in] value The new value to set for the specified State.
 *
 * @retval 0 The State's believed value has been updated successfully.
 * @retval -1 An error prevented the successful update of the State's
 *	believed value; @e errno will be set accordingly.
 *
 * @par Errors
 * - EINVAL: Either the Agent or State is invalid.
 * - ENOMEM: There wasn't enough system memory to update the
 *   value-map.
 */
int Agent_setValue( Agent* self, const State* state, int32_t value ) ;

/******************************************************************************
 * Agency
 ******************************************************************************/

/**
 * @class Agency
 * @brief Class expressing an agency of one or more Agents.
 * @extends List
 *
 * @details This class defines a container type used to express an
 * Agency which will execute an MDP. The Agency is a specialization of
 * the List container that manages an ordered list of Agent instance
 * representing the members of the Agency.
 */
typedef struct _agency {
	List_EXTEND ;
	const char	agency_reserved[ sizeof( void* ) ] ;
} Agency ;

/**
 * @brief Prototype of functions to iterate over members of an Agency.
 * @memberof Agency
 *
 * @details This type describes the prototype required of custom
 * implementations of the function to iterate over the members of an
 * Agency. The semantics expected of this function are as follows:
 *
 * - If the Iterator is the @c NULL iterator (i.e. its value is zero),
 *   then this function must return the first Agent of the Agency
 *   before updating the Iterator to precede the next Agent in the
 *   Agency.
 * - If the Iterator is not the @c NULL iterator, this function must
 *   continue iterating from the specified position in the Agency.
 *
 * @param[in] agency A pointer to the Agency over which to iterate.
 * @param[in,out] i A pointer to an Iterator referencing the next
 *	Agent that will be returned. If the Iterator is zero, the
 *	first Agent is returned. Otherwise iteration will continue
 *	from the specified Iterator position. The Iterator will be
 *	updated to point to the next Agent in sequence when this
 *	function returns.
 *
 * @return A pointer to an immutable Agent instance which is the next
 * Agent in the Agency. If there are no more Agents, this function
 * should return @c NULL.
 */
typedef const Agent* Agency_next_f( const Agency* agency, Iterator* i ) ;

/**
 * @brief Initialize a memory buffer as an Agency.
 * @memberof Agency
 *
 * @details This will initialize the specified memory buffer as an
 * Agency instance; optionally assigning a custom function for
 * deciding the order of its members.
 *
 * @param[in,out] self A pointer to a memory buffer that will be
 *	initialized as an Agency. The buffer referenced by this
 *	pointer must be large enough to express an Agency.
 * @param[in] opt_next An optional pointer to a custom function for
 *	iterating over the members of the Agency.
 */
void Agency_init( void* self, Agency_next_f* opt_next ) ;

/**
 * @brief Iterate over the members of an Agency.
 * @memberof Agency
 *
 * @details This function will iterate over the members of an Agency
 * in some pre-defined order. The order in which Agents are sequenced
 * may depend on the time-step of the MDP to which the Agency is
 * associated.
 *
 * @param[in] self A pointer to the Agency over which to iterate.
 * @param[in,out] i A pointer to an Iterator which indicates the
 *	position in the sequence of members of the Agency. If the
 *	Iterator referenced by this pointer is zero, the first member
 *	will be returned. Otherwise the member which follows the
 *	specified Iterator will be returned. The Iterator will be
 *	updated to reference the next member in the sequence when this
 *	function returns.
 *
 * @return A pointer to the next Agent instance in the Agency. If
 * there are no more Agents, this function returns @c NULL.
 */
const Agent* Agency_next( const Agency* self, Iterator* i ) ;

#endif	/* __AGENT_H__ */
