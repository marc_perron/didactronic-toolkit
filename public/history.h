/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef __HISTORY_H__
#define __HISTORY_H__

#include <state.h>

/**
 * @class History
 * @brief Class expressing a Task history.
 * @extends List
 *
 * @details Instances of this class represent the sequence of actions
 * followed by an Agent executing a Task. The History is comprised of
 * an ordered list of Action instance. The intervening states can be
 * determined by iteratively determining the product of the actions
 * with the initial state and the resulting subsequent states.
 */
typedef struct _history History ;

/**
 * @brief Create a new History instance.
 * @memberof History
 *
 * @details This will create a History instance on the heap. Initially
 * the History will be empty and its associated return value will be
 * zero. The return value will be updated as steps are added to the
 * History.
 *
 * @return A pointer to an new History instance created on the
 * heap. In the case of an error, this function returns @c NULL and
 * sets @e errno accordingly.
 *
 * @par Errors
 * - ENOMEM: There was not enough memory to allocate the History.
 */
History* History_Create() ;

/**
 * @brief Delete the specified History instance.
 * @memberof History
 *
 * @details This function will cleanup the specified History instance
 * and delete the associated data structure.
 *
 * @param[in,out] self The address of a pointer to a History instance
 *	that will be deleted. The addressed pointer will be set to
 *	@c NULL when the History is deleted.
 */
void History_Delete( History** self ) ;

/**
 * @brief Add a time step to a History.
 * @memberof History
 *
 * @details This function will append the specified macro Action
 * instance to the current History at the next discrete time
 * step. Steps will be added starting at time 0. Each step will have
 * an associated reward value. The total return of the current History
 * will be updated with the reward associated with the new Step.
 *
 * @param[in] self The address of a pointer to the History instance to
 *	which the Action will be added.
 * @param[in] state The State in which the Environment was when the
 *	specified macro Action was executed in the current History.
 * @param[in] macro_action A pointer to a State instance which
 *	represents the macro Action taken by an Agent executing a
 *	Task.
 * @param[in] reward The value of the reward obtained by the Agent by
 *	taking the specified macro Action.
 *
 * @return The time step in the history of the newly added macro
 * Action. This time step can be used to retrieve the macro Action and
 * its associated reward from the History instance at a later time. In
 * the case of an error, this function returns a negative value and
 * sets @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The history instance or macro Action is invalid.
 */
int History_addStep( History** self, const State* state, const State* macro_action, int32_t reward ) ;

/**
 * @brief Get the Action taken at a given time step.
 * @memberof History
 *
 * @details This function will retrieve the macro Action that was
 * executed by the Agent at time step @a t of the current History.
 *
 * @param[in] self A pointer to the History instance to query.
 * @param[in] t The time step of the Action to retrieve.
 *
 * @return A pointer to an immutable State instance representing the
 * macro Action taken at time step @a t of the History. In the case of
 * an error, this function will return @c NULL and set @e errno
 * accordingly.
 *
 * @par Error
 * - EINVAL: The History instance is invalid.
 * - ERANGE: There is no step corresponding with the time step.
 */
const State* History_getAction( const History* self, size_t t ) ;

/**
 * @brief Get the reward received at a given time step.
 * @memberof History
 *
 * @details This function will retrieve the reward that was received
 * at time step @a t of the specified History.
 *
 * @param[in] self A pointer to the History instance to query.
 * @param[in] t The time step of the Action to retrieve.
 *
 * @return An integer expressing the reward received at the specified
 * time step in the current History encoded as a fixed-point value.
 */
int32_t History_getReward( const History* self, size_t t ) ;

/**
 * @brief Get the total return of the specified History.
 * @memberof History
 *
 * @details This will return the total return value associated with
 * the current History instance.
 *
 * @param[in] self A pointer to a History intance.
 *
 * @return An integer expressing the return value associated with the
 * current History encoded as a fixed-point value.
 */
int32_t History_getReturn( const History* self ) ;

/**
 * @brief Get the state at a given time step of a History.
 * @memberof History
 *
 * @details This will retrieve the State of the Environment at time
 * step @a t of the current History of a Task execution. This State
 * represents the configuration of the Environment when the agent
 * executed the @a t-th macro Action of the Task for a particular
 * History instance.
 *
 * @param[in] self A pointer to an immutable History.
 * @param[in] t The time step for which to retrieve the Environment's
 *	State.
 *
 * @return The State at time step @a t in the current Task History. In
 * the case of an error, this function will return @c NULL and set
 * @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The History instance is invalid.
 * - ERANGE: The specified time step does not exist in the History.
 */
const State* History_getState( const History* self, size_t t ) ;

#endif	/* __HISTORY_H__ */
