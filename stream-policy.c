/* -*- Mode: C -*- */

/**
 * @file
 * @brief Implementation of a stream Policy.
 */

/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* Local includes */
#include "stream-policy.h"

struct _stream_policy {
	Policy_EXTEND ;
	const Environment*		env ;
	FILE*				istream ;
	StreamPolicy_readAction_f*	readAction ;
} ;

const State*
StreamPolicy_apply( const Policy* self_base, const State* s )
{
	const State* a = NULL ;
	const StreamPolicy* self = ( const StreamPolicy* )self_base ;
	if ( self && s && self->readAction ) {
		a = self->readAction( self->env, s, self->istream ) ;
	}
	return a ;
}

Policy*
StreamPolicy_Create( const Environment* environment, StreamPolicy_readAction_f* readAction, FILE* stream )
{
	StreamPolicy* policy = calloc( 1, sizeof( StreamPolicy ) ) ;
	if ( policy ) {
		policy->env = environment ;
		policy->readAction = readAction ;
		policy->istream = stream ;
		policy->apply = StreamPolicy_apply ;
	}
	return ( Policy* )policy ;
}
