/* -*- Mode C -*- */
/*
 * Copyright (c) 2014-2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup HashMap
 * @ingroup Test
 * @brief Unit tests for the HashMap container.
 * @{
 *
 * @file
 *
 * @brief Test program to validate the HashMap container.
 */

/* POSIX includes */
#include <stdarg.h>
#include <stdio.h>

/* Local includes */
#include "test.h"
#include <util/hashmap.h>

typedef struct _entry {
	uint16_t	key ;
	char		value[ 80 ] ;
} Entry ;

size_t
Entry_hash( const void* item )
{
	size_t hash = 0 ;
	const Entry* entry = item ;
	if ( entry ) {
		hash = entry->key / 16 ;
	}
	return hash ;
}

int
Entry_compare( const void* item1, const void* item2, size_t entry_size )
{
	int order = 0 ;
	const Entry* entry1 ;
	const Entry* entry2 ;
	if ( (entry1 = item1) && (entry2 = item2) ) {
		order = entry1->key - entry2->key ;
	}
	return order ;
}

int
test_HashMap_Create( va_list args )
{
	int rc = 0 ;
	HashMap** map = va_arg( args, HashMap** ) ;
	HashMap_hashFunc* hash = va_arg( args, HashMap_hashFunc* ) ;
	Set_Comparator* compare = va_arg( args, Set_Comparator* ) ;
	rc = ( (*map = HashMap_Create( hash, compare )) != NULL ) ;
	return rc ;
}

int
test_HashMap_addItem( va_list args )
{	
	HashMap* map ;
	Entry* entry ;
	int rc = 0 ;
		
	map = va_arg( args, HashMap* ) ;
	entry = va_arg( args, Entry* ) ;
	
	if ( map ) {
		if ( HashMap_addItem( map, entry, sizeof( *entry ) ) == 0 ) {
			/* TODO:: Inspect the item. */
			rc = 1 ;
		}
	}
	return rc ;
}

int
test_HashMap_getItem( va_list args )
{
	HashMap* map ;
	size_t hash ;
	const Entry* entry ;
	int pass = 0 ;

	map = va_arg( args, HashMap* ) ;
	hash = va_arg( args, size_t ) ;
	entry = va_arg( args, const Entry* ) ;

	if ( map && entry ) {
		Iterator i = HashMap_getItem( map, hash ) ;
		const Entry* item = Iterator_getItem( i, NULL ) ;
		if ( item ) {
			pass = !strcmp( item->value, entry->value ) ;
		}
	}
	return pass ;
}

int
test_Iterator_next( va_list args )
{
	int pass = 0 ;
	HashMap* map = va_arg( args, HashMap* ) ;
	const Entry* entries = va_arg( args, const Entry* ) ;
	size_t num_entries = va_arg( args, size_t ) ;
	if ( map ) {
		size_t x ;
		const Entry* member ;
		Iterator i = Set_getFirst( ( const Set* )map ) ;
		
		pass = 1 ;
		x = 0 ;
		while ( pass && x < num_entries && (member = Iterator_next( &i, NULL )) ) {
			pass = !strcmp( entries[ x ].value, member->value ) ;
			x++ ;
		}
	}
	return pass ;
}

int
main( int argc, char* argv[] )
{
	HashMap* map ;
	Unit results = { 0 } ;
	Entry entries[] = {
		{ .key = 31, .value = "First Entry" },
		{ .key = 39, .value = "Second Entry" },
		{ .key = 47, .value = "Third Entry" },
		{ .key = 55, .value = "Fourth Entry" },
	} ;
	
	Unit_runtest( &results, "Allocate a HashMap of Entrys...", 1, test_HashMap_Create, &map, Entry_hash, Entry_compare ) ;
	Unit_runtest( &results, "Add an item to an empty HashMap...", 1, test_HashMap_addItem, map, &entries[ 0 ] ) ;
	Unit_runtest( &results, "Add an item to a non-empty HashMap...", 1, test_HashMap_addItem, map, &entries[ 2 ] ) ;
	Unit_runtest( &results, "Get the first item with hash 2...", 1, test_HashMap_getItem, map, 2, &entries[ 2 ] ) ;
	Unit_runtest( &results, "Add an item to a non-empty HashMap...", 1, test_HashMap_addItem, map, &entries[ 1 ] ) ;
	Unit_runtest( &results, "Add an item to a non-empty HashMap...", 1, test_HashMap_addItem, map, &entries[ 3 ] ) ;
	Unit_runtest( &results, "Get the first item with hash 2...", 1, test_HashMap_getItem, map, 2, &entries[ 1 ] ) ;
	Unit_runtest( &results, "Validate the contents of the map...", 1, test_Iterator_next, map, entries, 4 ) ;
	HashMap_Delete( &map ) ;
	Unit_writeReport( stdout, &results ) ;
	return 0 ;
}

/**
 * @}
 */
