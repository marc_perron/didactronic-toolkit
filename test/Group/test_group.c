/* -*- Mode C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup Group
 * @ingroup Test
 * @brief Unit tests for the Group structure.
 * @{
 *
 * @file
 *
 * @brief Test program to validate Groups and associated types.
 */

/* POSIX includes */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/* Local includes */
#include <test.h>
#include <util/group.h>

static size_t
hashItem( const void* item )
{
	size_t hash = -1 ;
	const char* e = item ;
	if ( e ) {
		hash = *e ;
	}
	return hash ;
}

int
test_RelationSet_Create( va_list args )
{
	int pass = 0 ;
	RelationSet** relations = va_arg( args, RelationSet** ) ;
	HashMap_hashFunc* hashItem = va_arg( args, HashMap_hashFunc* ) ;
	if ( (*relations = RelationSet_Create( hashItem )) ) {
		pass = 1 ;
	}
	return pass ;
}

int
test_RelationSet_addRelation( va_list args )
{
	int pass = 0 ;
	RelationSet* relations = va_arg( args, RelationSet* ) ;
	const char* item = va_arg( args, const char* ) ;
	const char* image = va_arg( args, const char* ) ;

	if ( RelationSet_addRelation( relations, item, image ) == 0 ) {
		const char* tmp = RelationSet_findImage( relations, item ) ;
		pass = ( tmp == image ) ;
	}
	return pass ;
}

int
main( int argc, char* argv[] )
{
	Group* group = NULL ;
	Unit results = { 0 } ;
	RelationSet* relations = NULL ;
	char items[] = { 'A'-1, 'A', 'B', 'C', 'D', 'E', 'F', 'F'+1 } ;

	Unit_runtest( &results, "Allocate a relation set", 1, test_RelationSet_Create, &relations, hashItem ) ;
	Unit_runtest( &results, "Add relation Fs->F", 1, test_RelationSet_addRelation, relations, &items[ 7 ], &items[ 6 ] ) ;
	Unit_runtest( &results, "Add relation As^-1->F", 1, test_RelationSet_addRelation, relations, &items[ 0 ], &items[ 6 ] ) ;

	Group_Delete( &group ) ;
	HashMap_Delete( ( HashMap** )&relations ) ;
	Unit_writeReport( stdout, &results ) ;
	return 0 ;
}

/**
 * @}
 */
