/* -*- Mode C -*- */
/*
 * Copyright (c) 2014-2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup Dictionary
 * @ingroup Test
 * @brief Unit tests for the Dictionary container.
 * @{
 *
 * @file
 *
 * @brief Test program to validate the Dictionary container.
 */

/* POSIX includes */
#include <stdarg.h>
#include <stdio.h>

/* Local includes */
#include "test.h"
#include <util/dictionary.h>

typedef struct _entry {
	uint16_t	key ;
	char		value[ 80 ] ;
} Entry ;

size_t
Entry_hash( const HashMap* self, const void* item )
{
	size_t hash = 0 ;
	const Entry* entry = item ;
	if ( entry ) {
		hash = entry->key / 16 ;
	}
	return hash ;
}

int
Entry_compare( const void* item1, const void* item2, size_t entry_size )
{
	int order = 0 ;
	const Entry* entry1 ;
	const Entry* entry2 ;
	if ( (entry1 = item1) && (entry2 = item2) ) {
		order = entry1->key - entry2->key ;
	}
	return order ;
}

int
test_Dictionary_Create( va_list args )
{
	int rc = 0 ;
	return rc ;
}

int
test_Dictionary_addEntry( va_list args )
{	
	int rc = 0 ;	
	Dictionary* dict = va_arg( args, Dictionary* ) ;
	const void* key = va_arg( args, const void* ) ;
	size_t key_size = va_arg( args, size_t ) ;
	const void* value = va_arg( args, const void* ) ;
	size_t value_size = va_arg( args, size_t ) ;

	if ( dict ) {
		int ret ;
		ret = Dictionary_addEntry( dict, key, key_size, value, value_size ) ;
		if ( !ret ) {
			rc = 1 ;
		}
	}
	return rc ;
}

int
test_Dictionary_findEntries( va_list args )
{
	Iterator i ;
	int pass = 0 ;
	Dictionary* dict = va_arg( args, Dictionary* ) ;
	const void* key = va_arg( args, const void* ) ;

	if ( (i = Dictionary_findEntries( dict, key )) ) {
		pass = 1 ;
	}
	return pass ;
}

int
test_Dictionary_NextEntry( va_list args )
{
	int pass = 0 ;
	Iterator* current = va_arg( args, Iterator* ) ;
	size_t* size = va_arg( args, size_t* ) ;
	int more = va_arg( args, int ) ;

	const void* entry = Dictionary_NextEntry( current, size ) ;
	if ( more && entry ) {
		pass = 1 ;
	}
	else if ( !more && !entry ) {
		pass = 1 ;
	}
	return pass ;
}

int
main( int argc, char* argv[] )
{
	Dictionary* dict ;
	Unit results = { 0 } ;
	Entry entries[] = {
		{ .key = 31, .value = "First Entry" },
		{ .key = 31, .value = "Second Entry" },
		{ .key = 47, .value = "Third Entry" },
		{ .key = 55, .value = "Fourth Entry" },
	} ;
	
	Unit_runtest( &results, "Create a Dictionary...", 1, test_Dictionary_Create ) ;
	HashMap_Delete( &dict ) ;
	Unit_writeReport( stdout, &results ) ;
	return 0 ;
}

/**
 * @}
 */
