/* -*- Mode C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup List
 * @ingroup Test
 * @brief Unit tests for the List container.
 * @{
 *
 * @file
 * @brief Define the List Unit test program.
 */

/* POSIX includes. */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/* Local includes */
#include "test.h"

/* Unit under test. */
#include <util/list.h>

struct _entry {
	uint32_t	index ;
	uint32_t	final_index ;
	const char*	data ;
} entries[] = {
	{ .index = 0, .final_index = 0, .data = "Entry Zero" },
	{ .index = 640, .final_index = 642, .data = "Entry Six Fourty" },
	{ .index = 1, .final_index = 1, .data = "Entry One" },
	{ .index = 76, .final_index = 76, .data = "Entry Seventy Six" },
} ;

test_List_Create( va_list args )
{
	int rc = 0 ;
	List** list = va_arg( args, List** ) ;
	Set_Comparator* compare = va_arg( args, Set_Comparator* ) ;
	rc = ( (*list = List_Create( compare )) != NULL ) ;
	return rc ;
}

test_List_insert( va_list args )
{
	int rc = 0 ;
	List* list = va_arg( args, List* ) ;
	uint32_t idx = va_arg( args, uint32_t ) ;
	const char* data = va_arg( args, const char* ) ;
	if ( List_insert( list, idx, data, strlen( data ) ) == 0 ) {
		const char* item = List_getItem( list, idx ) ;
		rc = !( strcmp( item, data ) ) ;
	}
	return rc ;
}

test_List_getItem( va_list args )
{
	int rc = 0 ;
	size_t item_size = 0 ;
	List* list = va_arg( args, List* ) ;
	uint32_t idx = va_arg( args, uint32_t ) ;
	const char* exp_data = va_arg( args, const char* ) ;
	Iterator i = List_getIterator( list, idx ) ;
	const void* item = Iterator_getItem( i, &item_size ) ;
	if ( item ) {
		rc = ( item_size == strlen( item ) 
		       && !strcmp( exp_data, item ) ) ;
	}
	return rc ;
}

static int
compareEntries( const void* item1, const void* item2, size_t item_size )
{
	int order = 0 ;
	const char* entry1 = item1 ;
	const char* entry2 = item2  ;
	if ( entry1 && entry2 ) {
		order = strcmp( entry1, entry2 ) ;
	}
	return order ;
}

int
main( int argc, char* argv[] )
{
	List* list ;
	Unit results = { 0 } ;

	Unit_runtest( &results, "Create an empty List...", 1, test_List_Create, &list, compareEntries ) ;
	Unit_runtest( &results, "Insert item 0 into the list...", 1, test_List_insert, list, entries[ 0 ].index, entries[ 0 ].data ) ;
	Unit_runtest( &results, "Insert item 640 into the list...", 1, test_List_insert, list, entries[ 1 ].index, entries[ 1 ].data ) ;
	Unit_runtest( &results, "Insert item 1 into the List...", 1, test_List_insert, list, entries[ 2 ].index, entries[ 2 ].data ) ;
	Unit_runtest( &results, "Insert item 76 into the List...", 1, test_List_insert, list, entries[ 3 ].index, entries[ 3 ].data ) ;
	Unit_runtest( &results, "Validate item 0 of the List...", 1, test_List_getItem, list, entries[ 0 ].final_index, entries[ 0 ].data ) ;
	Unit_runtest( &results, "Validate item 642 of the List...", 1, test_List_getItem, list, entries[ 1 ].final_index, entries[ 1 ].data ) ;
	Unit_runtest( &results, "Validate item 1 of the List...", 1, test_List_getItem, list, entries[ 2 ].final_index, entries[ 2 ].data ) ;
	Unit_runtest( &results, "Validate item 76 of the List...", 1, test_List_getItem, list, entries[ 3 ].final_index, entries[ 3 ].data ) ;
	List_Delete( &list ) ;
	Unit_writeReport( stdout, &results ) ;
	return 0 ;
}

/**
 * @}
 */

