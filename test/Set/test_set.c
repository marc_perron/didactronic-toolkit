/* -*- Mode C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup Set
 * @ingroup Test
 * @brief Unit tests for the Set container.
 * @{
 *
 * @file
 * @brief Test program to validate the Set container.
 */

/* POSIX includes */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/* Local includes */
#include <test.h>
#include <util/set.h>

int
element_compare( const void* item1, const void* item2, size_t size )
{
	const char* e1 = item1 ;
	const char* e2 = item2 ;
	return strcmp( e1, e2 ) ;
}

int
test_Set_Create( va_list args )
{
	int pass = 0 ;
	Set** set_ptr = va_arg( args, Set** ) ;
	Set_Comparator* compare = va_arg( args, Set_Comparator* ) ;
	
	if ( set_ptr ) {
		*set_ptr = Set_Create( compare ) ;
		pass = ( *set_ptr != NULL ) ;
	}
	return pass ;
}

int
test_Set_insert( va_list args )
{
	int pass = 0 ;
	Set* set = va_arg( args, Set* ) ;
	void* item = va_arg( args, void* ) ;
	size_t item_size = va_arg( args, size_t ) ;
	if ( set ) {
		pass = ( Set_insert( set, item, item_size ) &&
			 Set_contains( set, item, item_size ) ) ;
	}
	return pass ;
}

int
test_Iterator_next( va_list args )
{
	int pass = 0 ;
	Set* set = va_arg( args, Set* ) ;
	char** items = va_arg( args, char** ) ;
	size_t num_items = va_arg( args, size_t ) ;
	if ( set ) {
		size_t x ;
		const char* item ;
		Iterator i = Set_getFirst( set ) ;

		pass = 1 ;
		x = 0 ;
		while ( pass && x < num_items && (item = Iterator_next( &i, NULL )) ) {
			pass = !strcmp( items[ x ], item ) ;
			x++ ;
		}
	}
	return pass ;
}

int
test_Set_Union( va_list args )
{
	int pass = 0 ;
	Set** set = va_arg( args, Set** ) ;
	const Set* set1 = va_arg( args, const Set* ) ;
	const Set* set2 = va_arg( args, const Set* ) ;
	size_t num_items = va_arg( args, size_t ) ;
	
	*set = Set_Union( set1, set2, NULL ) ;
	if ( *set && Set_getSize( *set ) == num_items ) {
		pass = 1 ;
	}
	return pass ;
}

int
main( int argc, char* argv[] )
{
	Set* set = NULL ;
	Set* set_union = NULL ;
	Set* set2 = Set_Create( element_compare ) ;
	Unit results = { 0 } ;
	char* items[ 4 ] = {
		"First Item",
		"Fourth Item",
		"Second Item",
		"Third Item",
	} ;
	Unit_runtest( &results, "Allocate a Set using the default comparator", 1, test_Set_Create, &set, element_compare ) ;
	Unit_runtest( &results, "Insert an item into the Set", 1, test_Set_insert, set, items[ 0 ], strlen( items[ 0 ] ) + 1 ) ;
	Unit_runtest( &results, "Insert an item into the Set", 1, test_Set_insert, set2, items[ 1 ], strlen( items[ 1 ] ) + 1 ) ;
	Unit_runtest( &results, "Insert an item into the Set", 1, test_Set_insert, set2, items[ 2 ], strlen( items[ 2 ] ) + 1 ) ;
	Unit_runtest( &results, "Insert an item into the Set", 1, test_Set_insert, set, items[ 2 ], strlen( items[ 2 ] ) + 1 ) ;
	Unit_runtest( &results, "Insert an item into the Set", 1, test_Set_insert, set, items[ 3 ], strlen( items[ 3 ] ) + 1 ) ;
	Unit_runtest( &results, "Test the Union of two sets", 1, test_Set_Union, &set_union, set, set2, 4 ) ;
	Unit_runtest( &results, "Iterate over the contents of the Set", 1, test_Iterator_next, set_union, items, 4 ) ;
	Set_Delete( &set ) ;

	Unit_writeReport( stdout, &results ) ;
	return 0 ;
}

/**
 * @}
 */
