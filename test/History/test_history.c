/* -*- Mode C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup History
 * @ingroup Test
 * @brief Unit tests for the History module.
 * @{
 *
 * @file
 * @brief Test program to validate the History container type.
 */

/* POSIX includes */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/* Local includes */
#include <history.h>
#include <test.h>

int
test_History_Create( va_list args )
{
	int rc = 0 ;
	History** history = va_arg( args, History** ) ;
	rc = ( (*history = History_Create()) != NULL ) ;
	return rc ;
}

int
test_History_Delete( va_list args )
{
	int rc = 0 ;
	History** history = va_arg( args, History** ) ;
	History_Delete( history ) ;
	if ( *history == NULL ) {
		rc = 1 ;
	}
	return rc ;
}

int
test_History_addStep( va_list args )
{
	int rc = 0 ;
	History** history = va_arg( args, History** ) ;
	const State* s = va_arg( args, const State* ) ;
	const State* a = va_arg( args, const State* ) ;
	int32_t reward = va_arg( args, int32_t ) ;
	if ( history && *history ) {
		const void* list = *history ;
		size_t size = List_getSize( list ) ;
		if ( History_addStep( history, s, a, reward ) >= 0 ) {
			rc = ( size+1 == List_getSize( list ) ) ;
		}
	}
	return rc ;
}

int
test_History_getReturn( va_list args )
{
	int rc = 0 ;
	const History* history = va_arg( args, const History* ) ;
	int32_t expected_return = va_arg( args, int32_t ) ;
	int32_t actual_return = History_getReturn( history ) ;
	rc = ( expected_return == actual_return ) ;
	return rc ;
}

int
main( int argc, char* argv[] )
{
	Unit results = { 0 } ;
	History* history = NULL ;

	Unit_runtest( &results, "Create an empty History...", 1, test_History_Create, &history ) ;
	Unit_runtest( &results, "Add a step to the History...", 1, test_History_addStep, &history, 0, 0, 1 );
	Unit_runtest( &results, "Add a step to the History...", 1, test_History_addStep, &history, 1, 0, 1 );
	Unit_runtest( &results, "Add a step to the History...", 1, test_History_addStep, &history, 2, 0, 1 );
	Unit_runtest( &results, "Ensure the return value is correct...", 1, test_History_getReturn, history, 3 ) ;
	Unit_runtest( &results, "Delete a History instance...", 1, test_History_Delete, &history ) ;
	Unit_writeReport( stdout, &results ) ;
	return 0 ;
}

/**
 * @}
 */
