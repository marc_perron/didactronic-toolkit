/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup Graph
 * @ingroup Test
 * @brief Unit tests for the Graph data structure.
 * @{
 *
 * @file
 *
 * @brief Test program to validate the Graph data structure.
 */

/* POSIX includes */
#include <stdbool.h>

/* Local includes */
#include "test.h"
#include <util/graph.h>

typedef struct _test_vertex {
	char	label ;
	bool	terminal ;
	size_t	ref ;
} Vertex ;

static size_t
Vertex_hash( const void* vertex_ptr )
{
	size_t hash = -1 ;
	const Vertex* v = vertex_ptr ;
	if ( v ) {
		hash = v->label - 'A' + 1 ;
	}
	return hash ;
}

int
test_Graph_init( va_list args )
{
	int pass = 0 ;
	Graph* graph = va_arg( args, Graph* ) ;
	const char* name = va_arg( args, const char* ) ;
	size_t k = va_arg( args, size_t ) ;
	if ( Graph_init( graph, name, k, Vertex_hash ) == 0 ) {
		pass = 1 ;
	}
	return pass ;
}

int
test_Graph_createVertex( va_list args )
{
	int pass = 0 ;
	Graph* graph = va_arg( args, Graph* ) ;
	size_t k = va_arg( args, size_t ) ;
	const Vertex* v = va_arg( args, const Vertex* ) ;
	size_t* ref = va_arg( args, size_t* ) ;
	if ( (*ref = Graph_createVertex( graph, k, v, sizeof( *v ) )) > 0 ) {
		pass = 1 ;
	}
	return pass ;
}

int
test_Graph_addEdge( va_list args )
{
	int pass = 0 ;
	Graph* graph = va_arg( args, Graph* ) ;
	size_t v1_ref = va_arg( args, size_t ) ;
	size_t v2_ref = va_arg( args, size_t ) ;
	const char* label = va_arg( args, const char* ) ;
	if ( Graph_addEdge( graph, v1_ref, v2_ref, label ) == 0 ) {
		pass = 1 ;
	}
	return pass ;
}

int
test_Graph_enumerateNeighbours( va_list args )
{
	int n = 0 ;
	int pass = 0 ;
	Graph* graph = va_arg( args, Graph* ) ;
	size_t v_ref = va_arg( args, size_t ) ;
	size_t num_neighbours = va_arg( args, size_t ) ;
	Iterator i = Graph_getNeighbourhood( graph, v_ref ) ;
	while ( Graph_NextNeighbour( &i, v_ref ) ) {
		n++ ;
	}

	pass = ( n == num_neighbours ) ;
	return pass ;
}

int
main( int argc, char* argv[] )
{
	const size_t k = 1 ;
	Graph graph = { 0 } ;
	Unit results = { 0 } ;
	Vertex vertices[] = {
		{ .label = 'A', .terminal = false },
		{ .label = 'B', .terminal = false },
		{ .label = 'C', .terminal = false },
		{ .label = 'D', .terminal = false },
		{ .label = 'E', .terminal = false },
		{ .label = 'F', .terminal = true },
	} ;
	Unit_runtest( &results, "Initialize a Graph named 'RandomWalk'...", 1, test_Graph_init, &graph, "RandomWalk", k ) ;
	Unit_runtest( &results, "Add vertex to the RandomWalk graph...", 1, test_Graph_createVertex, &graph, k-1, &vertices[ 0 ], &vertices[ 0 ].ref ) ;
	Unit_runtest( &results, "Add vertex to the RandomWalk graph...", 1, test_Graph_createVertex, &graph, k-1, &vertices[ 1 ], &vertices[ 1 ].ref ) ;
	Unit_runtest( &results, "Add vertex to the RandomWalk graph...", 1, test_Graph_createVertex, &graph, k-1, &vertices[ 2 ], &vertices[ 2 ].ref ) ;
	Unit_runtest( &results, "Add vertex to the RandomWalk graph...", 1, test_Graph_createVertex, &graph, k-1, &vertices[ 3 ], &vertices[ 3 ].ref ) ;
	Unit_runtest( &results, "Add vertex to the RandomWalk graph...", 1, test_Graph_createVertex, &graph, k-1, &vertices[ 4 ], &vertices[ 4 ].ref ) ;
	Unit_runtest( &results, "Add vertex to the RandomWalk graph...", 1, test_Graph_createVertex, &graph, k-1, &vertices[ 5 ], &vertices[ 5 ].ref ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 0 ].ref, vertices[ 1 ].ref, "AtoB(red)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 0 ].ref, vertices[ 5 ].ref, "AtoF(blue)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 1 ].ref, vertices[ 2 ].ref, "BtoC(red)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 1 ].ref, vertices[ 0 ].ref, "BtoA(blue)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 2 ].ref, vertices[ 3 ].ref, "CtoD(red)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 2 ].ref, vertices[ 1 ].ref, "CtoB(blue)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 3 ].ref, vertices[ 4 ].ref, "DtoE(red)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 3 ].ref, vertices[ 2 ].ref, "DtoC(blue)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 4 ].ref, vertices[ 5 ].ref, "EtoF(red)" ) ;
	Unit_runtest( &results, "Add an edge to the RandomWalk graph...", 1, test_Graph_addEdge, &graph, vertices[ 4 ].ref, vertices[ 3 ].ref, "EtoD(blue)" ) ;
	Unit_runtest( &results, "Enumerate the neighbours of vertex 'A'...", 1, test_Graph_enumerateNeighbours, &graph, vertices[ 0 ].ref, 2 ) ;
	Unit_runtest( &results, "Enumerate the neighbours of vertex 'B'...", 1, test_Graph_enumerateNeighbours, &graph, vertices[ 1 ].ref, 2 ) ;
	Unit_runtest( &results, "Enumerate the neighbours of vertex 'C'...", 1, test_Graph_enumerateNeighbours, &graph, vertices[ 2 ].ref, 2 ) ;
	Unit_runtest( &results, "Enumerate the neighbours of vertex 'D'...", 1, test_Graph_enumerateNeighbours, &graph, vertices[ 3 ].ref, 2 ) ;
	Unit_runtest( &results, "Enumerate the neighbours of vertex 'E'...", 1, test_Graph_enumerateNeighbours, &graph, vertices[ 4 ].ref, 2 ) ;
	Unit_runtest( &results, "Enumerate the neighbours of vertex 'F'...", 1, test_Graph_enumerateNeighbours, &graph, vertices[ 5 ].ref, 0 ) ;
	
	Graph_cleanup( &graph ) ;
	Unit_writeReport( stdout, &results ) ;
	return 0 ;
}

/**
 * @}
 */
