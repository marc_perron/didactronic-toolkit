/* -*- Mode C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup Test
 * @{
 *
 * @file
 *
 * @brief Declaration of utilities used to write unit tests.
 */

/* POSIX includes */
#include <stdarg.h>
#include <stdio.h>

/**
 * @class Unit
 * @brief Structure used to accumulate test results.
 *
 * @details An instance of this structure will be used to accumulate
 * the results of a series of tests on a single unit.
 */
typedef struct {
	/**
	 * The number of tests that were executed for the unit under
	 * test. */
	int	run ;
	/**
	 * The number of tests that were reported to have passed.
	 */
	int	pass ;
	/**
	 * The number of tests that were expected to pass.
	 */
	int	xpass ;
	/**
	 * The number of expected failures for unit under test.
	 */
	int	xfail ;
} Unit ;

/**
 * @brief Prototype of test functions.
 * @memberof Unit
 *
 * @details This type describes the prototype of functions which are
 * invoked by the unit test framework. Test functions will have a
 * variable number of arguments. The provider of the test function
 * must know a priori how many arguments are expected, and their
 * types.
 *
 * @see Unit_runtest
 *
 * @param[in] args A variadic list of arguments provided to the
 *	function under test.
 *
 * @retval 0 The test has failed.
 * @retval >0 The test has passed.
 * @retval <0 An error occurred while running the test.
 */
typedef int Unit_test_f( va_list args ) ;

/**
 * @brief Execute a single test and update the results.
 * @memberof Unit
 *
 * @details This function will execute the specified test, updating
 * the results accumulated thus far in the Unit structure. A
 * message will be written to the standard output followed by the
 * result of the test. A test can be marked as expected to fail by
 * setting the @a xpass argument to 0. The possible outcomes of a test
 * are:
 * - Expected Pass
 * - Unexpected Pass
 * - Expected Failure
 * - Unexpected Failure
 * - Test Error
 *
 * @pre This function takes a variable number of arguments, if the
 * correct number and types of arguments are not provided to the test
 * function, the behaviour is undefined (it will likely cause a
 * segmentation fault).
 *
 * @post The results accumulated so far for the Unit will be updated
 * with the result of the function under test.
 *
 * @param[in] self The test harness structure which represents the
 *	Unit under test. The test results will be accumulated in this
 *	structure.
 * @param[in] msg A message to print to the standard output which
 *	describes the current test"
 * @param[in] xpass A flag indicating whether the current test is
 *	expected to pass.
 * @param[in] func A pointer to the function under test.
 * @param[in,out] ... A variable number of arguments to pass to the
 *	test function. Arguments that are passed by reference may be
 *	updated by the test function for use in later tests (i.e. to
 *	set up data structures for more complex tests).
 */
void Unit_runtest( Unit* self, const char* msg, int xpass, Unit_test_f* func, ... ) ;

/**
 * @brief Write a test report to the specified file descriptor.
 * @memberof Unit
 *
 * @details This function will write a human readable report of the
 * tests accumulated thus far for the specified Unit. The report will
 * be written to the file stream specified by the provided descriptor.
 *
 * @param[in] self A pointer to a structure representing the Unit
 *	under test.
 * @param[in] fd A pointer to a FILE stream to which the report will
 *	be written.
 */
void Unit_writeReport( FILE* fd, const Unit* self ) ;

/**
 * @}
 */
