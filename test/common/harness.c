/* -*- Mode C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup Test
 * @brief Unit Test Framework
 *
 * @details The utilities defined in this module are used to build
 * unit tests that are used to validate the correctness of the
 * Didactronic Toolkit.
 * @{
 *
 * @file
 *
 * @brief Implementation of the unit test utilities.
 */

/* POSIX includes */
#include <stdio.h>

/* Local includes */
#include "test.h"

void
Unit_runtest( Unit* results, const char* msg, int xpass, Unit_test_f* func, ... )
{
	va_list args ;
	va_start( args, func ) ;
	fprintf( stdout, "%-70s", msg ) ;
	if ( !results || !func ) {
		fprintf( stdout, "[ error ]\n" ) ;
	}
	else {
		if ( func( args ) ) {
			results->pass++ ;
			fprintf( stdout, "[ pass ]\n" ) ;
			if ( xpass ) {
				results->xpass++ ;
			}
		}
		else if ( !xpass ) {
			results->xfail++ ;
			fprintf( stdout, "[ xfail ]\n" ) ;
		}
		else {
			fprintf( stdout, "[ fail ]\n" ) ;
		}
		results->run++ ;
	}
	va_end( args ) ;
}

void
Unit_writeReport( FILE* fd, const Unit* results )
{
	fprintf( fd, "\n\nTest Report:\n^^^^^^^^^^^^\n" ) ;
	fprintf( fd, "Tests Executed:\t\t %d\n", results->run ) ;
	fprintf( fd, "Passed:\t\t\t %d (%d new tests passed)\n", results->pass, results->pass - results->xpass ) ;
	fprintf( fd, "Expected Failures:\t %d\n", results->xfail ) ;
	fprintf( fd, "Un-expected Failures:\t %d\n", results->run - results->pass ) ;
}

/**
 * @}
 */
