/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>

/* Local includes */
#include "util/hashmap.h"

/**
 * @file
 * @brief Definition of a HashMap container type.
 *
 * @details This file defines an implementation of a HashMap container
 * which is a specialization of a Set that provides a hash index to
 * elements contained therein.
 */

typedef union _hashmap_impl
{
	HashMap				_public ;
	struct {
		List			_base ;
		HashMap_hashFunc*	hash ;
		size_t			slice_size ;
		size_t			min_index ;
	} ;
} HashMapImpl ;

static size_t
HashMap_defaultHash( const void* entry )
{
	size_t hash = 0 ;
	if ( entry ) {
		intptr_t wide_hash = ( intptr_t )entry ;
		int shift = sizeof( intptr_t ) - sizeof( size_t ) ;
		hash = wide_hash / 1024 ;
	}
	return hash ;
}

size_t
HashMap_hash( const HashMap* self, const void* entry )
{
	size_t hash = -1 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		HashMapImpl* hashmap = ( HashMapImpl* )self ;
		hash = hashmap->hash( entry ) ;
	}
	return hash ;
}

extern void List_init( List* self, Set_Comparator* compare ) ;

void
HashMap_init( HashMap* self, HashMap_hashFunc* hash, Set_Comparator* compareEntries )
{
	if ( self ) {
		HashMapImpl* _self = ( HashMapImpl* )self ;
		List_init( ( List* )self, compareEntries ) ;
		_self->hash = hash ? hash : HashMap_defaultHash ;
		_self->slice_size = SIZE_MAX/4096 ;
		_self->min_index = 0 ;
	}
}

HashMap*
HashMap_Create( HashMap_hashFunc* hash, Set_Comparator* compareEntries )
{
	HashMap* new = malloc( sizeof( HashMapImpl ) ) ;
	if ( !new ) {
		errno = ENOMEM ;
	}
	else {
		HashMap_init( new, hash, compareEntries ) ;
	}
	return new ;
}

void
HashMap_Delete( HashMap** self_ptr )
{
	HashMap* self ;
	if ( self_ptr && (self = *self_ptr) ) {
		List_Delete( ( List** )self_ptr ) ;
		*self_ptr = NULL ;
	}
}

int
HashMap_addItem( HashMap* self, const void* item, size_t item_size )
{
	int rc = -1 ;
	
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		HashMapImpl* _self = ( HashMapImpl* )self ;
		size_t idx = HashMap_hash( self, item ) ;

		if ( List_getSize( ( List* )self ) == 0 ) {
			_self->min_index = idx ;
			rc = List_insert( ( List* )self, 0, item, item_size ) ;
		}
		else {
			const void* element ;

			if ( idx < _self->min_index ) {
				List_pad( ( List* )self, _self->min_index - idx ) ;
				_self->min_index = idx ;
			}

			idx -= _self->min_index ;
			if ( (element = List_getItem( ( List* )self, idx )) ) {
				if ( Set_compare( ( Set* )self, element, item, item_size ) > 0 ) {
					rc = List_setItem( ( List* )self, idx, item, item_size ) ;
				}
				else if ( Set_insert( ( Set* )self, item, item_size ) ) {
					rc = 0 ;
				}
			}
			else if ( (rc = List_setItem( ( List* )self, idx, item, item_size )) != 0 ) {
				rc = List_insert( ( List* )self, idx, item, item_size ) ;
			}
		}
	}
	return rc ;
}

Iterator
HashMap_getItem( const HashMap* self, size_t hash )
{
	Iterator i = 0 ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else {
		const HashMapImpl* _self = ( const HashMapImpl* )self ;
		size_t idx = hash - _self->min_index ;
		i = List_getIterator( ( const List* )self, idx ) ;
	}
	return i ;
}
