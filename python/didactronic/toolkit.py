import numpy
import random
import itertools

class State:
    """The generic base implementation of States.  """
    def __init__( self, id ):
        self.id = id

    def __hash__( self ):
        return self.id

    def isTerminal( self ):
        return False


class Action:
    """Base class of Actions in a reinforcement learning task.

    This type defines the common base shared by all Action
    implementations in a reinforcement learning task.

    """

    def __init__( self, id = None ):
        """Create a new Action with the specified id.

        This initializer will be called by derived types to initialize
        the common action data.

        """
        self.id = id

    def apply( self, state ):
        """Apply the current action to the specified state.

        This function will apply the current action to the specified
        state to determine the possible following states. It is
        possible that the application of an action can yield more than
        one state. The default implementation of this function simply
        returns a list containing a single item; the unmodified state.

        """
        return [ state ]

    def isValid( self, state ):
        """Determine if the current action is valid in a given state.

        Derived types should override this function to assert whether
        or not the current action instance is valid in a given
        state. The default implementation assumes that the action is
        always valid.

        state: The state in which the validity of the current action
        	will be asserted.

        Return: A boolean flag indicating whether or not the action is
        valid in the given state (always True).

        """
        return True

    @staticmethod
    def CreateInteractively():
        id = input( "Action Id: " )
        return Action( id )

class Agent:
    """Abstract class representing an Agent in a reinforcement learning
    task.

    An Agent represents an actor and learner in a reinforcement
    learning task. An instance of a Task to execute will be associated
    with the Agent as well as a Policy that will be followed during
    execution. Instances of this class will expose an Agent's
    capabilities which represents the available controls.

    """

    def __init__( self, capabilities=[], policy=None ):
        """Create a new Agent instance with the specified set of capabilities.

        Initially, the task associated with the Agent will be
        under-specified. Therefore the state-value mapping will be
        zero; i.e. the agent will not express a preference for any
        particular state.

        capabilities: The initial capabilities of the current
        	agent. The capabilities are specific to the
        	environment in which the agent operates.
        policy: An optional policy that will be followed by the
        	agent. If no policy is specified, the agent will
        	select actions randomly.

        """
        # Set the agent's capabilities.
        self.capabilities = capabilities
        self.task = Task()
        self.policy = policy

    def getValidActions( self, state ):
        """Get the valid actions in the current state.

        This function will return the subset of the agent's
        capabilities that are valid in the current state.

        state: The state for which to retrieve valid actions.

        Return: A sequence of actions which are valid in the given
        state.

        """
        actions = []
        for action in self.capabilities:
            if action.isValid( state ):
                actions.append( action )
        return actions
        
    def nextAction( self, state, task, policy=None ):
        """Get the agent's next Action

        This will return the next action that will be taken by the
        agent in the current state in the execution of its associated
        task. The action will be selected according to the specified
        policy (by default this is a random action). If the agent does
        not have an associated task, then the action will be randomly
        selected since the agent will not have any goal to achieve.

        state: The current state in the agent's task.

        """
        action = None
        if policy == None:
            action = random.choice( self.capabilities )
        else:
            action = policy.selectAction( task, state, self.capabilities )
        return action

    def takeAction( self, state, action ):
        """Take an action in a specified state.

        This will have the agent take the given action in the
        specified state, resulting in a list of possible next
        states. The default implementation of this function simply
        returns the sequence containing the single action obtained by
        applying 

        state: The state in which the action will be taken.
        action: The action for the agent to take.

        return: A sequence of possible states that will follow the
        action taken.

        """
        return action.apply( state )

    def evaluatePolicy( self, policy, state, action ):
        actions = []
        for a in self.capabilities:
            if a.isValid( state ):
                actions.append( a )
        probabilities = policy.evaluate( self.task, state, action )
        return probabilities[ action ]

    def estimateActionValue( self, task, state, action ):
        """Estimate the value of the state-action pair.

        """
        return 0.

    def getStateValue( self, state ):
        """Retrieve the agent's task-specific value for a state.

        This will retrieve the task-specific value attributed to the
        specified state. As the agent learns the task, the value of
        the state will be updated to reflect how favourable it is for
        the agent to be in this state in order to complete the task.

        state: The state instance whose task-specific value will be
        	evaluated.

        """
        value = 0
        if self.task:
            value = self.task.getStateValue( state )

        return value

    def assignTask( self, task ):
        """Assign a task to an agent.

        This will assign the specified task instance to the current
        Agent instance. The task will direct the agent's behaviour in
        the associated environment.

        task: The task instance to assign to the agent. This may be a
        	fully specified task tha directs the agent's behavour,
        	or an under-specified task which requires the agent to
        	learn the task.

        """
        self.task = task

class Environment:
    def __init__( self, state = None ):
        self.states = set()
        self.actions = set()
        self.current_state = state

    def setCurrentState( self, state ):
        self.current_state = state

    def execute( self, action ):
        """Execute the current action.

        This will apply the specified action to the current state of
        the environment, then bind one of the resulting states as the
        environment's current state. The default implenentation of
        this function selects the state randomly. Specific domains
        will want to handle this in different ways.

        action: The action to apply to the current state of the
        	environment.

        return: The state resulting from applying the specified action
        to the environment's current state.

        """
        next = None
        if action:
            next_states = action.apply( self.current_state )
            if len( next_states ) > 0:
                self.current_state = random.choice( next_states )
                next = self.current_state
        return next

    def __format__( self, format_spec ):
        """Format the environment.

        The environment will be formatted in accordance with its
        current state.

        """
        return self.current_state.__format__( format_spec )


class Task(dict):
    """Abstract class expressing a reinforcement learning task.

    This class provides a common interface for reinforcement learning
    tasks. A task instance manages the mapping of
    state-values. Therefore a single task instance is typically
    associated with only one agent.

    """
    def __init__( self, goals=set() ):
        """Create a new Task instance.

        This will initialize the base task instance. Initially the
        task's goal set will be undefined. Goals can be added to the
        task definition latently.

        """
        self.goal_reward = 100.
        self.goals = goals

    def isGoal( self, state ):
        """Determine if the specified state is a goal.

        This function will assert whether or not the specified state
        instance corresponds with a goal state for the current task.

        state: A state instance to assert as either a goal or non-goal
        	for the current task.

        returns: True of the state is a goal state for the current
        task; otherwise returns False.

        """
        return state in self.goals

    def addGoal( self, state ):
        """Add a goal state to the current task.

        This function will add the specified state to the task's goal
        set. If the state is already a goal state for the current
        task, then this function is a nul-operation.

        """
        if state not in self.goals:
            self.goals.append( state )

    def getReward( self, state, action, result ):
        """Calculate a task-specific reward value.

        This is a virtual function which calculates the reward
        associated with the specified state-action-state
        triple. Derived types can provide custom definitions of this
        mapping. This function corresponds with the reward function of
        the Bellman equation.

        The default implementation of this function will ascribe a
        positive reward value for triples where the initial state is
        not a goal, and the final state is a goal. If both initial and
        final states are goals, the reward value is zero. If only the
        initial state is a goal, then the reward will be
        negative. Otherwise the reward will be zero.

        This function, along with transition probabilities, completely
        specifies the most important aspects of the dynamics of a
        finite Markov Decision Process (MDP).

        state: The initial state instance.
        action: The action selected by the agent in the initial state.
        result: The state resulting from taking the specified action
        	in the initial state.

        returns: The reward value associated with the specified
        state-action-state triple.

        """
        return 0

    def estimateActionValue( self, agent, state, action, complement=False ):
        """Estimate the value of the state-action pair.

        This will estimate the value of taking the specified action in
        the given state. The value is determined by determining the
        possible outcome states from an agent taking the specified
        action in the given state, the value of those states are added
        to the rewards received for transitioning to those
        states. Each value is then scaled by the probability of
        reaching the state.

        The naive implementation determines the probability by
        calculating the softmax of the resulting state values. For an
        adverseralial environment, these probabilities should be
        complemented.

        agent: The agent for which the action value will be
        	estimated.
        state: The initial state.
        action: The action that will be taken by the agent.
        complement: Complement the resulting state values to calculate
        	their probabilities. This is useful for situations
        	where the agent exists in an adversarial
        	environment. In this case, the lowest valued state is
        	the most likely since the adversary will want to
        	minimize its opponents return value.

        Returns the estimated value of taking the specified action in
        the given state.

        """
        value = 0.
        score = []
        values = []
        rewards = []
        # First get the list of values and rewards.
        for s in agent.takeAction( state, action ):
            values.append( self[ s ] )
            if complement:
                score.append( -self[ s ] )
            else:
                score.append( self[ s ] )
            rewards.append( self.getReward( state, action, s ) )

        # Calculate the state probabilities using softmax.
        probabilities = numpy.exp( score ) / numpy.sum( numpy.exp( score ), axis=0 )
            
        # Estimate the state-action value by calculating the sum of
        # the sum of each final state value and reward, proportionally
        # to the state's probability.
        for v, r, p in itertools.izip( values, rewards, probabilities ):
            value += p * ( v + r )

        return value

    def estimateStateValue( self, policy, agent, state, complement=False ):
        """Estimate the value of a state.

        This will estimate the value function for the given state. The
        state's value is the estimated return obtained when an agent,
        starting from the given state, follows the specified policy to
        select actions.

        This function expresses the Bellman equation for the state
        value which expresses a relationship between the value of a
        state and the values of its successor states.

        policy: The policy for which the state's value is evaluated.
        agent: The agent that is executing the task, following the
        	given policy.
        state: The state whose value will be estimated.  
        complement: Complement the resulting state-action values to
        	calculate their probabilities. This is useful for
        	situations where the agent exists in an adversarial
        	environment. In this case, the lowest valued state is
        	the most likely since the adversary will want to
        	minimize its opponents return value.

        Returns the estimated value of the given state.

        """
        value = 0.
        policy_map = policy.evaluate( agent, self, state )
        for action in iter( policy_map ):
            value += policy_map[ action ] * self.estimateActionValue( agent, state, action, complement ) ;

        return value

    def __missing__( self, key ):
        """For value mappings that do not exist, always return 0."""
        return 0.
