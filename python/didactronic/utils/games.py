from didactronic.toolkit import Agent
from didactronic.toolkit import Environment
from didactronic.toolkit import Task

class Player(Agent):
    """Class representing a player of a game.

    This is a specialization of the Agent class which represents the
    player of a game. 

    """
    def __init__( self, capabilities, policy=None ):
        Agent.__init__( self, capabilities, policy )

    def takeAction( self, state, action ):
        """Take the given action in the specified state.

        This is a specialization of the Agent which, given a state,
        and an action will determine the possible states that may
        follow its execution. The implementation of this function
        assumes that other agents in the game have the same
        capabilities. as the current one.

        state: The state in which to take the action.
        action: The action to take in the given state.

        Returns a sequence of states representing the possible
        outcomes of taking the specified action in the given state.

        """
        next_actions = []
        for s in action.apply( state ):
            for move in self.capabilities:
                next_actions += move.apply( s )
        return next_actions

class GameEnvironment(Environment):
    """Class expressing a game environment.

    This is a specialization of the Environment class which represents
    a game played by one or more Player agents.

    """
    def __init__( self, players = [] ):
        """Initialize the GameEnvironment instance.

        This will associate a sequence of players with the
        environment.

        """
        Environment.__init__( self )
        self.players = players
        for player in players:
            self.actions.union( player.capabilities )

    def play( self ):
        s = self.current_state
        while s.isTerminal() == False:
            player = self.players[ ( s.id & 0xf ) % 2 ]
            move = player.nextAction( s, player.task, player.policy )
            if move == None:
                break
            s = self.execute( move )
            if s != None and s not in self.states:
                self.states.add( s )

        print format( self.current_state )

class AdversarialTask(Task):
    """Class representing an adversarial task.

    This is a specialization of the Task class which assumes that
    execution is carried out by multiple competing agents. Estimations
    of action values for adversarial tasks will complement the
    resulting state probabilities since it is assumed that an opponent
    is likely to select an action that will minimize the value for the
    current agent.

    """
    def __init__( self, goals=None, anti_goals=None ):
        """Create a new adversarial task instance.

        This will initialize the current Task instance.

        """
        Task.__init__( self, goals )
        self.antigoal_reward = -100.
        self.antigoals = anti_goals

    def getReward( self, state, action, result ):
        """Get the reward for an adversarial task.

        This is the default implementation of the adversarial task
        reward function. By default it will return a reward of zero
        for all non-terminal states. The goal reward value if the
        state is in the goal set, or the anti-goal reward value if the
        state is in the anti-goal set.

        state: The state before an action was taken.
        action: The action taken in the initial state.
        result: The resulting state of taking the specified action in
        	the given initial state.

        Returns: the reward value.

        """
        if self.isGoal( result ):
            return self.goal_reward
        elif self.isAntiGoal( result ):
            return self.antigoal_reward
        else:
            return 0.

    def isAntiGoal( self, state ):
        """Determine if the specified state is an anti-goal.

        An anti-goal is a state the agent executing the task is
        actively trying to avoid. This will determine whether or not
        the specified state is one of those goals.

        """
        return state in self.antigoals

    def estimateActionValue( self, agent, state, action ):
        """Estimate the value of a state-action pair.

        This is the adversarial version of the base
        estimateActionValue() method which will be called with the
        complement flag set to True.

        agent: The agent for which the action value will be
        	estimated.
        state: The initial state.
        action: The action that will be taken by the agent.

        Returns the estimated value of taking the specified action in
        the given state.

        """
        if state.isTerminal():
            return self.getReward( state, action, state )
        else:
            return Task.estimateActionValue( self, agent, state, action, True )

    def estimateStateValue( self, policy, agent, state ):
        """Estimate the value of a state.

        This will estimate the value for the given state in an
        adversarial task. This function simply calls the base
        estimateStateValue() method, with the complement argument set
        to True.

        policy: The policy for which the state's value is evaluated.
        agent: The agent that is executing the task, following the
        	given policy.
        state: The state whose value will be estimated.  


        Returns: The estimated value of the given state provided the
        agent thereafter follows the specified policy.

        """
        return Task.estimateStateValue( self, policy, agent, state, True )
