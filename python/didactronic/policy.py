import random

class PolicyMap(dict):
    """Mapping of action to probabilities"""
    def __missing__( self, key ):
        return 0.

class Policy(dict):
    """The base class of all reinforcement learning policies.

    This class defines the interface expected of policy
    implementations. Custom policies should derive from this class to
    ensure that the base functionality is in place.

    The base policy is simply a dictionary which maps state to
    actions. The mappings are updated when the update() method is
    invoked.

    """
    def __missing__( self, key ):
        """Make sure the policy dictionary always returns a value"""
        return None

    def evaluate( self, agent, task, state ):
        """Evaluate the current policy.

        This function evaluates the current policy by determining the
        probability with which an agent will select an action from its
        capabilities in the given state while executing a task. The
        default implementation of this function will assign a uniform
        probability to the agent's actions.

        agent: The agent whose capabilities will be evaluated.
        task: The task instance for which the policy will be
        	evaluated.
        state: The state in which the policy will be evaluated.

        return: A mapping of actions to probability values in the
        range of [0,1] expressing the likelihood that an agent
        following this policy will select the specified action in the
        given state.

        """
        map = PolicyMap()
        for a in agent.capabilities:
            if a.isValid( state ):
                map[ action ] = 0.
        
        length = len( map )
        for key in iter( map ):
            map[ key ] = 1./length
        
        return map

    def update( self, state, action ):
        """Update the current policy.

        This function is the complement of the 'evaluate' method. It
        allows the policy to be updated for the specified action in
        the given state. This will affect the evaluation of the policy
        in future invocations.

        state: The state in which the policy will be evaluated.
        action: The action, or macro-action, to evaluate.

        """
        self[ state ] = action
        
    def apply( self, state ):
        """Apply the current policy"""
        return self[ state ]

    def selectAction( self, task, state, actions ):
        """Select an action from the specified list.

        This is the base implementation of the action selection
        function for a given policy. This will choose an action from
        the list of action given the current state and the task to
        execute. The default implementation randomly chooses an action
        from the list following a uniform distribution.

        task: The task being executed by the agent following the
        	current policy.
        state: The current state in which to choose an action.
        actions: The sequence of valid actions from which the policy
        	will choose one for the agent to perform.

        Returns: An action selected from the given list for the agent
        to execute in the specified state of the given task.

        """
        state = random.choice( actions )
        
class StreamPolicy(Policy):
    """Policy which allows a user to manually define the policy.

    This policy will prompt the user to enter an action for a given
    state by entering the action's ID. The semantics of the ID are not
    mandated by the policy. This is left up to the clients of the
    policy to make sure the right action is performed with the ID. It
    is recommended that this policy class be extended for the
    particular domain in which it will be used.

    """
    def __init__( self, ActionType = None ):
        type = ActionType
        if type == None:
            type = Action

        self.ActionType = type

    def selectAction( self, task, state, actions ):
        """Request an action for the given state"""
        print format( state )
        return self.ActionType.CreateInteractively()

class EpsilonGreedy(Policy):
    def __init__( self, epsilon ):
        self.epsilon = epsilon

    def _evaluateActions( self, actions, task, state ):
        best = ( self[ state ], task[ state ] )
        policy_map = PolicyMap()
        for action in actions:
            if action.isValid( state ):
                val = 0
                next_states = action.apply( state )
                for s in next_states:
                    val += task[ s ]
                val /= len( next_states )
                if best[ 0 ] == None or val > best[ 1 ]:
                    best = ( action, val )
                policy_map[ action ] = self.epsilon
        if best[ 0 ]:
            policy_map[ best[ 0 ] ] = 1. - self.epsilon
        
        return policy_map

    def evaluate( self, agent, task, state ):
        return EpsilonGreedy._evaluateActions( agent.capabilities, task, state )

    def selectAction( self, task, state, actions ):
        """Select an action following the epsilon-greedy policy.

        This will choose the greedy action from the list with
        probability 1-epsilon. Otherwise an action will randomly be
        chosen from the list following a uniform distribution.

        task: The task for which an action will be selected.
        state: The current state in the given task.
        actions: A sequence of actions from which the policy will
        	choose for the agent to execute in the given state.

        Returns: An action, selected according to the epsilon-greedy
        policy, for an agent to take in executing the given task from
        the specified state.

        """
        action = None
        selection_criteria = random.random()
        if self.epsilon > 0 and selection_criteria < self.epsilon:
            # Randomly choose an action.
            action = random.choice( actions )
        else:
            best = ( None, 0 )
            # Choose the greedy action
            policy_map = self._evaluateActions( actions, task, state )
            for action in iter( policy_map ):
                if policy_map[ action ] > best[ 1 ]:
                    best = ( action, policy_map[ action ] )
            action = best[ 0 ]

        return action
