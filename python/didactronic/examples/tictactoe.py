from didactronic.algorithms import DynamicProgramming
from didactronic.policy import EpsilonGreedy
from didactronic.policy import StreamPolicy
from didactronic.toolkit import Action
from didactronic.toolkit import Agent
from didactronic.toolkit import State
import didactronic.utils.games as games

class Move(Action):
    mask = [ 0x2AAAA0, 0x155550 ]

    def __init__( self, x, y ):
        """Create a new move in the tic-tac-toe domain.

        This will create and initialize a Move instance to represent
        placing a marker at the (x,y) grid position of a tic-tac-toe
        game board.

        x: The column in which this move will place a marker.
        y: The row in which this move will place a marker.

        """
        Action.__init__( self, 0x3 << (2*((y*3)+x)+4) )

    def willMark( self, x, y ):
        """Test whether the move will mark the specified grid cell.

        This will assert whether or not the grid cell at position
        (x,y) will be marked by applying the current Move.

        x: The column index of the grid-cell.
        y: The row index of the grid-cell.

        """
        mark = False
        if (((self.id) >> (2*((y*3)+x)+4)) & 0x3):
            mark = True
        return mark

    def __getMark( self, x, y ):
        mark = ' '
        if self.willMark( x, y ):
            mark = '*'
        return mark

    def __format__( self, format_spec ):
        """Format the move as a string which will print the move's position.

        """
        return "\n\t{}|{}|{}\n\t-+-+-\n\t{}|{}|{}\n\t-+-+-\n\t{}|{}|{}\n\n".format(
            self.__getMark( 0, 0 ),
            self.__getMark( 1, 0 ),
            self.__getMark( 2, 0 ),
            self.__getMark( 0, 1 ),
            self.__getMark( 1, 1 ),
            self.__getMark( 2, 1 ),
            self.__getMark( 0, 2 ),
            self.__getMark( 1, 2 ),
            self.__getMark( 2, 2 ) )

    def isValid( self, state ):
        """Determine if the current move is valid.

        This will assert whether or not the current move is valid in
        the given state

        """
        is_valid = False
        if state.isTerminal() == False:
            id = state.id
            ordinal = id & 0xf
            is_valid = ordinal < 9 and ( id & self.id ) == 0
        return is_valid

    def apply( self, state ):
        """Apply the current move to the specified state.

        This will determine the state which follows from the
        application of the current move in the current state.

        """
        id = state.id
        ordinal = id & 0xf
        next_states = []
        if ordinal < 9 and ( id & self.id ) == 0:
            id |= ( self.id & self.mask[ ordinal % 2 ] )
            # Update the move ordinal to indicate the next play.
            id += 1
            next_states.append( GridState( id ) )
        return next_states

    @staticmethod
    def CreateInteractively():
        x = -1
        y = -1
        while 0 > x or x > 2 or 0 > y or y > 2:
            pos = raw_input( "Select a grid position (e.g. a1; q=forfeit): " )
            if pos == 'q':
                return None
            x = ord( pos[ 0 ] ) - ord( 'a' )
            y = ord( pos[ 1 ] ) - ord( '1' )
        return Move( x, y )

class GridState(State):
    """A state in the Tic-Tac-Toe game.

    This class is a specialization of the base State class which
    expresses the state of a game of Tic-Tac-Toe. There are two class
    members associated with a GridState:

    - marks: The markers that can occupy a grid space.
    - terminals: An array of IDs expressing terminal states (i.e. wins
      or losses).

    The grid state's ID encodes the current configuration of the board
    (i.e. which player has played in which grid cell), and the ordinal
    number of the current play.

    """
    marks = " OX*"

    terminals = [
        # Row Wins
        Move( 0, 0 ).id | Move( 1, 0 ).id | Move( 2, 0 ).id,
        Move( 0, 1 ).id | Move( 1, 1 ).id | Move( 2, 1 ).id,
        Move( 0, 2 ).id | Move( 1, 2 ).id | Move( 2, 2 ).id,
        # Column Wins
        Move( 0, 0 ).id | Move( 0, 1 ).id | Move( 0, 2 ).id,
        Move( 1, 0 ).id | Move( 1, 1 ).id | Move( 1, 2 ).id,
        Move( 2, 0 ).id | Move( 2, 1 ).id | Move( 2, 2 ).id,
        # Diagonal Wins
        Move( 0, 0 ).id | Move( 1, 1 ).id | Move( 2, 2 ).id,
        Move( 2, 0 ).id | Move( 1, 1 ).id | Move( 0, 2 ).id ]

    def __init__( self, id ):
        """Create a state with the specified ID.

        This will allocate and initialize a new State instance and
        associate with it the specified Id. The Id encodes the game
        board's configuration in the current state. In other words it
        is possible to determine which markers occupy which grid cells
        from the Id.        

        """
        State.__init__( self, id )

    def getMark( self, x, y ):
        """Get the marker at the (x,y) position.

        This will return the marker that has been placed in the (x,y)
        grid position of the Tic-Tac-Toe game board in the current
        game state.

        x: The column index of the game board (zero based).
        y: The row index of the game board (zero based).

        """
        index = ((self.id) >> ((2*((y*3)+x))+4)) % 4 ;
        return self.marks[ index ]

    def getWinner( self ):
        """Return the marker associated with the winner.

        This function will, if the current state is a winning state,
        return the marker associated with the winner.

        Return: The letter associated with the winning marker (either
        'X' or 'O'). If there is no winner in the current state, this
        function will return None.

        """
        winner = None
        for mask in iter( GridState.terminals ):
            if ( mask & Move.mask[ 0 ] ) == ( self.id & mask ):
                winner = 'X'
            elif ( mask & Move.mask[ 1 ] ) == ( self.id & mask ):
                winner = 'O'
        return winner

    def isTerminal( self ):
        """Determine if the current state ends the game.

        This function will assert whether or not it is possible to
        continue playing the game. The game is over if either player
        made a winning move, or if there are no more free spaces in
        the grid.

        Return: True if the state is terminal; Fase otherwise.

        """
        is_terminal = ( self.getWinner() != None )
        if is_terminal == False:
            is_terminal = (self.id & 0xf) == 9

        return is_terminal

    def __format__( self, format_spec ):
        return "\n\t   a b c\n\n\t1: {}|{}|{}\n\t   -+-+-\n\t2: {}|{}|{}\n\t   -+-+-\n\t3: {}|{}|{}\n\n".format(
            self.getMark( 0, 0 ),
            self.getMark( 1, 0 ),
            self.getMark( 2, 0 ),
            self.getMark( 0, 1 ),
            self.getMark( 1, 1 ),
            self.getMark( 2, 1 ),
            self.getMark( 0, 2 ),
            self.getMark( 1, 2 ),
            self.getMark( 2, 2 ) )

class Player(games.Player):
    """Class representing a tic-tac-toe player.

    A player instance is a specialization of an Agent which works in
    the Tic-Tac-Toe environment. The agent's capabilities are defined
    to be the possible moves in a game of tic-tac-toe.

    """
    def __init__( self, policy=StreamPolicy( Move ) ):
        """Initialize the Player instance.

        This will create the set of the player's capabilities and
        associate it with the base Agent instance.

        policy: The policy that will be followed by the player when
    		playing Tic-Tac-Toe. By default, this policy is the
    		stream policy which will prompt the user to specify a
    		move for the agent.

        """
        capabilities = []
        for y in range(3):
            for x in range(3):
                capabilities.append( Move( x, y ) )
        Agent.__init__( self, capabilities, policy )

class TicTacToe(games.AdversarialTask):
    """Class representing the Tic-Tac-Toe task.

    This is a specialization of an AdversarialTask which represents
    the task of winning a game of Tic-Tac-Toe.

    """
    def __init__( self, goals, antigoals ):
        games.AdversarialTask.__init__( self, goals, antigoals )

    def isAntiGoal( self, state ):
        is_antigoal = False
        for t in self.antigoals:
            if state.id & t.id == t.id:
                is_antigoal = True
                break
        return is_antigoal

    def isGoal( self, state ):
        is_goal = False
        for goal in self.goals:
            if state.id & goal.id == goal.id:
                is_goal = True
                break
        return is_goal

class Board(games.GameEnvironment):

    """Class used to express a Tic-Tac-Toe reinforcement environment.

    This is a specialization of a reinforcement learning environment
    which expresses a game of Tic-Tac-Toe.

    """
    initial = GridState( 0 )

    def __init__( self, player1 = Player(), player2 = Player() ):
        games.GameEnvironment.__init__( self, [ player1, player2 ] )
        self.terminals = set()
        self.states.add( Board.initial )
        self.setCurrentState( Board.initial )

        p1_goals = []
        p2_goals = []
        for id in GridState.terminals:
            p1_goals.append( GridState( id & Move.mask[ 0 ] ) )
            p2_goals.append( GridState( id & Move.mask[ 1 ] ) )
        player1.assignTask( TicTacToe( p1_goals, p2_goals ) )
        player2.assignTask( TicTacToe( p2_goals, p1_goals ) )
        self.states.update( p1_goals )
        self.states.update( p2_goals )

    def play( self ):
        self.setCurrentState( Board.initial )
        games.GameEnvironment.play( self )
        winner = self.current_state.getWinner()
        if winner:
            print "======== {} Wins! ========".format( winner )
        elif self.current_state.isTerminal():
            print "========   Draw   ========"
        else:
            print "====== {} Forfeits! ======".format( GridState.marks[ ( self.current_state.id + 1 ) % 2 + 1 ] )

def usage():
    print "Usage tictactoe $s [options]"
    print "Options:"
    print "-h, --help		Print this usage message."
    print "-e, --epsilon=N.	Set the epsilon value used for epsilon greedy"
    print "             	policies."
    print "-p, --players=N	Specify the number of human players (default 1)"
    print "-i, --iterations=N	The number of iterations to play"

import getopt, sys

def main():
    try:
        opts, args = getopt.getopt( sys.argv[1:], "he:p:i:", [ "help", "epsilon=", "players=", "iterations=" ] )
    except getopt.GetOptError as err:
        print str( err )
        usage()
        sys.exit(2)

    epsilon = 0.
    num_players = 1
    iterations = 1
    for o, a in opts:
        if o in ( "-h", "--help" ):
            usage()
            sys.exit()
        elif o in ( "-e", "--epsilon" ):
            epsilon = a
        elif o in ( "-p", "--players" ):
            num_players = a
        elif o in ( "-i", "--iterations" ):
            iterations = a
        else:
            assert False, "Unhandled option"
            
    if num_players < 2:
        p2 = Player( EpsilonGreedy( epsilon ) )
    else:
        p2 = Player()
    
    if num_players < 1:
        p1 = Player( EpsilonGreedy( epsilon ) )
    else:
        p1 = Player()
    
    xo = Board( p1, p2 )
    while iterations > 0:
        xo.play()
        # Learn
        DynamicProgramming.GeneralizedPolicyIteration( p2, xo )
        iterations -= 1

if __name__ == "__main__":
    main()
