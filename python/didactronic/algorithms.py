class DynamicProgramming:
    """Class encapsulating functions for dynamic programming.

    This class groups together the functional blocks to use dynamic
    programming to learn arbitrary tasks.

    """
    threshold = 0.01
    
    @staticmethod
    def PolicyEvaluation( agent, environment ):
        delta = 0
        while delta < DynamicProgramming.threshold:
            # Sweep the entire state set.
            for state in environment.states:
                value = agent.task[ state ]
                action = agent.nextAction( state, agent.task )
                if action != None:
                    new_value = agent.task.estimateActionValue( agent, state, action )
                    value = abs( value - new_value )
                    if delta < value:
                        delta = value
                    agent.task[ state ] = new_value

    @staticmethod
    def PolicyImprovement( agent, environment ):
        stable = True
        for state in environment.states:
            best = ( None, 0. )
            actions = agent.getValidActions( state )
            policy_action = agent.policy.selectAction( agent.task, state, actions )
            for action in actions:
                value = agent.task.estimateActionValue( agent, state, action )
                if best[ 1 ] < value or best[ 0 ] == None:
                    best = ( action, value )
            # Update the policy
            agent.policy[ state ] = best[ 0 ]
            if policy_action != best[ 0 ]:
                stable = False
        
        return stable

    @staticmethod
    def GeneralizedPolicyIteration( agent, environment ):
        stable = False
        while not stable:
            DynamicProgramming.PolicyEvaluation( agent, environment )
            stable = DynamicProgramming.PolicyImprovement( agent, environment )
