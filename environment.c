/* -*- Mode: C -*- */
/*
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @file
 * @brief Definition of an Environment in a reinforcement learning
 *	task.
 */

/* POSIX includes */
#include <assert.h>
#include <errno.h>

/* Local includes */
#include "action.h"
#include "environment.h"
#include "model.h"
#include "state.h"
#include "transition.h"

typedef union _environment_impl
{
	Environment	_public ;
	struct {
		const State*				initial ;
		const State*				current ;
		StateSet*				states ;
		Environment_Apply_f*			apply ;
		Environment_Cleanup_f*			cleanup ;

		StateSet*				initial_states ;
		StateSet*				generators ;

		/**
		 * @private
		 * @brief Pointer to the callout to assert terminal
		 *	States.
		 * @memberof Environment
		 *
		 * @details This is a pointer to the virtual
		 * implementation of the function which will be
		 * invoked to determine whether or not a given state
		 * is Terminal. This function @e must be provided by
		 * derived Environment implementations.
		 *
		 * @see Environment_isTerminalState
		 */
		Environment_isTerminalProto*		isTerminal ;

		/**
		 * @private
		 * @brief Action validation callout function.
		 * @memberof Environment
		 *
		 * @details This is a pointer to the virtual function
		 * which implements a validator to determine whether
		 * an action is valid in a given state. Derived types
		 * can assign a custom implementation of this function
		 * to this isActionValid member of the Environment
		 * structure directly (when it is initialized).
		 *
		 * @see Environment_isActionValid
		 */
		Environment_isActionValidProto*		isActionValid ;

		/**
		 * @private
		 * @brief Callout used to determine transition
		 *	probabilities.
		 * @memberof Environment
		 *
		 * @details This is a pointer to the virtual
		 * implementation of a function which calculates the
		 * probability of reaching the @p next state by taking
		 * the specified @p action in the @p current
		 * State. The default implementation of this function
		 * simply calculates the probability from a uniform
		 * distribution of the possible States that follow the
		 * current State when taking the specified Action (if
		 * the Environment provides a model). Derived types
		 * can provide implementations of this function to
		 * perform a more intelligent calculation.
		 *
		 * @see Environment_transitionProbability
		 */
		Environment_transitionProbabilityProto*	transitionProbability ;

		/**
		 * @private
		 * @brief Callout used to execute an Action.
		 * @memberof Environment
		 *
		 * @details This is a pointer to the virtual implementation of
		 * a function which executes the specified Action in the
		 * current Environment; affecting its current State.
		 *
		 * @see Environment_execute
		 */
		Environment_executeProto*		execute ;
	} ;
} EnvironmentImpl ;

/**
 * @protected
 * @brief Assign an Action validator to an Environment instance.
 * @memberof Environment
 *
 * @details This will assign a custom Action validator function to the
 * current Environment instance. This function will be invoked when a
 * request is made for the Environment to validate an Action in a
 * given State.
 *
 * @see Environment_isActionValid
 *
 * @param[in,out] self A pointer to an Environment instance to
 *	initialize.
 * @param[in] opt_validator A pointer to an implementation of an
 *	Action validator function for a derived Environment type. If
 *	this argument is @e NULL, a default validator will be used.
 */
void
Environment_setActionValidator( Environment* self, Environment_isActionValidProto* opt_validator )
{
	if ( self ) {
		EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
		impl->isActionValid = opt_validator ;
	}
}

/**
 * @protected
 * @brief Assign a transition evaluator to an Environment instance.
 * @memberof Environment
 *
 * @details This will assign a custom Transition evaluation function
 * to the current Environment instance. This function will be invoked
 * when a request is made for the Environment to determine the
 * probability of reaching a given State when taking some Action. If
 * this argument is @e NULL, a default evaluator will be used.
 *
 * @see Environment_transitionProbability
 *
 * @param[in,out] self A pointer to an Environment instance to
 *	initialize.
 * @param[in] opt_evaluator An optional pointer to a function that
 *	will be used to calculate the probability of reaching a
 *	specified state when taking a given action from the current
 *	State. If this argument is @e NULL, a default implementation
 *	will be used.
 */
void
Environment_setTransitionEvaluator( Environment* self, Environment_transitionProbabilityProto* opt_evaluator )
{
	if ( self ) {
		EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
		impl->transitionProbability = opt_evaluator ;
	}
}

void
Environment_init( void* self,
		  StateSet* states,
		  Environment_isTerminalProto* isTerminal,
		  Environment_isActionValidProto* validator,
		  Environment_transitionProbabilityProto* evaluator )
{
	EnvironmentImpl* env = self ;
	if ( env ) {
		env->states = states ;
		env->isTerminal = isTerminal ;
		env->isActionValid = validator ;
		env->transitionProbability = evaluator ;
	}
}

Environment*
Environment_Create( StateSet* states )
{
	EnvironmentImpl* new = calloc( 1, sizeof( EnvironmentImpl ) ) ;
	Environment_init( new, states, NULL, NULL, NULL ) ;
	return new ? &new->_public : NULL ;
}

void
Environment_Delete( Environment** env )
{
	if ( env && *env ) {
		EnvironmentImpl* impl = ( EnvironmentImpl* )*env ;
		if ( impl->cleanup ) {
			impl->cleanup( *env ) ;
		}
		StateSet_Delete( &impl->states ) ;
		free( impl ) ;
		*env = NULL ;
	}
}

StateSet*
Environment_getStates( const Environment* self )
{
	StateSet* states = NULL ;
	EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
	if ( !impl ) {
		errno = EINVAL ;
	}
	else {
		states = impl->states ;
	}
	return states ;
}

bool
Environment_isTerminalState( const Environment* self, const State* state )
{
	EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
	assert( impl ) ;
	assert( impl->isTerminal ) ;
	return impl->isTerminal( self, state ) ;
}

bool
Environment_isActionValid( const Environment* self, const State* state, const Action* action )
{
	bool is_valid = false ;
	EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
	assert( impl ) ;
	if ( impl->isActionValid ) {
		is_valid = impl->isActionValid( self, state, action ) ;
	}
	return is_valid ;
}

TransitionList*
Environment_applyAction( const Environment* self, const State* s, const State* a )
{
	Model* model = State_getModel( s ) ;
	return ( TransitionList* )Model_getTransitions( model, a ) ;
}

/**
 * @protected
 * @brief Set the current State of the Environment.
 * @memberof Environment
 *
 * @details This function assignes the specified @p state as the
 * current State of the Environment.
 *
 * @param[in,out] self A pointer to an Environment instance whose
 *	current State will be assigned.
 * @param[in] state A pointer to an immutable State instance to set as
 *	the current State of the Environment.
 */
void
Environment_setCurrentState( Environment* self, const State* state )
{
	assert( self ) ;
	self->current = state ;
}

const State*
Environment_getCurrentState( const Environment* self )
{
	return self->current ;
}

/**
 * @protected
 * @brief Set the initial State of the Environment.
 * @memberof Environment
 *
 * @details This function assignes the specified @p state as the
 * initial State of the Environment. If the specified state does not
 * exist in the Environment, the initial state will be undefined.
 *
 * @param[in,out] self A pointer to an Environment instance whose
 *	initial State will be assigned.
 * @param[in] initial_state The ID of the initial state of the
 *	Environment. This state must exist in the Environment.
 */
void
Environment_setInitialState( Environment* self, StateID initial_state )
{
	EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
	assert( impl ) ;
	impl->initial = Environment_getState( self, initial_state ) ;
}

const State*
Environment_getInitialState( const Environment* self )
{
	return self->initial ;
}

const State*
Environment_getState( const Environment* self, StateID id )
{
	const State* s = NULL ;
	EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
	if ( !impl ) {
		errno = EINVAL ;
	}
	else {
		s = StateSet_getState( impl->states, id ) ;
	}
	return s ;
}

const State*
Environment_execute( Environment* self, const Action* action )
{
	EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
	if ( impl ) {
		if ( !impl->execute ) {
			errno = ENOSYS ;
		}
		else {
			impl->current = impl->execute( self, action ) ;
		}
	}
	return impl->current ;
}

int32_t
Environment_transitionProbability( const Environment* self, const State* current, const State* action, const State* next )
{
	int32_t probability = -1 ;
	EnvironmentImpl* impl = ( EnvironmentImpl* )self ;
	if ( impl ) {
		if ( !impl->transitionProbability ) {
			List* transitions = Environment_applyAction( self, current, action ) ;
			if ( transitions ) {
				probability = 100/List_getSize( transitions ) ;
			}
			else {
				errno = ENOSYS ;
			}
		}
		else {
			probability = impl->transitionProbability( self, current, action, next ) ;
		}
	}
	return probability ;
}

const State*
Environment_apply( Environment* self, const State* plan )
{
	const State* result = NULL ;
	EnvironmentImpl* impl = ( EnvironmentImpl* )self ;

	if ( !impl || !plan ) {
		errno = EINVAL ;
	}
	else if ( !impl->apply ) {
		errno = ENOSYS ;
	}
	else {
		const State* tmp = impl->apply( self, self->current, plan ) ;
		if ( tmp ) {
			/* TODO:: Ensure that the new state is
			 * cleaned up correctly. */
			impl->current = result ;
		}
	}
	return result ;
}
