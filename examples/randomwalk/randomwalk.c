/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup RandomWalk Random Walk
 * @ingroup Examples
 * @brief Example which demonstrates the Group presentation of a
 *	random walk Task.
 *  @{
 *
 * @file
 *
 * @brief Example program which will learn an optimal solution to the
 * RandomWalk task as described in
 * http://cyberdyne.vociferousvoid.org/main/play-tic-tac-toe-with-arthur-cayley-part2-expansion.
 */

#include <action.h>
#include <state.h>
#include <util/graph.h>
#include <util/group.h>

/**
 * @class Step
 * @brief A step in the random walk.
 * @extends Action
 *
 * @details This class is a specialization of the Action class
 * representing a single step forward in the random walk problem. The
 * inverse of a Step is a step backward.
 */
typedef Action Step ;

static void
Step_inverse( Step* self )
{
	if ( self ) {
		int32_t a = -1 * self->id ;
		self->id = ( ActionID )a ;
	}
}

static size_t
Step_hash( const void* abs_self )
{
	size_t hash = -1 ;
	const Step* self = abs_self ;
	if ( self ) {
		hash = self->id ;
	}
	return hash ;
}

static int
Step_compare( const void* item1, const void* item2, size_t item_size )
{
	int order = 0 ;
	const Step* s1 = item1 ;
	const Step* s2 = item2 ;
	if ( s1 && s2 ) {
		order = s1->id - s2->id ;
	}
	return order ;
}

static void
Step_apply( const Step* self, const State* s, State* result )
{
	if ( self && result ) {
		StateID sid = s ? State_getId( s ) : 'C' ;
		State_init( result, sid + self->id, 0 ) ;
	}
}

static size_t
State_hash( const void* item )
{
	return State_getId( item ) ;
}

static int
State_compare( const void* v1, const void* v2 )
{
	int order = 0 ;
	const State* s1 = v1 ;
	const State* s2 = v2 ;
	if ( s1 && s2 ) {
		order = State_getId( s1 ) - State_getId( s2 ) ;
	}
	return order ;
}

static void*
RandomWalk_product( const Group* self, const void* item1, const void* item2 )
{
	State* p = NULL ;
	const State* s1 = item1 ;
	const State* s2 = item2 ;

	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( s1 && s2 ) {
		const State* ident = Group_getIdentity( self ) ;
		p = State_Create( s1->id + s2->id - ident->id, 0 ) ;
	}
	return p ;		
}

static int32_t
RandomWalk_getReward( const void* model, const State* i, const State* f, const Action* a )
{
	int32_t reward = 0 ;
	if ( i && f ) {
		StateID iid = State_getId( i ) ;
		StateID fid = State_getId( f ) ;
		if ( fid == 'F' ) {
			if ( iid = 'A' ) {
				reward = -1 ;
			}
			else if ( iid = 'E' ) {
				reward = 1 ;
			}
		}
	}
	return reward ;
}

int
main( int argc, char* argv[] )
{
	State* C ;
	State* F ;
	State* Fr ;
	State* Al ;
	Step r, l ;
	Iterator i ;
	size_t t_size ;
	const State* t ;
	Group* group = NULL ;
	Graph cayley_graph = { 0 } ;
	Set* generators = Set_Create( Step_compare ) ;
	RelationSet* relations = RelationSet_Create( State_hash ) ;

	/* Define the generator set. */
	State_init( &r, 'C'+1, NULL ) ; 	/* step right */
	Set_insert( generators, &r, sizeof( r ) ) ;
	State_init( &l, 'C'-1, NULL ) ;		/* step left */
	Set_insert( generators, &l, sizeof( l ) ) ;	

	/* Define the group relations: A*l = F and F*r = F. */
	Al = State_Create( 'A'-1, 0 ) ;
	Fr = State_Create( 'F'+1, 0 ) ;
	F = State_Create( 'F', 0 ) ;
	RelationSet_addRelation( relations, Fr, F ) ;
	RelationSet_addRelation( relations, Al, F ) ;

	/* Create the group presentation. */
	C = State_Create( 'C', 0 ) ; /* Identity */
	group = Group_Create( generators, relations, C, RandomWalk_product ) ;

	/* Build the Cayley Graph of the Group */
	Graph_buildCayleyGraph( &cayley_graph, "RandomWalk", group, State_hash ) ;

	/* Create a CayleyGraph Model of the reinforcement learning
	 * task. */
	CayleyGraph_Model_Create( &cayley_graph, RandomWalk_getReward ) ;

	Graph_cleanup( &cayley_graph ) ;

	Set_Delete( &generators ) ;
	HashMap_Delete( ( HashMap** )&relations ) ;
	State_Delete( &Al ) ;
	State_Delete( &Fr ) ;
	State_Delete( &F ) ;
	State_Delete( &C ) ;
	return 0 ;
}

/** @} */
