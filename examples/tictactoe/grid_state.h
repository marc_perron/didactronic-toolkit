/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* Didactronic includes */
#include <state.h>

/**
 * @addtogroup xo
 * @{
 *
 * @file
 *
 * @brief Derived State type for the Tic-Tac-Toe task.
 */

/**
 * @class GridState
 * @implements State
 * @brief State that models a configuration of a Tic-Tac-Toe board.
 *
 * @details This class is a specialization of the State class which
 * enhances it with information about state of a Tic-Tac-Toe game. The
 * positions of the X and O markers in the grid are encoded in the ID
 * associated with the state. Each space in the grid is encoded using
 * two bits such that: '01' = O, '10' = X, and '00' means the space is
 * free. '11' is a special encoding expressing the superposition of
 * the X and O markers (this is expressed as a '*').
 *
 * Overall, a grid state's ID will be encoded using 22-bits: the most
 * significant18-bits for the grid encoding (9 spaces, two bits per
 * space). The least significant 4 bits of the ID express the sequence
 * number of the state. The sequence number describes the number of
 * steps that were followed to reach the state. This will be used to
 * determine the agent whose play is next.
 */
typedef struct _grid_state {
	State_EXTEND ;
	bool	last ;
} GridState ;

/**
 * @brief Get the marker at the specified position.
 * @memberof GridState
 *
 * @details This will return the marker that occupies the specified
 * grid position in the current BoardState. The marker at the
 * specified position will be encoded into the ID associated with the
 * GridState instance.
 *
 * @see GridState
 *
 * @param[in] self A pointer to a BoardState instance.
 * @param[in] row The row of the desired grid position.
 * @param[in] col The column of the desired grid position.
 *
 * @return A character expressing the marker that occupies the
 * specified grid position.
 *
 * @retval 'X' The grid position is occupied by an X marker.
 * @retval 'O' The grid position is occupied by an O marker.
 * @retval '*' The grid position is occupied by an undetermined
 *	marker.
 * @retval ' ' The grid position is unoccupied.
 * @retval 0 The grid position is invalid.
 */
char GridState_getMarker( const State* self, size_t row, size_t col ) ;

/**
 * @brief Serialize the BoardState instance.
 * @memberof GridState
 */
void GridState_serialize( const State* self, FILE* stream ) ;

/**
 * @}
 */
