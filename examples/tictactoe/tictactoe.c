/* -*- Mode: C -*- */
/*
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @addtogroup xo
 * @{
 *
 * @file tictactoe.c
 *
 * @brief Implementation of the Tic-Tac-Toe example program.
 *
 * @details All the required classes and functions required for the
 * Tic-Tac-Toe example program are defined in this file.
 */

/* POSIX includes */
#include <assert.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Didactronic includes */
#include <action.h>
#include <epsilon-greedy.h>
#include <history.h>
#include <stream-policy.h>
#include <transition.h>
#include <util/list.h>
#include <util/map.h>

/* Local includes */
#include "grid_state.h"
#include "move.h"
#include "tictactoe.h"

/******************************************************************************
 * Moves
 ******************************************************************************/

/**
 * @private
 * @memberof Move
 * @brief Bit mask to bind moves to the X marker.
 *
 * @details This mask is used internally to transform a move
 * specification into the placement of an X marker.
 */
static const StateID Move_XMASK = 0x2AAAA0 ;

/**
 * @private
 * @memberof Move
 * @brief Bit mask to bind moves to the O marker.
 *
 * @details This mask is used internally to transform a move
 * specification into the placement of an O marker.
 */
static const StateID Move_OMASK = 0x155550 ;

/**
 * @private
 * @memberof Move
 * @brief Determine the action specification for an (x,y) move.
 *
 * @details MACRO which evaluates to the specification of the action
 * of placing a marker at the specified (x,y) position in the
 * tic-tac-toe grid.
 */
#define MOVE( x, y )	( 0x3 << (2*(((y)*3)+(x))+4) )
#define MOVE_X( id )	( (id) & Move_XMASK )
#define MOVE_O( id )	( (id) & Move_OMASK )

StateID
Move_getId( const Move* self, char marker )
{
	assert( self ) ;
	StateID id = Action_getId( self ) ;
	switch ( marker ) {
	case 'X':
		id &= Move_XMASK ;
		break ;
	case 'O':
		id &= Move_OMASK ;
		break ;
	default:
		break ;
	}
	return id ;
}

int
Move_serialize( const Action* self, FILE* stream )
{
#define MARK( state, x, y ) marks[ ((state) >> ((2*(((y)*3)+(x)))+4)) & 0x3 ]

	const char* marks = " OX*" ;
	const char* str = "\n\t%c|%c|%c\n\t-+-+-\n\t%c|%c|%c\n\t-+-+-\n\t%c|%c|%c\n\n" ;
	ActionID move = Action_getId( self ) ;
	return fprintf( stream, str,
			MARK( move, 0, 0 ),
			MARK( move, 1, 0 ),
			MARK( move, 2, 0 ),
			MARK( move, 0, 1 ),
			MARK( move, 1, 1 ),
			MARK( move, 2, 1 ),
			MARK( move, 0, 2 ),
			MARK( move, 1, 2 ),
			MARK( move, 2, 2 ) ) ;
}

Move*
Move_Create( size_t row, size_t col )
{
	return Action_Create( MOVE( row, col ) + 1, NULL ) ;
}

Move*
Move_FindMove( const ActionSet* set, size_t row, size_t col )
{
	Move* move = NULL ;
	if ( row > 2 || col > 2 ) {
		errno = ERANGE ;
	}
	else {
		ActionID id = MOVE( col, row ) + 1 ;
		move = ActionSet_getAction( set, id ) ;
	}
	return move ;
}

/**
 * @private
 * @brief Get the ID of the state resulting from making a move.
 * @memberof Move
 *
 * @details This function will determine the ID of the State which
 * would follow from taking the current Move in the initial State
 * @a s.
 *
 * @param[in] self A pointer to an immutable Action instance.
 * @param[in] s A pointer to an immutable State instance.
 *
 * @return The ID of the State that follows @a s when performing the
 * current Action. If the Action is not valid, then the ID of @a s
 * will be returned.
 */
static StateID
Move_apply( const Move* self, const State* s )
{
	StateID s_id ;
	uint8_t move_ordinal ;
	static const StateID mask[] = {
		0x2AAAA0,	/* X */
		0x155550,	/* O */
	} ;

	assert( s ) ;
	assert( self ) ;

	s_id = s->id ;
	move_ordinal = s_id & 0xf ;
	if ( move_ordinal < 9 && !(s_id & self->id) ) {
		s_id |= ( self->id & mask[ move_ordinal % 2 ] ) ;
		/* Update the move ordinal to indicate the next
		 * play. */
		s_id += 1 ;
	}

	return s_id ;
}

/******************************************************************************
 * GridState
 ******************************************************************************/

char
GridState_getMarker( const State* self, size_t row, size_t col )
{
	char marker = 0 ;
	const char* marks = " OX*" ;
	if ( self && row < 3 && col < 3 ) {
		marker = marks[ ( State_getId( self ) >> ((2*(((col)*3)+(row)))+4) ) & 0x3 ] ;
	}
	return marker ;
}

void
GridState_serialize( const State* self, FILE* stream )
{
	if ( self ) {
		const char* str =
			"\n"
			 "\t   1 2 3\n\n"
			"\ta: %c|%c|%c\n"
			 "\t   -+-+-\n"
			"\tb: %c|%c|%c\n"
			 "\t   -+-+-\n"
			"\tc: %c|%c|%c\n\n" ;

		fprintf( stream, str,
			 GridState_getMarker( self, 0, 0 ),
			 GridState_getMarker( self, 1, 0 ),
			 GridState_getMarker( self, 2, 0 ),
			 GridState_getMarker( self, 0, 1 ),
			 GridState_getMarker( self, 1, 1 ),
			 GridState_getMarker( self, 2, 1 ),
			 GridState_getMarker( self, 0, 2 ),
			 GridState_getMarker( self, 1, 2 ),
			 GridState_getMarker( self, 2, 2 ) ) ;
	}
}

/******************************************************************************
 * Board
 ******************************************************************************/

static bool
Board_isTerminalStateID( StateID id )
{
	size_t i ;
	bool is_terminal = false ;
	static const StateID terminal_ids [] = {
		/* Row wins */
		MOVE( 0, 0 ) | MOVE( 1, 0 ) | MOVE( 2, 0 ),
		MOVE( 0, 1 ) | MOVE( 1, 1 ) | MOVE( 2, 1 ),
		MOVE( 0, 2 ) | MOVE( 1, 2 ) | MOVE( 2, 2 ),
		/* Col wins */
		MOVE( 0, 0 ) | MOVE( 0, 1 ) | MOVE( 0, 2 ),
		MOVE( 1, 0 ) | MOVE( 1, 1 ) | MOVE( 1, 2 ),
		MOVE( 2, 0 ) | MOVE( 2, 1 ) | MOVE( 2, 2 ),
		/* Diagonal wins */
		MOVE( 0, 0 ) | MOVE( 1, 1 ) | MOVE( 2, 2 ),
		MOVE( 2, 0 ) | MOVE( 1, 1 ) | MOVE( 0, 2 ),
	} ;

	for ( i = 0 ; i < sizeof( terminal_ids ) / sizeof( StateID ) && !is_terminal ; i++ ) {
		StateID mask = id & terminal_ids[ i ] ;
		is_terminal = ( MOVE_O( terminal_ids[ i ] ) == mask || MOVE_X( terminal_ids[ i ] ) == mask ) ;
	}

	/* TODO:: Set draw states as terminals. */

	return is_terminal ;
}
 
static bool
Board_isTerminalState( const Environment* self, const State* state )
{
	
	size_t i ;
	StateID id ;
	bool is_terminal = false ;
	if ( state && (id = State_getId( state )) > 0 ) {
		is_terminal = Board_isTerminalStateID( id ) ;
	}
	return is_terminal ;
}

GridState*
Board_createState( Board* self, StateID id )
{
	GridState* new = calloc( 1, sizeof( GridState ) ) ;
	if ( new ) {
		int32_t value = 0 ;
		bool terminal = false ;
		StateSet* states = Environment_getStates( &self->_Environment ) ;

		if ( Board_isTerminalStateID( MOVE_X( id ) ) ) {
			value = 30000 ;
			terminal = true ;
		}
		else if ( Board_isTerminalStateID( MOVE_O( id ) ) ) {
			value = -30000 ;
			terminal = true ;
		}
		State_init( ( State* )new, id, value, terminal, NULL ) ;
		StateSet_addState( states, ( State* )new ) ;
	}
	return new ;
}

int32_t
Board_transitionProbability( const Environment* self, const State* si, const State* a, const State* sf )
{
	int32_t probability = 0 ;
	if ( !self ) {
		probability = -1 ;
		errno = EINVAL ;
	}
	else if ( a && si ) {
		const List* transitions = Model_getTransitions( State_getModel( si ), a ) ;

		probability = 100 ;
		if ( transitions ) {
			Transition* t ;
			size_t i = 0 ;
			int32_t total = 0 ;
			int32_t samples = 0 ;

			while ( (t = TransitionList_getItem( transitions, i++ )) ) {
				Transition* t2 ;
				size_t j = 0 ;
				int32_t val = State_getValue( t->state ) ;

				t->weight = 0 ;
				while ( (t2 = TransitionList_getItem( transitions, j++ )) ) {
					if ( t2->state != t->state
					     && val < State_getValue( t2->state ) ) {
						t->weight += 1 ;
					}
				}
				total += ( 1 << t->weight ) ;
				if ( t->state == sf ) {
					samples = ( 1 << t->weight ) ;
				}
			}

			probability = 100 * samples / total ;
		}
	}
	return probability ;
}

bool
Board_isMoveValid( const Environment* self, const State* s, const Action* a )
{
	ActionID move = Action_getId( a ) ;
	StateID state = State_getId( s ) ;

	/* If in the current state, the square in the grid where the
	 * Action dictates to place the marker is unoccupied, the
	 * action is valid. The Environment doesn't know about goal
	 * States. */
	return !Board_isTerminalState( self, s ) &&!( state & move ) ;
}

State*
Board_complement( const Board* self, const State* state )
{
	State* complement = NULL ;
	if ( self ) {
		StateSet* states = Environment_getStates( ( const Environment* )self ) ;
		StateID x = State_getId( state ) & Move_XMASK ;
		StateID o = State_getId( state ) & Move_OMASK ;

		complement = StateSet_getState( states, ( x << 1 ) | ( o >> 1 ) ) ; 
	}
	return complement ;
}

extern void Environment_setCurrentState( Environment*, const State* ) ;
extern void Environment_setInitialState( Environment*, StateID ) ;

/**
 * @brief Determine the product of two states.
 * @memberof Board
 *
 * @details This will get the State instance that is the product of
 * the specified @a state and @a action. If this product does not
 * already exist, it will be created. The product of states applies
 * all the moves expressed in @a action to @a state, if this
 * application is valid (i.e. there are no markers occupying the same
 * grid location). The product State will be n-steps following the
 * initial @a state. The state ordinals, which occupy the least
 * significant nibble of each state's ID, will be used to determine
 * the time sequence of the resultant State.
 */
static const State*
Board_product( void* self, const State* state, const State* action )
{
	State* next = NULL ;
	Board* board = self ;
	const State* m = action ;
	const State* s = state ;

	if ( m && s ) {
		StateID id = State_getId( s ) ;
		StateID m_id = State_getId( m ) ;
		size_t move_ordinal = (m_id & 0xf) ;

		if ( !(id & (((m_id & Move_OMASK) << 1) | ((m_id & Move_XMASK) >> 1) | m_id) & ~0xf) ) {
			StateSet* states = Environment_getStates( self ) ;
			id |= m_id & 0x3ffff0 ;
			id += move_ordinal ? move_ordinal : 1 ;
			if ( !(next = StateSet_getState( states, id )) ) {
				next = ( State* )Board_createState( board, id ) ;
			}
		}
	}
	return next ;
}

const State*
Board_doMove( Board* board, const State* s, const State* a )
{
	const State* next = Board_product( board, s, a ) ;
	if ( next ) {
		Environment_setCurrentState( &board->_Environment, next ) ;
	}
	return next ;
} 

void
Board_Delete( Board** self_ptr )
{
	if ( self_ptr && *self_ptr ) {
		Board* self = *self_ptr ;
		*self_ptr = NULL ;
		/* Don't use StateSet_Delete() to free the terminals
		 * Set, otherwise the states contained therein will be
		 * double-free'd. */
		HashMap_Delete( ( HashMap** )&self->terminals ) ;
		Environment_Delete( ( Environment** )&self ) ;
	}
}

/* #define INITIAL_STATE_ID 0                  | MOVE_X(MOVE(0, 1)) | MOVE_O(MOVE(0,2)) | \ */
/* 			 0                  | MOVE_O(MOVE(1, 1)) | MOVE_X(MOVE(1,2)) | \ */
/* 	                 MOVE_X(MOVE(2, 0)) | MOVE_O(MOVE(2,1))  | 0 */
/* #define INITIAL_STATE_ID 0                  | MOVE_X(MOVE(0, 1)) | MOVE_O(MOVE(0,2)) | \ */
/* 	                 MOVE_X(MOVE(1, 0)) | MOVE_O(MOVE(1, 1)) | MOVE_X(MOVE(1,2)) | \ */
/* 	                 0                  | MOVE_O(MOVE(2,1))  | 0 */
#define INITIAL_STATE_ID 0	/* X moves first */

Board*
Board_Create( ActionSet* moves )
{
	Board* board = NULL ;

	fprintf( stderr, "%-70s", "Creating a model of the Tic-Tac-Toe environment..." ) ;
	if ( (board = calloc( 1, sizeof( Board ) )) ) {
		Iterator i ;
		State* initial ;
		State* const* s ;
		StateSet* states = StateSet_Create() ;

		Environment_init( board,
				  states,
				  Board_isTerminalState,
				  Board_isMoveValid,
				  Board_transitionProbability ) ;

		/* Set the environment product. */
		board->_Environment.apply = Board_product ;

		/* Create the Set of terminal States. */
		StateSet_init( &board->terminals ) ;

		/* Add the initial states. */
		initial = ( State* )Board_createState( board, INITIAL_STATE_ID ) ;
		Environment_setCurrentState( ( Environment* )board, initial ) ;
		Environment_setInitialState( ( Environment* )board, INITIAL_STATE_ID ) ;
		i = StateSet_getFirst( states ) ;
		while ( (s = Iterator_next( &i, NULL )) ) {
			if ( State_isTerminal( *s ) ) {
				StateSet_addState( &board->terminals, *s ) ;
			}
			else {
				/* Get all the following actions. */
				Action* const* a ;
				Iterator m = Set_getFirst( ( Set* )moves ) ;
				while ( (a = Iterator_next( &m, NULL )) ) {
					State morphism ;
					StateID id = Move_apply( *a, initial ) ;
					State_init( &morphism, id, 0, false, NULL ) ;
					Board_product( board, *s, &morphism ) ;
				}
			}
		}
	}
	fprintf( stderr, "\t[ done ]\n" ) ;
	return board ;
}

/******************************************************************************
 * Player
 ******************************************************************************/

/**
 * Singleton ActionSet expressing the generators for the tic-tac-toe environment.
 */
static ActionSet* Player_Moves = NULL ;

__attribute__((constructor))
void Player_Init()
{
	Player_Moves = ActionSet_Create() ;
	if ( Player_Moves ) {
		size_t x ;
		size_t y ;

		for ( x = 0 ; x < 3 ; x++ ) {
			for ( y = 0 ; y < 3 ; y++ ) {
				Move* move = Move_Create( x, y ) ;
				HashMap_addItem( Player_Moves, &move, sizeof( move ) ) ;
			}
		}
	}
}

__attribute__((destructor))
void Player_Fini()
{
	if ( Player_Moves ) {
		Action* a ;
		Iterator i = Set_getFirst( ( Set* )Player_Moves ) ;
		while ( Iterator_remove( &i, &a, sizeof( a ) ) ) {
			free( a ) ;
		}
		HashMap_Delete( &Player_Moves ) ;
	}
}

Agent*
Player_Create( char marker, Board* board )
{
	StateID mask = 0 ;
	Agent* player = NULL ;

	switch ( marker ) {
	case 'X':
	case 'x':
		mask = Move_XMASK ;
		break ;
	case 'O':
	case 'o':
		mask = Move_OMASK ;
		break ;
	default:
		break ;
	} ;


	if ( mask ) {
		/* TODO:: Create a Task for the player. */
		Iterator i ;
		State* const* s ;
		StateSet* goals ;
		const StateSet* terminals = &board->terminals ;
		Task* task = Task_Create( &board->_Environment ) ;

		i = StateSet_getFirst( terminals ) ;
		while ( (s = Iterator_next( &i, NULL )) ) {
			/* TODO:: Populate the goals StateSet. This
			 * will be more efficient if the Board keeps
			 * track of the set of all terminal states in
			 * the Environment when it is first
			 * created. */
			StateID id = State_getId( *s ) ;
			if ( Board_isTerminalStateID( id & mask ) ) {
				Task_addGoal( task, *s ) ;
			}
		}
		player = Agent_Create( Player_Moves, task ) ;
	}
	return player ;
}

/**
 * @brief Get the human Player's next move.
 * @memberof Player
 * @protected
 *
 * @details This will return the next Move to be taken by the Player
 * in the current @a state of the Tic-Tac-Toe game. The move will be
 * read from the standard input stream.
 *
 * @param[in] board A pointer to the Board Environment (this object is
 *	used to validate moves).
 * @param[in] state The current State in the Tic-Tac-Toe game.
 * @param[in] stream The input stream from which the move will be
 *	read. This argument is not used; the standard input will
 *	always be used.
 *
 * @return A pointer to a Move instance (as a base Action instance)
 * expressing the move that will be taken by the Player in the
 * specified state. The ownership of the memory returned by this
 * function is retained by the callee and must not be deleted by the
 * caller.
 */
const State*
Player_GetMove( const Environment* board, const State* state, FILE* stream )
{
	bool quit = false ;
	const State* move = NULL ;

	GridState_serialize( state, stdout ) ;
	do {
		int row ;
		int col ;
		char marker = (( GridState* )state)->last == 'X'?'O':'X' ;

		fprintf( stdout, "\nSelect move for %c (e.g.: a2): ", marker ) ;
		row = getchar() ;
		if ( row == 'q' ) {
			quit = true ;
		}
		else {
			row -= 'a' ;
			col = getchar() - '1' ;
			getchar() ;	/* newline */
			if ( col < 3 && row < 3 ) {
				Action* a = Move_FindMove( Player_Moves, row, col ) ;

				if ( !Board_isMoveValid( NULL, state, a ) ) {
					fprintf( stdout, "Invalid move: %c%c\n", row + 'a', col + '1' ) ;
					a = NULL ;
				}
				else {
					const State* i = Environment_getInitialState( board ) ;
					StateID id = Move_apply( a, i ) ;
					move = Environment_getState( board, id ) ;
				}
			}
		}
	} while ( !move && !quit ) ;

	return move ;
}

/******************************************************************************
 * Task
 ******************************************************************************/

typedef enum {
	RL_GPI,
	RL_FIRSTVISIT_MC,
	RL_NUM_ALGORITHMS,
} RL_ALGORITHM_ID ;

const static char* _algorithm_names[] = {
	[ RL_GPI ]		= "GPI",
	[ RL_FIRSTVISIT_MC ]	= "first-visit",
	[ RL_NUM_ALGORITHMS ]	= NULL,
} ;

struct _tictactoe {
	int32_t		gamma ;
	size_t		num_players ;
	Agent*		player[ 2 ] ;
	size_t		iterations ;
	RL_ALGORITHM_ID	algorithm ;
	StateID		initial ;
} ;

int32_t
TicTacToe_getReward( const Task* self_base, const State* s1, const State* a, const State* s2 )
{
	int32_t reward = 0 ;
	if ( a && s2 ) {
		StateID id = State_getId( s2 ) ;

		id = ( id >> (2*9) ) % 3 ;
		if ( id == 1 ) {
			/* Reward is 100 if the next state is
			 * a win state. */
			reward = 10000 ;
		}
		else {
			reward = -10000 ;
		}
	}
	return reward ;
}

static char
TicTacToe_FindWinner( const State* s )
{
	char winner = ' ' ;
	if ( s ) {
		StateID id = State_getId( s ) ;
		if ( Board_isTerminalStateID( MOVE_X( id ) ) ) {
			winner = 'X' ;
		}
		else if ( Board_isTerminalStateID( MOVE_O( id ) ) ) {
			winner = 'O' ;
		}
	}
	return winner ;
}

static History*
TicTacToe_playGame( const void* task, void* environment, const void* unused )
{
	char winner ;
	const State* move ;
	Board* board = environment ;
	const TicTacToe* self = task ;
	const State* state = Environment_getInitialState( environment ) ;

	History* episode = NULL ;

	do {
		const State* next = NULL ;
		move = Agent_nextAction( self->player[ 0 ], state ) ;

		if ( (next = Board_doMove( board, state, move )) ) {
			History_addStep( &episode, state, move, TicTacToe_getReward( task, state, move, next ) ) ;
			state = next ;
			move = Agent_nextAction( self->player[ 1 ], state ) ;
			if ( (next = Board_doMove( board, state, move )) ) {
				History_addStep( &episode, state, move, TicTacToe_getReward( task, state, move, next ) ) ;
				state = next ;
			}
		}
	} while ( move ) ;

	GridState_serialize( state, stdout ) ;
	switch ( (winner = TicTacToe_FindWinner( state )) ) {
	case 'X':
	case 'O':
		fprintf( stdout, "\n======== %c Wins!  ========\n", winner ) ;
		break ;
	default:
		fprintf( stdout, "\n========   Draw!    ========\n" ) ;
		break ;
	} ;

	fprintf( stdout, "Press any key to continue\n" ) ;
	getchar() ;
	return episode ;
}

TicTacToe*
TicTacToe_Create( Board* environment )
{
	TicTacToe* task = calloc( 1, sizeof( TicTacToe ) ) ;
	if ( task ) {
		Task_init( task,
			   environment,
			   TicTacToe_getReward ) ;
		task->iterations = 1000 ;
		task->gamma = 100 ;
		task->num_players = 1 ;
		task->player[ 0 ] = Player_Create( 'X', environment ) ;
		task->player[ 1 ] = Player_Create( 'O', environment ) ;
		task->initial = 0 ;
		task->algorithm = RL_GPI ;
	}
	return task ;
}

extern char* optarg ;
extern int optind, optopt ;

void
TicTacToe_printUsage( const char* progname )
{
	const char* basename = strrchr( progname, '/' ) + 1 ;
	fprintf( stdout,
		 "Usage: %s [options]\n\n"
		 "Options:\n\n"
		 "-a, --algorithm NAME          Specify the algorithm to use for learning the\n"
		 "                              Tic-Tac-Toe policy [default = GPI].\n"
		 "-e N                          Set the probability of selecting a random action\n"
		 "                              rathern than follow a greedy policy.\n"
		 "-g, --gamma N                 Set the discount value used when estimating\n"
		 "                              the state-value function. The value is assumed\n"
		 "                              to be a fixed-point value with a radix of 0.01\n"
		 "                              [default = 100 (1.0)].\n"
		 "-p, --players N               Set the number of human players to N [default = 1]\n"
		 "-i, --iterations N            The number of iterations to perform when playing\n"
		 "                              the AI against itself. This option is only used if\n"
		 "                              the number of players is 0. [default = 10]\n"
		 "-h, --help                    Print this message.\n"
		 "\n"
		 "Examples:\n"
		 "\n"
		 " -p0 --iterations 10          Play the AI against itself for 10 iterations.\n"
		 " -g40                         Use a discounting factor of 0.4\n"
		 " -p2                          Play against another human (kinda defeats the\n"
		 "                              purpose\n"
		 "\n"
		 "Algorithms:\n"
		 " - GPI             Generalized Policy Iteration (dynamic programming)\n"
		 " - first-visit     First-visit Monte Carlo method for estimating the optimal\n"
		 "                   function.\n"
		 "\n"
		 "Please report bugs to <marc_perron@vociferousvoid.org>\n"
		 , basename ? basename : "tictactoe" ) ;
}

bool
TicTacToe_getOptions( TicTacToe* self, int argc, char* argv[] )
{
	size_t i ;
	int option ;
	int longopt_idx ;
	bool exec = true ;

	static const char* optflags = "a:e:g:p:i:h" ;
	static const struct option optlist[] = {
		{ "algorithm", required_argument, NULL, 'a' },
		{ "gamma", required_argument, NULL, 'g' },
		{ "players", required_argument, NULL, 'p' },
		{ "iterations", required_argument, NULL, 'i' },
		{ "help", no_argument, NULL, 'h' }
	} ;

	while ( (option = getopt_long( argc, argv, optflags, optlist, &longopt_idx )) != -1 ) {
		switch ( option ) {
		case 'a':
			/* Get the algorithm to use for learning the
			 * tic-tac-toe policy. */
			for ( i = 0 ; i < RL_NUM_ALGORITHMS ; i++ ) {
				if ( _algorithm_names[ i ] && optarg && !strcasecmp( _algorithm_names[ i ], optarg ) ) {
					self->algorithm = i ;
					break ;
				}
			}
			break ;
		case 'e':
			break ;
		case 'g':
			/* Get the discounting factor used for
			 * state-value evaluation. */
			self->gamma = strtol( optarg, NULL, 10 ) ;
			break ;

		case 'p':
			self->num_players = strtol( optarg, NULL, 10 ) ;
			break ;

		case 'i':
			self->iterations = strtol( optarg, NULL, 10 ) ;
			break ;

		default:
			/* Unrecognized option. */

		case 'h':
			TicTacToe_printUsage( argv[ 0 ] ) ;
			exec = false ;
			break ;

		} ;
	}
	return exec ;
}

int
main( int argc, char* argv[] )
{
	int option ;
	Action* move ;
	Policy* p1 ;
	Policy* p2 ;
	State* state ;
	size_t x, y ;
	History* episode = NULL ;
	Board* board = Board_Create( Player_Moves ) ;
	TicTacToe* game = TicTacToe_Create( board ) ;

	if ( TicTacToe_getOptions( game, argc, argv ) ) {
		if ( game->num_players < 2 ) {
			/* Create the Policy */		
			fprintf( stderr, "%-70s", "Initialize a greedy policy for player 2..." ) ;
			p2 = EpsilonGreedy_Create( ( Task* )game, 0 ) ;
			fprintf( stderr, "\t[ done ]\n" ) ;
		}
		else {
			fprintf( stderr, "%-70s", "Setting player 2 as a human player..." ) ;
			p2 = StreamPolicy_Create( ( Environment* )board, Player_GetMove, NULL ) ;
			fprintf( stderr, "\t[ done ]\n" ) ;
		}
		Agent_setPolicy( game->player[ 1 ], p2 ) ;

		if ( game->num_players < 1 ) {
			fprintf( stderr, "%-70s", "Initialize a greedy policy for player 1..." ) ;
			p1 = EpsilonGreedy_Create( ( Task* )game, 0 ) ;
			fprintf( stderr, "\t[ done ]\n" ) ;
		}
		else {
			fprintf( stderr, "%-70s", "Setting player 1 as a human player..." ) ;
			p1 = StreamPolicy_Create( ( Environment* )board, Player_GetMove, NULL ) ;
			fprintf( stderr, "\t[ done ]\n" ) ;
		}
		Agent_setPolicy( game->player[ 0 ], p1 ) ;
	      
		/* Get the initial state */
		do {
			free( episode ) ;
			episode = TicTacToe_playGame( game, board, NULL ) ;
			switch ( game->algorithm ) {
			case RL_GPI:
				fprintf( stderr, "%-70s", "Determining policy using GPI..." ) ;
				DP_GeneralizedPolicyIteration( p2, game ) ;
				fprintf( stderr, "\t[ done ]\n" ) ;
				break ;
			case RL_FIRSTVISIT_MC:
				fprintf( stderr, "%-70s", "Determining policy using the first-visit Monte Carlo method..." ) ;
				MonteCarlo_FirstVisit( p2, episode ) ;
				fprintf( stderr, "\t[ done ]\n" ) ;
				break ;
			default:
				/* Error! */
				break ;
			} ;
		} while ( episode && ( game->num_players || game->iterations-- ) ) ;

		Board_Delete( &board ) ;
		free( p1 ) ;
		free( p2 ) ;
	}
	free( game ) ;
	return 0 ;
}

/** @} */
