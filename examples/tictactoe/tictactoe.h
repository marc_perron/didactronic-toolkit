/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * @defgroup Examples Example Programs
 * @brief Module which groups all elements used in the example
 *	programs.
 *
 * @details This module groups all the files, classes, functions, that
 * are used to implement the various example programs that demonstrate
 * the usage of the Didactronic toolkit.
 */

/**
 * @addtogroup xo Tic-Tac-Toe
 * @brief Example using the Didactronic toolkit to learn Tic-Tac-Toe.
 * @ingroup Examples
 *
 * @details The example program described in this module implements an
 * A.I., using the Didactronic toolkit, to learn an optimal policy for
 * playing Tic-Tac-Toe. Initially, the A.I. only has
 * rudimentary knowledge of the mechanics of Tic-tac-toe, and will
 * learn strategy through policy iteration.
 *
 * It is possible to invoke the Tic-tac-toe example to either: 1) have
 * a human player play against the A.I. (1 player) or 2) have the
 * A.I. play against itself (0 players).
 *
 * In the case where there is a human player, the @e X marker
 * represents the A.I. and the @e O marker represents the human
 * opponent opponent.
 *
 * In the case where there are no human players, the computer will
 * continue playing until either the number of iterations (specified
 * by the @c '-i ITERATIONS' command line option) have been peformed,
 * or the game stabilizes to a particular outcome.
 *
 * @{
 */

/**
 * @file
 * @brief Declaration of a Task example for the Tic-Tac-Toe domain.
 */

#ifndef __TICTACTOE_H__
#define __TICTACTOE_H__

/* POSIX includes */
#include <stdlib.h>

/* Didactronic includes */
#include "agent.h"
#include "environment.h"
#include "state.h"
#include "task.h"

/******************************************************************************
 * Board
 ******************************************************************************/

/**
 * @class Board
 * @implements Environment
 * @brief Class representing the Environment of a Tic-Tac-Toe task.
 *
 * @details This class is a specialization of Environment specific to
 * the Tic-Tac-Toe task. Instances of this class will generate the
 * complete set of all states for the task.
 */
typedef struct _board {
	Environment		_Environment ;
	StateSet		terminals ;
} Board ;

/**
 * @brief Create a Tic-Tac-Toe Board.
 * @memberof Board
 *
 * @details This will create the Board and allocate the set of all
 * possible states in a game of Tic-Tac-Toe, and link these states
 * with Actions. The initial configuration will mark all wining states
 * with a value of +300, and all losing states with a value of
 * -300. All transitions will initially have equal probability.
 *
 * @param[in] moves A pointer to an ActionSet containing all the valid
 *	Tic-Tac-Toe moves that can be taken by an Agent.
 */
Board* Board_Create( ActionSet* moves ) ;

/**
 * @brief Get the initial State of the Environment.
 * @memberof Board
 *
 * @details This will return the initial State in the current
 * Environment for the Tic-Tac-Toe task.
 *
 * @param[in] self A pointer to a Board instance.
 *
 * @return A pointer to the initial BoardState in the Tic-Tac-Toe
 * task Environment.
 */
State* Board_getInitialState( const Board* self ) ;

/**
 * @brief Find the complement of a State.
 * @memberof Board
 *
 * @details This will find the BoardState which is the complement of
 * the specified State instance. The complement of a BoardState is a
 * state with the same configuration but the markers swapped (i.e. X
 * markers set to O, and vice versa).
 *
 * @param[in] self A pointer to a Board instance.
 * @param[in] state A pointer to a BoardState instance whose
 *	complement is being requested.
 *
 * @return A pointer to a BoardState instance that represents the
 * complement of the current State. The ownership of the memory
 * returned by this function is retained by the callee and must not be
 * deleted by the caller. If the complement of the specified state is
 * not a valid board configuration for any reason (unlikely), this
 * function will return @c NULL.
 */
State* Board_complement( const Board* self, const State* state ) ;

/**
 * @brief Determine whether or not an action is valid in the current
 * 	state.
 * @memberof Board
 *
 * @details This will evaluate whether or not the action specified by
 * @a a is valid in State @a s. An action is valid if it expresses the
 * placement of a marker in an unoccupied square.
 *
 * @param[in] self A pointer to the Board instance describing the
 *	Tic-Tac-Toe environment.
 * @param[in] s A pointer to an immutable State instance.
 * @param[in] a A pointer to an immutable Action instance expressing a
 *	Move.
 *
 * @return A boolean flag indicating whether or not the action is valid.
 *
 * @retval true The action represents the placement of a marker in an
 * 	unoccupied grid square.
 * @retval false The action represents the placement of a marker in an
 *	occupied grid square.
 */
bool Board_isMoveValid( const Environment* self, const State* s, const Action* a ) ;

/******************************************************************************
 * Player
 ******************************************************************************/

/**
 * @class Player
 * @implements Agent
 * @brief Class representing a Player in the Tic-Tac-Toe game.
 *
 * @details This class is a specialization of the reinforcement
 * learning Agent class which is used to represent a player in a game
 * of Tic-Tac-Toe. This agent can be used to express either an AI or
 * human player, depending on the type of Policy associated with
 * it. Each player will also have an associated marker which will be
 * used to determine which marker the player is placing in the game
 * board.
 */
typedef Agent Player ;

/**
 * @brief Create a Player instance on the heap.
 * @memberof Player
 *
 * @details This function will allocate a new Player instance on the
 * heap. The ActionSet describing the Player's capabilities will also
 * be created by this function and associated with the Player.
 *
 * @return A pointer to a newly allocated Player instance initialized
 * with the capabilities of a player in a game of Tic-Tac-Toe. The
 * ownership of the memory returned by this function is returned to
 * the caller who is responsible for deleting it.
 *
 * @see Agent_Delete
 */
Player* Player_Create( char marker, Board* env ) ;

/******************************************************************************
 * TicTacToe
 ******************************************************************************/

/**
 * @class TicTacToe
 * @implements Task
 * @brief Class used to express Tic-Tac-Toe as a reinforcement
 *	learning task.
 *
 * @details This is a specialization of the Task class which is
 * specific to the Tic Tac Toe domain. This class provides custom
 * implementations of the transitionProbability() and getReward()
 * member functions.
 *
 * @see TicTacToe_transitionProbability
 * @see TicTacToe_getReward
 */
typedef struct _tictactoe TicTacToe ;

/**
 * @brief Create an instance of the TicTacToe Task on the heap.
 * @memberof TicTacToe
 *
 * @details This will create a new instance of TicTacToe which
 * provides a custom Task implementation.
 *
 * @param[in] environment A pointer to a Board instance expressing the
 *	Environment in which the Tic-Tac-Toe Task will be defined.
 *
 * @return A pointer to a TicTacToe instance created on the heap. The
 * ownership of the memory returned by this function is transferred to
 * the caller who is responsible for deleting it.
 */
TicTacToe* TicTacToe_Create( Board* environment ) ;

/**
 * @brief Get command line arguments relevant to the Tic-Tac-Toe Task.
 * @memberof TicTacToe
 *
 * @details This will parse command line options extracting argument
 * values that are relevant to the Tic-Tac-Toe task. As these
 * arguments are encountered, they will be removed from the argument
 * list (i.e. their entries will be set to @c NULL) so that they are
 * not processed by any other function to retrieve options. This
 * function will use the standard getopt() function to extract option
 * data.
 *
 * @param[in] self A pointer to a Tic-Tac-Toe Task instance.
 * @param[in] argc The number of command line arguments.
 * @param[in] argv An array of pointers to the argument strings. 
 *
 * @return A flag indicating whether or not to continue executing the
 * Tic-Tac-Toe task.
 */
bool TicTacToe_getOptions( TicTacToe* self, int argc, char* argv[] ) ;

/**
 * @brief Get the probability of reaching the specified final state
 *	following a given move.
 * @memberof TicTacToe
 *
 * @details This will calculate the probability that the specified @p
 * move will cause the Tic Tac Toe game to move from an @p initial
 * State to the specified @p final State. The probabilities are biased
 * toward the possible final state with the lowest value (which is the
 * best value for the opponent). Therefore the lower the final State's
 * value is, the more likely it will be to reach that state following
 * the application of a @p move; in other words, we are looking to
 * calculate @f$
 * P(\textrm{final}\mid\textrm{move}\wedge\textrm{initial}) @f$. This
 * is achieved by ascribing a sample weight, @f$w(s_{t+1})=2^n@f$ to
 * each possible final state, @f$s' \in S_{as}@f$ where @f$n@f$ is the
 * number of possible final states with a value strictly greater than
 * that of @p final. This will ensure that the state with the lowest
 * value is twice as likely to be chosen than that with the next
 * lowest value. Therefore the probability of reaching the @p final
 * state is:
 *
 * @f[
 * P(s_{t+1}) = \frac{w(s_{t+1})}{\sum_{s'}w(s')}, \forall s' \in S_{as} 
 * @f]
 *
 * @param[in] initial The initial State of the Tic Tac Toe game.
 * @param[in] move The action that was taken in that state.
 * @param[in] final The desired final State.
 *
 * @return A fixed point value, in quanta of 0.01 (i.e. 100 is
 * equivalent to 1.0 and 1 is equivalent to 0.01), expressing the
 * probability that the specified @p final state will be reached by
 * applying the specified @p move to the @p initial state.
 */
int32_t TicTacToe_transitionProbability( const State* initial, const Action* move, const State* final ) ;

/**
 * @brief Calculate the return for some step in the Task.
 * @memberof TicTacToe
 *
 * @details In the Tic-Tac-Toe Task, rewards of +300, -300, or 0 are
 * awarded for winning states, losing states, or draw states
 * respectively. The step of the Task is described by the @p initial
 * State, @p final State, and the @p Action. This function will
 * calculate the reward for that particular step.
 *
 * @param[in] self A pointer to a TicTacToe instance expressing the
 *	current Task.
 * @param[in] initial The initial State of the step in the Task.
 * @param[in] action The Action taken in the current step.
 * @param[in] final The final State of the current step in the Task.
 *
 * @return The reward for the current step in the Task expressed as a
 * fixed-point value quantized to 0.01.
 *
 * @retval 30000 The @p final State is a winning state.
 * @retval -30000 The @p final State is a losing state.
 * @retval 0 The @p final State is a non-terminal state or is a draw
 *	state.
 */
int32_t TicTacToe_getReward( const Task* self, const State* initial, const State* action, const State* final ) ;

/** @} */

#endif	/* __TICTACTOE_H__ */
