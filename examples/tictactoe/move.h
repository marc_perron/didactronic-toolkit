/* -*- Mode: C -*- */
/*
 * Copyright (c) 2016, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* Didactronic includes */
#include <action.h>

/**
 * @addtogroup xo
 * @{
 *
 * @file
 *
 * @brief Derived Action type for the Tic-Tac-Toe task.
 */

/**
 * @class Move
 * @extends Action
 * @brief An action type expressing moves in Tic-Tac-Toe.
 *
 * @details This class is a specialization of the generic Action class
 * which expresses the placing of a marker in a particular grid cell
 * for a Tic-Tac-Toe Task. 
 */
typedef Action Move ;

/**
 * @brief Create a Move instance.
 * @memberof Move
 * @ingroup xo
 *
 * @details This constructor creates a new Move instance on the heap
 * that expresses placing a marker at the specified @a row and @a col
 * in the tic tac toe grid.
 *
 * @param[in] row The row of the Tic Tac Toe grid where the marker
 *	will be placed by the new Move instance.
 * @param[in] col The column of the Tic Tac Toe grid where the marker
 *	will be placed by the new Move instance.
 *
 * @return A new instance of Move, created on the heap, expressing the
 * placing of a marker at the specified (@a row, @a col) position in
 * the Tic Tac Toe grid. The ownership of the memory returned by this
 * function is transferred to the caller who is responsible for
 * ensuring it is deleted.
 */
Move* Move_Create( size_t row, size_t col ) ;

/**
 * @brief Get the Id of the state resulting from making the specified
 *	move.
 * @memberof Move
 *
 * @details This will get the StateID of the State resulting from the
 * player represented by @a marker on an emtpy board.
 *
 * @param[in] self A pointer to the Move instance.
 * @param[in] marker The marker of the player that is making the
 *	current move. Valid markers are @c X, @c O, or @c *.
 *
 * @return The StateID of the State resulting from the player
 * represented by the specified @a marker on an empty board. In the
 * case where the marker is neither @c X or @c O, this function call
 * is equivalent to @e Action_getId().
 */
StateID Move_getId( const Move* self, char marker ) ;

/**
 * @brief Get a move corresponding with a particular marker placement.
 * @memberof Move
 *
 * @details This is a class function which will search the specified
 * ActionSet for a Move instance which corresponds with the placement
 * of a marker at the specified (@a row, @a col) position in a
 * Tic-Tac-Toe grid. This function has constant, O(1), complexity.
 *
 * @param[in] set A pointer to an ActionSet instance whose elements
 *	express all the valid moves in the Tic-Tac-Toe domain.
 * @param[in] row The row of the marker placement expressed by the
 *	Move to find in the set.
 * @param[in] col The column of the marker placement expressed by the
 *	Move to find in the set.
 *
 * @return A pointer to a Move instance which corresponds with the
 * specified marker placement. The ownership of the memory returned by
 * this function is retained by the callee, and must not be deleted by
 * the caller. In the case of an error, this function will return 
 * @c NULL and set @e errno accordingly.
 *
 * @par Errors
 * - EINVAL: The ActionSet instance is invalid.
 * - ERANGE: The row or column index is out of range.
 * - ENOENT: The corresponding move is not an element of the set.
 */
Move* Move_FindMove( const ActionSet* set, size_t row, size_t col ) ;

/**
 * @}
 */
