/* -*- Mode: C -*- */
/*
 * Copyright (c) 2014, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright-holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 */

/**
 * @addtogroup GamblersProblem Gambler's Problem
 * @ingroup Examples
 * @brief Example using the Didactronic toolkit to solve the gambler's
 *	problem.
 * @{
 *
 * @file
 *
 * @brief Definition of an example solution for the gambler's problem
 *	reinforcement learning Task.
 *
 * @details A test program demonstrating value iteration of dynamic
 * programming. The example used is the Gambler's Problem which is
 * described as follows:
 *
 * A gambler has the opportunity to make bets on the outcomes of a
 * sequence of coin flips. If the coin comes up heads, he win as many
 * dollars as he as staked on that flip; if it is tails, he loses his
 * stake. The game ends when the gambler wins by reaching his goal of
 * $G, or loses by running out of money. On each flip, the gambler
 * must decide what portion of his capital to stake, in integer number
 * of dollars. This problem can be formulated as an undiscounted,
 * episoded, finite MDP. The state is the gambler's capital, s in {1,
 * 2,..., 99} and the actions are stakes, a in {1, 2,...,
 * min(s,G-s)}. The reward is zero on all transitions, except those on
 * which the gambler reaches his goal, when it is +1. A state-value
 * function then gives the probability of winning from each state. A
 * policy is a mapping from levels of capital to stakes. The optimal
 * policy maximizes the probability of reaching the goal.
 */


/* POSIX includes */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Local includes */
#include "action.h"
#include "agent.h"
#include "environment.h"
#include "epsilon-greedy.h"
#include "policy.h"
#include "state.h"
#include "task.h"
#include "transition.h"
#include "util/list.h"
#include "util/map.h"

static int32_t _goal = 100 ;
static int32_t _win_prob = 50 ;	/* Probability of win is 0.5. */
static size_t  _iterations = 10 ;

/******************************************************************************
 * Gambler
 ******************************************************************************/

/**
 * @class Gambler
 * @implements Agent
 * @brief Class representing an gambler betting on coin flips.
 *
 * @details The Gambler class represents a player betting on the
 * outcome of a coin flip in order to reach a goal capital.
 */
typedef Agent Gambler ;

/**
 * @brief Create a Gambler Agent intsance.
 * @memberof Gambler
 *
 * @details This will create a Gambler instance that will be playing
 * to reach the desired @a goal capital. The Gambler's capabilities
 * are the possible bets that can be made given a non-zero capital.
 *
 * @param[in] goal The goal capital that the Agent is trying to reach.
 *
 * @return A pointer to a new Gambler instance created on the
 * heap. The ownership of the memory returned by this function is
 * transferred to the caller who is responsible for deleting it.
 */
Gambler*
Gambler_Create( uint32_t goal )
{
	Gambler* new = NULL ;
	ActionSet* bets = ActionSet_Create() ;
	if ( bets ) {
		ActionID bet = 1 ;
		while ( bet < goal ) {
			ActionSet_insert( bets, Action_Create( bet, NULL ) ) ;
			bet++ ;
		}
		if ( !(new = Agent_Create( bets, NULL )) ) {
			/* Allocation of the agent failed, delete the
			 * ActionSet to avoid a memory leak. */
			HashMap_Delete( &bets ) ;
		}
	}
	return new ;
}

/******************************************************************************
 * Capital
 ******************************************************************************/

/**
 * @class Capital
 * @implements Environment
 * @brief Class expressing the Capital in the coin flip game.
 *
 * @details Instances of this class are used to model the possible
 * evolution of a game involving a Gambler betting on the outcome of a
 * coin flip. The coin flip may or may not be fair. Meaning the
 * Environment must also model the probability of winning and losing.
 */
typedef Environment Capital ;

/**
 * @brief Determine if a bet is valid.
 * @memberof Capital
 * @private
 *
 * @details This will determine whether or not the specified @a bet is
 * valid given the current capital.
 *
 * @param[in] purse A pointer to a State instance representing the
 *	amount of capital currently held.
 * @param[in] bet The amount of the bet to place on the coin flip.
 * @param[in] goal The goal capital of the Gambler's problem Task.
 *
 * @return A flag indicating whether the @a bet is valid given the
 * current capital in the gambler's @a purse.
 *
 * @retval true The bet is valid.
 * @retval false There is not enough capital in the purse to place the
 *	specified @a bet.
 */
Capital_isBetValid( const State* purse, const Action* bet, uint32_t goal )
{
	StateID capital = State_getId( purse ) ;
	ActionID bet_amount = Action_getId( bet ) ;
	return ( capital > 0 && capital < goal && bet_amount <= capital ) ;
}

/**
 * @protected
 * @brief Get the probability of reaching the specified capital after
 *	placing the specified bet.
 * @memberof CoinFlip
 *
 * @details This will calculate the probability of reaching the
 * specified State of capital by placing the specified @a bet. The
 * final state will be the result of either a winning or losing flip.
 *
 * @param[in] self A pointer to a Capital instance modelling the
 *	current Environment.
 * @param[in] initial_capital The State instance expressing the
 *	initial capital previous to betting.
 * @param[in] bet The Action instance expressing the value of the bet.
 * @param[in] final_capital The State instance expressing the final
 *	capital following a coin flip.
 *
 * @return A fixed-point value, in quanta of 0.01 (i.e. 100 is 1.0 and
 * 1 is 0.01), expressing the probability that the specified final
 * capital will be reached by placing the specified bet.
 */
int32_t
Capital_transitionProbability( const Capital* self, const State* initial_capital, const Action* bet, const State* final_capital )
{
	int32_t probability = -1 ;
	if ( self && bet && initial_capital && final_capital ) {
		StateID icapital_value = State_getId( initial_capital ) ;
		StateID fcapital_value = State_getId( final_capital ) ;
		ActionID bet_value = Action_getId( bet ) ;

		probability = 0 ;
		if ( abs( icapital_value - fcapital_value ) == bet_value ) {
			if ( icapital_value > fcapital_value ) {
				probability = _win_prob ;
			}
			else {
				probability = 100 - _win_prob ;
			}
		}
	}
	return probability ;
}

/**
 * @brief Create a Capital environment instance on the heap.
 * @memberof Capital
 *
 * @details This will create an instance of the Capital class which
 * will model a game with the specified goal, and the specified
 * probability of a flip which leads to the Agent (Gambler) winning
 * the game.
 *
 * @param[in] goal The goal capital in the current environment.
 * @param[in] win_prob The probability that a coin flip is a winning
 *	flip. This probability is expressed as a fixed-point value
 *	quantized to 0.01 (i.e. 1 = 0.01).
 * @param[in] gambler A pointer to the Gambler Agent that will be
 *	betting on the coin flips.
 *
 * @return A pointer to a new Capital instance, created on the
 * heap. The ownership of the memory returned by this function is
 * transferred to the caller who is responsible for deleting it.
 *
 * @see Environment_Delete
 */
Capital*
Capital_Create( uint32_t goal, int32_t win_prob, const Gambler* gambler )
{
	Iterator i ;
	State* const* c ;
	int32_t capital ;
	Capital* new = NULL ;
	const ActionSet* stakes = Agent_getCapabilities( gambler ) ;
	StateSet* purse = StateSet_Create( NULL ) ;

	for ( capital = 0 ; capital <= goal ; capital++ ) {
		State* s = State_Create( capital, 0 ) ;
		Model_Create( s ) ;
		StateSet_addState( purse, s ) ;
	}
	i = StateSet_getFirst( purse ) ;
	while ( (c = Iterator_next( &i, NULL )) ) {
		Iterator j ;
		Action* const* a ;

		j = Set_getFirst( ( Set* )stakes ) ;
		while ( (a = Iterator_next( &j, NULL )) ) {
			if ( Capital_isBetValid( *c, *a, goal ) ) {
				State* c2 ;
				List* transitions ;
				Model* model = State_getModel( *c ) ;
				StateID initial_capital = State_getId( *c ) ;
				ActionID bet = Action_getId( *a ) ;
				/* Win */
				if ( (c2 = StateSet_getState( purse, initial_capital + bet )) ) {
					Model_addTransition( model, *a, c2 ) ;
				}
				/* Loss */
				if ( (c2 = StateSet_getState( purse, initial_capital - bet )) ) {
					Model_addTransition( model, *a, c2 ) ;
				}
			}
		}
	}
	if ( !(new = Environment_Create( purse )) ) {
		StateSet_Delete( &purse ) ;
	}
	Environment_setTransitionEvaluator( new, Capital_transitionProbability ) ;
	return new ;
}

/******************************************************************************
 * Task
 ******************************************************************************/

/**
 * @class CoinFlip
 * @implements Task
 * @brief Definition of the Gambler's Problem reinforcement learning Task.
 *
 * @details This class represents the reinforcement learning task of
 * betting on the outcomes of a coin flip given an initial capital, a
 * goal capital, and the probability of a coin flip being a winning
 * flip.
 */
typedef struct _coin_flip {
	Task_EXTEND ;
	size_t		iterations ;
	uint32_t	goal ;
	uint32_t	initial_capital ;
	int32_t		win_probability ;
} CoinFlip ;

bool
CoinFlip_getOptions( CoinFlip* self, int argc, char* argv[] )
{
	bool exec = true ;
	int option ;
	extern char* optarg ;
	extern int optind, optopt ;

	while ( (option = getopt( argc, argv, "G:p:i:c:h" )) != -1 ) {
		double tmp_double ;
		switch ( option ) {
		case 'G':
			/* Get the goal state value. */
			self->goal = strtol( optarg, NULL, 10 ) ;
			break ;
		case 'p':
			/* Get the probability of a winning flip. */
			self->win_probability = 100 * strtod( optarg, NULL ) ;
			break ;
		case 'c':
			/* Get the initial capital. */
			self->initial_capital = strtol( optarg, NULL, 10 ) ;
			break ;
		case 'i':
			/* Get the number of iterations to perform. */
			_iterations = strtol( optarg, NULL, 10 ) ;
			break ;
		case 'h':
			exec = false ;
			fprintf( stdout,
				 "Usage:\tgamblers-problem [OPTION...]\n"
				 "\n"
				 "  -G GOAL            Set the goal capital. GOAL is an integer number of dollars.\n"
				 "                     [default GOAL = 100]\n"
				 "  -c CAPITAL         The initial capital in dollars [default CAPITAL = 50]\n"
				 "  -p PROBABILITY     Specify the probability of a winning probability to the specified\n"
				 "                     value. The probability is expressed as a floating-point value.\n"
				 "                     [default PROBABILITY = 0.5].\n"
				 "  -i ITERATIONS      Specify the number of value iterations to perform.\n"
				 "                     [default ITERATIONS = 10]\n"
				 "  -h                 print this message.\n\n" ) ;
			break ;
		default:
			/* Unrecognized option */
			break ;
		} ;
	}

	return exec ;
}

/**
 * @brief Calculate the reward value for a particular state-action
 * 	pair.
 * @memberof CoinFlip
 *
 * @details Given a @a current state, @a next state, and an @a action,
 * this will calculate the reward for arriving at the @a next State by
 * taking the specified @a action from the @a current state. For the
 * CoinFlip task, the reward will be 1 whenever the goal state is
 * reached, -1 when the capital is exhausted, and 0 otherwise.
 *
 * @param[in] self_base A pointer to a CoinFlip instance.
 * @param[in] current The current state of the MDP.
 * @param[in] next The next state in the MDP.
 * @param[in] a The action that will lead to the @a next state
 * 	from the @a current one.
 *
 * @return A fixed-point value, quantized to 0.01, expressing the
 * reward associated with the state-action-state triple.
 *
 * @retval 100 The @a next state corresponds with the goal This is the
 *	fixed-point value expressing 1.0.
 * @retval -100 The @a next state corresponds with having zero capital
 *	remaining. This is the fixed-point value expressing -1.0.
 * @retval 0 The @a next state is neither the goal state, or the state
 *	of having zero capital.
 */
int32_t
CoinFlip_getReward( const Task* self_base, const State* current, const Action* a, const State* next )
{
	CoinFlip* self = ( CoinFlip* )self_base ;
	int reward = 0 ;
	if ( next ) {
		StateID capital = State_getId( next ) ;
		if ( capital >= self->goal ) {
			reward = 100 ;
		}
	}
	return reward ;
}

/**
 * @brief Allocate a new CoinFlip Task on the heap.
 * @memberof CoinFlip
 *
 * @details This will allocate a new CoinFlip reinforcement learning
 * Task. Parsing the command line arguments to determine the initial
 * capital, the goal capital, and the probability of a winning coin
 * flip. If any of these parameters are not specified, default values
 * will be used. The defaults are:
 *
 * - goal capital: $100
 * - initial capital: $50
 * - winning probability: 50% (0.5)
 *
 * @return A pointer to a new CoinFlip instance allocated on the
 * heap. The ownership of the memory returned by this function is
 * transferred to the caller who is responsible for deleting it.
 */
CoinFlip* CoinFlip_Create()
{
	CoinFlip* task = calloc( 1, sizeof( CoinFlip ) ) ;
	if ( task ) {
		Task_init( task, NULL, CoinFlip_getReward ) ;
		task->goal = 100 ;
		task->iterations = 10 ;
		task->initial_capital = 50 ;
		task->win_probability = 50 ;
	}
	return task ;
}

/******************************************************************************
 * Policy
 ******************************************************************************/

int
main( int argc, char* argv[] )
{
	size_t i ;
	int errcode = 0 ;
	Policy* policy = NULL ;
	Capital* purse = NULL ;
	Gambler* player = NULL ;
	CoinFlip* task = CoinFlip_Create() ;

	if ( CoinFlip_getOptions( task, argc, argv ) ) {
		fprintf( stderr, "%-40s", "Create the Gambler Agent..." ) ;
		player = Gambler_Create( task->goal ) ;
		fprintf( stderr, "\t[ done ]\n" ) ;

		fprintf( stderr, "%-40s", "Create the Environment (the purse)..." ) ;
		purse = Capital_Create( task->goal, task->win_probability, player ) ;
		fprintf( stderr, "\t[ done ]\n" ) ;

		fprintf( stderr, "%-40s", "Create a policy..." ) ;
		policy = EpsilonGreedy_Create( ( Task* )task, 0 ) ;
		fprintf( stderr, "\t[ done ]\n" ) ;

		for ( i = 0 ; i < _iterations ; i++ ) {
			fprintf( stderr, "%-40s", "Perform value iteration..." ) ;
			DP_ValueIteration( policy ) ;
			fprintf( stderr, "\t[ done ]\n" ) ;

		}

		fprintf( stdout, "\n=========\nHISTOGRAM\n=========\n" ) ;
		for ( i = 1 ; i < _goal ; i++ ) {
			StateSet* stakes = Environment_getStates( purse ) ;
			State* capital = StateSet_getState( stakes, i ) ;
			const State* bet = Policy_apply( policy, capital, 0 ) ;
			fprintf( stdout, "Capital: %d (%f), Bet: %d\n", State_getId( capital ), State_getRealValue( capital, 100 ), bet ? State_getId( bet ) : 0 ) ;
		}
		free( policy ) ;
		Environment_Delete( &purse ) ;
		Agent_Delete( &player ) ;
	}

	free( task ) ;
	return errcode ;
}

/** @} */
