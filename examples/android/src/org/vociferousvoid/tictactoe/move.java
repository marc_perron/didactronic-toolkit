package org.vociferousvoid.tictactoe ;

import org.vociferousvoid.cyberdyne.Action ;

public class Move extends Action
{
    public Move( int x, int y )
    {
	super( 0x3 << 2*((y*3)+x) ) ;
    }
}
