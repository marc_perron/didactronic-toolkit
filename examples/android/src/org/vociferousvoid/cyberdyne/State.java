/* -*- Mode: Java -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the <organization> nor the names of its
 *           contributors may be used to endorse or promote products
 *           derived from this software without specific prior written
 *           permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
package org.vociferousvoid.cyberdyne ;

/**
 * @brief Abstract state.
 *
 * @details This class expresses an abstract state in a reinforcement
 * learning task. States in a particular Task Environment should
 * derive from this class in order to be useable with the
 * reinforcement learning framework defined in the Cyberdyne
 * Toolkit. The base State type groups an ID, and the State's value.
 */
public class State {
    /**
     * @brief Private handle to the native State instance.
     */
    private long self ;

    /**
     * @brief Create a new State.
     *
     * @details This constructor will create a new State instance,
     * associating with it the specified @p id and @p value. This will
     * allocate a native implementation of the State class for which
     * this instances provides a stub.
     *
     * @param[in] id The ID of the new State instance. This ID must be
     * unique for a given Task Environment.
     * @param[in] value The floating point value initially assigned to
     * the State.
     */
    public State( int id, float value )
    {
	self = Create( id, value ) ;
    }

    /**
     * @brief Delete the current State instance.
     *
     * @details This function must be called when the State instance
     * is no longer needed. It will cleanup the current State
     * instance, and release the native implementation of the State
     * along the way. This function will also be called in the
     * finalize section of the class to try to ensure that all
     * instances of the State class are cleaned up before the program
     * exits.
     */
    public void delete()
    {
	Delete( self ) ;
	self = 0 ;
    }

    /**
     * @brief Called by the garbage collector when the State instance
     * can be cleaned up.
     */
    protected void finalize()
    {
	this.delete() ;
    }

    /**
     * @brief Get the State's Id.
     *
     * @details This function provides access to the unique Id
     * associated with the current State instance at construction
     * time. The Id returned by this function is guaranteed to be
     * unique within a single Task Environment.
     *
     * @return An integer uniquely identifying the current State in a
     * particular Task Environment.
     */
    public int getId() 
    {
	return getId( self ) ;
    }

    public float getValue()
    {
	return getValue( self ) ;
    }

    public void setValue( float value )
    {
	int fixed_value = ( int )( 100.0f * value ) ;
	setValue( self, fixed_value ) ;
    }

    /**
     * @brief Private interface to the native constructor.
     *
     * @details This function invokes the native constructor for the
     * State object. This is called by the State constructor.
     *
     * @param[in] id The Id to associate with the State.
     * @param[in] value The floating point representation of the
     * State's initial value. This value will be clamped to the
     * precision allowed by the native State implementation.
     *
     * @return A handle to the native State implementation.
     */
    private native long Create( int id, float value ) ;

    private native void Delete( long self_ptr ) ;
    private native int getId( long self ) ;
    private native float getValue( long self ) ;
    private native void setValue( long self, float value ) ;
}
