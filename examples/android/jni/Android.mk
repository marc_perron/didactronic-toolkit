LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE		:= cyberdyne-tk
LOCAL_SRC_FILES		:= ../../../.libs/libcyberdyn-tk.so
include $(PREBUILT_SHARED_LIBRARY)

LOCAL_MODULE		:= cyberdyne-jni
LOCAL_SRC_FILES		:= state-jni.c policy-jni.c
LOCAL_C_INCLUDES	:= $(LOCAL_PATH)/../../../public
LOCAL_SHARED_LIBRARIES	:= cyberdyne-tk
include $(BUILD_SHARED_LIBRARY)
