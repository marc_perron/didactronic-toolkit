/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <stdint.h>

/* NDK includes */
#include <jni.h>

/* Cyberdyne-TK includes */
#include <state.h>

jlong
org_vociferousvoid_cyberdyne_State_Create( JNIEnv* env, jobject obj, jint id, jfloat value )
{
	int32_t fixed_value = 100 * value ;
	return ( jlong )State_Create( id, fixed_value ) ;
}

void
org_vociferousvoid_cyberdyne_State_Delete( JNIEnv* env, jobject obj, jlong self_ptr )
{
	State* self = ( State* )self_ptr ;
	State_Delete( &self ) ;
}

jint
org_vociferousvoid_cyberdyne_State_getId( JNIEnv* env, jobject obj, jlong self )
{
	return State_getId( ( State* )self ) ;
}

jfloat
org_vociferousvoid_cyberdyne_State_getValue( JNIEnv* env, jobject obj, jlong self )
{
	return 0.01f * State_getValue( ( State* )self ) ;
}

void
org_vociferousvoid_cyberdyne_State_setValue( JNIEnv* env, jobject obj, jlong self, jfloat value )
{
	int32_t fixed_value = 100 * value ;
	State_setValue( ( State* )self, fixed_value ) ;
}
