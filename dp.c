/* -*- Mode: C -*- */
/*
 * dp.c - Provides implementations for various dynamic programming
 *	algorithms.
 *
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>

/* Local includes */
#include "dp.h"
#include "model.h"
#include "state.h"

void
DP_IterativePolicyEvaluation( Policy* policy, const Task* task )
{
	int32_t delta = 0 ;
	const StateSet* states = Task_getStates( task ) ;
	do {
		State* s ;
		State* const* s_ptr ;
		Iterator i = StateSet_getFirst( states ) ;
		while ( (s_ptr = Iterator_next( &i, NULL )) && (s = *s_ptr) ) {
			int32_t value = State_getValue( s ) ; /* V_k(s) */
			int32_t new_value = Task_estimateStateValue( task, policy, s ) ; /* V_{k+1}(s) */
			State_setValue( s, new_value ) ;

			value = abs( value - new_value ) ;
			if ( value > delta ) {
				delta = value ;
			}
		}
	} while ( delta > 0 ) ;
}

void
DP_PolicyEvaluation( Policy* policy, const Task* task, int32_t threshold )
{
	int32_t delta = 0 ;
	assert( policy ) ;
	do {
		State* s ;
		State* const* s_ptr ;
		Iterator i = StateSet_getFirst( Task_getStates( task ) ) ;
		/* Sweep the entire state set. */
		delta = 0 ;
		while ( (s_ptr = Iterator_next( &i, NULL )) && (s = *s_ptr) ) {
			int32_t value = State_getValue( s ) ;
			const State* a = Policy_apply( policy, s, 0 ) ;
			if ( a ) {
				int32_t new_value = Task_estimateActionValue( task, s, a ) ;
				value = abs( value - new_value ) ;
				if ( delta < value ) {
					delta = value ;
				}
				State_setValue( s, new_value ) ;
			}
		}
	} while ( delta > threshold ) ;
}

bool
DP_PolicyImprovement( Policy* policy, const Task* task )
{
	State* s ;
	State* const* s_ptr ;
	bool policy_stable = true ;
	Iterator i = StateSet_getFirst( Task_getStates( task ) ) ;
	while ( (s_ptr = Iterator_next( &i, NULL )) && (s = *s_ptr) ) {
		Mapping* mapping ;
		const State* best = NULL ;
		int32_t max_value = INT32_MIN ;
		Model* model = State_getModel( s ) ;
		const State* b = Policy_apply( policy, s, 0 ) ;
		Map_Iterator j = Model_getActions( model ) ;
		while ( (mapping = Map_Iterator_next( &j )) ) {
			const State* a = Mapping_getKey( mapping ) ;
			if ( a ) {
				int32_t value = Task_estimateActionValue( task, s, a ) ;
				if ( max_value < value ) {
					best = a ;
					max_value = value ;
				}
			}
		}
		Policy_update( policy, s, best ) ;
		if ( b != best ) {
			policy_stable = false ;
		}
	}
	return policy_stable ;
}

void
DP_GeneralizedPolicyIteration( Policy* policy, const Task* task )
{
	do {
		DP_PolicyEvaluation( policy, task, 1 ) ;
	} while ( !DP_PolicyImprovement( policy, task ) ) ;
}

void
DP_ValueIteration( Policy* policy, const Task* task )
{
	int32_t delta = 0 ;
	assert( policy ) ;
	assert( task ) ;
	do {
		/* Sweep the entire state set. */
		State* s ;
		State* const* s_ptr ;
		const StateSet* states = Task_getStates( task ) ;
		Iterator i = StateSet_getFirst( states ) ;
		delta = 0 ;
		while ( (s_ptr = Iterator_next( &i, NULL )) && (s = *s_ptr) ) {
			Mapping* mapping ;
			int32_t new_value = 0 ;
			Model* model = State_getModel( s ) ;
			int32_t value = State_getValue( s ) ;
			Map_Iterator j = Model_getActions( model ) ;
			while ( (mapping = Map_Iterator_next( &j )) ) {
				const State* a = Mapping_getKey( mapping ) ;
				if ( a ) {
					int32_t v = Task_estimateActionValue( task, s, a ) ;
					if ( v > new_value ) {
						new_value = v ;
					}
				}
			}
			value = abs( value - new_value ) ;
			if ( delta < value ) {
				delta = value ;
			}
			State_setValue( s, new_value ) ;
		}
	} while ( delta > 0 ) ;
}
