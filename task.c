/* -*- Mode: C -*- */
/*
 * Copyright (c) 2015, Marc Perron
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *         * Redistributions of source code must retain the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer.
 * 
 *         * Redistributions in binary form must reproduce the above
 *           copyright notice, this list of conditions and the following
 *           disclaimer in the documentation and/or other materials
 *           provided with the distribution.
 * 
 *         * Neither the name of the copyright holder nor the names of
 *           its contributors may be used to endorse or promote
 *           products derived from this software without specific
 *           prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * Marc Perron BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* POSIX includes */
#include <assert.h>

/* Local includes */
#include "agent.h"
#include "environment.h"
#include "history.h"
#include "task.h"

/******************************************************************************
 * Task
 ******************************************************************************/

typedef union _task_impl {
	Task	_base ;
	struct {
		Environment*			environment ;
		StateSet*			goals ;

		/**
		 * @protected
		 * @brief Callback to the reward function.
		 * @memberof Task
		 *
		 * @details Pointer to the virtual implementation of the
		 * getReward member function. This function is invoked by the
		 * Task_getReward function.
		 */
		Task_GetReward_f*		getReward ;
	} ;
} TaskImpl ;

/**
 * @protected
 * @brief Initialize the base Task instance.
 * @memberof Task
 *
 * @details This will initialize the base Task members of a derived
 * Task class. This function @e must be called in the constructors of
 * all Task implementations. It will assign the required callbacks to
 * the virtual function members and set the Environment on which the
 * Task operates. Initially the Task's goal set will be empty. Goals
 * can be added to the Task definition latently.
 *
 * @see Task_addGoal
 *
 * @param[in,out] self A pointer to a Task structure to initialize.
 * @param[in] env A pointer to an immutable Environment instance.
 * @param[in] getReward Callback which implements the getReward
 *	function.
 */
void
Task_init( void* self,
	   Environment* env,
	   Task_GetReward_f* getReward )
{
	TaskImpl* task = self ;
	if ( task ) {
		task->environment = env ;
		task->goals = NULL ;
		task->getReward = getReward ;
	}
}

/**
 * @protected
 * @brief Get the Task's Environment.
 * @memberof Task
 *
 * @details This will return the Environment instance expressing the
 * parameters of the environment in which the Task operates. Derived
 * types are expected to set the Environment when the Task is
 * initialized.
 *
 * @param[in] abs_self A pointer to an immutable Task instance.
 *
 * @return A pointer to an immutable Environment instance.
 */
const Environment*
Task_getEnvironment( const Task* abs_self )
{
	const TaskImpl* self = ( const TaskImpl* )abs_self ;
	const Environment* env = NULL ;
	if ( self ) {
		env = self->environment ;
	}
	return env ;
}

Task*
Task_Create( Environment* env )
{
	Task* new = NULL ;
	if ( !env ) {
		errno = EINVAL ;
	}
	else if ( !(new = calloc( 1, sizeof( Task ) )) ) {
		errno = ENOMEM ;
	}
	else {
		Task_init( new, env, NULL ) ;
	}
	return new ;
}

const StateSet*
Task_getStates( const Task* self )
{
	const Environment* env ;
	const StateSet* states = NULL ;
	if ( !self ) {
		errno = EINVAL ;
	}
	else if ( !(env = Task_getEnvironment( self )) ) {
		errno = ENOENT ;
	}
	else {
		states = Environment_getStates( env ) ;
	}
	return states ;
}

int32_t
Task_getReward( const Task* self, const State* initial, const State* action, const State* next )
{
	int32_t reward = 0 ;
	const TaskImpl* impl = ( const TaskImpl* )self ;
	assert( impl ) ;
	if ( impl->getReward ) {
		reward = impl->getReward( self, initial, action, next ) ;
	}
	else {
		/* Check the task's goal set to determine if the
		 * initial or final actions are members thereof. If
		 * the initial action is, but the final action isn't
		 * the reward should be negative. If both states are
		 * goal states, or neither are goal states, then the
		 * reward should be zero. If only the final state is a
		 * goal state, the reward should be positive. */
		if ( Task_isGoalState( self, initial ) ) {
			if ( !Task_isGoalState( self, next ) ) {
				reward = -1 ;
			}
		}
		else {
			if ( Task_isGoalState( self, next ) ) {
				reward = 1 ;
			}
		}
	}
	return reward ;
}

bool
Task_isGoalState( const Task* self, const State* state )
{
	assert( self ) ;
	size_t hash = HashMap_hash( self->goals, state ) ;
	return HashMap_getItem( self->goals, hash ) ;
}

bool
Task_isComplete( const Task* self )
{
	assert( self ) ;
	assert( self->environment ) ;
	return Task_isGoalState( self, Environment_getCurrentState( self->environment ) ) ;
}

int
Task_addGoal( Task* self, const State* goal )
{
	int rc = -1 ;
	if ( !self || !goal ) {
		errno = EINVAL ;
	}
	else {
		TaskImpl* impl = ( TaskImpl* )self ;
		if ( !impl->goals && (impl->goals = StateSet_Create()) ) {
			errno = ENOMEM ;
		}
		else {
			rc = HashMap_addItem( impl->goals, &goal, sizeof( goal ) ) ;
		}
	}
	return rc ;
}

int32_t
Task_estimateActionValue( const Task* abs_self, const State* s, const State* a )
{
	int32_t value = 0 ;
	if ( abs_self && s ) {
		size_t i ;
		const Transition* next ;
		const TaskImpl* self = ( const TaskImpl* )abs_self ;
		const TransitionList* transitions = Environment_applyAction( self->environment, s, a ) ;

		i = 0 ;
		while ( (next = TransitionList_getItem( transitions, i++ )) ) {
			value += Environment_transitionProbability( self->environment, s, a, next->state ) *
				( Task_getReward( abs_self, s, a, next->state ) +
				  State_getValue( next->state ) ) / 100 ;
		}
	}
	return value ;
}

int32_t
Task_estimateStateValue( const Task* self, const Policy* policy, const State* s )
{
	Mapping* mapping ;
	int32_t value = 0 ;
	Model* model = State_getModel( s ) ;
	/* TODO:: Add a function in Environment to get the valid
	 * actions in the specified State (i.e. wrap the following
	 * call). */
	Map_Iterator i = Model_getActions( model ) ;
	while ( (mapping = Map_Iterator_next( &i )) ) {
		size_t j = 0 ;
		const State* a = Mapping_getKey( mapping ) ;

		value += Policy_evaluate( policy, s, a ) * Task_estimateActionValue( self, s, a ) / 100 ;
	}
	return value ;
}

History*
Task_execute( const void* self, Environment* environment, const Agency* agency )
{
	const State* action ;
	History* episode = NULL ;
	const Task* task = self ;
	const State* s = Environment_getInitialState( environment ) ;
	
	do {
		Iterator i = 0 ;
		const State* next = NULL ;
		const Agent* agent = NULL ;
		/* For each agent in the agency: get the next
		 * action */
		action = NULL ;
		while ( (agent = Agency_next( agency, &i )) ) {
			action = Agent_nextAction( agent, s ) ;
			if ( (next = Environment_apply( environment, action )) ) {
				uint32_t reward = Task_getReward( task, s, action, next ) ;
				History_addStep( &episode, s, action, reward ) ;
				s = next ;
			}
		}
	} while ( action ) ;

	return episode ;
}
